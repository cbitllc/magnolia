﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="contractQuestionnaire.aspx.cs" Inherits="Magnolia.app.contractQuestionnaire" MasterPageFile="~/app/Master.master" %>
<%@ MasterType VirtualPath="~/app/Master.master" %>
<%@ Register Assembly="DevExpress.Web.v20.2, Version=20.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxDiagram.v20.2, Version=20.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxDiagram" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        input {text-transform:uppercase;}
        textarea {text-transform:uppercase;}
        span {text-transform:uppercase;}
        .spanTitle{text-transform:capitalize;}
        .groupStyle{position:static;overflow:auto;}
        .layoutStyle{position:static;overflow:auto;}
        .hint{width:400px;}
</style>
    <script type="text/javascript">
        var sessionTimeoutWarning = "55";
        var sessionTimeout = "<%= HttpContext.Current.Session.Timeout %>";
        
        var sTimeout = parseInt(sessionTimeoutWarning) * 60 * 1000;
        var timeout = (1000 * 60 * 60) - .5;
        setTimeout('SessionWarning()', sTimeout);
        setTimeout('SessionMessage()', timeout);
        function SessionWarning() {
            var message = "Your session will expire in " +
                (parseInt(sessionTimeout) - parseInt(sessionTimeoutWarning)) +
                " minutes.\nPlease save the contract before the session expires.\nAny work in progress and not saved prior to the session timeout will be lost.";
            alert(message);
        }
        function SessionMessage() {
            var message = "Your current session has expired.\nPlease close the system and log back in to proceed.\nAny work that was in progress will be lost.";
            alert(message);
        }

        function OnLoad(s, e) {
            ValidateContractType();
        }
        function OnContractTypeChanged(s, e) {
            ValidateContractType();
            OnActionChanged();
        }
        function ValidateContractType(s, e) {
            //if (ddlDepartments.GetSelectedIndex() < 0 || ddlParties.GetSelectedIndex() < 0 || ddlContractTypes.GetSelectedIndex() < 0 || ddlActions.GetSelectedIndex() < 0 || (ddlMasterContracts.GetVisible() == true && ddlMasterContracts.GetSelectedIndex() < 0)) {
            //    formContract.GetItemByName('PurchaseGroup').SetVisible(false);
            //}
            //else {
            //    formContract.GetItemByName('PurchaseGroup').SetVisible(true);
            //}
            hdnDepartment.Set("department", ddlDepartments.GetSelectedIndex());
            if (ddlDepartments.GetSelectedIndex() < 0 && hdnNewPartyName.Get("businessName") == "") {
                //formContract.GetItemByName('Party').SetVisible(false);
                ddlParties.SetVisible(false);
                //hlNewParty.SetVisible(false);
                formContract.GetItemByName('PartyLink').SetVisible(false);
                formContract.GetItemByName('Party').SetCaption("");
                document.getElementById('aParty').style.visibility = 'hidden';
            }
            else {
                //formContract.GetItemByName('Party').SetVisible(true);
                ddlParties.SetVisible(true);
                //hlNewParty.SetVisible(true);
                formContract.GetItemByName('PartyLink').SetVisible(true);
                formContract.GetItemByName('Party').SetCaption("Party/Vendor:");
                document.getElementById('aParty').style.visibility = 'visible';
            }
            if (ddlParties.GetSelectedIndex() < 0) {
                //formContract.GetItemByName('contractTypeId').SetVisible(false);
                ddlContractTypes.SetVisible(false);
                formContract.GetItemByName('contractTypeId').SetCaption("");
                document.getElementById('aContractType').style.visibility = 'hidden';
            }
            else {
                //formContract.GetItemByName('contractTypeId').SetVisible(true);
                ddlContractTypes.SetVisible(true);
                formContract.GetItemByName('contractTypeId').SetCaption("Contract Type:");
                document.getElementById('aContractType').style.visibility = 'visible';
            }
            if (ddlContractTypes.GetSelectedIndex() < 0) {
                //formContract.GetItemByName('Action').SetVisible(false);
                ddlActions.SetVisible(false);
                formContract.GetItemByName('Action').SetCaption("");
                document.getElementById('aActions').style.visibility = 'hidden';
            }
            else {
                //formContract.GetItemByName('Action').SetVisible(true);
                ddlActions.SetVisible(true);
                formContract.GetItemByName('Action').SetCaption("Action:");
                document.getElementById('aActions').style.visibility = 'visible';
            }
            if (ddlActions.GetSelectedIndex() < 0) {
                //formContract.GetItemByName('masterContractId').SetVisible(false);
                ddlMasterContracts.SetVisible(false);
                formContract.GetItemByName('masterContractId').SetCaption("");
                document.getElementById('aMasterContract').style.visibility = 'hidden';
                formContract.GetItemByName('PurchaseGroup').SetVisible(false);
                formContract.GetItemByName('corporation').SetVisible(false);
                formContract.GetItemByName('corporationStanding').SetVisible(false);
                document.getElementById('aCorporation').style.visibility = 'hidden';
                document.getElementById('aCorporationStanding').style.visibility = 'hidden';
            }
            else {
                //formContract.GetItemByName('Routing').SetVisible(true);
                formContract.GetItemByName('PurchaseGroup').SetVisible(true);
                if (ddlPurchasingGroup.GetValue() != null) {
                    formContract.GetItemByName('corporation').SetVisible(true);
                }
                else {
                    formContract.GetItemByName('corporation').SetVisible(false);
                    formContract.GetItemByName('corporationStanding').SetVisible(false);
                    document.getElementById('aCorporation').style.visibility = 'hidden';
                    document.getElementById('aCorporationStanding').style.visibility = 'hidden';
                }
                if (ddlActions.GetSelectedIndex() == 0) {
                    //formContract.GetItemByName('masterContractId').SetVisible(true);
                    ddlMasterContracts.SetVisible(false);
                    formContract.GetItemByName('masterContractId').SetCaption("");
                    document.getElementById('aMasterContract').style.visibility = 'hidden';
                }
                else {
                    //formContract.GetItemByName('masterContractId').SetVisible(false);
                    ddlMasterContracts.SetVisible(true);
                    formContract.GetItemByName('masterContractId').SetCaption("Master Contract:");
                    document.getElementById('aMasterContract').style.visibility = 'visible';
                }
            }
            if (rbCorporation.GetSelectedIndex() != null && rbCorporation.GetSelectedIndex() == 1) {
                formContract.GetItemByName('corporationStanding').SetVisible(false);
                document.getElementById('aCorporationStanding').style.visibility = 'hidden';
            }
        }
        function OnPartiesSelectedIndexChanged(s, e) {
            if (s.GetSelectedIndex() == 0) {
                hdnSender.Set("sender", s.name);
                popExclusions.Show();
                txtBusinessName.Focus();
                e.processOnServer = false;
            } else {
                popParty.Hide();
            }
            //ValidateContractType(s, e);
        }
        function OnActionChanged(s, e) {
            if (ddlContractTypes.GetText() == 'MAINTENANCE' && (ddlActions.GetText() == 'RENEWAL' || ddlActions.GetText() == 'ANNUAL REVIEW')) {
                alert('For maintenance contract renewal requests, obtain a 12 month service history from our vendor and attach to the Contract Summary Sheet. As your request moves up the approval ladder, reviewers can see the value in the requested contract based on the difference between actual services and keeping the equipment on the maintenance contract.');
            }
            ValidateContractType(s, e);
        }
        function OnPurchasingGroupChanged(s, e) {
            //if (cbPurchasingGroup.GetValue() == '0') {
            //    formContract.GetItemByName('PurchaseGroup').SetVisible(true);
            //    //formContract.GetItemByName('PartyGroup').SetVisible(false);
            //    if (ddlPurchasingGroup.GetSelectedIndex() < 0 || (ddlPurchasingGroup.GetText() == 'Other Vendor')) {
            //        formContract.GetItemByName('Buttons').SetVisible(false);
            //        formContract.GetItemByName('ContractTerms').SetVisible(false);
            //        //if (ddlPurchasingGroup.GetValue() == 0) {
            //        //    formContract.GetItemByName('PartyGroup').SetVisible(true);
            //        //} 
            //    } else {
            //        formContract.GetItemByName('Buttons').SetVisible(true);
            //        formContract.GetItemByName('ContractTerms').SetVisible(true);
            //        OnDateChanged();
            //    }
            //} else {
            if (ddlPurchasingGroup.GetValue() != null) {
                formContract.GetItemByName('corporation').SetVisible(true);
                document.getElementById('aCorporation').style.visibility = 'visible';
                //OnDateChanged();
            }
                //formContract.GetItemByName('PurchaseGroup').SetVisible(false);
                //formContract.GetItemByName('PartyGroup').SetVisible(false);
                //formContract.GetItemByName('Buttons').SetVisible(true);
            //}
        }
        function OnPurchaseGroupChanged(s, e) {
            if (ddlPurchasingGroup.GetValue() == 0) {
                //formContract.GetItemByName('PartyGroup').SetVisible(true);
            }
            else {
                //formContract.GetItemByName('PartyGroup').SetVisible(false); 
                formContract.GetItemByName('ContractTerms').SetVisible(true);
                OnDateChanged();
            }
        }
        function OnCorporationChanged(s, e) {
            if (rbCorporation.GetValue() == "1") {
                formContract.GetItemByName('corporationStanding').SetVisible(true);
                document.getElementById('aCorporationStanding').style.visibility = 'visible';
                //formContract.GetItemByName('ContractTerms').SetVisible(false);
            }
            else {
                if (rbCorporation.GetValue() == "0") {
                    formContract.GetItemByName('corporationStanding').SetVisible(false);
                    document.getElementById('aCorporationStanding').style.visibility = 'hidden';
                    formContract.GetItemByName('ContractTerms').SetVisible(true);
                    OnDateChanged();
                }
            }
            if (formContract.GetItemByName('corporationStanding').GetVisible() == true && rbCorporationStanding.GetValue() != null) {
                formContract.GetItemByName('ContractTerms').SetVisible(true);
                OnDateChanged();
            }
        }
        function OnDateChanged(s, e) {
            if (deStartDate.GetDate() != null && deEndDate.GetDate() != null) {
                if (deStartDate.GetDate() > deEndDate.GetDate()) {
                    alert('The Contract End Date cannnot come before the Contract Start Date.');
                    deEndDate.SetIsValid(false);
                    deStartDate.SetIsValid(false);
                } else {
                    var length = Math.floor((new Date(deEndDate.GetDate()) - new Date(deStartDate.GetDate())) / (1000 * 60 * 60 * 24));
                    if (length > 1826) {
                        alert('The Contract length cannot be greater than five years.');
                        deEndDate.SetIsValid(false);
                        deStartDate.SetIsValid(false);
                    } else {
                        deEndDate.SetIsValid(true);
                        deStartDate.SetIsValid(true);
                    }
                    diff = new Date(
                        deEndDate.GetDate().getFullYear()-deStartDate.GetDate().getFullYear(), 
                        deEndDate.GetDate().getMonth()-deStartDate.GetDate().getMonth(), 
                        deEndDate.GetDate().getDate()-deStartDate.GetDate().getDate()
                    );
                    var answer = '';
                    var years = diff.getYear(); 
                    if (years > 0) {
                        answer = years + ((years == 1) ? ' Year' : ' Years');
                    }
                    var months = diff.getMonth(); 
                    if (months > 0) {
                        if (answer.length > 0) {
                            answer = answer + ' ';
                        }
                        answer += months + ((months == 1) ? ' Month' : ' Months');
                    }
                    var days = diff.getDate();
                    if (days > 0) {
                        if (answer.length > 0) {
                            answer = answer + ' ';
                        }
                        answer += days + ((days == 1) ? ' Day' : ' Days');
                    }
                    //NEED CLARIFICATION!!
                    //answer = years;
                    if (deStartDate.GetDate() == deEndDate.GetDate()) {
                        answer = '0';
                    }
                    else {
                        answer = ((deEndDate.GetDate() - deStartDate.GetDate()) / 365 / (24 * 60 * 60 * 1000)).toFixed(2);
                        //if (months != null && months > 0) {
                        //    answer = (months / 12).toFixed(2);
                        //}
                    }
                    txtContractLength.SetText(answer);
                    lblContractLength.SetText(years);
                    CalculateTotalCost();
               }
            }
        }
        function CalculateTotalCost(s, e) {
            var length = txtContractLength.GetText();
            var totalCost = spinAnnualCost.GetValue();
            if (length >= 1) {
                totalCost = totalCost * length;
            }
            spinEstimatedTotalCost.SetValue(totalCost);
        }
        function OutclauseChanged(s, e) {
            formContract.GetItemByName('Details').SetVisible(true);
            if (ddlOutclause.GetValue() == '1') {
                formContract.GetItemByName('notificationDaysBreach').SetVisible(true);
                formContract.GetItemByName('notificationDaysNoBreach').SetVisible(true);
                if (spinDaysBreach.GetValue() != null && spinDaysNoBreach.GetValue() != null) {
                    formContract.GetItemByName('Details').SetVisible(true);
                }
            } else {
                //if (confirm('This selection will require Chief Officer initials before it can be submitted for signatures.')){
                    formContract.GetItemByName('Details').SetVisible(true);
                    formContract.GetItemByName('notificationDaysBreach').SetVisible(false);
                    formContract.GetItemByName('notificationDaysNoBreach').SetVisible(false);
                //}
            }
        }
        function BreachDaysCheck(s, e) {
            if (spinDaysBreach.GetValue() != null && spinDaysNoBreach.GetValue() != null) {
                formContract.GetItemByName('Details').SetVisible(true);
                if (spinDaysBreach.GetValue() > spinDaysNoBreach.GetValue()) {
                    alert('Notification Days Breach is more than Notification Days No Breach. Please verify your entries.');
                }
            }
        }
        function CalculateDifference(s, e) {
            //if (spinAnnualCost.GetValue() != null && spinAnnualCost.GetValue() > 50000) {
            //    if (confirm('This selection will require Chief Officer initials before it can be submitted for signatures.') == false) {
            //        spinAnnualCost.SetValue(0);
            //    }
            //}
            if (spinAnnualCost.GetValue() != null && spinPriorYearCost.GetValue() != null) {
                spinCostDifference.SetNumber((spinAnnualCost.GetNumber() - spinPriorYearCost.GetNumber()).toFixed(2));
            }
            CalculateTotalCost();
        }
        function OnReferenceChange(s, e) {
            formContract.GetItemByName('BooksReference').SetVisible(false);
            formContract.GetItemByName('BooksReference').SetCaption("");
            txtBooksReference.SetVisible(false);
            formContract.GetItemByName('MedicareAdjustmentsReference').SetVisible(false);
            formContract.GetItemByName('MedicareAdjustmentsReference').SetCaption("");
            txtMedicareReference.SetVisible(false);
            if (rbAccessToBooks.GetValue() == "1") {
                txtBooksReference.SetVisible(true);
                formContract.GetItemByName('BooksReference').SetVisible(true);
                formContract.GetItemByName('BooksReference').SetCaption("Books/Records Reference Page/Paragraph");
            }
            if (rbMedicareAdjustments.GetValue() == "1") {
                formContract.GetItemByName('MedicareAdjustmentsReference').SetVisible(true);
                formContract.GetItemByName('MedicareAdjustmentsReference').SetCaption("Medicare Reference Page/Paragraph");
                txtMedicareReference.SetVisible(true);
                formContract.GetItemByName('BooksReference').SetVisible(true);
            }
            txtHipaaReference.SetVisible(false);
            formContract.GetItemByName('HIPAAReference').SetCaption("");
            formContract.GetItemByName('HIPAAReference').SetVisible(false);
            formContract.GetItemByName('GoverningLawReference').SetVisible(false);
            formContract.GetItemByName('GoverningLawReference').SetCaption("");
            txtGoverningLawReference.SetVisible(false);
            if (rbHipaaBA.GetValue() == "1") {
                txtHipaaReference.SetVisible(true);
                formContract.GetItemByName('HIPAAReference').SetVisible(true);
                formContract.GetItemByName('HIPAAReference').SetCaption("HIPAA/BAA Reference Page/Paragrap");
            }
            if (rbGoverningLaw.GetValue() == "YES") {
                formContract.GetItemByName('GoverningLawReference').SetVisible(true);
                formContract.GetItemByName('GoverningLawReference').SetCaption("Governing Law Reference Page/Paragraph");
                txtGoverningLawReference.SetVisible(true);
                formContract.GetItemByName('HIPAAReference').SetVisible(true);
            }
        }
        function OnSave(s, e) {
            formContract.GetItemByName('ContractTerms').SetVisible(true);
            formContract.GetItemByName('Details').SetVisible(true);
            formContract.GetItemByName('Regulations').SetVisible(true);
            formContract.GetItemByName('Buttons').SetVisible(true);
            var message = "";
            if (deStartDate.GetDate() == null) {
                message = "Start Date is required."
                deStartDate.SetIsValid(false);
            }
            if (deEndDate.GetDate() == null) {
                message = "End Date is required."
                deEndDate.SetIsValid(false);
            }
            if (message == "")
            {
                if (deStartDate.GetDate() > deEndDate.GetDate()) {
                    message = 'The Contract End Date cannnot come before the Contract Start Date.';
                    deEndDate.SetIsValid(false);
                    deStartDate.SetIsValid(false);
                }
            }
            var length = Math.floor((new Date(deEndDate.GetDate()) - new Date(deStartDate.GetDate())) / (1000 * 60 * 60 * 24));
            if (length > 1826) {
                if (message != "") {
                    message = message + "\r\n";
                }
                message = message + 'The Contract length cannot be greater than five years.';
                deEndDate.SetIsValid(false);
                deStartDate.SetIsValid(false);
            }
            if (ddlOutclause.GetValue() == "1") {
                if (spinDaysBreach.GetValue() == null && spinDaysNoBreach.GetValue() == null) {
                    if (message != "") {
                        message = message + "\r\n";
                    }
                    message = message + "Notification days are required when there is an Outclause.";
                    spinDaysBreach.SetIsValid(false);
                    spinDaysNoBreach.SetIsValid(false);
                }
            }
            if (rbAccessToBooks.GetValue() == "1" && (txtBooksReference.GetText() == null || txtBooksReference.GetText() == "")) {
                if (message != "") {
                    message = message + "\r\n";
                }
                message = message + "Books/Records Reference Page/Paragraph is required.";
                txtBooksReference.SetIsValid(false);
            }
            if (rbMedicareAdjustments.GetValue() == "1" && (txtMedicareReference.GetText() == null || txtMedicareReference.GetText() == "")) {
                if (message != "") {
                    message = message + "\r\n";
                }
                message = message + "Medicare Reference Page/Paragraph is required.";
                txtMedicareReference.SetIsValid(false);
            }
            if (rbHipaaBA.GetValue() == "1" && (txtHipaaReference.GetText() == null || txtHipaaReference.GetText() == "")) {
                if (message != "") {
                    message = message + "\r\n";
                }
                message = message + "HIPAA/BAA Reference Page/Paragraph is required.";
                txtHipaaReference.SetIsValid(false);
            }
            if (rbGoverningLaw.GetValue() == "YES" && (txtGoverningLawReference.GetText() == null || txtGoverningLawReference.GetText() == "")) {
                if (message != "") {
                    message = message + "\r\n";
                }
                message = message + "Governing Law Reference Page/Paragraph is required.";
                txtGoverningLawReference.SetIsValid(false);
            }
            if (message != "") {
                alert(message);
                e.processOnServer = false;
            }
            else {
                var today = new Date(new Date().toDateString());
                var endDate = new Date(deEndDate.GetDate().toDateString());
                if (endDate <= today) {
                    e.processOnServer = confirm("The contract end date is in the past. This contract will be placed in a canceled/terminated status. Please verify the date. Click OK to continue or Cancel to make changes.");
                }
            }
        }
        function ShowHint(hint, hintStyle) {
            switch (hint) {
                case 'hintParties': hintParties.Show(hintStyle);
                    break;
                case 'hintContractType': hintContractType.Show(hintStyle);
                    break;
                case 'hintActions': hintActions.Show(hintStyle);
                    break;
                case 'hintMasterContract': hintMasterContract.Show(hintStyle);
                    break;
                case 'hintPurchaseGroup': hintPurchaseGroup.Show(hintStyle);
                    break;
                case 'hintAutoRenewal': hintAutoRenewal.Show(hintStyle);
                    break;
                case 'hintBudgeted': hintBudgeted.Show(hintStyle);
                    break;
                case 'hintOutclause': hintOutclause.Show(hintStyle);
                    break;
                case 'hintNotificationDays': hintNotificationDays.Show(hintStyle);
                    break;
                case 'hintNotificationDaysNoBreach': hintNotificationDaysNoBreach.Show(hintStyle);
                    break;
                case 'hintDetails': hintDetails.Show(hintStyle);
                    break;
                case 'hintEstimatedTotalCost': hintEstimatedTotalCost.Show(hintStyle);
                    break;
                case 'hintAnnualCost': hintAnnualCost.Show(hintStyle);
                    break;
                case 'hintPriorYearCost': hintPriorYearCost.Show(hintStyle);
                    break;
                case 'hintExclusivity': hintExclusivity.Show(hintStyle);
                    break;
                case 'hintGlEOC': hintGlEOC.Show(hintStyle);
                    break;
                case 'hintAccessToBooks': hintAccessToBooks.Show(hintStyle);
                    break;
                case 'hintMedicareAdjustments': hintMedicareAdjustments.Show(hintStyle);
                    break;
                case 'hintHipaaBAA': hintHipaaBAA.Show(hintStyle);
                    break;
                case 'hintGoverningLaw': hintGoverningLaw.Show(hintStyle);
                    break;
                case 'hintGSA': hintGSA.Show(hintStyle);
                    break;
                case 'hintPhysicianRationale': hintPhysicianRationale.Show(hintStyle);
                    break;
                case 'hintContractEmployees': hintContractEmployees.Show(hintStyle);
                    break;
                case 'hintVendorSigned': hintVendorSigned.Show(hintStyle);
                    break;
                case 'hintElectronicAgreement': hintElectronicAgreement.Show(hintStyle);
                    break;
                case 'hintCorporation': hintCorporation.Show(hintStyle);
                    break;
                case 'hintCorporationStanding': hintCorporationStanding.Show(hintStyle);
                    break;
                default: break;
            }
        }
        function stopRKey(evt) {
            var evt = (evt) ? evt : ((event) ? event : null);
            var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
            if ((evt.keyCode == 13) && (node.type == "text")) { return false; }
        }

        document.onkeypress = stopRKey;
    </script>
    <div style="overflow:auto;">
        <dx:ASPxCallbackPanel ID="cbContract" ClientInstanceName="cbContract" runat="server" OnCallback="cbContract_Callback">
            <PanelCollection>
                <dx:PanelContent>
                    <dx:ASPxFormLayout ID="formContract" ClientInstanceName="formContract" runat="server" DataSourceID="dsContract" Paddings-Padding="10" EnableTheming="true" Theme="Metropolis" EncodeHtml="false">
                        <ClientSideEvents Init="OnLoad" />
                        <Items>
                            <dx:LayoutGroup ColumnCount="3" Name="ContractType" Caption="Contract" CssClass="groupStyle" VerticalAlign="Middle" SettingsItemCaptions-VerticalAlign="Middle">
                                <Items>
                                    <dx:LayoutItem FieldName="contractId" Visible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <dx:ASPxLabel ID="lblContractId" runat="server"></dx:ASPxLabel>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColumnSpan="1" Caption="Department" FieldName="departmentId">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">&nbsp;&nbsp;&nbsp;</div>
                                                    <dx:ASPxComboBox ID="ddlDepartments" ClientInstanceName="ddlDepartments" runat="server" DataSourceID="dsDepartments" ValueField="departmentId" TextField="department">
                                                        <ClientSideEvents SelectedIndexChanged="ValidateContractType" />
                                                    </dx:ASPxComboBox>
                                                </div>
                                                <asp:SqlDataSource ID="dsDepartments" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                                                    SelectCommand="usp_getDepartments" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColumnSpan="1" Caption="Party/Vendor" FieldName="partyId" Name="Party">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">
                                                        <a href="javascript:ShowHint('hintParties','.partyHint');" id="aParty"><i class="fa fa-info-circle partyHint"></i></a>
                                                    </div>
                                                    <dx:ASPxComboBox ID="ddlParties" ClientInstanceName="ddlParties" runat="server" DataSourceID="dsParties" TextField="businessName" ValueField="partyId" OnSelectedIndexChanged="ddlParties_SelectedIndexChanged" AutoPostBack="true"></dx:ASPxComboBox>
                                                </div>
                                                <dx:ASPxHint ID="hintParties" ClientInstanceName="hintParties" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Party/Vendor" Content="Select the LEGAL NAME of the party/vendor.<br/>If not available in the list, select “Add Party” button and complete the form, entering the LEGAL NAME of the party/vendor." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                                <%--<dx:ASPxHyperLink ID="hlNewParty" ClientInstanceName="hlNewParty" runat="server" Text="Add Party" Font-Underline="true" ForeColor="Green" Cursor="pointer" NavigateUrl="newPartyRequest.aspx" ClientVisible="false"></dx:ASPxHyperLink>--%>
                                                <asp:SqlDataSource ID="dsParties" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                                                    SelectCommand="usp_getParties" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColumnSpan="1" Caption="Contract Type" FieldName="contractTypeId" Name="contractTypeId">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">
                                                        <a href="javascript:ShowHint('hintContractType','.contractTypeHint');" id="aContractType"><i class="fa fa-info-circle contractTypeHint"></i></a>
                                                    </div>
                                                    <dx:ASPxComboBox ID="ddlContractTypes" ClientInstanceName="ddlContractTypes" runat="server" ClientVisible="false" DataSourceID="dsContractTypes" ValueField="contractTypeId" TextField="contractType">
                                                        <ClientSideEvents SelectedIndexChanged="OnContractTypeChanged" />
                                                    </dx:ASPxComboBox>
                                                </div>
                                                <dx:ASPxHint ID="hintContractType" ClientInstanceName="hintContractType" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Contract Type" Content="<dl><dt>Vendor</dt><dd>Vendor provides a service (i.e. Clean linens, mobile MRIs), good, supplies, maintenance, or equipment. For maintenance contract renewal requests, obtain a 12 month service history from the vendor and upload to the contract request. Can also be consulting, professional fees, schools, management fees, etc.</dd><dt>Administrative</dt><dd>Administrative, Consulting, NDA, Managed Care</dd><dt>Education</dt><dd>Education Assistance requests.</dd><dt>Employment</dt><dd>Employment Agreement.</dd><dt>Lease/Rent</dt><dd>Space or Property</dd><dt>PHO<dd>Managed Care</dd></dl>" TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                                <asp:SqlDataSource ID="dsContractTypes" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                                                    SelectCommand="usp_getContractTypes" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:EmptyLayoutItem></dx:EmptyLayoutItem>
                                    <dx:LayoutItem ColumnSpan="1" Caption=" " Name="PartyLink" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>          
                                            <dx:LayoutItemNestedControlContainer>
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">&nbsp;&nbsp;&nbsp;</div>
                                                    <dx:ASPxHyperLink ID="hlNewParty" ClientInstanceName="hlNewParty" runat="server" Text="Add Party" Font-Underline="true" ForeColor="Green" Cursor="pointer" NavigateUrl="newPartyRequest.aspx"></dx:ASPxHyperLink>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>                                    
                                    </dx:LayoutItem>
                                    <dx:EmptyLayoutItem></dx:EmptyLayoutItem>
                                    <dx:LayoutItem ColumnSpan="1" Caption="Action" FieldName="actionId" Name="Action">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">
                                                        <a href="javascript:ShowHint('hintActions','.actionsHint');" id="aActions"><i class="fa fa-info-circle actionsHint"></i></a>
                                                    </div>
                                                    <dx:ASPxComboBox ID="ddlActions" ClientInstanceName="ddlActions" runat="server" ClientVisible="false" DataSourceID="dsActions" ValueField="actionId" TextField="action">
                                                        <ClientSideEvents SelectedIndexChanged="OnActionChanged" />
                                                    </dx:ASPxComboBox>
                                                </div>
                                                <dx:ASPxHint ID="hintActions" ClientInstanceName="hintActions" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Action" Content="<dl><dt>New</dt><dd>New contract that has not existed or replaces previous contract with new terms</dd><dt>Renewal</dt><dd>Review and Renewal of an existing contract that is ending, no changes in terms.</dd><dt>Amendment</dt><dd>Changes to an existing contract</dd><dt>Auto Renewal Review</dt><dd>Review of a contract annually that automatically renews each year with no contract end date. The review is good for one year. Verify contract is still in good standing and add any details of information that may be needed in the future. If any terms or amounts change, you must enter as a NEW contract.</dd><dt>Cancel/Inactivate</dt><dd>The contract is expiring or is being terminated.</dd><dt>Other</dt><dd>Any action not identified above.</dd></dl>" TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                                <asp:SqlDataSource ID="dsActions" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                                                    SelectCommand="usp_getActions" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColumnSpan="1" Caption="Master Contract" FieldName="masterContractId" Name="masterContractId">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">
                                                        <a href="javascript:ShowHint('hintMasterContract','.masterContractHint');" id="aMasterContract"><i class="fa fa-info-circle masterContractHint"></i></a>
                                                    </div>
                                                    <dx:ASPxComboBox ID="ddlMasterContracts" ClientInstanceName="ddlMasterContracts" runat="server" ValueField="masterContractNumber" TextField="businessName" DataSourceID="dsMasterContracts" ClientVisible="false">
                                                        <ClientSideEvents SelectedIndexChanged="ValidateContractType" />
                                                    </dx:ASPxComboBox>
                                                </div>
                                                <dx:ASPxHint ID="hintMasterContract" ClientInstanceName="hintMasterContract" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Master Contract" Content="If this contract is a subordinate or amendment to a previously entered contract, select the master contract from this list." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                                <asp:SqlDataSource ID="dsMasterContracts" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                                                    SelectCommand="usp_getMasterContracts" SelectCommandType="StoredProcedure">
                                                    <SelectParameters>
                                                        <asp:ControlParameter Name="partyId" ControlID="ddlParties" PropertyName="Value" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
<%--                                </Items>
                            </dx:LayoutGroup>
                            <dx:LayoutGroup ColumnCount="1" GroupBoxDecoration="Box" ClientVisible="false" Caption="Purchase Group" Name="PurchasingGroup" VerticalAlign="Middle" SettingsItemCaptions-VerticalAlign="Middle">
                                <Items>--%>
                                    <dx:LayoutItem ColSpan="1" Caption="Purchase Group" FieldName="purchasingGroup" Name="PurchaseGroup">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">
                                                        <a href="javascript:ShowHint('hintPurchaseGroup','.purchaseGroupHint');"><i class="fa fa-info-circle purchaseGroupHint"></i></a>
                                                    </div>
                                                    <dx:ASPxComboBox ID="ddlPurchasingGroup" ClientInstanceName="ddlPurchasingGroup" runat="server" DataSourceID="dsPurchasingGroups" TextField="groupName" ValueField="purchaseGroupId">
                                                        <ClientSideEvents SelectedIndexChanged="OnPurchasingGroupChanged" Init="OnPurchasingGroupChanged" />
                                                    </dx:ASPxComboBox>
                                                </div>
                                                <dx:ASPxHint ID="hintPurchaseGroup" ClientInstanceName="hintPurchaseGroup" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Purchase Group" Content="Select the appropriate purchase group to which the contract is affiliated. If affiliated to a purchase group, select none.<br/>If not on purchase group, please ensure the contract adheres to proper quote and bid purchasing procedures.<br/>(Over $10K requires multiple quotes and over $50K requires a bid)" TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                                <asp:SqlDataSource ID="dsPurchasingGroups" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" SelectCommand="usp_getPurchasingGroupsAdmin" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColumnSpan="1" Caption="Is Party a Corporation?" FieldName="isCorporation" Name="corporation">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">
                                                        <a href="javascript:ShowHint('hintCorporation','.corporationHint');" id="aCorporation"><i class="fa fa-info-circle corporationHint"></i></a>
                                                    </div>
                                                    <dx:ASPxRadioButtonList ID="rbCorporation" ClientInstanceName="rbCorporation" runat="server" RepeatDirection="Horizontal" Border-BorderStyle="None" ValidationSettings-RequiredField-IsRequired="true">
                                                        <Items>
                                                            <dx:ListEditItem Text="Yes" Value="1" />
                                                            <dx:ListEditItem Text="No" Value="0" />
                                                        </Items>
                                                        <ClientSideEvents SelectedIndexChanged="OnCorporationChanged" />
                                                    </dx:ASPxRadioButtonList>
                                                </div>
                                                <dx:ASPxHint ID="hintCorporation" ClientInstanceName="hintCorporation" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Is Party a Corporation?" Content="Perform a corporation check by searching the secretary of state of the state of incorporation. Print and upload good standing documentation. If unsure how to do this, call the contract administrator." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColumnSpan="2" Caption="Corporation in Good Standing?" FieldName="isCorporationGoodStanding" Name="corporationStanding">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">
                                                        <a href="javascript:ShowHint('hintCorporationStanding','.corporationStandingHint');" id="aCorporationStanding"><i class="fa fa-info-circle corporationStandingHint"></i></a>
                                                    </div>
                                                    <dx:ASPxRadioButtonList ID="rbCorporationStanding" ClientInstanceName="rbCorporationStanding" runat="server" RepeatDirection="Horizontal" Border-BorderStyle="None" ValidationSettings-RequiredField-IsRequired="true">
                                                        <Items>
                                                            <dx:ListEditItem Text="Yes" Value="1" />
                                                            <dx:ListEditItem Text="No" Value="0" />
                                                        </Items>
                                                        <ClientSideEvents SelectedIndexChanged="OnCorporationChanged" />
                                                    </dx:ASPxRadioButtonList>
                                                </div>
                                                <dx:ASPxHint ID="hintCorporationStanding" ClientInstanceName="hintCorporationStanding" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Corporation in Good Standing?" Content="Perform a corporation check by searching the secretary of state of the state of incorporation. Print and upload good standing documentation. If unsure how to do this, call the contract administrator." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                </Items>
                            </dx:LayoutGroup>
                            <dx:LayoutGroup ColumnCount="3" GroupBoxDecoration="Box" Caption="Contract Terms" Name="ContractTerms" VerticalAlign="Middle" SettingsItemCaptions-VerticalAlign="Middle">
                                <Items>
                                    <dx:LayoutItem ColSpan="1" Caption="Start Date" FieldName="startDate" Name="StartDate">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">&nbsp;&nbsp;&nbsp;</div>
                                                    <dx:ASPxDateEdit ID="deStartDate" ClientInstanceName="deStartDate" runat="server">
                                                        <ClientSideEvents  DateChanged="OnDateChanged" ValueChanged="OnDateChanged" /><%--Init="function (s,e){s.SetDate(new Date());}"--%>
                                                    </dx:ASPxDateEdit>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="End Date" FieldName="endDate" Name="EndDate">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">&nbsp;&nbsp;&nbsp;</div>
                                                    <dx:ASPxDateEdit ID="deEndDate" ClientInstanceName="deEndDate" runat="server">
                                                        <ClientSideEvents  DateChanged="OnDateChanged" ValueChanged="OnDateChanged" /><%--Init="function (s,e){s.SetDate(new Date());}"--%>
                                                    </dx:ASPxDateEdit>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Contract Length" FieldName="contractLength" Name="ContractLength">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">&nbsp;&nbsp;&nbsp;</div>
                                                    <dx:ASPxTextBox ID="txtContractLength" ClientInstanceName="txtContractLength" runat="server" BackColor="WhiteSmoke"></dx:ASPxTextBox>
                                                    <dx:ASPxLabel ID="lblContractLength" ClientInstanceName="lblContractLength" runat="server" ClientVisible="false"></dx:ASPxLabel>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Auto Renewal" FieldName="isAutoRenewal" Name="AutoRenewal">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">
                                                        <a href="javascript:ShowHint('hintAutoRenewal','.autoRenewalHint');"><i class="fa fa-info-circle autoRenewalHint"></i></a>
                                                    </div>
                                                    <dx:ASPxRadioButtonList ID="ddlAutoRenewal" runat="server" RepeatDirection="Horizontal" Border-BorderStyle="None" ValidationSettings-RequiredField-IsRequired="true">
                                                        <Items>
                                                            <dx:ListEditItem Text="Yes" Value="1" />
                                                            <dx:ListEditItem Text="No" Value="0" />
                                                        </Items>
                                                        <%--<ClientSideEvents SelectedIndexChanged="function(s,e){if (s.GetValue() == '1'){alert('This selection will require Chief Officer initials before it can be submitted for signatures.');}}" />--%>
                                                        <ClientSideEvents SelectedIndexChanged="function(s,e){if (s.GetValue() == '1'){ddlOutclause.SetValue(1); OutclauseChanged(s,e); }}" />
                                                    </dx:ASPxRadioButtonList>
                                                </div>
                                                <dx:ASPxHint ID="hintAutoRenewal" ClientInstanceName="hintAutoRenewal" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Auto Renewal" Content="Does the contract contain terms that whereby the contract will renewal automatically at the end of the term without MRHC actions?" TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Budgeted" FieldName="isBudgeted">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">
                                                        <a href="javascript:ShowHint('hintBudgeted','.budgetedHint');"><i class="fa fa-info-circle budgetedHint"></i></a>
                                                    </div>
                                                    <dx:ASPxRadioButtonList ID="rbBudgeted" ClientInstanceName="rbBudgeted" runat="server" RepeatDirection="Horizontal" Border-BorderStyle="None" ValidationSettings-RequiredField-IsRequired="true">
                                                        <Items>
                                                            <dx:ListEditItem Text="Yes" Value="1" />
                                                            <dx:ListEditItem Text="No" Value="0" />
                                                        </Items>
                                                    </dx:ASPxRadioButtonList>
                                                </div>
                                                <dx:ASPxHint ID="hintBudgeted" ClientInstanceName="hintBudgeted" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Budgeted" Content="Was this purchase budgeted in the current year’s budget?" TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Budgeted Amount" FieldName="budgetedAmount">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">&nbsp;&nbsp;&nbsp;</div>
                                                    <dx:ASPxSpinEdit ID="spinBudgetedAmount" ClientInstanceName="spinBudgetedAmount" runat="server"></dx:ASPxSpinEdit>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Outclause" FieldName="isOutClause">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">
                                                        <a href="javascript:ShowHint('hintOutclause','.outclauseHint');"><i class="fa fa-info-circle outclauseHint"></i></a>
                                                    </div>
                                                    <dx:ASPxRadioButtonList ID="ddlOutclause" ClientInstanceName="ddlOutclause" runat="server" RepeatDirection="Horizontal" Border-BorderStyle="None" ValidationSettings-RequiredField-IsRequired="true">
                                                        <Items>
                                                            <dx:ListEditItem Text="Yes" Value="1" />
                                                            <dx:ListEditItem Text="No" Value="0" />
                                                        </Items>
                                                        <ClientSideEvents SelectedIndexChanged="OutclauseChanged" Init="function (s,e) { if (ddlOutclause.GetValue() != null){ formContract.GetItemByName('Details').SetVisible(true); }}" />
                                                    </dx:ASPxRadioButtonList>
                                                </div>
                                                <dx:ASPxHint ID="hintOutclause" ClientInstanceName="hintOutclause" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Outclause" Content="Review the terms of the contract and select if the contract contains an out clause." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Notification Days Breach" FieldName="notificationDaysBreach" ClientVisible="false" Name="notificationDaysBreach">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">
                                                        <a href="javascript:ShowHint('hintNotificationDays','.notificationDaysBreachHint');"><i class="fa fa-info-circle notificationDaysBreachHint"></i></a>
                                                    </div>
                                                    <dx:ASPxSpinEdit ID="spinDaysBreach" ClientInstanceName="spinDaysBreach" runat="server" DecimalPlaces="0" AllowNull="true" >
                                                        <ClientSideEvents NumberChanged="BreachDaysCheck" />
                                                    </dx:ASPxSpinEdit>
                                                </div>
                                                <dx:ASPxHint ID="hintNotificationDays" ClientInstanceName="hintNotificationDays" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Notification Days Breach" Content="Enter the number of days MRHC must provide a notification prior to terminating the contract for a non-cured breach." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Notification Days No Breach" FieldName="notificationDaysNoBreach" ClientVisible="false" Name="notificationDaysNoBreach">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">
                                                        <a href="javascript:ShowHint('hintNotificationDaysNoBreach','.notificationDaysNoBreach');"><i class="fa fa-info-circle notificationDaysNoBreach"></i></a>
                                                    </div>
                                                    <dx:ASPxSpinEdit ID="spinDaysNoBreach" ClientInstanceName="spinDaysNoBreach" runat="server" DecimalPlaces="0" AllowNull="true">
                                                        <ClientSideEvents NumberChanged="BreachDaysCheck" />
                                                    </dx:ASPxSpinEdit>
                                                </div>
                                                <dx:ASPxHint ID="hintNotificationDaysNoBreach" ClientInstanceName="hintNotificationDaysNoBreach" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Notificaiton Days No Breach" Content="Enter the number of days MRHC must provide a notification prior to terminating the contract for any cause." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                </Items>
                            </dx:LayoutGroup>
                            <dx:LayoutGroup ColumnCount="3" Name="Details" Caption="Contract Details" VerticalAlign="Middle" SettingsItemCaptions-VerticalAlign="Top">
                                <Items>
                                    <dx:LayoutItem ColSpan="3" Caption="Details" FieldName="details">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">
                                                        <a href="javascript:ShowHint('hintDetails','.detailsHint');"><i class="fa fa-info-circle detailsHint"></i></a>
                                                    </div>
                                                    <dx:ASPxMemo ID="memoDetails" ClientInstanceName="memoDetails" runat="server" Width="98%" Rows="5" MaxLength="2000">
                                                        <ValidationSettings>
                                                            <RequiredField IsRequired="true" />
                                                        </ValidationSettings>
                                                        <ClientSideEvents TextChanged="function (s,e){ formContract.GetItemByName('Regulations').SetVisible(true); formContract.GetItemByName('Buttons').SetVisible(true); }" Init="function (s,e) {if (memoDetails.GetText() != null && memoDetails.GetText() != '') {formContract.GetItemByName('Regulations').SetVisible(true); formContract.GetItemByName('Buttons').SetVisible(true); }}" />
                                                    </dx:ASPxMemo>
                                                </div>
                                                <dx:ASPxHint ID="hintDetails" ClientInstanceName="hintDetails" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Contract Details" Content="Summary of goods or services provided and the business case for the contract.<ul><li>Why do we need it?</li><li>What is ROI?</li><li>It is a complementary service to a valued service?</li><li>New offering to better serve the community?</li><li>It is required by a regulatory agency? provide documentation.</li><li>What are the alternatives?</li><li>How does this purchase align with the current strategic plan?</li></ul>" TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Total Cost" FieldName="estimatedTotalCost">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">
                                                        <a href="javascript:ShowHint('hintEstimatedTotalCost','.estimatedTotalCostHint');"><i class="fa fa-info-circle estimatedTotalCostHint"></i></a>
                                                    </div>
                                                    <dx:ASPxSpinEdit ID="spinEstimatedTotalCost" ClientInstanceName="spinEstimatedTotalCost" runat="server" DecimalPlaces="2" AllowNull="true" ReadOnly="true" BackColor="WhiteSmoke"></dx:ASPxSpinEdit>
                                                </div>
                                                <dx:ASPxHint ID="hintEstimatedTotalCost" ClientInstanceName="hintEstimatedTotalCost" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Total Cost" Content="Fill in the PROPOSED ANNUAL COST in the appropriate box.<br/>Fill in LAST YEARS COST in the appropriate box.<br/>Verify through accounts payable your ACTUAL spend.<br/>Cost difference is automatically calculated. If Total Cost is $50K or more, the contract will go to the Board of Trustees for approval. " TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Annual Cost" FieldName="annualCost">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">
                                                        <a href="javascript:ShowHint('hintAnnualCost','.annualCostHint');"><i class="fa fa-info-circle annualCostHint"></i></a>
                                                    </div>
                                                    <dx:ASPxSpinEdit ID="spinAnnualCost" ClientInstanceName="spinAnnualCost" runat="server" DecimalPlaces="2" AllowNull="true" >
                                                        <ClientSideEvents NumberChanged="CalculateDifference" />
                                                    </dx:ASPxSpinEdit>
                                                </div>
                                                <dx:ASPxHint ID="hintAnnualCost" ClientInstanceName="hintAnnualCost" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Annual Cost" Content="Enter the annual costs associated with the contract." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Last Year's Cost" FieldName="priorYearCost">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">
                                                        <a href="javascript:ShowHint('hintPriorYearCost','.priorYearHint');"><i class="fa fa-info-circle priorYearHint"></i></a>
                                                    </div>
                                                    <dx:ASPxSpinEdit ID="spinPriorYearCost" ClientInstanceName="spinPriorYearCost" runat="server" DecimalPlaces="2" AllowNull="true" >
                                                        <ClientSideEvents NumberChanged="CalculateDifference" />
                                                    </dx:ASPxSpinEdit>
                                                </div>
                                                <dx:ASPxHint ID="hintPriorYearCost" ClientInstanceName="hintPriorYearCost" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Last Year's Cost" Content="Fill in the PROPOSED ANNUAL COST in the appropriate box.<br/>Fill in LAST YEAR'S COST in the appropriate box.<br/>Verify through accounts payable your ACTUAL spend.<br/>Cost difference is automatically calculated." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Cost Difference" >
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">&nbsp;&nbsp;&nbsp;</div>
                                                    <dx:ASPxSpinEdit ID="spinCostDifference" ClientInstanceName="spinCostDifference" runat="server" DecimalPlaces="2" Number="0" ReadOnly="true" BackColor="WhiteSmoke"></dx:ASPxSpinEdit>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Exclusivity" FieldName="isExclusivity">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">
                                                        <a href="javascript:ShowHint('hintExclusivity','.exclusivityHint');"><i class="fa fa-info-circle exclusivityHint"></i></a>
                                                    </div>
                                                    <dx:ASPxRadioButtonList ID="ddlExclusivity" runat="server" RepeatDirection="Horizontal" Border-BorderStyle="None" >
                                                        <Items>
                                                            <dx:ListEditItem Text="Yes" Value="1" />
                                                            <dx:ListEditItem Text="No" Value="0" />
                                                        </Items>
                                                        <%--<ClientSideEvents SelectedIndexChanged="function(s,e){if (s.GetValue() == '1'){alert('This selection will require Chief Officer initials before it can be submitted for signatures.');}}" />--%>
                                                    </dx:ASPxRadioButtonList>
                                                </div>
                                                <dx:ASPxHint ID="hintExclusivity" ClientInstanceName="hintExclusivity" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Exclusivity" Content="Does this contract limit who MRHC can do business with?" TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="EOC" FieldName="glEOC">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">
                                                        <a href="javascript:ShowHint('hintGlEOC','.glEOCHint');"><i class="fa fa-info-circle glEOCHint"></i></a>
                                                    </div>
                                                    <dx:ASPxComboBox ID="ddlGlEOC" ClientInstanceName="ddlGlEOC" runat="server" DataSourceID="dsEOC" TextField="eocDescription" ValueField="eocId">
                                                        <ClientSideEvents SelectedIndexChanged="function(s,e){formContract.GetItemByName('Regulations').SetVisible(true); formContract.GetItemByName('Buttons').SetVisible(true);}" />
                                                    </dx:ASPxComboBox>
                                                </div>
                                                <dx:ASPxHint ID="hintGlEOC" ClientInstanceName="hintGlEOC" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="General Ledger/EOC" Content="Choose the EOC category to which the charges will be accrued for the contract. When multiple EOCs, select the largest EOC to be used" TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                                <asp:SqlDataSource ID="dsEOC" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" SelectCommand="usp_getEocs" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Payment Term" FieldName="paymentTerm">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">&nbsp;&nbsp;&nbsp;</div>
                                                    <dx:ASPxComboBox ID="ddlPaymentTerm" runat="server" DataSourceID="dsPaymentTypes" TextField="paymentType" ValueField="paymentType">
                                                        <ClientSideEvents SelectedIndexChanged="function(s,e){ 
                                                            formContract.GetItemByName('Regulations').SetVisible(true);
                                                            formContract.GetItemByName('Buttons').SetVisible(true);
                                                            if(s.GetValue() == 'Other') {
                                                                formContract.GetItemByName('paymentTermOther').SetVisible(true);
                                                            } else {
                                                                formContract.GetItemByName('paymentTermOther').SetVisible(false);
                                                            }}" />
                                                    </dx:ASPxComboBox>
                                                    <asp:SqlDataSource ID="dsPaymentTypes" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" SelectCommand="usp_getPaymentTypes" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Vendor/Party Signed?" FieldName="isPartySigned">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">
                                                        <a href="javascript:ShowHint('hintVendorSigned','.vendorSignedHint');"><i class="fa fa-info-circle outclauseHint"></i></a>
                                                    </div>
                                                    <dx:ASPxComboBox ID="ddlPartySigned" runat="server">
                                                        <Items>
                                                            <dx:ListEditItem Text="Yes" Value="Y" />
                                                            <dx:ListEditItem Text="No" Value="N" />
                                                            <dx:ListEditItem Text="N/A" Value="NA" />
                                                        </Items>
                                                        <ClientSideEvents SelectedIndexChanged="function (s,e){ formContract.GetItemByName('Regulations').SetVisible(true); formContract.GetItemByName('Buttons').SetVisible(true); }" />
                                                        <ValidationSettings RequiredField-IsRequired="true"></ValidationSettings>
                                                    </dx:ASPxComboBox>
                                                </div>
                                                <dx:ASPxHint ID="hintVendorSigned" ClientInstanceName="hintVendorSigned" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Is Party/Vendor Signed" Content="If the Vendor has already signed the contract, select Yes. Otherwise, select No." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="ElectronicAgreement?" FieldName="isElectronicAgreement" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">
                                                        <a href="javascript:ShowHint('hintElectronicAgreement','.electronicAgreementHint');"><i class="fa fa-info-circle electronicAgreementHint"></i></a>
                                                    </div>
                                                    <dx:ASPxComboBox ID="ddlElectronicAgreement" runat="server">
                                                        <Items>
                                                            <dx:ListEditItem Text="Yes" Value="1" />
                                                            <dx:ListEditItem Text="No" Value="0" />
                                                        </Items>
                                                    </dx:ASPxComboBox>
                                                </div>
                                                <dx:ASPxHint ID="hintElectronicAgreement" ClientInstanceName="hintElectronicAgreement" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Is Contract an Electronic Agreement" Content="If the contract must be electronically signed outside of the CMS System, select Yes. Otherwise select No." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Other Term" FieldName="paymentTerm" ClientVisible="false" Name="paymentTermOther">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">&nbsp;&nbsp;&nbsp;</div>
                                                    <dx:ASPxTextBox ID="txtPaymentOther" runat="server"></dx:ASPxTextBox>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                </Items>
                            </dx:LayoutGroup>
                            <dx:LayoutGroup ColumnCount="4" Name="Regulations" Caption="Regulations" VerticalAlign="Middle" SettingsItemCaptions-VerticalAlign="Middle">
                                <Items>
                                    <dx:LayoutItem ColSpan="2" Caption="Access to Books/Records" FieldName="isAccessToBooks">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">
                                                        <a href="javascript:ShowHint('hintAccessToBooks','.accessToBooksHint');"><i class="fa fa-info-circle accessToBooksHint"></i></a>
                                                    </div>
                                                    <dx:ASPxRadioButtonList ID="rbAccessToBooks" ClientInstanceName="rbAccessToBooks" runat="server" RepeatDirection="Horizontal" Border-BorderStyle="None" ValidationSettings-RequiredField-IsRequired="true">
                                                        <Items>
                                                            <dx:ListEditItem Text="Yes" Value="1" />
                                                            <dx:ListEditItem Text="No" Value="0" />
                                                        </Items>
                                                        <ClientSideEvents ValueChanged="OnReferenceChange" />
                                                    </dx:ASPxRadioButtonList>
                                                </div>
                                                <dx:ASPxHint ID="hintAccessToBooks" ClientInstanceName="hintAccessToBooks" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Access To Books" Content="If the contract cost will be greater than $10,000 per year we are required to have certain language in the contract.<br/>If this is not stated in the contract provided to you by your vendor, ask them to include it before you route your contract request through for approval.<br/>WHAT THE CONTRACT NEEDS TO SAY - <br/><br/>&quot;____________________________ (Vendor Name) agrees that until the expiration of four (4) years after the furnishing of services pursuant to this agreement,<br/>it shall make available upon request of the Secretary of the Department of Health and Human Services, or upon request of the Comptroller General, or any of their duly authorized representatives,<br/>the contract, books, documents and records that are necessary to certify the nature and extent of payments to __________________________ (Vendor Name).&quot;<br/><br/>If yes, please enter the page and paragraph (page-paragraph) of the associated language in the reference field." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="2" Caption="Adjustments for Medicare Changes" FieldName="isMedicareAdjustments">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">
                                                        <a href="javascript:ShowHint('hintMedicareAdjustments','.medicareAdjustmentsHint');"><i class="fa fa-info-circle medicareAdjustmentsHint"></i></a>
                                                    </div>
                                                    <dx:ASPxRadioButtonList ID="rbMedicareAdjustments" ClientInstanceName="rbMedicareAdjustments" runat="server" RepeatDirection="Horizontal" Border-BorderStyle="None" ValidationSettings-RequiredField-IsRequired="true">
                                                        <Items>
                                                            <dx:ListEditItem Text="Yes" Value="1" />
                                                            <dx:ListEditItem Text="No" Value="0" />
                                                        </Items>
                                                        <ClientSideEvents ValueChanged="OnReferenceChange" />
                                                    </dx:ASPxRadioButtonList>
                                                </div>
                                                <dx:ASPxHint ID="hintMedicareAdjustments" ClientInstanceName="hintMedicareAdjustments" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Medicare Adjustments" Content="If the service or supply could be affected by Medicare reimbursement, regulations, etc., the contract needs to include the following statement:<br/><br/>&ldquo;This agreement can be amended if Medicare or Medicaid issues regulations which are in conflict with this existing agreement.&rdquo;:<br/><br/>If yes, please enter the page and paragraph (page-paragraph) of the associated language in the reference field." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="2" Caption="Books/Records Reference Page/Paragraph" FieldName="accessToBooksReference" Name="BooksReference" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">&nbsp;&nbsp;&nbsp;</div>
                                                    <dx:ASPxTextBox ID="txtBooksReference" ClientInstanceName="txtBooksReference" runat="server"></dx:ASPxTextBox>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="2" Caption="Medicare Reference Page/Paragraph" FieldName="medicareAdjustmentsReference" Name="MedicareAdjustmentsReference" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">&nbsp;&nbsp;&nbsp;</div>
                                                    <dx:ASPxTextBox ID="txtMedicareReference" ClientInstanceName="txtMedicareReference" runat="server"></dx:ASPxTextBox>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="2" Caption="HIPAA Clause/Business Agreement" FieldName="isHipaaBA">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">
                                                        <a href="javascript:ShowHint('hintHipaaBAA','.hipaaBAAHint');"><i class="fa fa-info-circle hipaaBAAHint"></i></a>
                                                    </div>
                                                    <dx:ASPxRadioButtonList ID="rbHipaaBA" ClientInstanceName="rbHipaaBA" runat="server" RepeatDirection="Horizontal" Border-BorderStyle="None" ValidationSettings-RequiredField-IsRequired="true">
                                                        <Items>
                                                            <dx:ListEditItem Text="Yes" Value="1" />
                                                            <dx:ListEditItem Text="No" Value="0" />
                                                        </Items>
                                                        <ClientSideEvents ValueChanged="OnReferenceChange" />
                                                    </dx:ASPxRadioButtonList>
                                                </div>
                                                <dx:ASPxHint ID="hintHipaaBAA" ClientInstanceName="hintHipaaBAA" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="HIPAA Clause/Business Agreement" Content="If the vendor you are contracting with has access to PROTECTED HEALTH INFORMATION in any form, the vendor must have a signed Business Associates Agreement with Magnolia Regional Health Center.<br/>The BAA form can be found in the forms section of Magnet:<br/><a href='https://magnet.mrhc.org/leadership/deptlead/Business%20Associate%20Agreement/Forms/AllItems.aspx'  target='_blank'>Business Associate Agreement Form</a>" TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="2" Caption="Governing Law (MS)" FieldName="isGoverningLaw">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">
                                                        <a href="javascript:ShowHint('hintGoverningLaw','.governingLawHint');"><i class="fa fa-info-circle governingLawHint"></i></a>
                                                    </div>
                                                    <dx:ASPxRadioButtonList ID="rbGoverningLaw" ClientInstanceName="rbGoverningLaw" runat="server" RepeatDirection="Horizontal" Border-BorderStyle="None" ValidationSettings-RequiredField-IsRequired="true">
                                                        <Items>
                                                            <dx:ListEditItem Text="Yes" Value="YES" />
                                                            <dx:ListEditItem Text="No" Value="NO" />
                                                            <dx:ListEditItem Text="Implied" Value="IMPLIED" />
                                                        </Items>
                                                        <ClientSideEvents ValueChanged="OnReferenceChange" />
                                                    </dx:ASPxRadioButtonList>
                                                </div>
                                                <dx:ASPxHint ID="hintGoverningLaw" ClientInstanceName="hintGoverningLaw" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Governing Law (MS)" Content="All MRHC contracts must state the governing law as the State of Mississippi.<br/>Most contracts are written with governing law in a different state.<br/>We must ask the vendor to change this to the State of Mississippi.<br/>If yes, please enter the page and paragraph (page-paragraph) of the associated language in the reference field.<br/>If a STATE is not mentioned, check the IMPLIED box." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="2" Caption="HIPAA/BAA Reference Page/Paragraph" FieldName="hipaaReference" Name="HIPAAReference" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">&nbsp;&nbsp;&nbsp;</div>
                                                    <dx:ASPxTextBox ID="txtHipaaReference" ClientInstanceName="txtHipaaReference" runat="server"></dx:ASPxTextBox>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="2" Caption="Governing Law Reference Page/Paragraph" FieldName="governingLawReference" Name="GoverningLawReference" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">&nbsp;&nbsp;&nbsp;</div>
                                                    <dx:ASPxTextBox ID="txtGoverningLawReference" ClientInstanceName="txtGoverningLawReference" runat="server"></dx:ASPxTextBox>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>                                
                                    <dx:LayoutItem ColSpan="2" Caption="GSA/HHS/OIG" FieldName="isGsa">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <a href="javascript:ShowHint('hintGSA','.gsaHint');"><i class="fa fa-info-circle gsaHint"></i></a>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <dx:ASPxRadioButtonList ID="rbGSA" runat="server" RepeatDirection="Horizontal" Border-BorderStyle="None" ValidationSettings-RequiredField-IsRequired="true">
                                                        <Items>
                                                            <dx:ListEditItem Text="Yes" Value="1" />
                                                            <dx:ListEditItem Text="No" Value="0" />
                                                        </Items>
                                                    </dx:ASPxRadioButtonList>
                                                </div>
                                                <dx:ASPxHint ID="hintGSA" ClientInstanceName="hintGSA" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="GSA/HHS/OIG" Content="Magnolia Regional Health Center vendors must undergo an OIG investigation BEFORE we can conduct business with them. This is done when new vendors are selected and annually thereafter for all MRHC vendors.<br/><br/>If an OIG/GSA/HHS exclusion investigation shows that a vendor or individual has been excluded from the Medicare program, we cannot do business with them.<br/><br/>In no event will Magnolia Regional Health Center hire, engage as a contractor or otherwise do business with an individual or entity who<br/><br><ol><li> is currently excluded, suspended, debarred or otherwise ineligible to participate in the federal Medicare and Medicaid programs and/or </li><li> has been convicted of a criminal offense related to the provision of healthcare items and service without being reinstated in the federal Medicare program.</li></ol><br/>OIG checks will automatically be performed against the contracting party entered into the “Party” field and this box will automatically be checked if the party did not return a finding against the database search.<br/><br/>See Policy: Magnolia Regional Health Center Screening of Employees, Contractors and Medical Staff under the Policies section on the MRHC intranet home page for more information." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="2" Caption="Physician Rationale" FieldName="isPhysicianRationale">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">
                                                        <a href="javascript:ShowHint('hintPhysicianRationale','.physicianRationaleHint');"><i class="fa fa-info-circle physicianRationaleHint"></i></a>
                                                    </div>
                                                    <dx:ASPxRadioButtonList ID="rbPhysicianRationale" runat="server" RepeatDirection="Horizontal" Border-BorderStyle="None" ValidationSettings-RequiredField-IsRequired="true">
                                                        <Items>
                                                            <dx:ListEditItem Text="Yes" Value="1" />
                                                            <dx:ListEditItem Text="No" Value="0" />
                                                        </Items>
                                                    </dx:ASPxRadioButtonList>
                                                </div>
                                                <dx:ASPxHint ID="hintPhysicianRationale" ClientInstanceName="hintPhysicianRationale" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Physician Rationale" Content="If a physician rationale is required, please upload to the documents section of the contract." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="2" Caption="Contracted Employees" FieldName="isContractEmployees">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">
                                                        <a href="javascript:ShowHint('hintContractEmployees','.contractEmployeesHint');"><i class="fa fa-info-circle contractEmployeesHint"></i></a>
                                                    </div>
                                                    <dx:ASPxRadioButtonList ID="rbContractEmployees" runat="server" RepeatDirection="Horizontal" Border-BorderStyle="None" ValidationSettings-RequiredField-IsRequired="true">
                                                        <Items>
                                                            <dx:ListEditItem Text="Yes" Value="1" />
                                                            <dx:ListEditItem Text="No" Value="0" />
                                                        </Items>
                                                    </dx:ASPxRadioButtonList>
                                                </div>
                                                <dx:ASPxHint ID="hintContractEmployees" ClientInstanceName="hintContractEmployees" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Contracted Employees" Content="Does the contract include contract staff or labor?" TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="2" Caption="Restricted Data" FieldName="securityLevelId" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">&nbsp;&nbsp;&nbsp;</div>
                                                    <dx:ASPxRadioButtonList ID="rbSecurityLevel" runat="server" RepeatDirection="Horizontal" Border-BorderStyle="None" ValidationSettings-RequiredField-IsRequired="true">
                                                        <Items>
                                                            <dx:ListEditItem Text="Yes" Value="1" />
                                                            <dx:ListEditItem Text="No" Value="0" />
                                                        </Items>
                                                    </dx:ASPxRadioButtonList>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="2" FieldName="statusId" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <dx:ASPxLabel ID="lblStatusId" runat="server"></dx:ASPxLabel>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="2" Caption="Entry User Id" FieldName="entryUserId" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <dx:ASPxTextBox ID="txtEntryUserId" runat="server"></dx:ASPxTextBox>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="2" Caption="Current Status" FieldName="status" CaptionCellStyle-CssClass="spanTitle" CaptionStyle-CssClass="spanTitle" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <dx:ASPxLabel ID="lblStatus" runat="server"></dx:ASPxLabel>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="2" Caption="Status Date" FieldName="statusDate" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <dx:ASPxDateEdit ID="deStatusDate" runat="server" ReadOnly="true">
                                                    <ClientSideEvents Init="function (s,e){s.SetDate(new Date());}" />
                                                </dx:ASPxDateEdit>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="2" Caption="Entry User" FieldName="fullName" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <dx:ASPxTextBox ID="txtEntryName" runat="server"></dx:ASPxTextBox>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>                                    
                                    <dx:LayoutItem ColSpan="2" Caption="Entry Date" FieldName="entryDate" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <dx:ASPxDateEdit ID="deEntryDate" runat="server">
                                                    <ClientSideEvents Init="function (s,e){s.SetDate(new Date());}" />
                                                </dx:ASPxDateEdit>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="2"  FieldName="routingSequence" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <dx:ASPxLabel ID="lblRoutingSequence" runat="server"></dx:ASPxLabel>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                </Items>
                            </dx:LayoutGroup>
                            <dx:LayoutGroup GroupBoxDecoration="None" ColumnCount="3" Name="Buttons" VerticalAlign="Middle" SettingsItemCaptions-VerticalAlign="Middle">
                                <Items>
                                    <%--<dx:LayoutItem ShowCaption="False" ColumnSpan="1">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxButton ID="btnDelete" runat="server" OnClick="btnDelete_Click" Text="Delete" ClientVisible="false"></dx:ASPxButton>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>--%>
                                    <dx:EmptyLayoutItem></dx:EmptyLayoutItem>
                                    <dx:LayoutItem ShowCaption="False" ColumnSpan="1" HorizontalAlign="Right">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" AutoPostBack="false" CausesValidation="false">
                                                    <ClientSideEvents Click="function(s,e){ if(confirm('Are you sure you want to cancel this request?')==true){window.location='contracts.aspx';}else{e.cancel;}}" />
                                                </dx:ASPxButton>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ShowCaption="False" ColumnSpan="1" HorizontalAlign="Right">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxButton ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save">
                                                    <ClientSideEvents Click="OnSave" />
                                                </dx:ASPxButton>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                </Items>
                            </dx:LayoutGroup>
                        </Items>
                    </dx:ASPxFormLayout>
                    <dx:ASPxLabel ID="lblSender" ClientInstanceName="lblSender" runat="server"></dx:ASPxLabel>
                    <dx:ASPxHiddenField ID="hdnSender" ClientInstanceName="hdnSender" runat="server"></dx:ASPxHiddenField>
                    <dx:ASPxHiddenField ID="hdnOIGCheck" ClientInstanceName="hdnOIGCheck" runat="server"></dx:ASPxHiddenField>
                    <dx:ASPxHiddenField ID="hdnOIGCount" ClientInstanceName="hdnOIGCount" runat="server"></dx:ASPxHiddenField>
                    <dx:ASPxHiddenField ID="hdnContractId" ClientInstanceName="hdnContractId" runat="server"></dx:ASPxHiddenField>
                    <dx:ASPxHiddenField ID="hdnDepartment" ClientInstanceName="hdnDepartment" runat="server"></dx:ASPxHiddenField>
                    <dx:ASPxHiddenField ID="hdnNewPartyName" ClientInstanceName="hdnNewPartyName" runat="server"></dx:ASPxHiddenField>
                    <dx:ASPxPopupControl ClientInstanceName="popParty" ID="popParty" runat="server" ShowHeader="false" Modal="true" Top="300" Left="300" 
                            PopupHorizontalAlign="OutsideRight" PopupVerticalAlign="Below" Width="300px" Height="300px" PopupElementID="ddlParties" OnLoad="popParty_Load" 
                            ClientSideEvents-PopUp="function(s,e){ASPxClientEdit.ClearEditorsInContainerById('divParty'); txtPartyBusinessName.Focus(); txtPartyBusinessName.SetText(hdnNewPartyName.Get('businessName'));}" >
                        <ContentCollection>
                            <dx:PopupControlContentControl>
                                <div id="divParty" runat="server">
                                    <dx:ASPxFormLayout ID="formParty" ClientInstanceName="formParty" runat="server">
                                        <Items>
                                            <dx:LayoutGroup Caption="Enter Party/Vendor Details" VerticalAlign="Middle" SettingsItemCaptions-VerticalAlign="Middle">
                                                <Items>
                                                    <dx:LayoutItem ColSpan="1" FieldName="partyId" Visible="false">
                                                        <LayoutItemNestedControlCollection>
                                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                                <dx:ASPxLabel ID="lblPartyId" runat="server"></dx:ASPxLabel>
                                                            </dx:LayoutItemNestedControlContainer>
                                                        </LayoutItemNestedControlCollection>
                                                    </dx:LayoutItem>
                                                    <dx:LayoutItem ColSpan="1" Caption="Business Name" FieldName="businessName">
                                                        <LayoutItemNestedControlCollection>
                                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                                <dx:ASPxTextBox ID="txtPartyBusinessName" ClientInstanceName="txtPartyBusinessName" runat="server"></dx:ASPxTextBox>
                                                            </dx:LayoutItemNestedControlContainer>
                                                        </LayoutItemNestedControlCollection>
                                                    </dx:LayoutItem>
                                                    <dx:LayoutItem ColSpan="1" Caption="Contact Person" FieldName="contactName">
                                                        <LayoutItemNestedControlCollection>
                                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                                <dx:ASPxTextBox ID="txtPartyContactName" runat="server"></dx:ASPxTextBox>
                                                            </dx:LayoutItemNestedControlContainer>
                                                        </LayoutItemNestedControlCollection>
                                                    </dx:LayoutItem>
                                                    <dx:LayoutItem ColSpan="1" Caption="Address" FieldName="address">
                                                        <LayoutItemNestedControlCollection>
                                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                                <dx:ASPxTextBox ID="txtPartyAddress" runat="server"></dx:ASPxTextBox>
                                                            </dx:LayoutItemNestedControlContainer>
                                                        </LayoutItemNestedControlCollection>
                                                    </dx:LayoutItem>
                                                    <dx:LayoutItem ColSpan="1" Caption="City" FieldName="city">
                                                        <LayoutItemNestedControlCollection>
                                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                                <dx:ASPxTextBox ID="txtPartyCity" runat="server"></dx:ASPxTextBox>
                                                            </dx:LayoutItemNestedControlContainer>
                                                        </LayoutItemNestedControlCollection>
                                                    </dx:LayoutItem>
                                                    <dx:LayoutItem ColSpan="1" Caption="State" FieldName="state">
                                                        <LayoutItemNestedControlCollection>
                                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                                <dx:ASPxTextBox ID="txtPartyState" runat="server"></dx:ASPxTextBox>
                                                            </dx:LayoutItemNestedControlContainer>
                                                        </LayoutItemNestedControlCollection>
                                                    </dx:LayoutItem>
                                                    <dx:LayoutItem ColSpan="1" Caption="Zip Code" FieldName="zip">
                                                        <LayoutItemNestedControlCollection>
                                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                                <dx:ASPxTextBox ID="txtPartyZip" runat="server"></dx:ASPxTextBox>
                                                            </dx:LayoutItemNestedControlContainer>
                                                        </LayoutItemNestedControlCollection>
                                                    </dx:LayoutItem>
                                                    <dx:LayoutItem ColSpan="1" Caption="Phone Number" FieldName="phone">
                                                        <LayoutItemNestedControlCollection>
                                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                                <dx:ASPxTextBox ID="txtPartyPhone" runat="server"></dx:ASPxTextBox>
                                                            </dx:LayoutItemNestedControlContainer>
                                                        </LayoutItemNestedControlCollection>
                                                    </dx:LayoutItem>
                                                    <dx:LayoutItem ColSpan="1" Caption="Email Address" FieldName="email">
                                                        <LayoutItemNestedControlCollection>
                                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                                <dx:ASPxTextBox ID="txtPartyEmail" runat="server"></dx:ASPxTextBox>
                                                            </dx:LayoutItemNestedControlContainer>
                                                        </LayoutItemNestedControlCollection>
                                                    </dx:LayoutItem>
                                                    <dx:LayoutItem ColSpan="1" Caption="Party Type" FieldName="partyTypeId">
                                                        <LayoutItemNestedControlCollection>
                                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                                <dx:ASPxComboBox ID="ddlPartyTypes" runat="server" DataSourceID="dsPartyTypes" TextField="partyType" ValueField="partyTypeId"></dx:ASPxComboBox>
                                                                <asp:SqlDataSource ID="dsPartyTypes" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                                                                    SelectCommand="usp_getPartyTypes" SelectCommandType="StoredProcedure">
                                                                </asp:SqlDataSource>
                                                            </dx:LayoutItemNestedControlContainer>
                                                        </LayoutItemNestedControlCollection>
                                                    </dx:LayoutItem>
                                                    <dx:LayoutItem ColSpan="1" Caption="Approval Date" FieldName="approvalDate" ClientVisible="false">
                                                        <LayoutItemNestedControlCollection>
                                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                                <dx:ASPxLabel ID="lblPartyApprovalDate" runat="server"></dx:ASPxLabel>
                                                            </dx:LayoutItemNestedControlContainer>
                                                        </LayoutItemNestedControlCollection>
                                                    </dx:LayoutItem>
                                                </Items>
                                            </dx:LayoutGroup>
                                            <dx:LayoutGroup GroupBoxDecoration="None" ColumnCount="2" VerticalAlign="Middle" SettingsItemCaptions-VerticalAlign="Middle">
                                                <Items>
                                                    <dx:LayoutItem ShowCaption="False" HorizontalAlign="Right">
                                                        <LayoutItemNestedControlCollection>
                                                            <dx:LayoutItemNestedControlContainer>
                                                                <dx:ASPxButton ID="btnCancelParty" runat="server" Text="Cancel">
                                                                    <ClientSideEvents Click="function(s,e){popParty.Hide(); formContract.GetItemByName('Party').SetVisible(true); ddlDepartments.SetSelectedIndex(hdnDepartment.Get('department')); hdnOIGCheck.Set('check','false'); hdnNewPartyName.Set('businessName',''); ddlParties.SetSelectedIndex(-1);}" />
                                                                </dx:ASPxButton>
                                                            </dx:LayoutItemNestedControlContainer>
                                                        </LayoutItemNestedControlCollection>
                                                    </dx:LayoutItem>
                                                    <dx:LayoutItem ShowCaption="False" HorizontalAlign="Right">
                                                        <LayoutItemNestedControlCollection>
                                                            <dx:LayoutItemNestedControlContainer>
                                                                <dx:ASPxButton ID="btnSaveParty" runat="server" OnClick="btnSaveParty_Click" Text="Save">
                                                                    <ClientSideEvents Click="function (s,e){ formContract.GetItemByName('Party').SetVisible(true); ddlDepartments.SetSelectedIndex(hdnDepartment.Get('department'));}" />
                                                                </dx:ASPxButton>
                                                            </dx:LayoutItemNestedControlContainer>
                                                        </LayoutItemNestedControlCollection>
                                                    </dx:LayoutItem>
                                                </Items>
                                            </dx:LayoutGroup>
                                        </Items>
                                    </dx:ASPxFormLayout>
                                    <asp:SqlDataSource ID="dsParty" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                                        InsertCommand="usp_addParty" InsertCommandType="StoredProcedure">
                                        <InsertParameters>
                                            <asp:ControlParameter ControlID="formParty$txtPartyBusinessName" PropertyName="Text" Name="businessName" />
                                            <asp:ControlParameter ControlID="formParty$txtPartyContactName" PropertyName="Text" Name="contactName" />
                                            <asp:ControlParameter ControlID="formParty$txtPartyAddress" PropertyName="Text" Name="address" />
                                            <asp:ControlParameter ControlID="formParty$txtPartyCity" PropertyName="Text" Name="city" />
                                            <asp:ControlParameter ControlID="formParty$txtPartyState" PropertyName="Text" Name="state" />
                                            <asp:ControlParameter ControlID="formParty$txtPartyZip" PropertyName="Text" Name="zip" />
                                            <asp:ControlParameter ControlID="formParty$txtPartyPhone" PropertyName="Text" Name="phone" />
                                            <asp:ControlParameter ControlID="formParty$txtPartyEmail" PropertyName="Text" Name="email" />
                                            <asp:ControlParameter ControlID="formParty$ddlPartyTypes" PropertyName="Value" Name="partyTypeId" />
                                            <asp:Parameter Name="@approvalDate" DefaultValue="<%= DateTime.Now%>" />
                                            <asp:ControlParameter ControlID="formParty$ddlDepartments" PropertyName="Value" Name="departmentId" />
                                        </InsertParameters>
                                    </asp:SqlDataSource>
                                    <dx:ASPxLabel ID="lblPartyValidation" runat="server" Font-Bold="true" Font-Size="Medium" ForeColor="Red" Visible="false"></dx:ASPxLabel>
                                </div>
                            </dx:PopupControlContentControl>
                        </ContentCollection>
                    </dx:ASPxPopupControl>
                    <dx:ASPxLabel ID="sqlErrorMessage" runat="server" ClientVisible="false"></dx:ASPxLabel>
                    <dx:ASPxLabel ID="lblQuery" runat="server" ClientVisible="false"></dx:ASPxLabel>
                    <asp:SqlDataSource ID="dsContract" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                        OnUpdating="dsContract_Updating" OnSelecting="dsContract_Selecting" OnInserting="dsContract_Inserting"
                        OnInserted="dsContract_Inserted" OnSelected="dsContract_Selected" OnUpdated="dsContract_Updated"
                        DeleteCommand="usp_deleteContract" DeleteCommandType="StoredProcedure" 
                        SelectCommand="usp_getContractsEnhanced" SelectCommandType="StoredProcedure"
                        InsertCommand="usp_addContract" InsertCommandType="StoredProcedure"
                        UpdateCommand="usp_updateContract" UpdateCommandType="StoredProcedure">
                        <InsertParameters>
                            <asp:ControlParameter ControlID="formContract$ddlParties" PropertyName="Value" Name="partyId" />
                            <asp:ControlParameter ControlID="formContract$ddlContractTypes" PropertyName="Value" Name="contractTypeId" />
                            <asp:ControlParameter ControlID="formContract$deStartDate" PropertyName="Date" Name="startDate" DefaultValue="<%= DateTime.Now%>" />
                            <asp:ControlParameter ControlID="formContract$deEndDate" PropertyName="Date" Name="endDate" DefaultValue="<%= DateTime.Now%>" />
                            <asp:ControlParameter ControlID="formContract$lblRoutingSequence" PropertyName="Text" Name="routingSequence" />
                            <asp:ControlParameter ControlID="formContract$lblStatusId" PropertyName="Text" Name="statusId" DefaultValue="2" />
                            <asp:SessionParameter SessionField="UserID" Name="entryUserId" />
                            <asp:ControlParameter ControlID="formContract$deEntryDate" PropertyName="Date" Name="entryDate" DefaultValue="<%= DateTime.Now%>" />
                            <asp:ControlParameter ControlID="formContract$ddlActions" PropertyName="Value" Name="actionId" />
                            <asp:Parameter Name="isPurchasingGroup" DefaultValue="1" />
                            <asp:ControlParameter ControlID="formContract$ddlPurchasingGroup" PropertyName="Value" Name="purchasingGroup" />
                            <asp:ControlParameter ControlID="formContract$ddlAutoRenewal" PropertyName="Value" Name="isAutoRenewal" />
                            <asp:ControlParameter ControlID="formContract$ddlOutclause" PropertyName="Value" Name="isOutClause" />
                            <asp:ControlParameter ControlID="formContract$spinDaysBreach" PropertyName="Value" Name="notificationDaysBreach" />
                            <asp:ControlParameter ControlID="formContract$spinDaysNoBreach" PropertyName="Value" Name="notificationDaysNoBreach" />
                            <asp:ControlParameter ControlID="formContract$ddlExclusivity" PropertyName="Value" Name="isExclusivity" />
                            <asp:ControlParameter ControlID="formContract$memoDetails" PropertyName="Text" Name="details" />
                            <asp:ControlParameter ControlID="formContract$spinAnnualCost" PropertyName="Value" Name="annualCost" DefaultValue="0" />
                            <asp:ControlParameter ControlID="formContract$spinPriorYearCost" PropertyName="Value" Name="priorYearCost" DefaultValue="0" />
                            <asp:ControlParameter ControlID="formContract$spinEstimatedTotalCost" PropertyName="Value" Name="estimatedTotalCost" DefaultValue="0" />
                            <asp:ControlParameter ControlID="formContract$ddlPaymentTerm" PropertyName="Value" Name="paymentTerm" />
                            <asp:ControlParameter ControlID="formContract$ddlGlEOC" PropertyName="Value" Name="glEOC" />
                            <asp:ControlParameter ControlID="formContract$rbAccessToBooks" PropertyName="Value" Name="isAccessToBooks" />
                            <asp:ControlParameter ControlID="formContract$txtBooksReference" PropertyName="Text" Name="accessToBooksReference" />
                            <asp:ControlParameter ControlID="formContract$rbMedicareAdjustments" PropertyName="Value" Name="isMedicareAdjustments" />
                            <asp:ControlParameter ControlID="formContract$rbHipaaBA" PropertyName="Value" Name="isHipaaBA" />
                            <asp:ControlParameter ControlID="formContract$rbGSA" PropertyName="Value" Name="isGsa" />
                            <asp:ControlParameter ControlID="formContract$rbPhysicianRationale" PropertyName="Value" Name="isPhysicianRationale" />
                            <asp:ControlParameter ControlID="formContract$rbGoverningLaw" PropertyName="Value" Name="isGoverningLaw" />
                            <asp:ControlParameter ControlID="formContract$rbContractEmployees" PropertyName="Value" Name="isContractEmployees" />
                            <asp:ControlParameter ControlID="formContract$deStatusDate" PropertyName="Date" Name="statusDate" DefaultValue="<%= DateTime.Now%>" />
                            <asp:ControlParameter ControlID="formContract$rbSecurityLevel" PropertyName="Value" Name="securityLevelId" DefaultValue="0" />
                            <asp:ControlParameter ControlID="formContract$lblContractLength" PropertyName="Text" Name="contractLength" />
                            <asp:ControlParameter ControlID="formContract$ddlDepartments" PropertyName="Value" Name="departmentId" />
                            <asp:ControlParameter ControlID="formContract$txtPaymentOther" PropertyName="Text" Name="paymentOther" />
                            <asp:ControlParameter ControlID="formContract$ddlMasterContracts" PropertyName="Value" Name="masterContractId" />
                            <asp:ControlParameter ControlID="formContract$txtMedicareReference" PropertyName="Text" Name="medicareAdjustmentsReference" />
                            <asp:ControlParameter ControlID="formContract$txtHipaaReference" PropertyName="Text" Name="hipaaReference" />
                            <asp:ControlParameter ControlID="formContract$txtGoverningLawReference" PropertyName="Text" Name="governingLawReference" />
                            <asp:ControlParameter ControlID="formContract$rbBudgeted" PropertyName="Value" Name="isBudgeted" />
                            <asp:ControlParameter ControlID="formContract$spinBudgetedAmount" PropertyName="Value" Name="budgetedAmount" DefaultValue="0" />
                            <asp:ControlParameter ControlID="formContract$ddlPartySigned" PropertyName="Value" Name="isPartySigned" />
                            <asp:ControlParameter ControlID="formContract$ddlElectronicAgreement" PropertyName="Value" Name="isElectronicAgreement" />
                            <asp:ControlParameter ControlID="formContract$rbCorporation" PropertyName="Value" Name="isCorporation" />
                            <asp:ControlParameter ControlID="formContract$rbCorporationStanding" PropertyName="Value" Name="isCorporationGoodStanding" />
                            <asp:Parameter Name="contractId" Type="Int32" Direction="Output" Size="32" />
                            <asp:Parameter Name="isValid" Type="Boolean" Direction="Output" Size="4" />
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:ControlParameter ControlID="formContract$ddlParties" PropertyName="Value" Name="partyId" />
                            <asp:ControlParameter ControlID="formContract$ddlContractTypes" PropertyName="Value" Name="contractTypeId" />
                            <asp:ControlParameter ControlID="formContract$deStartDate" PropertyName="Date" Name="startDate" DefaultValue="<%= DateTime.Now%>" />
                            <asp:ControlParameter ControlID="formContract$deEndDate" PropertyName="Date" Name="endDate" DefaultValue="<%= DateTime.Now%>" />
                            <asp:ControlParameter ControlID="formContract$lblRoutingSequence" PropertyName="Text" Name="routingSequence" />
                            <asp:ControlParameter ControlID="formContract$lblStatusId" PropertyName="Text" Name="statusId" DefaultValue="2" />
                            <asp:SessionParameter SessionField="UserID" Name="entryUserId" />
                            <asp:ControlParameter ControlID="formContract$deEntryDate" PropertyName="Date" Name="entryDate" DefaultValue="<%= DateTime.Now%>" />
                            <asp:ControlParameter ControlID="formContract$ddlActions" PropertyName="Value" Name="actionId" />
                            <asp:Parameter Name="isPurchasingGroup" DefaultValue="1" />
                            <asp:ControlParameter ControlID="formContract$ddlPurchasingGroup" PropertyName="Value" Name="purchasingGroup" />
                            <asp:ControlParameter ControlID="formContract$ddlAutoRenewal" PropertyName="Value" Name="isAutoRenewal" />
                            <asp:ControlParameter ControlID="formContract$ddlOutclause" PropertyName="Value" Name="isOutClause" />
                            <asp:ControlParameter ControlID="formContract$spinDaysBreach" PropertyName="Number" Name="notificationDaysBreach" DefaultValue="0" />
                            <asp:ControlParameter ControlID="formContract$spinDaysNoBreach" PropertyName="Number" Name="notificationDaysNoBreach" DefaultValue="0" />
                            <asp:ControlParameter ControlID="formContract$ddlExclusivity" PropertyName="Value" Name="isExclusivity" />
                            <asp:ControlParameter ControlID="formContract$memoDetails" PropertyName="Text" Name="details" />
                            <asp:ControlParameter ControlID="formContract$spinAnnualCost" PropertyName="Number" Name="annualCost" DefaultValue="0" />
                            <asp:ControlParameter ControlID="formContract$spinPriorYearCost" PropertyName="Number" Name="priorYearCost" DefaultValue="0" />
                            <asp:ControlParameter ControlID="formContract$spinEstimatedTotalCost" PropertyName="Number" Name="estimatedTotalCost" DefaultValue="0" />
                            <asp:ControlParameter ControlID="formContract$ddlPaymentTerm" PropertyName="Value" Name="paymentTerm" />
                            <asp:ControlParameter ControlID="formContract$ddlGlEOC" PropertyName="Value" Name="glEOC" />
                            <asp:ControlParameter ControlID="formContract$rbAccessToBooks" PropertyName="Value" Name="isAccessToBooks" />
                            <asp:ControlParameter ControlID="formContract$txtBooksReference" PropertyName="Text" Name="accessToBooksReference" />
                            <asp:ControlParameter ControlID="formContract$rbMedicareAdjustments" PropertyName="Value" Name="isMedicareAdjustments" />
                            <asp:ControlParameter ControlID="formContract$rbHipaaBA" PropertyName="Value" Name="isHipaaBA" />
                            <asp:ControlParameter ControlID="formContract$rbGSA" PropertyName="Value" Name="isGsa" />
                            <asp:ControlParameter ControlID="formContract$rbPhysicianRationale" PropertyName="Value" Name="isPhysicianRationale" />
                            <asp:ControlParameter ControlID="formContract$rbGoverningLaw" PropertyName="Value" Name="isGoverningLaw" />
                            <asp:ControlParameter ControlID="formContract$rbContractEmployees" PropertyName="Value" Name="isContractEmployees" />
                            <asp:ControlParameter ControlID="formContract$deStatusDate" PropertyName="Date" Name="statusDate" DefaultValue="<%= DateTime.Now%>" />
                            <asp:ControlParameter ControlID="formContract$rbSecurityLevel" PropertyName="Value" Name="securityLevelId" DefaultValue="0" />
                            <asp:ControlParameter ControlID="formContract$lblContractLength" PropertyName="Text" Name="contractLength" />
                            <asp:ControlParameter ControlID="formContract$ddlDepartments" PropertyName="Value" Name="departmentId" />
                            <asp:ControlParameter ControlID="formContract$txtPaymentOther" PropertyName="Text" Name="paymentOther" />
                            <asp:ControlParameter ControlID="formContract$ddlMasterContracts" PropertyName="Value" Name="masterContractId" />
                            <asp:ControlParameter ControlID="formContract$txtMedicareReference" PropertyName="Text" Name="medicareAdjustmentsReference" />
                            <asp:ControlParameter ControlID="formContract$txtHipaaReference" PropertyName="Text" Name="hipaaReference" />
                            <asp:ControlParameter ControlID="formContract$txtGoverningLawReference" PropertyName="Text" Name="governingLawReference" />
                            <asp:ControlParameter ControlID="formContract$rbBudgeted" PropertyName="Value" Name="isBudgeted" />
                            <asp:ControlParameter ControlID="formContract$spinBudgetedAmount" PropertyName="Value" Name="budgetedAmount" DefaultValue="0" />
                            <asp:ControlParameter ControlID="formContract$ddlPartySigned" PropertyName="Value" Name="isPartySigned" />
                            <asp:ControlParameter ControlID="formContract$ddlElectronicAgreement" PropertyName="Value" Name="isElectronicAgreement" />
                            <asp:ControlParameter ControlID="formContract$rbCorporation" PropertyName="Value" Name="isCorporation" />
                            <asp:ControlParameter ControlID="formContract$rbCorporationStanding" PropertyName="Value" Name="isCorporationGoodStanding" />
                            <asp:QueryStringParameter Name="contractId" QueryStringField="id" />
                            <asp:Parameter Name="isValid" Direction="Output" Size="4" Type="Boolean" />
                        </UpdateParameters>
                        <DeleteParameters>
                            <asp:QueryStringParameter Name="contractId" QueryStringField="id" />
                        </DeleteParameters>
                        <SelectParameters>
                            <asp:QueryStringParameter Name="contractId" QueryStringField="id" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <dx:ASPxPopupControl ID="popExclusions" ClientInstanceName="popExclusions" runat="server" Modal="true" HeaderText="" Left="75" Top="100" ShowCloseButton="true" AllowDragging="true" AllowResize="true">
                        <SettingsAdaptivity Mode="Always" VerticalAlign="WindowBottom" HorizontalAlign="WindowCenter" MinHeight="80%" MinWidth="90%" />
                        <ContentCollection>
                            <dx:PopupControlContentControl>
                                <div style="display:flex; flex-direction:row;">
                                    <dx:ASPxLabel id="lblBusinessName" runat="server" Text="Business Name:"></dx:ASPxLabel>
                                    &nbsp;&nbsp;&nbsp;
                                    <dx:ASPxTextBox ID="txtBusinessName" ClientInstanceName="txtBusinessName" runat="server" ValidationSettings-RequiredField-IsRequired="true" Width="200"></dx:ASPxTextBox>
                                    &nbsp;&nbsp;&nbsp;
                                    <dx:ASPxCheckBox ID="cbFuzzy" runat="server" Checked="false" Text="Include Fuzzy Search Results?"></dx:ASPxCheckBox>
                                    &nbsp;&nbsp;&nbsp;
                                    <dx:ASPxButton ID="btnSearchExclusions" runat="server" Text="Search Exclusions" OnClick="btnSearchExclusions_Click"></dx:ASPxButton>
                                    <br /><br />
                                </div>
                                <div id="divExclusionResults" runat="server" visible="false" style="width:100%;">
                                    <dx:ASPxLabel ID="lblExclusions" ClientInstanceName="lblExclusions" runat="server" Text="The party/vendor entered matches one or more on the exclusions list.<br/>If you are sure your party/vendor is not the same as one listed below, click Add Party below." Font-Bold="true" ForeColor="Red" Font-Size="Small" EncodeHtml="false"></dx:ASPxLabel>
                                    <br /><br />
                                    <dx:ASPxGridView ID="gvExclusions" runat="server" ClientInstanceName="gvExclusions" Width="100%">
                                        <Columns>
                                            <dx:GridViewDataColumn FieldName="Classification" Caption="Classification"></dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn FieldName="EntityName" Caption="Entity Name"></dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn FieldName="FirstName" Caption="First Name"></dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn FieldName="MiddleInitial" Caption="Middle Initial"></dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn FieldName="LastName" Caption="Last Name"></dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn FieldName="Address" Caption="Address"></dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn FieldName="City" Caption="City"></dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn FieldName="State" Caption="State"></dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn FieldName="ZipCode" Caption="Zip Code"></dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn FieldName="ExclusionType" Caption="Exclusion Type"></dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn FieldName="ActiveDate" Caption="Active Date"></dx:GridViewDataColumn>
                                        </Columns>
                                        <Settings ShowHeaderFilterButton="true"  />
                                        <SettingsPager Mode="ShowPager" PageSize="10"></SettingsPager>
                                    </dx:ASPxGridView>
                                    <br /><br />
                                    <div style="width:100%;text-align:left;">
                                        <dx:ASPxButton ID="btnExclusionCancel" runat="server" Text="Cancel">
                                            <ClientSideEvents Click="function(s,e){popExclusions.Hide();ddlDepartments.SetSelectedIndex(hdnDepartment.Get('department'));}" />
                                        </dx:ASPxButton>
                                        &nbsp;&nbsp;&nbsp;
                                        <dx:ASPxButton ID="btnExclusionSave" runat="server" Text="Add Party" AutoPostBack="true" OnClick="btnExclusionSave_Click"></dx:ASPxButton>
                                    </div>
                                    <br /><br />
                                    <div style="width:100%;text-align:left;">
                                        <dx:ASPxLabel ID="lblExclusionNote" ClientInstanceName="lblExclusionNote" runat="server" Font-Bold="true" ForeColor="Red" Text="*Only Add Party if you are sure your party is not the same as one listed above."></dx:ASPxLabel>
                                    </div>
                                </div>
                            </dx:PopupControlContentControl>
                        </ContentCollection>
                    </dx:ASPxPopupControl>
                     <dx:ASPxPopupControl ID="popRouteLookup" ClientInstanceName="popRouteLookup" runat="server" HeaderText="" ShowCloseButton="true" AllowDragging="true" AllowResize="true">
                        <SettingsAdaptivity Mode="Always" VerticalAlign="WindowTop" HorizontalAlign="WindowCenter" MinHeight="100%" MinWidth="50%" />
                        <ContentCollection>
                            <dx:PopupControlContentControl>
    <%--                            <div style="overflow:auto">
                                    <dx:ASPxLabel ID="lblTest" ClientInstanceName="lblTest" runat="server" ClientVisible="false"></dx:ASPxLabel>
                                     <dx:ASPxDiagram ID="Diagram" ClientInstanceName="Diagram" runat="server" NodeDataSourceID="SqlDataSource1" AutoZoom="Disabled" ReadOnly="true" SimpleView="true" EnableTheming="true" Theme="Material" >
                                         <Mappings>
                                            <Node Key="roleId" Text="roleName" ParentKey="parentId" TextStyle="{font-size:10pt,text-wrap:normal;word-wrap: break-word;}" />
                                         </Mappings>
                                         <SettingsToolbox Visibility="Disabled"></SettingsToolbox>
                                         <SettingsSidePanel Visibility="Disabled" />
                                         <SettingsAutoLayout Type="Auto" />
                                    </dx:ASPxDiagram>
                                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                                        SelectCommand="usp_getRoute" SelectCommandType="StoredProcedure">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="lblTest" PropertyName="Text" Name="routingId" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </div>--%>
                            </dx:PopupControlContentControl>
                        </ContentCollection>
                    </dx:ASPxPopupControl>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxCallbackPanel>
    </div>
</asp:Content>