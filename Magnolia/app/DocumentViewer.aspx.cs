﻿using System;
using System.IO;
using System.Text;

namespace Magnolia.app
{
    public partial class DocumentViewer : System.Web.UI.Page
    {
        public string strFilename = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            //strFilename = Request.QueryString["url"];
            //Response.Redirect(Request.QueryString["url"]);
            //Response.Write(strFilename);
            //if (strFilename.Contains("doc") || strFilename.Contains("xls"))
            //{
            //    Response.ClearContent();
            //    Response.AppendHeader("content-disposition", "attachment; filename=" + strFilename);
            //    if (strFilename.Contains("doc"))
            //    {
            //        Response.ContentType = "application/msword";
            //    }
            //    if (strFilename.Contains("xls"))
            //    {
            //        Response.ContentType = "application/vnd.ms-excel";
            //    }
            //        Response.Flush();
            //        strFilename = "";
            //}
            //string FilePath = Server.UrlDecode(Request.QueryString["url"]); //.ToString().Replace(@"\", "/").Replace(@"mrhc.org\pub\Public", "Documents");
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                try
                {
                    Byte[] byteArray = null;
                    using (MemoryStream mem = ContractSummarySheet.Form(Convert.ToInt32(Request.QueryString["id"])))
                    {
                        if (mem != null)
                        {
                            byteArray = mem.ToArray();
                        }
                    }
                    if (byteArray != null && byteArray.Length > 0)
                    {
                        Response.ContentType = "application/pdf";
                        Response.AddHeader("content-length", Convert.ToString(byteArray.Length - 1));
                        Response.BinaryWrite(byteArray);
                        Response.Flush();
                        Response.End();
                    }
                }
                catch(Exception ex)
                {
                    Message.Alert(ex.Message);
                }
            }
            else
            {
                System.Data.DataSet ds = null;
                if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "pending")
                {
                    ds = DataAccess.GetDataSet("usp_getPendingDocumentUrl", new string[] { "@documentId" }, new string[] { Request.QueryString["documentId"].ToString() });
                }
                else
                {
                    ds = DataAccess.GetDataSet("usp_getDocumentUrl", new string[] { "@documentId" }, new string[] { Request.QueryString["documentId"].ToString() });
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    string FilePath = ds.Tables[0].Rows[0]["url"].ToString().Replace(@"//mrhc.org/pub/Public/Contract Database/", System.Configuration.ConfigurationManager.AppSettings["documentVirtualPath"]).Replace(@"/DEV/DEV/",@"/DEV/"); //Server.UrlDecode(Session["documentURL"].ToString()); //.ToString().Replace(@"\", "/").Replace(@"mrhc.org\pub\Public", "Documents");
                    
                    string documentType = ds.Tables[0].Rows[0]["documentType"].ToString().ToUpper();
                    string fileName = ds.Tables[0].Rows[0]["fileName"].ToString();
                    System.Net.WebClient User = new System.Net.WebClient();
                    try
                    {
                        Byte[] FileBuffer = User.DownloadData(FilePath);
                        if (FileBuffer != null)
                        {
                            switch(documentType)
                            {
                                case "PDF":  Response.ContentType = "application/pdf";
                                    break;
                                case "WORD": Response.ContentType = "application/msword";
                                    break;
                                case "EXCEL": Response.ContentType = "application/vnd.ms-excel";
                                    break;
                                default: Response.ContentType = "text/plain";
                                    break;
                            }
                            if (documentType != "PDF")
                                Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
                            Response.AddHeader("content-length", FileBuffer.Length.ToString());
                            Response.BinaryWrite(FileBuffer);
                            Response.Flush();
                        }
                    }
                    catch (Exception ex)
                    {
                        Message.Alert(ex.Message);
                    }
                }
                else
                {
                    Message.Alert("File not found.");
                }
            }
        }
    }
}