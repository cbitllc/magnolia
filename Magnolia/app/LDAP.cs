﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.DirectoryServices;
using System.IO;

namespace Magnolia.app
{
    public class LDAP
    {
        public static string AuthenticateUser(string domainName, string userName, string password)
        {
            string ret = "false";
            try
            {
                //DirectoryEntry de = new DirectoryEntry("LDAP://mrhc.org", txtuser.Value.ToString(), txtpassword.Value.ToString(), AuthenticationTypes.Secure);
                //DirectorySearcher dirSearch = new DirectorySearcher(de);
                //dirSearch.FindOne();

                using (DirectoryEntry de = new DirectoryEntry("LDAP://mrhc.org", userName, password))
                {
                    using (DirectorySearcher dsearch = new DirectorySearcher(de))
                    {
                        //SearchResult results = null;
                        //results = 
                        dsearch.FindOne();
                        ret = "true";
                    }
                }
            }
            catch (Exception ex)
            {
                Message.Alert(ex.Message);
                ret = ex.Message;
            }
            return ret;
        }

        public static AppUser GetUser(string userName)
        {
            AppUser newUser = new AppUser();
            try
            {
                //SearchResultCollection results;
                // Build User Searcher
                using (DirectoryEntry de = new DirectoryEntry(GetCurrentDomainPath()))
                {
                    using (DirectorySearcher ds = BuildUserSearcher(de))
                    {
                        //ds.Filter = "(objectClass=*)";
                        //SearchResult srSchema = ds.FindOne();
                        //var attributes = new List<string>();
                        //foreach (string attributeName in srSchema.Properties["allowedAttributes"])
                        //{
                        //    if (!attributeName.StartsWith("msExch"))
                        //    {
                        //        newUser.ldap += " " + attributeName + " ";
                        //    }
                        //}
                        //if (newUser.ldap == "")
                        //{
                        //    newUser.Error = "No Attributes Found.";
                        //}

                        // Set the filter to look for a specific user
                        ds.Filter = "(&(objectCategory=User)(objectClass=person)(samaccountname=" + userName + "))";
                        SearchResult sr = ds.FindOne();
                        //results = ds.FindAll();
                        //foreach (SearchResult sr in results)
                        //{
                        if (sr != null && sr.Properties.Count > 0) // && sr.Properties["name"][0].ToString().Contains("svccms"))
                        {
                            if (sr.Properties["name"].Count > 0)
                                newUser.Name = sr.Properties["name"][0].ToString();
                            if (sr.Properties["mail"].Count > 0)
                                newUser.EmailAddress = sr.Properties["mail"][0].ToString();
                            if (sr.Properties["sAMAccountName"].Count > 0)
                                newUser.SamAccountName = sr.Properties["sAMAccountName"][0].ToString();
                            if (sr.Properties["ipPhone"].Count > 0)
                                newUser.PhoneNumber = sr.Properties["ipPhone"][0].ToString();
                            if (sr.Properties["memberOf"].Count > 0)
                            {
                                foreach (string member in sr.Properties["memberOf"])
                                {
                                    newUser.GroupMembership.Add(member);
                                }
                            }
                            newUser.ldap = " UserName=" + newUser.Name + " DisplayName=" + newUser.DisplayName + " PhoneNumber=" + newUser.PhoneNumber + " EmailAddress=" + newUser.EmailAddress + " SamAccountName=" + newUser.SamAccountName + " MemberOf: ";
                            foreach (string member in newUser.GroupMembership)
                            {
                                newUser.ldap += member + ", ";
                                if (member.Contains("G.CMS.") == true)
                                {
                                    string group = member.Replace("CN=G.CMS.", "G.CMS.");
                                    int end = group.IndexOf(",");
                                    if (newUser.ldapGroup != "")
                                    {
                                        newUser.ldapGroup += ",";
                                    }
                                    newUser.ldapGroup += group.Substring(0, end);
                                    newUser.ldap += "GROUP= " + newUser.ldapGroup + ", ";
                                }
                                if (newUser.department == "")
                                {
                                    int startIndex = member.IndexOf("OU=") + 3;
                                    string partial = member.Substring(startIndex);
                                    int midIndex = partial.IndexOf(",");
                                    newUser.department = partial.Substring(0, midIndex).Replace("_GROUPS", "");
                                }
                                if (member.ToUpper().Contains("G.CMS.A_EMPLOYMENT") == true)
                                {
                                    newUser.CanSeeEmployeeContracts = true;
                                }
                                if (member.ToUpper().Contains("G.CMS.A_EDUCATION") == true)
                                {
                                    newUser.CanSeeEducationContracts = true;
                                }
                                if (member.ToUpper().Contains("G.CMS.A_LEASE") == true)
                                {
                                    newUser.CanSeeLeaseContracts = true;
                                }
                                if (member.ToUpper().Contains("G.CMS.A_ADMINISTRATIVE") == true)
                                {
                                    newUser.CanSeeAdministrativeContracts = true;
                                }
                                if (member.ToUpper().Contains("G.CMS.MC") == true || member.ToUpper().Contains("G.CMS.RC") == true || member.ToUpper().Contains("G.CMS.PP"))
                                {
                                    newUser.CanSeeePhoContracts = true;
                                }
                            }
                            if (newUser.ldapGroup == "")
                            {
                                newUser.ldapGroup = "G.CMS.Requestor";
                            }
                            if (newUser.department.Length == 0)
                            {
                                newUser.department = "";
                            }
                            //string[] props = new string[sr.Properties.PropertyNames.Count];
                            //sr.Properties.PropertyNames.CopyTo(props, 0);
                            //foreach (string prop in props)
                            //{
                            //    newUser.ldap += " " + prop + " ";
                            //}
                            //newUser.Error = "";
                            //break;
                            string path = HttpContext.Current.Server.MapPath("/Logs/ldapLog.txt");
                            if (File.Exists(path))
                            {
                                using (StreamWriter sw = File.AppendText(path))
                                {
                                    sw.WriteLine(newUser.Name + ": " + newUser.ldapGroup);
                                }
                            }
                        }
                        else
                        {
                            newUser.Error = "USER IS null";
                        }
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                newUser.Error = "GetUser: " + ex.Message;
            }
            return newUser;
        }

        private static string GetCurrentDomainPath()
        {
            try
            {
                using (DirectoryEntry de = new DirectoryEntry("LDAP://RootDSE"))
                {
                    return "LDAP://" + de.Properties["defaultNamingContext"][0].ToString();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("GetCurrentDomainPath:" + ex.Message);
            }
        }

        private static DirectorySearcher BuildUserSearcher(DirectoryEntry de)
        {
            try
            {
                using (DirectorySearcher ds = new DirectorySearcher(de))
                {
                    //ds.SearchScope = SearchScope.Base;
                    // ds.PropertiesToLoad.Add("allowedAttributes");
                    ds.PropertiesToLoad.Add("name");
                    ds.PropertiesToLoad.Add("displayName");
                    ds.PropertiesToLoad.Add("mail");
                    ds.PropertiesToLoad.Add("phone");
                    ds.PropertiesToLoad.Add("givenName");
                    ds.PropertiesToLoad.Add("sn");
                    ds.PropertiesToLoad.Add("adspath");
                    ds.PropertiesToLoad.Add("samaccountname");
                    ds.PropertiesToLoad.Add("userPrincipalName");
                    ds.PropertiesToLoad.Add("distinguishedName");
                    ds.PropertiesToLoad.Add("memberOf");
                    return ds;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("DirectorySearcher:" + ex.Message);
            }
        }

    }
}

public class AppUser
{
    public string Name { get; set; } = "";
    public string DisplayName { get; set; } = "";
    public string EmailAddress { get; set; } = "";
    public string SamAccountName { get; set; } = "";
    public string PhoneNumber { get; set; } = "";
    public List<string> GroupMembership { get; set; } = new List<string>();
    public string ldap { get; set; } = "";
    public string Error { get; set; } = "";
    public string ldapGroup { get; set; } = "";
    public string department { get; set; } = "";
    public Boolean CanSeeEmployeeContracts { get; set; } = false;
    public Boolean CanSeeEducationContracts { get; set; } = false;
    public Boolean CanSeeLeaseContracts { get; set; } = false;
    public Boolean CanSeeAdministrativeContracts { get; set; } = false;
    public Boolean CanSeeePhoContracts { get; set; } = false;
 }