﻿using System;
using System.IO;
using DevExpress.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Magnolia.app
{
    public partial class adminPartyRequests : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ((Master)Page.Master).SetHeader = "Admin Party Requests";
            ((Master)Page.Master).SetHelpURL = "admin.aspx";
            if (!Page.IsPostBack)
            {
                Session["dsDocuments"] = null;
            }
            if (Session["dsDocuments"] == null)
            {
                DataSet ds = DataAccess.GetDataSet("usp_getAllPendingDocuments", new string[] { }, new string[] { });
                Session["dsDocuments"] = ds;
            }
            ((GridViewDataColumn)gvPartyRequests.Columns["Documents"]).DataItemTemplate = new PendingTemplate();
        }

        private void Page_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError();
            Message.Alert(exc.Message);

            // Handle specific exception.
            if (exc is System.Web.HttpUnhandledException)
            {
                Message.Alert("An error occurred on this page. Please verify your information to resolve the issue.");
            }
            // Clear the error from the server.
            Server.ClearError();
        }

        protected void gvPartyRequests_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e)
        {
            if (e.VisibleIndex >= 0 && e.GetValue("requestStatusId").ToString() == "2")

                foreach (System.Web.UI.WebControls.TableCell cell in e.Row.Cells)
                {
                    cell.Attributes.Add("onclick", "event.cancelBubble = true");
                    cell.Attributes.Add("style", "background-color:lightgray");
                }
        }

        protected void dsPartyRequests_Updated(object sender, SqlDataSourceStatusEventArgs e)
        {
            if (e.AffectedRows > 0)
            {
                if (e.Command.Parameters["@requestStatusId"].Value.ToString() == "2")
                {
                    int index = Convert.ToInt32(gvPartyRequests.FindVisibleIndexByKeyValue(e.Command.Parameters["@requestId"].Value.ToString()));
                    string userEmail = gvPartyRequests.GetRowValues(index, "requestedByEmail").ToString();
                    string userName = gvPartyRequests.GetRowValues(index, "requestedBy").ToString();
                    string body = "The party request you submitted has been approved.";
                    body += "<br/><br/>You can now enter your new contract here: <a href='" + System.Configuration.ConfigurationManager.AppSettings["site"].ToString() + "/app/contractQuestionnaire.aspx'>New Request</a>";
                    Message.SendMail(userEmail, userName, "CMS New Party Request Approved", body, null);
                    if (e.Command.Parameters["@isPaid"].Value.ToString() == "1" || e.Command.Parameters["@isPaid"].Value.ToString().ToLower() == "true")
                    {
                        object[] row = (object[])gvPartyRequests.GetRowValuesByKeyValue(e.Command.Parameters["@requestId"].Value, new string[] { "businessName", "contactName", "address", "city", "state", "zip", "phone", "email" });
                        body = "A request to add a new paid Party/Vendor has been submitted.";
                        body += "<br/>Party/Vendor Name: " + row[0].ToString();
                        body += "<br/>Contact Name: " + row[1].ToString();
                        body += "<br/>Address: " + row[2].ToString();
                        body += "<br/>City: " + row[3].ToString();
                        body += "<br/>State: " + row[4].ToString();
                        body += "<br/>Zip Code: " + row[5].ToString();
                        body += "<br/>Phone Number: " + row[5].ToString();
                        body += "<br/>Email Address: " + row[6].ToString();
                        body += "<br/>Requestor: " + Session["UserName"].ToString();
                        body += "<br/>Requestor Email: <a href='mailto:" + Session["email"].ToString() + "'>" + Session["email"].ToString() + "</a>";
                        Message.SendMail("CMSNewParty@mrhc.org", "CMS New Party Group", "CMS - New Paid Party/Vendor Request", body);
                    }
                    if (e.Command.Parameters["@newPartyId"].Value != null && Convert.ToInt32(e.Command.Parameters["@newPartyId"].Value) > 0)
                    {
                        string pendingDirectory = System.Configuration.ConfigurationManager.AppSettings["documentPath"] + "PARTY\\PENDING\\" + e.Command.Parameters["@requestId"].Value.ToString();
                        string partyDirectory = System.Configuration.ConfigurationManager.AppSettings["documentPath"] + "PARTY\\" + e.Command.Parameters["@newPartyId"].Value.ToString();
                        if (Directory.Exists(pendingDirectory))
                        {
                            string[] files = Directory.GetFiles(pendingDirectory);
                            if (files.Length > 0)
                            {
                                if (!Directory.Exists(partyDirectory))
                                {
                                    Directory.CreateDirectory(partyDirectory);
                                }
                                for (int i = 0; i < files.Length; i++)
                                {
                                    File.Copy(files[i], partyDirectory + "\\" + Path.GetFileName(files[i]));
                                    //File.Move(files[i], partyDirectory + Path.GetFileName(files[i]));
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (e.Command.Parameters["@requestStatusId"].Value.ToString() == "3")
                    {
                        int index = Convert.ToInt32(gvPartyRequests.FindVisibleIndexByKeyValue(e.Command.Parameters["@requestId"].Value.ToString()));
                        string userEmail = gvPartyRequests.GetRowValues(index, "requestedByEmail").ToString();
                        string userName = gvPartyRequests.GetRowValues(index, "requestedBy").ToString();
                        string body = "The party request you submitted has been denied.";
                        body += "<br/><br/>Please contact a CMS Administrator for further details.";
                        Message.SendMail(userEmail, userName, "CMS New Party Request Denied", body, null);
                    }
                }
            }
        }
    }
}

public class PendingTemplate : ITemplate
{
    public void InstantiateIn(Control container)
    {
        try
        {

            GridViewDataItemTemplateContainer gcontainer = (GridViewDataItemTemplateContainer)container;
            int key = Convert.ToInt32(DataBinder.Eval(gcontainer.DataItem, "requestId"));
            DataSet ds = (DataSet)System.Web.HttpContext.Current.Session["dsDocuments"];
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                string prevURL = "";
                DataView dv = ds.Tables[0].DefaultView;
                dv.RowFilter = "requestId = " + key.ToString();
                DataTable dt = dv.ToTable();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string thisURL = "";
                    if (dt.Rows[i]["url"] != null)
                    {
                        thisURL = dt.Rows[i]["url"].ToString();
                    }
                    if (prevURL != thisURL) 
                    {
                        ASPxHyperLink hl = new ASPxHyperLink();
                        hl.NavigateUrl = @"DocumentViewer.aspx?documentId=" + dt.Rows[i]["documentId"] + "&type=pending"; 
                        hl.ImageHeight = Unit.Pixel(30);
                        hl.ImageWidth = Unit.Pixel(30);
                        hl.ToolTip = dt.Rows[i]["fileName"].ToString() + ' ' + dt.Rows[i]["uploadDate"].ToString();
                        hl.Target = "_blank";
                        switch (dt.Rows[i]["documentTypeId"])
                        {
                            case 1:
                                hl.ImageUrl = "../images/Word.png";
                                break;
                            case 2:
                                hl.ImageUrl = "../images/PDF.png";
                                break;
                            case 3:
                                hl.ImageUrl = "../images/Excel.png";
                                break;
                            default:
                                hl.ImageUrl = "../images/Other.png";
                                break;
                        }
                        gcontainer.Controls.Add(hl);
                    }
                    prevURL = thisURL;
                }
            }
        }
        catch (Exception ex)
        {
            string x = ex.Message;
        }
    }
}
