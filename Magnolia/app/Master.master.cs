﻿using System;
using DevExpress.Web;
using System.Web.UI;
using System.Collections.Generic;
using System.Web.Security;

namespace Magnolia.app
{
    public partial class Master : System.Web.UI.MasterPage
    {
        //System.Data.DataSet ds;

        protected void Page_Init(object sender, EventArgs e)
        {
            if(Session.Keys.Count == 0)
            {
                string url = Request.Url.AbsoluteUri;
                FormsAuthentication.SignOut();
                if (Page.IsCallback){
                    ASPxWebControl.RedirectOnCallback("../login.aspx?ReturnURL=" + url);
                }
                else
                {
                    Response.Redirect("../login.aspx?ReturnURL=" + url);
                }
            }
            else
            {
                if (Session["isAdmin"] != null && Session["isAdmin"] != System.DBNull.Value && Convert.ToBoolean(Session["isAdmin"]) == true)
                {
                    liAdmin.Visible = true;
                    //liUser.Visible = false;
                }
                else
                {
                    liAdmin.Visible = false;
                    //liUser.Visible = true;
                }
                //spanUserName.InnerText = Session["UserName"].ToString() ;
                userName.InnerText = Session["UserName"].ToString();
                if (!IsPostBack)
                {
                    //List<ContractTotalsData> contractStats = GetInspectionTotals();
                    //Top Notification Number
                    //if (Convert.ToInt16(contractStats[0].totalCurrentUser) == 0)
                    //{
                    //    outstandingContracts.Visible = false;
                    //}
                    //else {
                    //    outstandingContracts.Visible = true;
                    //    outstandingContracts.InnerHtml = contractStats[0].totalCurrentUser;
                    //}
                    ////All Totals
                    //lblPercent.InnerHtml = contractStats[0].completionPercent;
                    //progressPercent.Attributes["aria-valuenow"] = contractStats[0].completionPercent;
                    //progressPercent.Style.Add("width", contractStats[0].completionPercent);
                    //countContracts.InnerHtml = "Total Percent Completion: " + contractStats[0].completionPercent;
                    //tasks.InnerHtml = contractStats[0].incompletionCount;
                    ////Current User Totals
                    //lblMyPercent.InnerHtml = contractStats[0].percentCurrentUser;
                    //myProgressPercent.Attributes["aria-valuenow"] = contractStats[0].percentCurrentUser;
                    //myProgressPercent.Style.Add("width", contractStats[0].percentCurrentUser);
                    //myCountContracts.InnerHtml = "My Percent Completion: " + contractStats[0].percentCurrentUser;
                    //mytasks.InnerHtml = contractStats[0].incompleteCurrentUser;
                    //string url = Page.ToString().Replace("ASP.", "").Replace("_", ".").Replace("app.", "");
                    //hlHelp.ClientSideEvents.Click = @"function(s,e){{showHelp();}}";
                    //hlHelp.NavigateUrl = "";
                    //popHelp.ContentUrl = String.Format(@"/Help/{0}", url); //String.Format(@"function (s, e) {{ window.open('/Help/{0}', 'help', 'toolbar=no,location=no,addressbar=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=500,height=500'); }}", url);
                }
            }
        }

        public string SetHeader
        {
            get { return lblHeader.Text; }
            set{ lblHeader.Text = value; }
        }

        public string SetHelpURL
        {
            get { return hlHelp.NavigateUrl; }
            set {
                hlHelp.ClientSideEvents.Click = String.Format(@"function (s, e) {{ window.open('../Help/{0}', 'help', 'toolbar=no,location=no,addressbar=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=500,height=500'); }}", value);
                hlHelp.NavigateUrl = "";
            }
        }

        public Boolean SetHelpVisible
        {
            get { return hlHelp.Visible; }
            set { hlHelp.Visible = value; }
        }

        protected void btnLogOff_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
            //FormsAuthentication.RedirectToLoginPage();
           Response.Redirect("../login.aspx");
        }

        public List<ContractTotalsData> GetInspectionTotals()
        {

            List<ContractTotalsData> contractData = new List<ContractTotalsData>();
            //try
            //{
            //    if (Session["dsTotals"] == null)
            //    {
            //        //ds = DataAccess.GetDataSet2("usp_getInspectionFormCompletionTotals", new string[] { }, new string[] { });
            //        Session["dsTotals"] = ds;
            //    }
            //    else
            //    {
            //        ds = (System.Data.DataSet)Session["dsTotals"];
            //    }
            //    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            //    {
            //        int totalIncomplete = ds.Tables[0].Select("completion_percent<100").Length;
            //        int totalComplete = ds.Tables[0].Select("completion_percent=100").Length;
            //        int totalInspections = ds.Tables[0].Rows.Count;
            //        double totalFieldsComplete = Convert.ToDouble(ds.Tables[0].Compute("sum(total_completed)", ""));
            //        int totalFields = Convert.ToInt16(ds.Tables[0].Compute("sum(total_columns)", ""));
            //        double percent = 0;
            //        if (totalFields > 0)
            //        {
            //            percent = (Math.Round(totalFieldsComplete / totalFields, 2) * 100);
            //        }
            //        else
            //        {
            //            percent = 0;
            //        }
            //        string fullName = Session["UserName"].ToString();
            //        int incompleteUser = ds.Tables[0].Select("completion_percent<100 and fullName='" + fullName + "'").Length;
            //        int completeUser = ds.Tables[0].Select("completion_percent=100 and fullName='" + fullName + "'").Length;
            //        int totalUser = 0;
            //        totalUser = ds.Tables[0].Select("fullName='" + fullName + "'").Length;
            //        double userPercent = 0;
            //        double totalUserFields = Convert.ToDouble(ds.Tables[0].Compute("sum(total_columns)", "fullName='" + fullName + "'"));
            //        double totalUserFieldsComplete = Convert.ToDouble(ds.Tables[0].Compute("sum(total_completed)", "fullName='" + fullName + "'"));
            //        if (!DBNull.Value.Equals(totalUserFields))
            //        {
            //            if (totalUserFields > 0)
            //            {
            //                userPercent = (Math.Round(totalUserFieldsComplete / totalUserFields, 2) * 100);
            //            }
            //            else
            //            {
            //                userPercent = 0;
            //            }
            //        }
            //        else
            //        {
            //            userPercent = 100;
            //        }
            //        contractData.Add(new ContractTotalsData()
            //        {
            //            completionPercent = percent.ToString() + "%",
            //            completionCount = totalComplete.ToString(),
            //            incompletionCount = totalIncomplete.ToString(),
            //            totalInspectionCount = ds.Tables[0].Rows.Count.ToString(),
            //            completeCurrentUser = completeUser.ToString(),
            //            incompleteCurrentUser = incompleteUser.ToString(),
            //            totalCurrentUser = totalUser.ToString(),
            //            percentCurrentUser = userPercent.ToString() + "%"
            //        });
            //    }
            //    return contractData;
            //}
            //catch (Exception ex)
            //{
            //    contractData.Add(new ContractTotalsData()
            //    {
            //        completionPercent = "0%",
            //        completionCount = "0",
            //        incompletionCount = "0",
            //        totalInspectionCount = "0",
            //        completeCurrentUser = "0",
            //        incompleteCurrentUser = "0",
            //        totalCurrentUser = "0",
            //        percentCurrentUser = "0"
            //    });
                return contractData;
            //}
        }

    }
}