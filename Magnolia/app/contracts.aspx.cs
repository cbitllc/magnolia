﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;

namespace Magnolia.app
{
    public partial class contracts : System.Web.UI.Page
    {

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                if (Session["myContracts"] != null && Session["myContracts"].ToString() == "1")
                {
                    gvContracts.FilterExpression = "1 in (myContracts, mySubmittals)";
                    cbMine.Checked = true;
                }
                else
                {
                    gvContracts.FilterExpression = "";
                    cbMine.Checked = false;
                }
                gvContracts.DataSource = dsContracts;
                gvContracts.DataBind();
                if (!Page.IsPostBack)
                {
                    Session["dsDocuments"] = null;
                }
                if (Session["dsDocuments"] == null)
                {
                    DataSet ds = DataAccess.GetDataSet("usp_getAllDocuments", new string[] { "@folder" }, new string[] { "CONTRACT" });
                    Session["dsDocuments"] = ds;
                }
            ((GridViewDataColumn)gvContracts.Columns["Documents"]).DataItemTemplate = new CustomTemplate();
            }
            catch(Exception ex)
            {
                Message.LogError("contracts", "Page_Init", Session["UserName"].ToString(), ex.Message);
                Message.Alert(ex.Message);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetHeader = "Available Contracts";
                ((Master)Page.Master).SetHelpURL = "contracts.aspx";
            }
            catch (Exception ex)
            {
                Message.LogError("contracts", "Page_Load", Session["UserName"].ToString(), ex.Message);
                Message.Alert(ex.Message);
            }
        }

        //protected void dsContracts_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        //{
        //    lblQuery.Text = GetQuery(e.Command.CommandText, e.Command.Parameters);
        //}

        //private string GetQuery(string CommandQuery, System.Data.Common.DbParameterCollection parmCollection)
        //{
        //    string query = CommandQuery;
        //    string parms = "";
        //    foreach (System.Data.SqlClient.SqlParameter parm in parmCollection)
        //    {
        //        parms += parm.ParameterName + " = ";
        //        if (parm.Value == null)
        //        {
        //            parms += "null,";

        //        }
        //        else
        //        {
        //            if (parm.DbType == System.Data.DbType.String || parm.DbType == System.Data.DbType.DateTime)
        //            {
        //                parms += "'" + parm.Value.ToString() + "',";
        //            }
        //            else
        //            {
        //                parms += parm.Value.ToString() + ",";
        //            }
        //        }
        //    }
        //    query = query + " " + parms.Substring(0, parms.Length - 1);
        //    return query;
        //}

        //protected void dsContracts_Selected(object sender, SqlDataSourceStatusEventArgs e)
        //{
        //    if (e.Exception != null)
        //    {
        //        sqlErrorMessage.Text = e.Exception.Message;
        //        sqlErrorMessage.ClientVisible = true;
        //        lblQuery.ClientVisible = true;
        //        e.ExceptionHandled = true;
        //        ClientScript.RegisterClientScriptBlock(Page.GetType(), "error", "<script>alert('Error getting Contract.');window.location='contracts.aspx';</script>");
        //    }
        //}

        protected void cbMine_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbMine.Checked == true)
                {
                    gvContracts.FilterExpression = "1 in (myContracts, mySubmittals)";
                    Session["myContracts"] = "1";
                }
                else
                {
                    gvContracts.FilterExpression = "";
                    Session["myContracts"] = "0";
                }
                gvContracts.DataBind();
            }
            catch (Exception ex)
            {
                Message.LogError("contracts", "cbMine_CheckedChanged", Session["UserName"].ToString(), ex.Message);
                Message.Alert(ex.Message);
            }
        }

        //protected void gvContracts_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        //{
        //    //if (Convert.ToBoolean(Session["isAdmin"]) == false)
        //    //{
        //    //    if e.Row.Cells[]
        //}

        protected void gvContracts_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            try
            {
                if (Convert.ToBoolean(Session["isAdmin"]) == false)
                {
                    if (e.DataColumn.FieldName == "entryUserId")
                    {
                        DevExpress.Web.Internal.HyperLinkDisplayControl hl = (DevExpress.Web.Internal.HyperLinkDisplayControl)e.Cell.Controls[0];
                        DevExpress.Web.ASPxLabel lbl = new DevExpress.Web.ASPxLabel();
                        lbl.Text = hl.Text;
                        e.Cell.Controls.Clear();
                        e.Cell.Controls.Add(lbl);
                    }
                }
            }
            catch (Exception ex)
            {
                Message.LogError("contracts", "HtmlDataCellPrepared", Session["UserName"].ToString(), ex.Message);
                Message.Alert(ex.Message);
            }
        }

        protected void gvContracts_SearchPanelEditorInitialize(object sender, ASPxGridViewSearchPanelEditorEventArgs e)
        {
            try
            {
                e.Editor.Width = Unit.Pixel(900);
            }
            catch (Exception ex)
            {
                Message.LogError("contracts", "SearchPanelEditorInitialize", Session["UserName"].ToString(), ex.Message);
                Message.Alert(ex.Message);
            }
        }

        private void Page_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError();
            Message.Alert(exc.Message);

            // Handle specific exception.
            if (exc is System.Web.HttpUnhandledException)
            {
                Message.LogError("contracts", "Page_Error", Session["UserID"].ToString(), exc.Message);
                Message.Alert("An error occurred on this page. Please verify your information to resolve the issue.");
            }
            // Clear the error from the server.
            Server.ClearError();
        }

        protected void dsContracts_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            try {
                e.Command.CommandTimeout = 0;
            }
            catch (Exception ex)
            {
                Message.LogError("contracts", "dsContracts_Selecting", Session["UserName"].ToString(), ex.Message);
                Message.Alert(ex.Message);
            }
        }

        protected void dsContracts_Selected(object sender, SqlDataSourceStatusEventArgs e)
        {
            try { }
            catch (Exception ex)
            {
                Message.LogError("contracts", "dsContracts_Selected", Session["UserName"].ToString(), ex.Message);
                Message.Alert(ex.Message);
            }
        }

        protected void gvContracts_HeaderFilterFillItems(object sender, ASPxGridViewHeaderFilterEventArgs e)
        {
            if (e.Column.FieldName == "estimatedTotalCost")
            {
                e.Values.Clear();
                if (e.Column.SettingsHeaderFilter.Mode == GridHeaderFilterMode.List)
                    e.AddShowAll();
                int step = 10000;
                for (int i = 0; i < 10; i++)
                {
                    double start = step * i;
                    double end = start + step - 0.01;
                    e.AddValue(string.Format("{0:c} to {1:c}", start, end), "", string.Format("[estimatedTotalCost] >= {0} and [estimatedTotalCost] <= {1}", start, end));
                }
                e.AddValue(string.Format("> {0:c}", 100000), "", "[estimatedTotalCost] > 100000");
            }
        }

        //protected void cbInactive_CheckedChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (cbInactive.Checked == true)
        //        {
        //            dsContracts.SelectParameters["inactive"].DefaultValue = "1";
        //        }
        //        else
        //        {
        //            dsContracts.SelectParameters["inactive"].DefaultValue = "0";
        //        }
        //        gvContracts.DataBind();
        //    }
        //    catch (Exception ex)
        //    {
        //        Message.LogError("contracts", "cbInactive_CheckedChanged", Session["UserName"].ToString(), ex.Message);
        //        Message.Alert(ex.Message);
        //    }
        //}
    }

    public class Document
    {
        public Int32 ID { get; set; }
        public string Path { get; set; }
        public string FileName { get; set; }
        public Int32 Type { get; set; }
        public Int32 ContractID { get; set; }
    }

    public class CustomTemplate : ITemplate
    {
        public void InstantiateIn(Control container)
        {
            try
            {

                GridViewDataItemTemplateContainer gcontainer = (GridViewDataItemTemplateContainer)container;
                int key = Convert.ToInt32(DataBinder.Eval(gcontainer.DataItem, "contractId"));
                string masterContractNumber = "";
                if (DataBinder.Eval(gcontainer.DataItem, "masterContractNumber") != null)
                {
                    masterContractNumber = DataBinder.Eval(gcontainer.DataItem, "masterContractNumber").ToString();
                }
                DataSet ds = (DataSet)System.Web.HttpContext.Current.Session["dsDocuments"];
                if (ds != null && ds.Tables.Count>0 && ds.Tables[0].Rows.Count>0)
                {
                    string prevURL = "";
                    //if (ds.Tables[0].Rows[0]["url"]!= null)
                    //{
                    //    prevURL = ds.Tables[0].Rows[0]["url"].ToString();
                    //}
                    DataView dv = ds.Tables[0].DefaultView;
                    dv.RowFilter = "contractId = " + key.ToString();
                    DataTable dt = dv.ToTable();
                    for(int i = 0; i < dt.Rows.Count; i++) {
                        string thisMasterNumber = "";
                        if (dt.Rows[i]["masterContractNumber"] != null)
                        {
                            thisMasterNumber = dt.Rows[i]["masterContractNumber"].ToString();
                        }
                        string thisURL = "";
                        if (dt.Rows[i]["url"] != null)
                        {
                            thisURL = dt.Rows[i]["url"].ToString();
                        }
                        if(prevURL != thisURL) // || (thisMasterNumber == masterContractNumber && thisMasterNumber != "" && masterContractNumber != "")))
                        {
                            ASPxHyperLink hl = new ASPxHyperLink();
                            // hl.NavigateUrl = @"DocumentViewer.aspx?url=" + System.Web.HttpContext.Current.Server.UrlEncode(dt.Rows[i]["url"].ToString());
                            hl.NavigateUrl = @"DocumentViewer.aspx?documentId=" + dt.Rows[i]["documentId"]; //System.Web.HttpContext.Current.Server.UrlEncode(dt.Rows[i]["url"].ToString());
                            hl.ImageHeight = Unit.Pixel(30);
                            hl.ImageWidth = Unit.Pixel(30);
                            hl.ToolTip = dt.Rows[i]["fileName"].ToString() + ' ' + dt.Rows[i]["uploadDate"].ToString();
                            hl.Target = "_blank";
                            switch (dt.Rows[i]["documentTypeId"])
                            {
                                case 1:
                                    hl.ImageUrl = "../images/Word.png";
                                    break;
                                case 2:
                                    hl.ImageUrl = "../images/PDF.png";
                                    break;
                                case 3:
                                    hl.ImageUrl = "../images/Excel.png";
                                    break;
                                case 6:
                                    hl.ImageUrl = "../images/JPG.png";
                                    break;
                                case 7:
                                    hl.ImageUrl = "../images/HTML.png";
                                    break;
                                default:
                                    hl.ImageUrl = "../images/Other.png";
                                    break;
                            }
                            gcontainer.Controls.Add(hl);
                        }
                        prevURL = thisURL;
                    }
                }
            }
            catch (Exception ex)
            {
                string x = ex.Message;
            }
        }
    }

    }