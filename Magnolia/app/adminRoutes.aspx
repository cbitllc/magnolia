﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="adminRoutes.aspx.cs" Inherits="Magnolia.app.adminRouting" MasterPageFile="~/app/Master.master" %>
<%@ Register Assembly="DevExpress.Web.ASPxDiagram.v20.2, Version=20.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxDiagram" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v20.2, Version=20.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraDiagram.v20.2, Version=20.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraDiagram" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Diagram.v20.2.Core, Version=20.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Diagram" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<dx:ASPxCallbackPanel ID="cbRoutes" runat="server" ClientInstanceName="cbRoutes" OnCallback="cbRoutes_Callback">
<PanelCollection><dx:PanelContent>
    <br />
    <style>
        input {text-transform:uppercase;}
        textarea {text-transform:uppercase;}
        span {text-transform:uppercase;}
        .spanTitle{text-transform:capitalize;}
        .gridHeader {  
            background-color: white;  
            color:black;
        }  
        .gridHeader a {  
            color: black;  
        }  
    </style>
    <script type="text/javascript">
        function AdjustHeight(s, e) {
            s.SetHeight(window.innerHeight * .65);
            s.SetWidth(window.innerWidth * .35);
        }
    </script>
    <div style="max-width:50%; top:0px; position:relative; overflow:auto;">
        <div style="display:flex; flex-direction:row; overflow:auto;">
            <dx:ASPxLabel ID="lblRouting" runat="server" Text="Select Route:" Font-Bold="true"></dx:ASPxLabel>
            &nbsp;&nbsp;&nbsp;
            <dx:ASPxComboBox ID="ddlRoutes" ClientInstanceName="ddlRoutes" runat="server" DataSourceID="dsRoutes" TextField="routingName" ValueField="routingId" OnSelectedIndexChanged="ddlRoutes_SelectedIndexChanged" AutoPostBack="true" EnableViewState="true">
            </dx:ASPxComboBox>
            &nbsp;&nbsp;&nbsp;
            <dx:ASPxButton ID="btnNewRoute" runat="server"  Text="New Route" AutoPostBack="false">
                <ClientSideEvents Click="function(s,e){popNewRoute.Show();txtName.Focus()}" />
            </dx:ASPxButton>
            &nbsp;&nbsp;&nbsp;
            <dx:ASPxButton ID="btnDeleteRoute" runat="server"  Text="Delete Route" OnClick="btnDeleteRoute_Click">
                <ClientSideEvents Click="function(s,e){if(confirm('Are you sure you want to delete this route?')==false){e.processOnServer=false;}}" />
            </dx:ASPxButton>
            &nbsp;&nbsp;&nbsp;
            <dx:ASPxButton ID="btnRefresh" runat="server" Text="Refresh" OnClick="btnRefresh_Click"></dx:ASPxButton>
        </div>
        <br />
        <div style="display:flex; flex-direction:row;overflow:auto;">
            <dx:ASPxLabel ID="lblRouteDescription" runat="server" Text="Route Description:" Font-Bold="true"></dx:ASPxLabel>
            &nbsp;&nbsp;&nbsp;
            <dx:ASPxMemo ID="txtRouteDescription" runat="server" Width="250" Rows="3" MaxLength="2000"></dx:ASPxMemo>
            &nbsp;&nbsp;&nbsp;
            <div>
                <dx:ASPxButton ID="btnSaveDescription" runat="server" Text="Save Changes" OnClick="btnSaveDescription_Click"></dx:ASPxButton>
                <br /><span style="color:red">*Saves the route description</span>
            </div>
        </div>
        <br /><br />
        <asp:SqlDataSource ID="dsRoutes" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
            SelectCommand="usp_getRoutes" SelectCommandType="StoredProcedure"
            DeleteCommand="usp_deleteRoute" DeleteCommandType="StoredProcedure" OnDeleting="dsRoutes_Deleting"
            UpdateCommand="usp_updateRouteDescription" UpdateCommandType="StoredProcedure">
            <SelectParameters>
                <asp:Parameter Name="includeInactive" DefaultValue="1" />
            </SelectParameters>
            <DeleteParameters>
                <asp:ControlParameter ControlID="ddlRoutes" PropertyName="Value" Name="routingId" />
                <asp:Parameter Name="count" Direction="Output" DbType="Int16" Size="4" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:ControlParameter ControlID="ddlRoutes" PropertyName="Value" Name="routingId" />
                <asp:ControlParameter ControlID="txtRouteDescription" PropertyName="Text" Name="description" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <dx:ASPxGridView ID="gvRoutes" ClientInstanceName="gvRoutes" runat="server" DataSourceID="dsRoute" Width="80%" KeyFieldName="rid" OnInit="gvRoutes_Init" OnRowUpdating="gvRoutes_RowUpdating" Settings-VerticalScrollBarMode="Auto">
            <ClientSideEvents Init="AdjustHeight" EndCallback="function(s,e){cbRoutes.PerformCallback('Delete');}" />
            <Columns>
                <dx:GridViewCommandColumn ShowEditButton="true" CellStyle-HorizontalAlign="Left">
                    <HeaderTemplate>
                        <a href="javascript:gvRoutes.AddNewRow()" style="text-decoration:underline;">New</a>
                    </HeaderTemplate>
                </dx:GridViewCommandColumn>
                <dx:GridViewCommandColumn ShowDeleteButton="true" CellStyle-HorizontalAlign="Left" Caption=" "></dx:GridViewCommandColumn>
                <dx:GridViewDataSpinEditColumn FieldName="routingSequence" Caption="Sequence" PropertiesSpinEdit-MinValue="1" Visible="false"></dx:GridViewDataSpinEditColumn>
                <dx:GridViewDataComboBoxColumn FieldName="roleId" Caption="Role">
                    <PropertiesComboBox DataSourceID="dsRoleNames" ValueField="roleId" TextField="roleName"></PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
                <dx:GridViewDataComboBoxColumn FieldName="parentId" Caption="Parent Role" PropertiesComboBox-AllowNull="true" PropertiesComboBox-ConvertEmptyStringToNull="true">
                    <PropertiesComboBox DataSourceID="dsRoleNames" ValueField="roleId" TextField="roleName" AllowNull="true" NullText=""></PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
                <dx:GridViewDataColumn FieldName="routingId" Visible="false"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="roleName" Visible="false"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="rid" Visible="false"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="routingSequence" Visible="false"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="description" Visible="false"></dx:GridViewDataColumn>
            </Columns>
            <SettingsPager Mode="ShowAllRecords"></SettingsPager>
            <SettingsBehavior ConfirmDelete="true" />
            <SettingsDataSecurity AllowDelete="true" AllowEdit="true" AllowInsert="true" />
            <SettingsEditing Mode="PopupEditForm" UseFormLayout="true" EditFormColumnCount="1"></SettingsEditing>
            <SettingsPager Mode="ShowPager" PageSize="50"></SettingsPager>
            <SettingsPopup>
                <HeaderFilter MinHeight="140px"></HeaderFilter>
                <EditForm HorizontalAlign="Center" VerticalAlign="Middle" Width="400px"></EditForm>
            </SettingsPopup>
            <SettingsCommandButton NewButton-Text="Add" EditButton-Text="Update"></SettingsCommandButton>
            <SettingsText ConfirmDelete="Are you sure you want to delete this sequence?" />
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="dsRoleNames" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
            SelectCommand="usp_getUserRoles" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        <asp:SqlDataSource ID="dsRoute" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
            DeleteCommand="usp_deleteRoutingSequenceById" DeleteCommandType="StoredProcedure" OnDeleted="dsRoute_Deleted"
            InsertCommand="usp_AddRoutingSequence" InsertCommandType="StoredProcedure" OnInserting="dsRoute_Inserting"  
            SelectCommand="usp_getRoute" SelectCommandType="StoredProcedure"
            UpdateCommand="usp_updateRouteById" UpdateCommandType="StoredProcedure" CancelSelectOnNullParameter="false">
            <SelectParameters>
                <asp:Parameter Name="routingId" />
            </SelectParameters>
            <DeleteParameters>
                <asp:Parameter Name="rid" />
            </DeleteParameters>
            <InsertParameters>
                <asp:ControlParameter ControlID="ddlRoutes" PropertyName="Value" Name="routingId" />
                <asp:Parameter Name="parentId" DefaultValue="Null" ConvertEmptyStringToNull="true" />
                <asp:Parameter Name="roleId" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="rid" />
                <asp:Parameter Name="parentId" />
                <asp:Parameter Name="roleId" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </div>
    <div style="width:40%; top:170px; height:80%; left:60%; position:fixed;overflow:scroll">
        <dx:ASPxDiagram ID="Diagram" ClientInstanceName="Diagram" runat="server" NodeDataSourceID="SqlDataSource1" Width="100%" Height="100%" AutoZoom="FitToWidth" SettingsToolbox-Visibility="Collapsed">
            <ClientSideEvents EndCallback="function (s,e){ gvRoutes.Refresh(); }" /><%----%>
            <Mappings>
                <Node Key="roleId" Text="roleName" ParentKey="parentId" />
            </Mappings>
        </dx:ASPxDiagram>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
            SelectCommand="usp_getRoute" SelectCommandType="StoredProcedure"
            DeleteCommand="usp_deleteRoutingSequence" DeleteCommandType="StoredProcedure" 
            UpdateCommand="usp_updateRoute" UpdateCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="ddlRoutes" PropertyName="Value" Name="routingId" />
            </SelectParameters>
            <DeleteParameters>
                <asp:Parameter Name="roleId" />
                <asp:ControlParameter ControlID="ddlRoutes" PropertyName="Value" Name="routingId" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:ControlParameter ControlID="ddlRoutes" PropertyName="Value" Name="routingId" />
                <asp:Parameter Name="parentId" />
                <asp:Parameter Name="roleId" />
                <asp:Parameter Name="roleName" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </div>
    <dx:ASPxPopupControl ID="popNewRoute" ClientInstanceName="popNewRoute" Modal="true" Width="600px" Height="250px" runat="server" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" HeaderText="Add New Route" ShowCloseButton="true">
        <SettingsAdaptivity Mode="Always" />
        <ContentCollection>
            <dx:PopupControlContentControl Width="100%">
                <table style="width:100%" align="center">
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr>
                        <td style="width:50%"><dx:ASPxLabel ID="lblName" runat="server" Text="New Route Name:" Font-Bold="true"></dx:ASPxLabel></td>
                        <td>
                            <dx:ASPxTextBox ID="txtName" ClientInstanceName="txtName" runat="server">
                                <ValidationSettings RequiredField-IsRequired="true"></ValidationSettings>
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr>
                        <td><dx:ASPxLabel ID="lblClone" runat="server" Text="Select Route to Clone or<br/>'Add All Steps' for a Custom Route: " EncodeHtml="false" Font-Bold="true"></dx:ASPxLabel></td>
                        <td>
                            <dx:ASPxComboBox ID="ddlCloneRoutes" runat="server" DataSourceID="dsCloneRoute" ValueField="routingId" TextField="routingName" AutoPostBack="true" ValidationSettings-RequiredField-IsRequired="true"></dx:ASPxComboBox>
                            <asp:SqlDataSource id="dsCloneRoute" runat="server"  ConnectionString="<%$ ConnectionStrings:cmsConn %>"
                                SelectCommand="usp_getRoutesAdmin" SelectCommandType="StoredProcedure">
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr>
                        <td style="text-align:center" colspan="2">
                            <dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" AutoPostBack="false">
                                <ClientSideEvents Click="function(s,e){popNewRoute.Hide();}" />
                            </dx:ASPxButton>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <dx:ASPxButton ID="btnCloneRoute" runat="server" OnClick="btnCloneRoute_Click" Text="Add Route"></dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</dx:PanelContent></PanelCollection></dx:ASPxCallbackPanel>
</asp:Content>