﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using System.Data.SqlClient;

namespace Magnolia.app
{
    public partial class adminContracts : System.Web.UI.Page
    {
        List<Document> ContractDocuments = new List<Document>();

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Session["dsDocuments"] = null;
            }
            if (Session["dsDocuments"] == null)
            {
                using (SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cmsConn"].ToString()))
                {
                    string query = "usp_getAllDocuments";
                    DataSet ds = null;
                    using (SqlCommand myCommand = new SqlCommand(query, myConnection))
                    {
                        myCommand.CommandType = CommandType.StoredProcedure;
                        myCommand.CommandTimeout = 0;
                        using (SqlDataAdapter mAdapter = new SqlDataAdapter(myCommand))
                        {
                            ds = new DataSet();
                            myConnection.Open();
                            mAdapter.Fill(ds);
                        }
                    }
                    myConnection.Close();
                    Session["dsDocuments"] = ds;
                }
            }
            ((GridViewDataColumn)gvContracts.Columns["Documents"]).DataItemTemplate = new CustomTemplate();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Master.SetHeader = "Admin Contract Report";
            Master.SetHelpVisible = false;
            if (!Page.IsPostBack)
            {
                if (Session["myContracts"] != null && Session["myContracts"].ToString() == "1")
                {
                    gvContracts.FilterExpression = "1 in (myContracts, mySubmittals)";
                    cbMine.Checked = true;
                }
                else
                {
                    gvContracts.FilterExpression = "";
                    cbMine.Checked = false;
                }
                gvContracts.DataBind();
            }
        }

        protected void gvContracts_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (Convert.ToBoolean(Session["isAdmin"]) == false)
            {
                if (e.DataColumn.FieldName == "entryUserId")
                {
                    DevExpress.Web.Internal.HyperLinkDisplayControl hl = (DevExpress.Web.Internal.HyperLinkDisplayControl)e.Cell.Controls[0];
                    DevExpress.Web.ASPxLabel lbl = new DevExpress.Web.ASPxLabel();
                    lbl.Text = hl.Text;
                    e.Cell.Controls.Clear();
                    e.Cell.Controls.Add(lbl);
                }
            }
        }

        protected void cbMine_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbMine.Checked == true)
                {
                    gvContracts.FilterExpression = "1 in (myContracts, mySubmittals)";
                    Session["myContracts"] = "1";
                }
                else
                {
                    gvContracts.FilterExpression = "";
                    Session["myContracts"] = "0";
                }
                gvContracts.DataBind();
            }
            catch (Exception ex)
            {
                Message.LogError("contracts", "cbMine_CheckedChanged", Session["UserName"].ToString(), ex.Message);
                Message.Alert(ex.Message);
            }
        }

        protected void gvContracts_HeaderFilterFillItems(object sender, ASPxGridViewHeaderFilterEventArgs e)
        {
            if (e.Column.FieldName == "estimatedTotalCost")
            {
                e.Values.Clear();
                if (e.Column.SettingsHeaderFilter.Mode == GridHeaderFilterMode.List)
                    e.AddShowAll();
                int step = 10000;
                for (int i = 0; i < 10; i++)
                {
                    double start = step * i;
                    double end = start + step - 0.01;
                    e.AddValue(string.Format("{0:c} to {1:c}", start, end), "", string.Format("[estimatedTotalCost] >= {0} and [estimatedTotalCost] <= {1}", start, end));
                }
                e.AddValue(string.Format("> {0:c}", 100000), "", "[estimatedTotalCost] > 100000");
            }
        }

        protected void gvContracts_SearchPanelEditorInitialize(object sender, ASPxGridViewSearchPanelEditorEventArgs e)
        {
            e.Editor.Width = Unit.Pixel(900);
        }

        protected void dsContracts_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.CommandTimeout = 0;
        }

        private void Page_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError();
            Message.Alert(exc.Message);
            if (exc is System.Web.HttpUnhandledException)
            {
                Message.LogError("default", "Page_Error", Session["UserID"].ToString(), exc.Message);
                Message.Alert("An error occurred on this page. " + exc.Message);
            }
            Server.ClearError();
        }
    }
}