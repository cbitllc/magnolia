﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Magnolia.app
{
    public partial class adminTypes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ((Master)Page.Master).SetHeader = "Admin Types";
            ((Master)Page.Master).SetHelpURL = "admin.aspx";
        }

        protected void ddlTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            gvActions.Visible = false;
            gvContractTypes.Visible = false;
            gvDepartments.Visible = false;
            gvUploadDocumentTypes.Visible = false;
            gvEOC.Visible = false;
            gvPartyTypes.Visible = false;
            gvPaymentType.Visible = false;
            gvPurchaseGroups.Visible = false;
            gvStageTypes.Visible = false;
            gvStatusTypes.Visible = false;
            switch (ddlTypes.Value)
            {
                case "actions":
                    gvActions.Visible = true;
                    break;
                case "contract":
                    gvContractTypes.Visible = true;
                    break;
                case "department":
                    gvDepartments.Visible = true;
                    break;
                case "document":
                    gvUploadDocumentTypes.Visible = true;
                    break;
                case "EOC":
                    gvEOC.Visible = true;
                    break;
                case "party":
                    gvPartyTypes.Visible = true;
                    break;
                case "payment":
                    gvPaymentType.Visible = true;
                    break;
                case "purchaseGroups":
                    gvPurchaseGroups.Visible = true;
                    break;
                case "status":
                    gvStatusTypes.Visible = true;
                    break;
            }
        }

        protected void dsContractTypes_Deleted(object sender, System.Web.UI.WebControls.SqlDataSourceStatusEventArgs e)
        {
            try
            {
                if (e.Command.Parameters["@count"].Value.ToString() == "0")
                {
                    throw new Exception("This contract type cannot be deleted because it is associated with existing contracts. This contract type has been automatically marked inactive.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void dsDepartments_Deleted(object sender, System.Web.UI.WebControls.SqlDataSourceStatusEventArgs e)
        {
            try
            {
                if (e.Command.Parameters["@count"].Value.ToString() == "0")
                {
                    throw new Exception("This department cannot be deleted because it is associated with existing contracts. This department has been automatically marked inactive.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void dsUploadDocType_Deleted(object sender, System.Web.UI.WebControls.SqlDataSourceStatusEventArgs e)
        {
            try
            {
                if (e.Command.Parameters["@count"].Value.ToString() == "0")
                {
                    throw new Exception("This document type cannot be deleted because it is associated with existing contracts. This document type has been automatically marked inactive.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void dsPartyTypes_Deleted(object sender, System.Web.UI.WebControls.SqlDataSourceStatusEventArgs e)
        {
            try
            {
                if (e.Command.Parameters["@count"].Value.ToString() == "0")
                {
                    throw new Exception("This party type cannot be deleted because it is associated with existing contracts. This party type has been automatically marked inactive.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void Page_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError();
            Message.Alert(exc.Message);

            // Handle specific exception.
            if (exc is System.Web.HttpUnhandledException)
            {
                Message.Alert("An error occurred on this page. Please verify your information to resolve the issue.");
            }
            // Clear the error from the server.
            Server.ClearError();
        }

        protected void dsPurchaseGroups_Deleted(object sender, System.Web.UI.WebControls.SqlDataSourceStatusEventArgs e)
        {
            try
            {
                if (e.Command.Parameters["@count"].Value.ToString() == "0")
                {
                    throw new Exception("This purchase group cannot be deleted because it is associated with existing contracts. This purchase group has been automatically marked inactive.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void dsEOC_Deleted(object sender, System.Web.UI.WebControls.SqlDataSourceStatusEventArgs e)
        {
            try
            {
                if (e.Command.Parameters["@count"].Value.ToString() == "0")
                {
                    throw new Exception("This EOC cannot be deleted because it is associated with existing contracts. This EOC has been automatically marked inactive.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void gvContractTypes_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
        {
            if (e.Exception != null && e.Exception.Message.Contains("existing contracts"))
            {
                gvContractTypes.JSProperties["cpMessage"] = e.Exception.Message;
                e.ExceptionHandled = true;
                gvContractTypes.DataBind();
            }
        }

        protected void gvEOC_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
        {
            if (e.Exception != null && e.Exception.Message.Contains("existing contracts"))
            {
                gvEOC.JSProperties["cpMessage"] = e.Exception.Message;
                e.ExceptionHandled = true;
                gvEOC.DataBind();
            }
        }

        protected void gvPartyTypes_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
        {
            if (e.Exception != null && e.Exception.Message.Contains("existing contracts"))
            {
                gvPartyTypes.JSProperties["cpMessage"] = e.Exception.Message;
                e.ExceptionHandled = true;
                gvPartyTypes.DataBind();
            }
        }

        protected void gvPurchaseGroups_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
        {
            if (e.Exception != null && e.Exception.Message.Contains("existing contracts"))
            {
                gvPurchaseGroups.JSProperties["cpMessage"] = e.Exception.Message;
                e.ExceptionHandled = true;
                gvPurchaseGroups.DataBind();
            }
        }

        protected void gvDepartments_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
        {
            if (e.Exception != null && e.Exception.Message.Contains("existing contracts"))
            {
                gvDepartments.JSProperties["cpMessage"] = e.Exception.Message;
                e.ExceptionHandled = true;
                gvDepartments.DataBind();
            }
        }

        protected void gvUploadDocumentTypes_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
        {
            if (e.Exception != null && e.Exception.Message.Contains("existing contracts"))
            {
                gvUploadDocumentTypes.JSProperties["cpMessage"] = e.Exception.Message;
                e.ExceptionHandled = true;
                gvUploadDocumentTypes.DataBind();
            }
        }

        protected void dsContractTypes_Inserted(object sender, SqlDataSourceStatusEventArgs e)
        {
            string folder = e.Command.Parameters["@folder"].Value.ToString().ToUpper();
            string dir = System.Configuration.ConfigurationManager.AppSettings["documentPath"] + folder;

            if (!System.IO.Directory.Exists(dir))
            {
                System.IO.Directory.CreateDirectory(dir);
            }
            string x = GetQuery(e.Command.CommandText, e.Command.Parameters);
        }

        protected void gvContractTypes_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            string oldFolder = e.OldValues["folder"].ToString();
            string newFolder = e.NewValues["folder"].ToString();
            string dir = System.Configuration.ConfigurationManager.AppSettings["documentPath"] + oldFolder;
            string newDir = System.Configuration.ConfigurationManager.AppSettings["documentPath"] + newFolder;
            if (System.IO.Directory.Exists(dir))
            {
                System.IO.Directory.Move(dir, newDir);
            }
        }

        protected void gvActions_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
        {
            if (e.Exception != null && e.Exception.Message.Contains("existing contracts"))
            {
                gvActions.JSProperties["cpMessage"] = e.Exception.Message;
                e.ExceptionHandled = true;
                gvActions.DataBind();
            }
        }

        protected void dsActions_Deleting(object sender, SqlDataSourceCommandEventArgs e)
        {
            string x = GetQuery(e.Command.CommandText, e.Command.Parameters);
        }

        private string GetQuery(string CommandQuery, System.Data.Common.DbParameterCollection parmCollection)
        {
            string query = CommandQuery;
            string parms = "";
            foreach (System.Data.SqlClient.SqlParameter parm in parmCollection)
            {
                parms += parm.ParameterName + " = ";
                if (parm.Value == null)
                {
                    parms += "null,";

                }
                else
                {
                    if (parm.DbType == System.Data.DbType.String || parm.DbType == System.Data.DbType.DateTime)
                    {
                        parms += "'" + parm.Value.ToString() + "',";
                    }
                    else
                    {
                        parms += parm.Value.ToString() + ",";
                    }
                }
            }
            query = query + " " + parms.Substring(0, parms.Length - 1);
            return query;
        }

        protected void dsActions_Deleted(object sender, System.Web.UI.WebControls.SqlDataSourceStatusEventArgs e)
        {
            try
            {
                if (e.Command.Parameters["@count"].Value.ToString() == "0")
                {
                    throw new Exception("This action cannot be deleted because it is associated with existing contracts. This action has been automatically marked inactive.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void dsStatusTypes_Deleted(object sender, SqlDataSourceStatusEventArgs e)
        {
            try
            {
                if (e.Command.Parameters["@count"].Value.ToString() == "0")
                {
                    throw new Exception("This status cannot be deleted because it is associated with existing contracts. This status has been automatically marked inactive.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void gvStatusTypes_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
        {
            if (e.Exception != null && e.Exception.Message.Contains("existing contracts"))
            {
                gvStatusTypes.JSProperties["cpMessage"] = e.Exception.Message;
                e.ExceptionHandled = true;
                gvStatusTypes.DataBind();
            }
        }

        protected void gvPaymentType_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
        {
            if (e.Exception != null && e.Exception.Message.Contains("existing contracts"))
            {
                gvPaymentType.JSProperties["cpMessage"] = e.Exception.Message;
                e.ExceptionHandled = true;
                gvPaymentType.DataBind();
            }
        }

        protected void dsPaymentType_Deleted(object sender, SqlDataSourceStatusEventArgs e)
        {
            try
            {
                if (e.Command.Parameters["@count"].Value.ToString() == "0")
                {
                    throw new Exception("This payment term cannot be deleted because it is associated with existing contracts. This payment term has been automatically marked inactive.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }
        }
    }
}