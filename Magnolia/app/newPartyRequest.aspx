﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="newPartyRequest.aspx.cs" Inherits="Magnolia.app.newPartyRequest" MasterPageFile="~/app/Master.master" %>
<%@ MasterType VirtualPath="~/app/Master.master" %>
<%@ Register Assembly="DevExpress.Web.v20.2, Version=20.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        input {text-transform:uppercase;}
        textarea {text-transform:uppercase;}
        span {text-transform:uppercase;}
        .spanTitle{text-transform:capitalize;}
        .groupStyle{position:static;overflow:auto;}
        .layoutStyle{position:static;overflow:auto;}
    </style>
    <script type="text/javascript">
        function onCorporationChanged (s, e) {
            if (s.GetValue() == "1") {
                formParty.GetItemByName('isCorporationVerified').SetVisible(true);
                formParty.GetItemByName('corporationVerifiedDate').SetVisible(true);
            }
            else {
                formParty.GetItemByName('isCorporationVerified').SetVisible(false);
                formParty.GetItemByName('corporationVerifiedDate').SetVisible(false);
            }
        }
        function OnPaidChanged(s, e) {
            if (ddlPaid.GetValue() == "1") {
                //formParty.GetItemByName("PartyRequest").SetVisible(false);
                //formParty.GetItemByName("Upload").SetVisible(true);
                btnSave.SetText('Upload Documents');
                hdnButton.Set('button', 'Upload');
            }
            else {
                btnSave.SetText('Send Request');
                hdnButton.Set('button', 'Send');
            }
            formParty.GetItemByName("SaveRequest").SetVisible(true);
        }
    </script>
    <dx:ASPxFormLayout ID="formParty" ClientInstanceName="formParty" runat="server" DataSourceID="dsPartyRequest" Paddings-Padding="10" Width="25%">
        <Items>
            <dx:LayoutGroup ColumnCount="1" Name="PartyRequest" Width="100%" Caption="Request New Party/Vendor" CssClass="groupStyle" VerticalAlign="Middle" SettingsItemCaptions-VerticalAlign="Middle">
                <Items>
                    <dx:LayoutItem FieldName="partyTypeId" Caption="Party/Vendor Type">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxComboBox ID="ddlPartyTypes" runat="server" DataSourceID="dsPartyTypes" TextField="partyType" ValueField="partyTypeId" ValidationSettings-RequiredField-IsRequired="true"></dx:ASPxComboBox>
                                <asp:SqlDataSource ID="dsPartyTypes" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                                    SelectCommand="usp_getPartyTypes" SelectCommandType="StoredProcedure">
                                </asp:SqlDataSource>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem FieldName="businessName" Caption="Party/Vendor Name">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxTextBox ID="txtBusinessName" runat="server" ValidationSettings-RequiredField-IsRequired="true"></dx:ASPxTextBox>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem FieldName="isCorporation" Caption="Corporation?">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxComboBox ID="ddlCorporation" runat="server" ValidationSettings-RequiredField-IsRequired="true">
                                    <%--<ClientSideEvents SelectedIndexChanged="onCorporationChanged" />--%>
                                    <Items>
                                        <dx:ListEditItem Text="Yes" Value="1" />
                                        <dx:ListEditItem Text="No" Value="0" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem FieldName="isCorporationVerified" Name="isCorporationVerified" Caption="Coporation In Good Standing?" ClientVisible="false">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxLabel ID="lblCorporationVerified" runat="server" Text="Verifiy Corporation is in Good Standing with the State of Mississippi"></dx:ASPxLabel>
                                <dx:ASPxHyperLink ID="hlCorporationVerified" runat="server" NavigateUrl="https://corp.sos.ms.gov/corp/portal/c/page/corpBusinessIdSearch/portal.aspx" Text="https://corp.sos.ms.gov/corp/portal/c/page/corpBusinessIdSearch/portal.aspx" Target="_blank"></dx:ASPxHyperLink>
                                <dx:ASPxComboBox ID="ddlCorporationVerified" ClientInstanceName="ddlCorporationVerified" runat="server">
                                    <Items>
                                        <dx:ListEditItem Text="Yes" Value="1" />
                                        <dx:ListEditItem Text="No" Value="0" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem FieldName="corporationVerifiedDate" Name="corporationVerifiedDate" Caption="Good Standing Verification Date" ClientVisible="false">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxDateEdit ID="deCorporationVerified" ClientInstanceName="deCorporationVerified" runat="server"></dx:ASPxDateEdit>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem FieldName="contactName" Caption="Contact Name">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxTextBox ID="txtContactName" runat="server"></dx:ASPxTextBox>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem FieldName="address" Caption="Address">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxTextBox ID="txtAddress" runat="server"></dx:ASPxTextBox>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem FieldName="city" Caption="City">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxTextBox ID="txtCity" runat="server"></dx:ASPxTextBox>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem FieldName="state" Caption="State">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxTextBox ID="txtState" runat="server"></dx:ASPxTextBox>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem FieldName="zip" Caption="Zip Code">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxTextBox ID="txtZip" runat="server"></dx:ASPxTextBox>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem FieldName="phone" Caption="Phone Number">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxTextBox ID="txtPhone" runat="server"></dx:ASPxTextBox>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem FieldName="email" Caption="Email Address">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxTextBox ID="txtEmail" runat="server"></dx:ASPxTextBox>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem FieldName="isPaid" Caption="Paid by Accounts Payable?">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxComboBox ID="ddlPaid" ClientInstanceName="ddlPaid" runat="server">
                                    <Items>
                                        <dx:ListEditItem Text="YES" Value="1" />
                                        <dx:ListEditItem Text="NO" Value="0" />
                                    </Items>
                                    <ValidationSettings RequiredField-IsRequired="true"></ValidationSettings>
                                    <ClientSideEvents SelectedIndexChanged="OnPaidChanged" />
                                </dx:ASPxComboBox>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                </Items>
            </dx:LayoutGroup>
            <dx:LayoutGroup ShowCaption="True" Caption="Upload W-9" GroupBoxDecoration="Box" Name="Upload" ClientVisible="false">
                <Items>
                    <dx:LayoutItem ShowCaption="False">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer>
                                 <script type="text/javascript">
                                     var uploadInProgress = false,
                                         submitInitiated = false,
                                         uploadErrorOccurred = false;
                                     uploadedFiles = [];
                                     function onFileUploadComplete(s, e) {
                                         var callbackData = e.callbackData.split("|"),
                                             uploadedFileName = callbackData[0],
                                             isSubmissionExpired = callbackData[1] === "True";
                                         uploadedFiles.push(uploadedFileName);
                                         if (e.errorText.length > 0 || !e.isValid) {
                                             uploadErrorOccurred = true;
                                             UploadedFilesTokenBox.isValid = false;
                                             alert("Error uploading file(s)");
                                         }
                                         if (isSubmissionExpired && UploadedFilesTokenBox.GetText().length > 0) {
                                             var removedAfterTimeoutFiles = UploadedFilesTokenBox.GetTokenCollection().join("\n");
                                             alert("The following files have been removed from the server due to the defined 5 minute timeout: \n\n" + removedAfterTimeoutFiles);
                                             UploadedFilesTokenBox.ClearTokenCollection();
                                         }
                                     }
                                     function onFileUploadStart(s, e) {
                                         uploadInProgress = true;
                                         uploadErrorOccurred = false;
                                         UploadedFilesTokenBox.SetIsValid(true);
                                     }
                                     function onFilesUploadComplete(s, e) {
                                         uploadInProgress = false;
                                         for (var i = 0; i < uploadedFiles.length; i++)
                                             UploadedFilesTokenBox.AddToken(uploadedFiles[i]);
                                         updateTokenBoxVisibility();
                                         uploadedFiles = [];
                                         if (submitInitiated) {
                                             SubmitButton.SetEnabled(true);
                                             SubmitButton.DoClick();
                                         }
                                     }
                                     function onSubmitButtonInit(s, e) {
                                         s.SetEnabled(true);
                                     }
                                     function onSubmitButtonClick(s, e) {
                                         ASPxClientEdit.ValidateGroup();
                                         if (!formIsValid()) {
                                             e.processOnServer = false;
                                         }
                                         else if (uploadInProgress) {
                                             s.SetEnabled(false);
                                             submitInitiated = true;
                                             e.processOnServer = false;
                                         }
                                     }
                                     function onTokenBoxValidation(s, e) {
                                         var isValid = DocumentsUploadControl.GetText().length > 0 || UploadedFilesTokenBox.GetText().length > 0;
                                         e.isValid = isValid;
                                         if (!isValid) {
                                             e.errorText = "No files have been uploaded. Upload at least one file.";
                                         }
                                     }
                                     function onTokenBoxValueChanged(s, e) {
                                         updateTokenBoxVisibility();
                                     }
                                     function updateTokenBoxVisibility() {
                                         var isTokenBoxVisible = UploadedFilesTokenBox.GetTokenCollection().length > 0;
                                         UploadedFilesTokenBox.SetVisible(isTokenBoxVisible);
                                     }
                                     function formIsValid() {
                                         return !ValidationSummary.IsVisible() && ddlUploadDocumentType.GetIsValid() && UploadedFilesTokenBox.GetIsValid() && !uploadErrorOccurred;
                                     }
                                 </script>
                                        <dx:ASPxHiddenField runat="server" ID="HiddenField" ClientInstanceName="HiddenField" />
                                        <dx:ASPxFormLayout ID="FormLayout" runat="server" Width="100%" ColCount="1" UseDefaultPaddings="false">
                                            <ClientSideEvents Init="AdjustHeight" />
                                            <Items>
                                                <dx:LayoutGroup ShowCaption="False" GroupBoxDecoration="None" Width="100%" UseDefaultPaddings="false" ColumnCount="1">
                                                    <Items>
                                                        <dx:LayoutItem Caption="Document Type" CaptionStyle-Font-Bold="true" CaptionSettings-Location="Top" ColumnSpan="1">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer>
                                                                    <dx:ASPxComboBox ID="ddlUploadDocumentType" ClientInstanceName="ddlUploadDocumentType" runat="server" DataSourceID="dsUploadDocumentTypes" TextField="uploadDocumentType" ValueField="uploadDocumentTypeId">
                                                                        <ValidationSettings  Display="Dynamic" ErrorDisplayMode="Text" ErrorTextPosition="Bottom" ValidationGroup="DocumentTypeValidation">
                                                                            <RequiredField IsRequired="True" ErrorText="Document Type is required" />
                                                                        </ValidationSettings>
                                                                    </dx:ASPxComboBox>
                                                                    <asp:SqlDataSource ID="dsUploadDocumentTypes" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>"
                                                                        SelectCommand="usp_getUploadDocumentTypes" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                        <dx:LayoutItem Caption="Replace Existing File" CaptionStyle-Font-Bold="true" CaptionSettings-Location="Left" ColumnSpan="1" Name="ReplaceFile" ClientVisible="false">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer>
                                                                    <dx:ASPxCheckBox ID="cbReplaceFile" runat="server" Checked="false"></dx:ASPxCheckBox>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                        <dx:LayoutGroup Caption="Documents" ColumnCount="1">
                                                            <Items>
                                                                <dx:LayoutItem ShowCaption="False" ColumnSpan="1">
                                                                    <LayoutItemNestedControlCollection>
                                                                        <dx:LayoutItemNestedControlContainer>
                                                                            <div id="dropZone">
                                                                                <dx:ASPxUploadControl runat="server" ID="DocumentsUploadControl" ClientInstanceName="DocumentsUploadControl" Width="100%"
                                                                                    AutoStartUpload="true" ShowProgressPanel="True" ShowTextBox="false" BrowseButton-Text="Add documents" FileUploadMode="OnPageLoad"
                                                                                    OnFileUploadComplete="DocumentsUploadControl_FileUploadComplete">
                                                                                    <AdvancedModeSettings EnableMultiSelect="true" EnableDragAndDrop="true" ExternalDropZoneID="dropZone" />
                                                                                    <ValidationSettings ShowErrors="true" GeneralErrorText="Error uploading file(s)"></ValidationSettings>
                                                                                    <ClientSideEvents
                                                                                        FileUploadComplete="onFileUploadComplete"
                                                                                        FilesUploadComplete="onFilesUploadComplete"
                                                                                        FilesUploadStart="onFileUploadStart" />
                                                                                </dx:ASPxUploadControl>
                                                                                <br />
                                                                                <dx:ASPxTokenBox runat="server" Width="100%" ID="UploadedFilesTokenBox" ClientInstanceName="UploadedFilesTokenBox"
                                                                                    NullText="Select the documents to submit" AllowCustomTokens="false" ClientVisible="false">
                                                                                    <ClientSideEvents Init="updateTokenBoxVisibility" ValueChanged="onTokenBoxValueChanged" Validation="onTokenBoxValidation" />
                                                                                    <ValidationSettings EnableCustomValidation="true"/><%-- ValidationGroup="UploadValidation" --%>
                                                                                </dx:ASPxTokenBox>
                                                                                <br />
                                                                                <%--<p class="Note">
                                                                                    <dx:ASPxLabel ID="MaxFileSizeLabel" runat="server" Text="Maximum file size: 4 MB." Font-Size="8pt" />
                                                                                </p>--%>
                                                                                <dx:ASPxValidationSummary runat="server" ID="ValidationSummary" ClientInstanceName="ValidationSummary" RenderMode="BulletedList" Width="250px" ShowErrorAsLink="false" ForeColor="Red" Font-Bold="true" /><%-- ValidationGroup="UploadValidation"--%>
                                                                            </div>
                                                                        </dx:LayoutItemNestedControlContainer>
                                                                    </LayoutItemNestedControlCollection>
                                                                </dx:LayoutItem>
                                                            </Items>
                                                        </dx:LayoutGroup>
                                                        <dx:LayoutItem ShowCaption="False" HorizontalAlign="Right" ColumnSpan="1">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer>
                                                                    <dx:ASPxButton runat="server" ID="SubmitButton" ClientInstanceName="SubmitButton" Text="Submit" AutoPostBack="True"
                                                                        OnClick="SubmitButton_Click"  ClientEnabled="false" ValidateInvisibleEditors="true">
                                                                        <ClientSideEvents Init="onSubmitButtonInit" Click="onSubmitButtonClick" />
                                                                    </dx:ASPxButton>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                        <dx:EmptyLayoutItem Height="5" />
                                                    </Items>
                                                </dx:LayoutGroup>
                                                <dx:LayoutGroup GroupBoxDecoration="None" ShowCaption="False" Name="ResultGroup" Visible="false" Width="50%" UseDefaultPaddings="false">
                                                    <Items>
                                                        <dx:LayoutItem ShowCaption="False">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer>
                                                                    <dx:ASPxRoundPanel ID="RoundPanel" runat="server" HeaderText="Uploaded files" Width="100%">
                                                                        <PanelCollection>
                                                                            <dx:PanelContent>
                                                                                <b>Description:</b>
                                                                                <dx:ASPxLabel runat="server" ID="DescriptionLabel" />
                                                                                <br />
                                                                                <br />
                                                                                <dx:ASPxListBox ID="SubmittedFilesListBox" runat="server" Width="100%" Height="150px">
                                                                                    <ItemStyle CssClass="ResultFileName" />
                                                                                    <Columns>
                                                                                        <dx:ListBoxColumn FieldName="OriginalFileName" />
                                                                                        <dx:ListBoxColumn FieldName="FileSize" Width="15%"/>
                                                                                    </Columns>
                                                                                </dx:ASPxListBox>
                                                                            </dx:PanelContent>
                                                                        </PanelCollection>
                                                                    </dx:ASPxRoundPanel>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                    </Items>
                                                </dx:LayoutGroup>
                                            </Items>
                                            <SettingsAdaptivity AdaptivityMode="SingleColumnWindowLimit" SwitchToSingleColumnAtWindowInnerWidth="1000" />
                                        </dx:ASPxFormLayout>         
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                </Items>
            </dx:LayoutGroup>
            <dx:LayoutGroup ShowCaption="False" GroupBoxDecoration="None" VerticalAlign="Middle" Name="SaveRequest" ClientVisible="false">
                <Items>
                    <dx:LayoutItem ShowCaption="False" HorizontalAlign="Right">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer>
                                <dx:ASPxButton ID="btnSave" ClientInstanceName="btnSave" runat="server" OnClick="btnSave_Click"></dx:ASPxButton>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                </Items>
            </dx:LayoutGroup>
        </Items>
    </dx:ASPxFormLayout>
    <asp:SqlDataSource ID="dsPartyRequest" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" InsertCommand ="usp_addPartyRequest" InsertCommandType="StoredProcedure" OnInserted="dsPartyRequest_Inserted">
        <InsertParameters>
            <asp:ControlParameter ControlID="formParty$ddlPartyTypes" PropertyName="Value" Name="partyTypeId" />
            <asp:ControlParameter ControlID="formParty$txtBusinessName" PropertyName="Text" Name="businessName" />
            <asp:ControlParameter ControlID="formParty$txtContactName" PropertyName="Text" Name="contactName" />
            <asp:ControlParameter ControlID="formParty$txtAddress" PropertyName="Text" Name="address" />
            <asp:ControlParameter ControlID="formParty$txtCity" PropertyName="Text" Name="city" />
            <asp:ControlParameter ControlID="formParty$txtState" PropertyName="Text" Name="state" />
            <asp:ControlParameter ControlID="formParty$txtZip" PropertyName="Text" Name="zip" />
            <asp:ControlParameter ControlID="formParty$txtPhone" PropertyName="Text" Name="phone" />
            <asp:ControlParameter ControlID="formParty$txtEmail" PropertyName="Text" Name="email" />
            <asp:ControlParameter ControlID="formParty$ddlCorporation" PropertyName="Value" Name="isCorporation" />
            <asp:ControlParameter ControlID="formParty$ddlCorporationVerified" PropertyName="Value" Name="isCorporationVerified" />
            <asp:ControlParameter ControlID="formParty$deCorporationVerified" PropertyName="Value" Name="corporationVerifiedDate" />
            <asp:ControlParameter ControlID="formParty$ddlPaid" PropertyName="Value" Name="isPaid" />
            <asp:SessionParameter Name="userId" SessionField="UserID" />
            <asp:Parameter Name="requestId" Direction="Output" DbType="Int16" Size="4" />
        </InsertParameters>
    </asp:SqlDataSource>
    <dx:ASPxHiddenField ID="hdnButton" ClientInstanceName="hdnButton" runat="server"></dx:ASPxHiddenField>
</asp:Content>