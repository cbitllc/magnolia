﻿using System.Net;
using System.IO;
using System.Xml;
using System.ServiceModel.Channels;
using System.Data;
using System;
using System.Web;
using DevExpress.Office.Utils;
using System.Collections.Generic;
using Magnolia.DocuSignWeb;

namespace Magnolia.app
{
    public class DocuSign
    {
        public List<DocusignDocument> Documents = new List<DocusignDocument>();
        List<DocuSignWeb.Document> envelopeDocuments = new List<DocuSignWeb.Document>();
        public string DocumentName { get; set; } = "";
        public string DocumentPath { get; set; } = "";
        public MemoryStream DocumentStream { get; set; } = null;
        public string RecipientEmail { get; set; } = "";
        public string RecipientUserName { get; set; } = "";
        public string EnvelopeSubject { get; set; } = "Contract Signature Required";
        public string EnvelopeEmailBlurb { get; set; } = "Please review and sign the attached document at your earliest convenience.";
        public string AnchorSigText { get; set; } = "signer1sig";
        public string AnchorDateText { get; set; } = "signer1date";
        public int CustomSignatureHeight { get; set; } = 50;
        public bool CustomSignatureHeightIsSpecified { get; set; } = false;
        public string ContractID { get; set; } = "0";
        public int PartyID { get; set; } = 0;
        public int ContractTypeID { get; set; } = 0;
        public string EmailType { get; set; } = "";
        public string CEOEmail { get; set; } = "";
        public string CEOName { get; set; } = "";
        public int DocumentXCoord { get; set; } = 0;
        public int DocumentYCoord { get; set; } = 0;
        public string SignatureKeyword { get; set; }
        public string DateKeyword { get; set; }
        public string InitialsKeyword { get; set; }
        //DEV accountID = "f26fe3b3-88c6-4f5b-b338-9b3d9dfd01f3"  PROD accountId = "23d11fb2-7f1a-476f-ab4d-f95451472aae"
        //DEV username = "docusign@mrhc.org"
        //DEV password = "S3oNZ4$0Br$@cZzKrnEB"
        //DEV integratorKey = "e7af9d91-be6f-4674-a461-13f9deea189b"    PROD integratorKey = "9ba64a80-e138-47fe-8b0e-f3872a9547c4"
        private static string accountId = System.Configuration.ConfigurationManager.AppSettings["system"] == "PRO" ? "23d11fb2-7f1a-476f-ab4d-f95451472aae" : "f26fe3b3-88c6-4f5b-b338-9b3d9dfd01f3";// "23d11fb2-7f1a-476f-ab4d-f95451472aae";//  "ff993661-5e82-455a-ba1f-819428c1e866";// "f26fe3b3-88c6-4f5b-b338-9b3d9dfd01f3"; //System.Configuration.ConfigurationManager.AppSettings["docusignEnvironment"] == "DEV" ? "304173e4-4580-4e0d-b495-bf375bcfa9e8" : "e58fef29-f40e-4db1-93c6-5c833480d666";
        private static string userName = "docusign@mrhc.org";// "docusign@mrhc.org";// "2273c65a-baee-4da7-a65a-ab25265f0fa7"; // System.Configuration.ConfigurationManager.AppSettings["docusignEnvironment"] == "DEV" ? "jodie@cbit-llc.com" : "20d3aaf1-87d9-4dd4-a3d1-ef0ece7e7023";
        private static string password = "S3oNZ4$0Br$@cZzKrnEB"; //"S3oNZ4$0Br$@cZzKrnEB";//System.Configuration.ConfigurationManager.AppSettings["docusignEnvironment"] == "DEV" ? "GymnAstics1!" : "W1r3l3ss";
        private static string integratorKey = System.Configuration.ConfigurationManager.AppSettings["system"] == "PRO" ? "4a4519fb-46a2-4e43-8c45-a44eb7c566bf" : "e7af9d91-be6f-4674-a461-13f9deea189b"; //"9ba64a80-e138-47fe-8b0e-f3872a9547c4";// "4a4519fb-46a2-4e43-8c45-a44eb7c566bf"; //"ed567ec6-4145-4791-a5b0-16ca4957d6c0";
        public int SignatureCount { get; set; } 
        public int DateCount { get; set; } 
        public int InitialsCount { get; set; } 
        private static string Authenticate()
        {
            string apiURL = System.Configuration.ConfigurationManager.AppSettings["system"] == "PRO" ? "https://na4.docusign.net": "https://demo.docusign.net/restapi/v2/login_information";// "https://na4.docusign.net"; //System.Configuration.ConfigurationManager.AppSettings["docusignEnvironment"] == "DEV" ? "https://demo.docusign.net/restapi/v2/login_information" : "https://www.docusign.net/restapi/v2/login_information";
            string baseURL = "";
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                HttpWebRequest oRequest = (HttpWebRequest)initializeRequest(apiURL, "GET", "", userName, password, integratorKey);
                using (HttpWebResponse oResponse = (HttpWebResponse)oRequest.GetResponse())
                {
                    using (StreamReader oStreamReader = new StreamReader(oResponse.GetResponseStream()))
                    {
                        string sXML = oStreamReader.ReadToEnd();
                        baseURL = parseDataFromResponse(sXML, "baseUrl");
                        oStreamReader.Close();
                        oStreamReader.Dispose();
                    }
                    oResponse.Close();
                    oResponse.Dispose();
                }
            }
            catch (Exception ex)
            {
                Message.Alert("Error in DocuSign Authentication. " + ex.Message);
            }
            return baseURL;
        }

        public string CreateAndSend()
        {
            DocuSignWeb.Envelope envelope = null;
            if (System.Configuration.ConfigurationManager.AppSettings["system"] == "DEV")
            {
                CEOEmail = "CMSAdministrators@mrhc.org";
                CEOName = "Test Site - " + CEOName;
                EnvelopeSubject = "Test Site - " + EnvelopeSubject;
                EnvelopeEmailBlurb = "***This email was generated by the CMS DEV Test Site.***<br/><br/>" + EnvelopeEmailBlurb;
            }
            if (System.Configuration.ConfigurationManager.AppSettings["system"] == "LOCALHOST")
            {
                CEOEmail = "jodie@cbit-llc.com";
                CEOName = "LOCALHOST - " + CEOName;
                EnvelopeSubject = "LOCALHOST - " + EnvelopeSubject;
                EnvelopeEmailBlurb = "***This email was generated by the CMS LOCALHOST Site.***<br/><br/>" + EnvelopeEmailBlurb;
            }
            DocuSignWeb.Recipient receiver = new DocuSignWeb.Recipient () {Email = CEOEmail, UserName = CEOName, Type = DocuSignWeb.RecipientTypeCode.Signer, ID = "1", RoutingOrder = 1};
            DocuSignWeb.Recipient recipient = new DocuSignWeb.Recipient() { Email = RecipientEmail, UserName = RecipientUserName, Type = DocuSignWeb.RecipientTypeCode.CarbonCopy, ID = "2", RoutingOrder = 2 };
            envelope = new DocuSignWeb.Envelope() { Subject = EnvelopeSubject, EmailBlurb = EnvelopeEmailBlurb, AccountId = accountId };
            DataSet dsManagers = DataAccess.GetDataSet("usp_getContractManagers", null, null);
            Recipient[] allRecipients = null;
            if (dsManagers != null && dsManagers.Tables.Count > 0)
            {
                allRecipients = new Recipient[dsManagers.Tables[0].Rows.Count + 1];
                for (int i = 0; i < dsManagers.Tables[0].Rows.Count; i++)
                {
                    recipient = new Recipient()
                    {
                        Email = dsManagers.Tables[0].Rows[i]["emailAddress"].ToString(),
                        UserName = dsManagers.Tables[0].Rows[i]["fullName"].ToString(),
                        Type = DocuSignWeb.RecipientTypeCode.CarbonCopy,
                        ID = "2",
                        RoutingOrder = 2
                    };
                    allRecipients[i] = recipient;
                }
                allRecipients[allRecipients.Length - 1] = receiver;
            }
            else
            {
                allRecipients = new Recipient [] { receiver,recipient};
            }
            envelope.Recipients = allRecipients; // new DocuSignWeb.Recipient[] {recipient, receiver};
            string SR = "";
            byte[] sContentsa = null;
            try
            {
                DocuSignWeb.Document summary = new DocuSignWeb.Document() { ID = "0", Name = "ContractSummary" };
                sContentsa = ContractSummarySheet.Form(Convert.ToInt32(ContractID)).ToArray();
                summary.PDFBytes = sContentsa;
                envelopeDocuments.Add(summary);
            }
            catch { }
            foreach (DocusignDocument thisDocument in Documents)
            {

                DocumentName += thisDocument.DocumentName + ",";
                DocuSignWeb.Document doc = new DocuSignWeb.Document() { ID = "1", Name = thisDocument.DocumentName };
                if (thisDocument.DocumentStream != null)
                {
                    sContentsa = thisDocument.DocumentStream.ToArray();
                } else { 
                    SR = thisDocument.DocumentPath + doc.Name;
                    sContentsa = System.IO.File.ReadAllBytes(SR);
                    }
                doc.PDFBytes = sContentsa;
                thisDocument.PDFByteLength = sContentsa.Length;
                DataSet ds = DataAccess.GetDataSet("usp_getDocuSignConfig",null, null);
                if (ds != null && ds.Tables.Count>0 && ds.Tables[0].Rows.Count >0)
                {
                    SignatureCount = Convert.ToInt32(ds.Tables[0].Rows[0]["maxSignatureCount"]);
                    DateCount = Convert.ToInt32(ds.Tables[0].Rows[0]["maxDateCount"]);
                    InitialsCount = Convert.ToInt32(ds.Tables[0].Rows[0]["maxInitialsCount"]);
                    SignatureKeyword = Convert.ToString(ds.Tables[0].Rows[0]["signatureKeyword"]);
                    DateKeyword = Convert.ToString(ds.Tables[0].Rows[0]["dateKeyword"]);
                    InitialsKeyword = Convert.ToString(ds.Tables[0].Rows[0]["initialsKeyword"]);
                }

                List<DocuSignWeb.Tab> tabs = new List<DocuSignWeb.Tab>();
                for (int i = 1; i < SignatureCount; i++)
                {
                    DocuSignWeb.AnchorTab anchor = new DocuSignWeb.AnchorTab();
                    anchor.AnchorTabString = SignatureKeyword + i;
                    anchor.MatchWholeWord = true;
                    anchor.MatchWholeWordSpecified = true;
                    anchor.IgnoreIfNotPresent = true;
                    anchor.IgnoreIfNotPresentSpecified = true;
                    anchor.YOffset = -0.25;
                    anchor.Unit = DocuSignWeb.UnitTypeCode.Inches;
                    anchor.UnitSpecified = true;
                    DocuSignWeb.Tab tab = new DocuSignWeb.Tab();
                    tab.DocumentID = "1";
                    tab.RecipientID = "1";
                    tab.Type = DocuSignWeb.TabTypeCode.SignHere;
                    tab.AnchorTabItem = anchor;
                    tabs.Add(tab);
                }
                for (int i = 1; i < DateCount; i ++)
                { 
                    DocuSignWeb.AnchorTab anchorDate = new DocuSignWeb.AnchorTab();
                    anchorDate.AnchorTabString = DateKeyword + i;
                    anchorDate.MatchWholeWord = true;
                    anchorDate.MatchWholeWordSpecified = true;
                    anchorDate.IgnoreIfNotPresent = true;
                    anchorDate.IgnoreIfNotPresentSpecified = true;
                    anchorDate.YOffset = -0.25;
                    anchorDate.Unit = DocuSignWeb.UnitTypeCode.Inches;
                    anchorDate.UnitSpecified = true;
                    DocuSignWeb.Tab tabDate = new DocuSignWeb.Tab();
                    tabDate.DocumentID = "1";
                    tabDate.RecipientID = "1";
                    tabDate.Type = DocuSignWeb.TabTypeCode.DateSigned;
                    tabDate.AnchorTabItem = anchorDate;
                    tabs.Add(tabDate);
                }
                for (int i = 1; i < InitialsCount; i++)
                {
                    DocuSignWeb.AnchorTab anchorDate = new DocuSignWeb.AnchorTab();
                    anchorDate.AnchorTabString = InitialsKeyword + i;
                    anchorDate.MatchWholeWord = true;
                    anchorDate.MatchWholeWordSpecified = true;
                    anchorDate.IgnoreIfNotPresent = true;
                    anchorDate.IgnoreIfNotPresentSpecified = true;
                    anchorDate.YOffset = -0.25;
                    anchorDate.Unit = DocuSignWeb.UnitTypeCode.Inches;
                    anchorDate.UnitSpecified = true;
                    DocuSignWeb.Tab tabDate = new DocuSignWeb.Tab();
                    tabDate.DocumentID = "1";
                    tabDate.RecipientID = "1";
                    tabDate.Type = DocuSignWeb.TabTypeCode.DateSigned;
                    tabDate.AnchorTabItem = anchorDate;
                    tabs.Add(tabDate);
                }
                if (tabs.Count > 0)
                {
                    envelope.Tabs = tabs.ToArray();
                }
                envelopeDocuments.Add( doc );
            }
            DocumentName = DocumentName.Substring(0, DocumentName.Length - 1);
            envelope.Documents = envelopeDocuments.ToArray();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            using (DocuSignWeb.DSAPIServiceSoapClient client = new DocuSignWeb.DSAPIServiceSoapClient())
            { 
                using (System.ServiceModel.OperationContextScope scope = new System.ServiceModel.OperationContextScope(client.InnerChannel))
                {
                    string auth = "<DocuSignCredentials><Username>" + userName + "</Username><Password>" + password + "</Password><IntegratorKey>" + integratorKey + "</IntegratorKey></DocuSignCredentials>";
                    HttpRequestMessageProperty httpRequestProperty = new HttpRequestMessageProperty();
                    httpRequestProperty.Headers.Add("X-DocuSign-Authentication", auth);
                    System.ServiceModel.OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = httpRequestProperty;
                    DocuSignWeb.EnvelopeStatus statusSend = null;
                    try
                    {
                        statusSend = client.CreateAndSendEnvelope(envelope);
                        LogRecord(statusSend.EnvelopeID, statusSend.ACStatusDate, envelope.Documents.Length,1);
                        return statusSend.EnvelopeID;
                    }
                    catch (Exception ex)
                    {
                        string envelopeId = "Error";
                        if (statusSend != null)
                        {
                            envelopeId = statusSend.EnvelopeID;
                        }
                        Message.Alert("Error seinding envelope." + ex.Message);
                        LogRecord(envelopeId, DateTime.Now, 0, 0);
                        return "0";
                    }
                    finally
                    {
                        scope.Dispose();
                        envelope = null;
                        client.Close();
                     }
                   }
                } 
            }

        private void LogRecord(string envelopeId, DateTime statusDate, int documentCount, int success)
        {
            foreach (DocusignDocument doc in Documents)
            {
                DataSet ds = DataAccess.GetDataSet("usp_updateDocusignTracking",
                    new string[] { "@envelopeId", "@emailDate", "@recipientEmail", "@fileName", "@contractId", "@isEmailSuccess", "@documentId", "@PDFBytes" },
                    new string[] { envelopeId, statusDate.ToString(), CEOEmail, doc.DocumentName, ContractID, success.ToString(), doc.DocumentID.ToString(), doc.PDFByteLength.ToString() });
            }
        }

        private static string getResponseBody(HttpWebRequest oRequest)
        {
            HttpWebResponse oResponse = (HttpWebResponse)oRequest.GetResponse();
            string sResponseString = "";
            using (StreamReader oStreamReader = new StreamReader(oResponse.GetResponseStream()))
                    {
                sResponseString = oStreamReader.ReadToEnd();
            }
            return sResponseString;
        }

        private static void configureMultiPartFormDataRequest(HttpWebRequest oRequest, string sXmlBody, string sDocumentName, string sContentType)
        {
            oRequest.ContentType = "multipart/form-data; boundary=BOUNDARY";
            string sRequestBodyStart = "\r\n\r\n--BOUNDARY\r\n" + "Content-Type: application/xml\r\n" + "Content-Disposition: form-data\r\n" + "\r\n" +
                    sXmlBody + "\r\n\r\n--BOUNDARY\r\n" + "Content-Type: " + sContentType + "\r\n" + "Content-Disposition: file; filename='" + sDocumentName + "'; documentId=1\r\n" + "\r\n";
            string sRequestBodyEnd = "\r\n--BOUNDARY--\r\n\r\n";
            using (FileStream oFileStream = File.OpenRead(System.IO.Path.GetTempPath() + sDocumentName))
            {
                byte[] bodyStart = System.Text.Encoding.UTF8.GetBytes(sRequestBodyStart.ToString());
                byte[] bodyEnd = System.Text.Encoding.UTF8.GetBytes(sRequestBodyEnd.ToString());
                using (Stream oDataStream = oRequest.GetRequestStream())
                {
                    oDataStream.Write(bodyStart, 0, sRequestBodyStart.ToString().Length);
                    byte[] buffer = new byte[4096];
                    int iLen = 0;
                    iLen = oFileStream.Read(buffer, 0, 4096);
                    do
                    {
                        oDataStream.Write(buffer, 0, iLen);
                        iLen = oFileStream.Read(buffer, 0, 4096);
                    }  while (iLen > 0);
                    oDataStream.Write(bodyEnd, 0, sRequestBodyEnd.ToString().Length);
                    oDataStream.Close();
                }
                oFileStream.Close();
            }
        }

        private static HttpWebRequest initializeRequest(string sUrl, string sMethod, string sBody,string sEmail,string sPassword,string sIntegratorKey)
        {
            HttpWebRequest oRequest = (HttpWebRequest)WebRequest.Create(sUrl);
            oRequest.KeepAlive = false;
            oRequest.Method = sMethod;
            AddRequestHeaders(oRequest, sEmail, sPassword, sIntegratorKey);
            if (sBody != "") { addRequestBody(oRequest, sBody); }
            return (oRequest);
        }

        private static void AddRequestHeaders(HttpWebRequest oRequest, string sEmail, string sPassword, string sIntegratorKey)
        {
            oRequest.Headers.Add("X-DocuSign-Authentication",
            string.Format("<DocuSignCredentials><Username>{0}</Username><Password>{1}</Password><IntegratorKey>{2}</IntegratorKey></DocuSignCredentials>", sEmail, sPassword, sIntegratorKey));
            oRequest.Accept = "application/xml";
            oRequest.ContentType = "application/xml";
        }

        private static void addRequestBody(HttpWebRequest oRequest, string sRequestBody)
        {
            byte[] body = System.Text.Encoding.UTF8.GetBytes(sRequestBody);
            using (Stream oDataStream = oRequest.GetRequestStream())
            {
                oDataStream.Write(body, 0, sRequestBody.Length);
                oDataStream.Close();
           }
        }

        private static string parseDataFromResponse(string sXML , string sSearchToken)
        { 
            using (XmlReader oXmlReader = XmlReader.Create(new StringReader(sXML)))
            {
                do 
                {
                    if (oXmlReader.NodeType == XmlNodeType.Element && oXmlReader.Name == sSearchToken)
                    {
                        return oXmlReader.ReadString();
                    }
                } while (oXmlReader.Read());
            }
            return "";
        }

        public bool CheckStatusByEnvelopeId(string envelopeId)
        {
            Authenticate();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            using (var client = new DocuSignWeb.DSAPIServiceSoapClient())
            {
                using (var scope = new System.ServiceModel.OperationContextScope(client.InnerChannel))
                {
                    string auth = "<DocuSignCredentials><Username>" + userName + "</Username><Password>" + password + "</Password><IntegratorKey>" + integratorKey + "</IntegratorKey></DocuSignCredentials>";
                    HttpRequestMessageProperty httpRequestProperty = new HttpRequestMessageProperty();
                    httpRequestProperty.Headers.Add("X-DocuSign-Authentication", auth);
                    System.ServiceModel.OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = httpRequestProperty;
                    DocuSignWeb.EnvelopeStatus envelopeStatus = null;
                    try
                    {
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                        envelopeStatus = client.RequestStatus(envelopeId);
                        if (envelopeStatus != null && envelopeStatus.Completed > envelopeStatus.Created)
                        {
                            try
                            {
                                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                                DocuSignWeb.EnvelopePDF envPDF = client.RequestPDF(envelopeId);
                                if (envPDF != null)
                                {
                                    UpdateDataChanges(envPDF, envelopeStatus);
                                    return true;
                                }
                            }
                            catch (Exception ex)
                            {
                                Message.Alert("Error Checking envelope status. " + ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Message.Alert("Error Retrieving envelope. " + ex.Message);
                    }
                    finally
                    {
                        scope.Dispose();
                        client.Close();
                    }
                }
            }
            return false;
        }

        private void UpdateDataChanges(DocuSignWeb.EnvelopePDF envPDF, DocuSignWeb.EnvelopeStatus status)
        {
            try
            {
                CopyDocument(envPDF, status);
            }
            catch (Exception ex)
            {
                string errorMessage = "Error Updating DocuSign Data (" + envPDF.EnvelopeID + "): " + ex.Message;
                Message.Alert(errorMessage);
            }
        }

        public void CopyDocument(DocuSignWeb.EnvelopePDF envPDF, DocuSignWeb.EnvelopeStatus status)
        {
            try
            {
                string fileName = "";
                string path = "";
                DocuSignWeb.DocumentPDFs docPDFs;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                using (var client = new DocuSignWeb.DSAPIServiceSoapClient())
                {
                    using (var scope = new System.ServiceModel.OperationContextScope(client.InnerChannel))
                    {
                        try
                        {
                            string auth = "<DocuSignCredentials><Username>" + userName + "</Username><Password>" + password + "</Password><IntegratorKey>" + integratorKey + "</IntegratorKey></DocuSignCredentials>";
                            HttpRequestMessageProperty httpRequestProperty = new HttpRequestMessageProperty();
                            httpRequestProperty.Headers.Add("X-DocuSign-Authentication", auth);
                            System.ServiceModel.OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = httpRequestProperty;
                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                            string envelopeID = envPDF.EnvelopeID;
                            docPDFs = client.RequestDocumentPDFs(envelopeID);
                        }
                        finally
                        {
                            scope.Dispose();
                            client.Close();
                        }
                    }
                }
                if (docPDFs != null && docPDFs.DocumentPDF.Length > 0)
                {
                    DataSet ds = DataAccess.GetDataSet("usp_updateDocusignTracking",
                        new string[] { "@envelopeId", "@isEmailSigned", "@emailSignedDate" },
                        new string[] { envPDF.EnvelopeID, status.Signed == null ? "0" : "1", status.Signed.ToString() });
                    if (ds.Tables[0].Columns.Contains("path"))
                    {
                        try
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                for (int z = 0; z < docPDFs.DocumentPDF.Length; z++)
                                 {
                                    fileName = ds.Tables[0].Rows[i]["fileName"].ToString();
                                    path = ds.Tables[0].Rows[i]["path"].ToString() + @"\" + fileName;
                                    DocuSignWeb.DocumentPDF docPDF = new DocuSignWeb.DocumentPDF();
                                    docPDF = docPDFs.DocumentPDF[z];
                                    if (docPDF.Name != "ContractSummary")
                                    {
                                        if (fileName.Replace("-Signed", "") == docPDF.Name)
                                        {
                                            File.WriteAllBytes(path, docPDF.PDFBytes);
                                        }
                                        if (ds.Tables[0].Rows[i]["uploadDocumentTypeId"] != System.DBNull.Value && ds.Tables[0].Rows[i]["uploadDocumentTypeId"].ToString() == "9")
                                        {
                                            string body2 = "Contract has been approved. Purchase requisition is attached for processing.";
                                            body2 += "<br/><br/>You can view the contract and details here: <a href='" + System.Configuration.ConfigurationManager.AppSettings["site"].ToString() + "/app/contractDetails.aspx?id=" + ds.Tables[0].Rows[0]["signedContractId"] + "'>" + "https://cm/cms/app/contractDetails.aspx?id=" + ds.Tables[0].Rows[0]["signedContractId"] + "</a>";
                                            MemoryStream mem = new MemoryStream(docPDF.PDFBytes);
                                            Message.SendMail("purchasinggroup@mrhc.org", "Purchasing Group", "Contract Purchase Requisition", body2, mem, docPDF.Name);
                                        }
                                    }
                                }
                            }
                            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0]["requestorEmail"] != null && ds.Tables[0].Rows[0]["requestorEmail"].ToString() != "" && ds.Tables[0].Rows[0]["requestor"] != null && ds.Tables[0].Rows[0]["requestor"].ToString() != "")
                            {
                                string body = "A Contract you Submitted for " + ds.Tables[0].Rows[0]["businessName"] + " has been signed by all internal parties";
                                if (ds.Tables[0].Rows[0]["isPartySigned"] != null && ds.Tables[0].Rows[0]["isPartySigned"].ToString() == "Y")
                                {
                                    body += ". Please send a fully exeucted copy to the Vendor for their records.";
                                    body += "<br/><br/>You can obtain the fully executed contract here: <a href='" + System.Configuration.ConfigurationManager.AppSettings["site"].ToString() + "/app/contractDetails.aspx?id=" + ds.Tables[0].Rows[0]["signedContractId"] + "'>" + "https://cm/cms/app/contractDetails.aspx?id=" + ds.Tables[0].Rows[0]["signedContractId"] + "</a>";
                                }
                                else
                                {
                                    body += " and is ready to be signed by the Vendor.";
                                    body += "<br/><br/>You can view the current status here: <a href='" + System.Configuration.ConfigurationManager.AppSettings["site"].ToString() + "/app/contractDetails.aspx?id=" + ds.Tables[0].Rows[0]["signedContractId"] + "'>" + "https://cm/cms/app/contractDetails.aspx?id=" + ds.Tables[0].Rows[0]["signedContractId"] + "</a>";
                                }
                                foreach (DocumentPDF docPDF in docPDFs.DocumentPDF)
                                {
                                    if (docPDF.Name.ToUpper().Contains("CONTRACT"))
                                    {
                                        MemoryStream memAttach = new MemoryStream(docPDF.PDFBytes);
                                        fileName = docPDF.Name;
                                        Message.SendMail(ds.Tables[0].Rows[0]["requestorEmail"].ToString(), ds.Tables[0].Rows[0]["requestor"].ToString(), "Contract for " + ds.Tables[0].Rows[0]["businessName"] + " Action", body, memAttach, docPDF.Name);
                                        break;
                                    }
                                }
                                Message.SendMail(ds.Tables[0].Rows[0]["requestorEmail"].ToString(), ds.Tables[0].Rows[0]["requestor"].ToString(), "Contract for " + ds.Tables[0].Rows[0]["businessName"] + " Action", body);
                            }
                        }
                        catch (Exception ex)
                        {
                            Message.Alert("Error retrieving file. " + ex.Message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Error Copying DocuSign Document (" + envPDF.EnvelopeID + "): " + ex.Message;
                Message.Alert(errorMessage);
            }
        }
    }

    public class DocusignDocument
    {
        public string RecipientEmail { get; set; }
        public string RecipientUserName  { get; set; }
        public string CEOEmail  { get; set; }
        public string CEOName { get; set; }
        public string DocumentName { get; set; }
        public string DocumentPath { get; set; }
        public int DocumentID { get; set; }
        public MemoryStream DocumentStream = new MemoryStream();
        public long PDFByteLength { get; set; }
        public int UploadOrder { get; set; }
    }

}