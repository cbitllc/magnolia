﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="Magnolia.app.Profile" MasterPageFile="~/app/Master.master" %>
<%@ MasterType VirtualPath="Master.master" %>
<%@ Register Assembly="DevExpress.Web.v20.2, Version=20.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <dx:ASPxFormLayout ID="formUsers" ClientInstanceName="formUsers" runat="server" DataSourceID="dsUser">
        <Items>
            <dx:LayoutGroup Caption="User Details">
                <Items>
                    <dx:LayoutItem ColSpan="1" Caption="Name" FieldName="fullName">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxLabel ID="txtUserFullName" runat="server"></dx:ASPxLabel>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem ColSpan="1" Caption="User Name" FieldName="username" ClientVisible="false">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxLabel ID="txtUserName" runat="server"></dx:ASPxLabel>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem ColSpan="1" Caption="Phone Number" FieldName="phoneNumber">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxTextBox ID="txtUserPhone" runat="server"></dx:ASPxTextBox>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem ColSpan="1" Caption="Email Address" FieldName="emailAddress">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxTextBox ID="txtUserEmail" runat="server"></dx:ASPxTextBox>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                </Items>
            </dx:LayoutGroup>
            <dx:LayoutGroup Caption="ByPass Dates">
                <Items>
                    <dx:LayoutItem ShowCaption="False" FieldName="bypassStart" HorizontalAlign="Center">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer>
                                <dx:ASPxDateEdit ID="deBypassStart" runat="server"></dx:ASPxDateEdit>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem ShowCaption="False" FieldName="bypassEnd" HorizontalAlign="Center">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer>
                                <dx:ASPxDateEdit ID="deBypassEnd" runat="server"></dx:ASPxDateEdit>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                </Items>
            </dx:LayoutGroup>
            <dx:LayoutGroup GroupBoxDecoration="None" ColumnCount="3" Height="500">
                <Items>
                    <dx:LayoutItem ShowCaption="False" HorizontalAlign="Right" VerticalAlign="Top">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer>
                                <dx:ASPxButton ID="btnSaveUser" runat="server" OnClick="btnSaveUser_Click" Text="Save"></dx:ASPxButton>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                </Items>
            </dx:LayoutGroup>
        </Items>
    </dx:ASPxFormLayout>
    <asp:SqlDataSource ID="dsUser" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
        SelectCommand="usp_getUser" SelectCommandType="StoredProcedure"
        UpdateCommand="usp_updateUser" UpdateCommandType="StoredProcedure" OnUpdating="dsUser_Updating">
        <UpdateParameters>
            <asp:ControlParameter ControlID="formUsers$txtUserPhone" PropertyName="Text" Name="phoneNumber" />
            <asp:ControlParameter ControlID="formUsers$txtUserEmail" PropertyName="Text" Name="emailAddress" />
            <asp:ControlParameter ControlID="formUsers$deBypassStart" PropertyName="Date" Name="bypassStart" />
            <asp:ControlParameter ControlID="formUsers$deBypassEnd" PropertyName="Date" Name="bypassEnd" />
            <asp:SessionParameter SessionField="UserID" Name="userId" />
        </UpdateParameters>
        <SelectParameters>
            <asp:SessionParameter SessionField="UserID" Name="userId" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>