﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="adminUsers.aspx.cs" Inherits="Magnolia.app.adminUsers" MasterPageFile="~/app/Master.master" %>
<%@ Register Assembly="DevExpress.Web.v20.2, Version=20.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <style>
        input {text-transform:uppercase;}
        textarea {text-transform:uppercase;}
        span {text-transform:uppercase;}
        .spanTitle{text-transform:capitalize;}
        .gridHeader {  
            background-color: white;  
            color:black;
        }  
        .gridHeader a {  
            color: black;  
        }  
        .cb {background-color:lightgray}
    </style>
    <div style="width:100%; top:170px; height:100%; position:fixed; ">
        <dx:ASPxPageControl ID="tabUsers" ClientInstanceName="tabUsers" runat="server" ActiveTabIndex="0" Width="100%">
            <TabPages>
                <dx:TabPage Name="users" Text="Users">
                    <ContentCollection>
                        <dx:ContentControl>
                            <dx:ASPxCallbackPanel ID="cbUsers" ClientInstanceName="cbUsers" runat="server" OnCallback="cbUsers_Callback" Width="100%">
                                <PanelCollection>
                                    <dx:PanelContent>
                                        <dx:ASPxFormLayout ID="formUsers" ClientInstanceName="formUsers" runat="server" DataSourceID="dsUser">
                                            <Items>
                                                <dx:LayoutGroup GroupBoxDecoration="None">
                                                    <Items>
                                                        <dx:LayoutItem Caption="Select User">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer>
                                                                    <dx:ASPxComboBox ID="ddlUsers" ClientInstanceName="ddlUsers" runat="server" DataSourceID="dsUsers" TextField="fullName" ValueField="userId">
                                                                        <ClientSideEvents SelectedIndexChanged="function(s,e){cbUsers.PerformCallback(s.GetValue());}" />
                                                                    </dx:ASPxComboBox>
                                                                    <asp:SqlDataSource ID="dsUsers" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                                                                        SelectCommand="usp_getUsers" SelectCommandType="StoredProcedure">
                                                                    </asp:SqlDataSource> 
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                    </Items>
                                                </dx:LayoutGroup>
                                                <dx:EmptyLayoutItem />
                                                <dx:LayoutGroup Caption="Enter User Details" ColumnCount="2">
                                                    <Items>
                                                        <dx:LayoutItem ColSpan="2" FieldName="userId" Visible="false">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer runat="server">
                                                                    <dx:ASPxLabel ID="lblUserId" runat="server"></dx:ASPxLabel>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                        <dx:LayoutItem ColSpan="2" Caption="Full Name" FieldName="fullName">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer runat="server">
                                                                    <dx:ASPxTextBox ID="txtUserFullName" runat="server"></dx:ASPxTextBox>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                        <dx:LayoutItem ColSpan="2" Caption="User Name" FieldName="username">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer runat="server">
                                                                    <dx:ASPxTextBox ID="txtUserName" runat="server"></dx:ASPxTextBox>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                        <dx:LayoutItem ColSpan="2" Caption="Phone Number" FieldName="phoneNumber">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer runat="server">
                                                                    <dx:ASPxTextBox ID="txtUserPhone" runat="server"></dx:ASPxTextBox>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                        <dx:LayoutItem ColSpan="2" Caption="Email Address" FieldName="emailAddress">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer runat="server">
                                                                    <dx:ASPxTextBox ID="txtUserEmail" runat="server"></dx:ASPxTextBox>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                        <dx:LayoutItem ColSpan="2" Caption="Department" FieldName="departmentId" Visible="false">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer runat="server">
                                                                    <dx:ASPxComboBox ID="ddlDepartments" ClientInstanceName="ddlDepartments" runat="server" DataSourceID="dsDepartments" TextField="department" ValueField="departmentId"></dx:ASPxComboBox>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                        <dx:LayoutItem ColSpan="2" Caption="Role" FieldName="roleId">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer runat="server">
                                                                    <dx:ASPxComboBox ID="ddlUserRoles" ClientInstanceName="ddlUserRoles" runat="server" DataSourceID="dsRoles" TextField="roleName" ValueField="roleId">
                                                                        <ClientSideEvents SelectedIndexChanged="function (s,e){ if (s.GetValue() == '0'){ tabUsers.SetActiveTabIndex(1); ddlUserRolesEdit.SetSelectedIndex(ddlUserRolesEdit.FindItemByValue(0).index);}}" />
                                                                    </dx:ASPxComboBox>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                        <dx:LayoutItem ColSpan="1" Caption="Inactive?" FieldName="inactive">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer runat="server">
                                                                    <dx:ASPxCheckBox ID="cbInactive" runat="server"></dx:ASPxCheckBox>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                        <dx:LayoutItem ColSpan="1" Caption="Restricted Data?" FieldName="isSensitiveData">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer runat="server">
                                                                    <dx:ASPxCheckBox ID="cbSensitive" runat="server" Enabled="false"></dx:ASPxCheckBox>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                        <dx:LayoutItem ColSpan="1" Caption="Education?" FieldName="isEducationVisible">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer runat="server">
                                                                    <dx:ASPxCheckBox ID="cbEducation" runat="server" Enabled="false"></dx:ASPxCheckBox>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                        <dx:LayoutItem ColSpan="1" Caption="Employment?" FieldName="isEmploymentVisible">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer runat="server">
                                                                    <dx:ASPxCheckBox ID="cbEmployment" runat="server" Enabled="false"></dx:ASPxCheckBox>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                        <dx:LayoutItem ColSpan="1" Caption="Lease?" FieldName="isLeaseVisible">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer runat="server">
                                                                    <dx:ASPxCheckBox ID="cbLease" runat="server" Enabled="false"></dx:ASPxCheckBox>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                        <dx:LayoutItem ColSpan="1" Caption="Administrative?" FieldName="isAdministrativeVisible">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer runat="server">
                                                                    <dx:ASPxCheckBox ID="cbAdministrative" runat="server" Enabled="false"></dx:ASPxCheckBox>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                    </Items>
                                                </dx:LayoutGroup>
                                                <dx:LayoutGroup Caption="ByPass Dates">
                                                    <Items>
                                                        <dx:LayoutItem ShowCaption="False" FieldName="bypassStart" HorizontalAlign="Center">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer>
                                                                    <dx:ASPxDateEdit ID="deBypassStart" runat="server"></dx:ASPxDateEdit>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                        <dx:LayoutItem ShowCaption="False" FieldName="bypassEnd" HorizontalAlign="Center">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer>
                                                                    <dx:ASPxDateEdit ID="deBypassEnd" runat="server"></dx:ASPxDateEdit>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                    </Items>
                                                </dx:LayoutGroup>
                                                <dx:LayoutGroup GroupBoxDecoration="None" ColumnCount="3">
                                                    <Items>
                                                        <dx:LayoutItem ShowCaption="False">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer>
                                                                    <dx:ASPxButton ID="btnDeleteUser" runat="server" OnClick="btnDeleteUser_Click" Text="Delete"></dx:ASPxButton>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem><dx:EmptyLayoutItem />
                                                        <dx:LayoutItem ShowCaption="False" HorizontalAlign="Right">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer>
                                                                    <dx:ASPxButton ID="btnSaveUser" runat="server" OnClick="btnSaveUser_Click" Text="Save"></dx:ASPxButton>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                    </Items>
                                                </dx:LayoutGroup>
                                            </Items>
                                        </dx:ASPxFormLayout>
                                        <asp:SqlDataSource ID="dsUser" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                                            DeleteCommand="usp_deleteUser" DeleteCommandType="StoredProcedure" OnDeleted="dsUser_Deleted" 
                                            InsertCommand="usp_addUser" InsertCommandType="StoredProcedure" OnInserting="dsUser_Inserting"
                                            SelectCommand="usp_getUser" SelectCommandType="StoredProcedure"
                                            UpdateCommand="usp_updateUser" UpdateCommandType="StoredProcedure" OnUpdating="dsUser_Updating">
                                            <InsertParameters>
                                                <asp:ControlParameter ControlID="formUsers$txtUserFullName" PropertyName="Text" Name="fullName" />
                                                <asp:ControlParameter ControlID="formUsers$txtUserName" PropertyName="Text" Name="userName" />
                                                <asp:ControlParameter ControlID="formUsers$txtUserPhone" PropertyName="Text" Name="phoneNumber" />
                                                <asp:ControlParameter ControlID="formUsers$txtUserEmail" PropertyName="Text" Name="emailAddress" />
                                                <asp:ControlParameter ControlID="formUsers$ddlUserRoles" PropertyName="Value" Name="roleId" />
                                                <asp:ControlParameter ControlID="formUsers$deBypassStart" PropertyName="Date" Name="bypassStart" />
                                                <asp:ControlParameter ControlID="formUsers$deBypassEnd" PropertyName="Date" Name="bypassEnd" />
                                                <asp:ControlParameter ControlID="formUsers$cbInactive" PropertyName="Checked" Name="inactive" />
                                            </InsertParameters>
                                            <UpdateParameters>
                                                <asp:ControlParameter ControlID="formUsers$txtUserFullName" PropertyName="Text" Name="fullName" />
                                                <asp:ControlParameter ControlID="formUsers$txtUserName" PropertyName="Text" Name="userName" />
                                                <asp:ControlParameter ControlID="formUsers$txtUserPhone" PropertyName="Text" Name="phoneNumber" />
                                                <asp:ControlParameter ControlID="formUsers$txtUserEmail" PropertyName="Text" Name="emailAddress" />
                                                <asp:ControlParameter ControlID="formUsers$ddlUserRoles" PropertyName="Value" Name="roleId" />
                                                <asp:ControlParameter ControlID="formUsers$ddlUsers" PropertyName="Value" Name="userId" />
                                                <asp:ControlParameter ControlID="formUsers$deBypassStart" PropertyName="Date" Name="bypassStart" />
                                                <asp:ControlParameter ControlID="formUsers$deBypassEnd" PropertyName="Date" Name="bypassEnd" />
                                                <asp:ControlParameter ControlID="formUsers$cbInactive" PropertyName="Checked" Name="inactive" />
                                            </UpdateParameters>
                                            <DeleteParameters>
                                                <asp:ControlParameter ControlID="formUsers$ddlUsers" PropertyName="Value" Name="userId" />
                                                <asp:Parameter Name="count" Direction="Output" DbType="Int16" Size="4" />
                                            </DeleteParameters>
                                            <SelectParameters>
                                                <asp:Parameter Name="userId" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                        <asp:SqlDataSource ID="dsRoles" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                                            SelectCommand="usp_getUserRolesAdmin" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                                        <asp:SqlDataSource ID="dsDepartments" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                                            SelectCommand="usp_getDepartments" SelectCommandType="StoredProcedure">
                                            <SelectParameters>
                                                <asp:Parameter Name="includeInactive" DefaultValue="1" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxCallbackPanel>
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
                <dx:TabPage Name="roles" Text="User Roles">
                    <ContentCollection>
                        <dx:ContentControl>
                            <dx:ASPxCallbackPanel ID="cbUserRoles" ClientInstanceName="cbUserRoles" runat="server" OnCallback="cbUserRoles_Callback" Width="100%">
                                <PanelCollection>
                                    <dx:PanelContent>
                                        <dx:ASPxFormLayout ID="formUserRoles" ClientInstanceName="formUserRoles" runat="server" DataSourceID="dsUserRole">
                                            <Items>
                                                <dx:LayoutGroup GroupBoxDecoration="None">
                                                    <Items>
                                                        <dx:LayoutItem Caption="Select User Role">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer>
                                                                    <dx:ASPxComboBox ID="ddlUserRolesEdit" ClientInstanceName="ddlUserRolesEdit" runat="server" DataSourceID="dsUserRoles" TextField="roleName" ValueField="roleId">
                                                                        <ClientSideEvents SelectedIndexChanged="function(s,e){cbUserRoles.PerformCallback(s.GetValue());}" />
                                                                    </dx:ASPxComboBox>
                                                                    <asp:SqlDataSource ID="dsUserRoles" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                                                                        SelectCommand="usp_getUserRolesAdmin" SelectCommandType="StoredProcedure">
                                                                    </asp:SqlDataSource> 
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                    </Items>
                                                </dx:LayoutGroup>
                                                <dx:EmptyLayoutItem />
                                                <dx:LayoutGroup Caption="Enter Role Details">
                                                    <Items>
                                                        <dx:LayoutItem ColSpan="1" FieldName="roleId" Visible="false">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer runat="server">
                                                                    <dx:ASPxLabel ID="lblRoleId" runat="server"></dx:ASPxLabel>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                        <dx:LayoutItem ColSpan="1" Caption="Role Name" FieldName="roleName">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer runat="server">
                                                                    <dx:ASPxTextBox ID="txtRoleName" runat="server"></dx:ASPxTextBox>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                        <dx:LayoutItem ColSpan="1" Caption="LDAP Group" FieldName="ldapGroup">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer runat="server">
                                                                    <dx:ASPxTextBox ID="txtLdapGroup" runat="server"></dx:ASPxTextBox>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                        <dx:LayoutItem ColSpan="1" Caption="Restricted Data" FieldName="securityLevelId">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer runat="server">
                                                                    <dx:ASPxComboBox ID="ddlSecurityLevelId" runat="server">
                                                                        <Items>
                                                                            <dx:ListEditItem Text="No" Value="0" />
                                                                            <dx:ListEditItem Text="Yes" Value="1" />
                                                                        </Items>
                                                                    </dx:ASPxComboBox>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                        <dx:LayoutItem ColSpan="1" Caption="Inactive?" FieldName="inactive">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer runat="server">
                                                                    <dx:ASPxCheckBox ID="cbInactiveRole" runat="server"></dx:ASPxCheckBox>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                    </Items>
                                                </dx:LayoutGroup>
                                                <dx:LayoutGroup GroupBoxDecoration="None" ColumnCount="3">
                                                    <Items>
                                                        <dx:LayoutItem ShowCaption="False">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer>
                                                                    <dx:ASPxButton ID="btnDeleteUserRole" runat="server" OnClick="btnDeleteUserRole_Click" Text="Delete"></dx:ASPxButton>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem><dx:EmptyLayoutItem />
                                                        <dx:LayoutItem ShowCaption="False" HorizontalAlign="Right">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer>
                                                                    <dx:ASPxButton ID="btnSaveUserRole" runat="server" OnClick="btnSaveUserRole_Click" Text="Save"></dx:ASPxButton>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                    </Items>
                                                </dx:LayoutGroup>
                                            </Items>
                                        </dx:ASPxFormLayout>
                                        <asp:SqlDataSource ID="dsUserRole" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                                            DeleteCommand="usp_deleteUserRole" DeleteCommandType="StoredProcedure" OnDeleted="dsUserRole_Deleted" 
                                            InsertCommand="usp_addUserRole" InsertCommandType="StoredProcedure" 
                                            SelectCommand="usp_getUserRole" SelectCommandType="StoredProcedure"
                                            UpdateCommand="usp_updateUserRole" UpdateCommandType="StoredProcedure">
                                            <InsertParameters>
                                                <asp:ControlParameter ControlID="formUserRoles$txtRoleName" PropertyName="Text" Name="roleName" />
                                                <asp:ControlParameter ControlID="formUserRoles$txtLdapGroup" PropertyName="Text" Name="ldapGroup" />
                                                <asp:ControlParameter ControlID="formUserRoles$ddlSecurityLevelId" PropertyName="Value" Name="securityLevelId" />
                                                <asp:ControlParameter ControlID="formUserRoles$cbInactiveRole" PropertyName="Checked" Name="inactive" />
                                            </InsertParameters>
                                            <UpdateParameters>
                                                <asp:ControlParameter ControlID="formUserRoles$txtRoleName" PropertyName="Text" Name="roleName" />
                                                <asp:ControlParameter ControlID="formUserRoles$txtLdapGroup" PropertyName="Text" Name="ldapGroup" />
                                                <asp:ControlParameter ControlID="formUserRoles$ddlSecurityLevelId" PropertyName="Value" Name="securityLevelId" />
                                                <asp:ControlParameter ControlID="formUserRoles$ddlUserRolesEdit" PropertyName="Value" Name="roleId" />
                                                <asp:ControlParameter ControlID="formUserRoles$cbInactiveRole" PropertyName="Checked" Name="inactive" />
                                            </UpdateParameters>
                                            <DeleteParameters>
                                                <asp:ControlParameter ControlID="formUserRoles$ddlUserRolesEdit" PropertyName="Value" Name="roleId" />
                                                <asp:Parameter Name="count" Direction="Output" DbType="Int16" Size="4" />
                                            </DeleteParameters>
                                            <SelectParameters>
                                                <asp:Parameter Name="roleId" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxCallbackPanel>
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
            </TabPages>
        </dx:ASPxPageControl>
    </div>
</asp:Content>