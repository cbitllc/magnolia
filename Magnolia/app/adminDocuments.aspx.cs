﻿using System;
using DevExpress.Web;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Magnolia.app
{
    public partial class adminDocuments : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ((Master)Page.Master).SetHeader = "Admin Documents";
            ((Master)Page.Master).SetHelpVisible = false;
        }

        //protected void gvDocuments_CellEditorInitialize(object sender, DevExpress.Web.ASPxGridViewEditorEventArgs e)
        //{
        //    ASPxGridView gridView = sender as ASPxGridView;
        //    if (gridView.IsEditing && e.Column.FieldName == "contractId")
        //    {
        //        ASPxComboBox comboFolder = e.Editor as ASPxComboBox;
        //        comboFolder.Callback += comboFolder_OnCallback;
        //        var currentContractTypeId = gridView.GetRowValues(e.VisibleIndex, "contractTypeId");
        //        if (e.KeyValue != DBNull.Value && e.KeyValue != null && currentContractTypeId != null && currentContractTypeId != DBNull.Value)
        //        {
        //            FillFolderCombo(comboFolder, currentContractTypeId.ToString());
        //        }
        //        else
        //        {
        //            comboFolder.DataSourceID = null;
        //            comboFolder.Items.Clear();
        //        }
        //    }

        //}

        //protected void FillFolderCombo(ASPxComboBox cmb, string contractTypeId)
        //{
        //    if (string.IsNullOrEmpty(contractTypeId)) return;

        //    cmb.DataSourceID = null;
        //    dsFolders.SelectParameters["contractTypeId"].DefaultValue = contractTypeId;
        //    cmb.DataSource = dsFolders;
        //    cmb.DataBindItems();
        //}
        //void comboFolder_OnCallback(object source, CallbackEventArgsBase e)
        //{
        //    FillFolderCombo(source as ASPxComboBox, e.Parameter);
        //}

        protected void dsFolders_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            string x = GetQuery(e.Command.CommandText, e.Command.Parameters);
        }

        private string GetQuery(string CommandQuery, System.Data.Common.DbParameterCollection parmCollection)
        {
            string query = CommandQuery;
            string parms = "";
            foreach (System.Data.SqlClient.SqlParameter parm in parmCollection)
            {
                parms += parm.ParameterName + " = ";
                if (parm.Value == null)
                {
                    parms += "null,";

                }
                else
                {
                    if (parm.DbType == System.Data.DbType.String || parm.DbType == System.Data.DbType.DateTime)
                    {
                        parms += "'" + parm.Value.ToString() + "',";
                    }
                    else
                    {
                        parms += parm.Value.ToString() + ",";
                    }
                }
            }
            if (parms.Length > 0)
                query = query + " " + parms.Substring(0, parms.Length - 1);
            return query;
        }

        protected void dsDocuments_Updating(object sender, SqlDataSourceCommandEventArgs e)
        {
            string x = GetQuery(e.Command.CommandText, e.Command.Parameters);
        }
    }
}