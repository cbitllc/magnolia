﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.IO;
using DevExpress.Web;
using DevExpress.Pdf;
using System.Web.UI.WebControls;

namespace Magnolia.app
{

    public partial class OCR : System.Web.UI.Page
    {
        protected string SubmissionID
        {
            get
            {
                return HiddenField.Get("SubmissionID").ToString();
            }
            set
            {
                HiddenField.Set("SubmissionID", value);
            }
        }

        UploadedFilesStorage UploadedFilesStorage
        {
            get { return UploadControlHelper.GetUploadedFilesStorageByKey(SubmissionID); }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SubmissionID = UploadControlHelper.GenerateUploadedFilesStorageKey();
                UploadControlHelper.AddUploadedFilesStorage(SubmissionID);
                ((Master)Page.Master).SetHeader = "Upload Existing Contract";

            }

        }

        protected void dsDocuments_Deleting(object sender, SqlDataSourceCommandEventArgs e)
        {
            //Int32 id = Convert.ToInt32(e.Command.Parameters["@documentId"].Value);
            //Int32 idx = gvDocuments.FindVisibleIndexByKeyValue(id);
            //string[] fields = { "path", "fileName" };
            //object[] values = gvDocuments.GetRowValues(idx, fields) as object[];
            //if (values != null)
            //{
            //    File.Delete(Server.MapPath(values[0] + "/" + values[1]));
            //}
        }


        protected void DocumentsUploadControl_FileUploadComplete(object sender, DevExpress.Web.FileUploadCompleteEventArgs e)
        {
            bool isSubmissionExpired = false;
            if (UploadedFilesStorage == null)
            {
                isSubmissionExpired = true;
                UploadControlHelper.AddUploadedFilesStorage(SubmissionID);
            }
            UploadedFileInfo tempFileInfo = UploadControlHelper.AddUploadedFileInfo(SubmissionID, e.UploadedFile.FileName);
            e.UploadedFile.SaveAs(tempFileInfo.FilePath);
            if (e.IsValid)
                e.CallbackData = tempFileInfo.UniqueFileName + "|" + isSubmissionExpired;
        }

        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            ValidateInputData();
            List<UploadedFileInfo> resultFileInfos = new List<UploadedFileInfo>();
            string description = DescriptionTextBox.Text;
            bool allFilesExist = true;
            if (UploadedFilesStorage == null)
                UploadedFilesTokenBox.Tokens = new TokenCollection();
            foreach (string fileName in UploadedFilesTokenBox.Tokens)
            {
                UploadedFileInfo demoFileInfo = UploadControlHelper.GetDemoFileInfo(SubmissionID, fileName);
                FileInfo fileInfo = new FileInfo(demoFileInfo.FilePath);
                if (fileInfo.Exists)
                {
                    //demoFileInfo.FileSize = Utils.FormatSize(fileInfo.Length);
                    resultFileInfos.Add(demoFileInfo);
                }
                else
                    allFilesExist = false;
            }

            if (allFilesExist && resultFileInfos.Count > 0)
            {
                ProcessSubmit(description, resultFileInfos);
                UploadControlHelper.RemoveUploadedFilesStorage(SubmissionID);
                ASPxEdit.ClearEditorsInContainer(FormLayout, true);
            }
            else
            {
                UploadedFilesTokenBox.ErrorText = "Submit failed because files have been removed from the server due to the 5 minute timeout.";
                UploadedFilesTokenBox.IsValid = false;
            }
        }

        void ValidateInputData()
        {
            bool isInvalid = string.IsNullOrEmpty(DescriptionTextBox.Text) || UploadedFilesTokenBox.Tokens.Count == 0;
            if (isInvalid)
                throw new Exception("Invalid input data");
        }

        protected void ProcessSubmit(string description, List<UploadedFileInfo> fileInfos)
        {
            //DescriptionLabel.Value = Server.HtmlEncode(description);
            foreach (UploadedFileInfo fileInfo in fileInfos)
            {
                // process uploaded files here
                string dir = Server.MapPath("/Documents/" + txtContractId.Text);
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
                byte[] fileContent = File.ReadAllBytes(fileInfo.FilePath);
                using (var stream = File.Open(fileInfo.FilePath, FileMode.Open))
                {
                    // Use stream
                    //FileStream fc = new FileStream(fileInfo.FilePath, FileMode.OpenOrCreate);
                    if (stream.Length > 0)
                    {
                        using (var fc = File.Create(dir + "/" + fileInfo.OriginalFileName))
                        {
                            stream.Seek(0, SeekOrigin.Begin);
                            stream.CopyTo(fc);
                        }
                        //fc.Write(fileContent, 0, fileContent.Length);
                        //File.WriteAllBytes(dir, fileContent);
                    }
                }
                string path = "/Documents/" + txtContractId.Text + "/";
                string fileName = path + fileInfo.OriginalFileName;
                dsDocuments.InsertParameters["fileName"].DefaultValue = fileInfo.OriginalFileName;
                dsDocuments.InsertParameters["path"].DefaultValue = path;
                dsDocuments.InsertParameters["description"].DefaultValue = DescriptionTextBox.Text;
                dsDocuments.Insert();
                string fileText = ExtractTextFromPDF(fileName);
            }
            //SubmittedFilesListBox.DataSource = fileInfos;
            //SubmittedFilesListBox.DataBind();
            //FormLayout.FindItemOrGroupByName("ResultGroup").Visible = true;
        }

        string ExtractTextFromPDF(string filePath)
        {
            string documentText = "";
            try
            {
                using (PdfDocumentProcessor documentProcessor = new PdfDocumentProcessor())
                {
                    documentProcessor.LoadDocument(Server.MapPath(filePath));
                    documentText = documentProcessor.Text;
                    PdfFormData formData = documentProcessor.GetFormData();
                    IList<string> names = formData.GetFieldNames();
                }
            }
            catch (Exception ex) {
                documentText = ex.Message;
            }
            return documentText;
        }

    }
}