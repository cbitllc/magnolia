﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="adminPartyRequests.aspx.cs" Inherits="Magnolia.app.adminPartyRequests" MasterPageFile="~/app/Master.master" %>
<%@ Register Assembly="DevExpress.Web.v20.2, Version=20.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <style>
        input {text-transform:uppercase;}
        textarea {text-transform:uppercase;}
        span {text-transform:uppercase;}
        .spanTitle{text-transform:capitalize;}
        .gridHeader {  
            background-color: white;  
            color:black;
        }  
        .gridHeader a {  
            color: black;  
        }  
    </style>
    <script type="text/javascript">
        function AdjustHeight(s, e) {
            gvPartyRequests.SetHeight(window.innerHeight * .75);
            gvPartyRequests.SetWidth(window.innerWidth * .9);
        }
    </script>
        <div style="overflow:auto;">
            <dx:ASPxGridView ID="gvPartyRequests" ClientInstanceName="gvPartyRequests" runat="server" DataSourceID="dsPartyRequests"  KeyFieldName="requestId" Settings-VerticalScrollBarMode="Auto" Settings-HorizontalScrollBarMode="Auto" OnHtmlRowCreated="gvPartyRequests_HtmlRowCreated">
            <ClientSideEvents Init="AdjustHeight" />
                <Columns>
                    <dx:GridViewDataColumn FieldName="requestId" Visible="false"></dx:GridViewDataColumn>
                    <dx:GridViewDataComboBoxColumn FieldName="requestStatusId" Caption="Status" HeaderStyle-CssClass="gridHeader" SettingsHeaderFilter-Mode="CheckedList" Width="70">
                        <PropertiesComboBox ValueField="requestStatusId" TextField="requestStatus" DataSourceID="dsPartyRequestStatus"></PropertiesComboBox>
                    </dx:GridViewDataComboBoxColumn>
                    <dx:GridViewDataColumn FieldName="businessName" Caption="Business Name" Name="businessName" HeaderStyle-CssClass="gridHeader" SettingsHeaderFilter-Mode="CheckedList"></dx:GridViewDataColumn>
                    <dx:GridViewDataCheckColumn FieldName="isCorporation" Caption="Corp?" HeaderStyle-CssClass="gridHeader" Width="70" HeaderStyle-HorizontalAlign="Center"></dx:GridViewDataCheckColumn>
                    <dx:GridViewDataCheckColumn FieldName="isCorporationVerified" Caption="Corporation<br/>Verified?" HeaderStyle-CssClass="gridHeader" HeaderStyle-HorizontalAlign="Center" Visible="false"></dx:GridViewDataCheckColumn>
                    <dx:GridViewDataDateColumn FieldName="corporationVerifiedDate" Caption="Corporation<br/>Verified<br/>Date" HeaderStyle-CssClass="gridHeader" HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center" Visible="false"></dx:GridViewDataDateColumn>
                    <dx:GridViewDataColumn FieldName="contactName" Caption="Contact Name" HeaderStyle-CssClass="gridHeader" SettingsHeaderFilter-Mode="CheckedList"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn FieldName="address" Caption="Address" HeaderStyle-CssClass="gridHeader" SettingsHeaderFilter-Mode="CheckedList"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn FieldName="city" Caption="City" HeaderStyle-CssClass="gridHeader" SettingsHeaderFilter-Mode="CheckedList"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn FieldName="state" Caption="State" HeaderStyle-CssClass="gridHeader" SettingsHeaderFilter-Mode="CheckedList" Width="50"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn FieldName="zip" Caption="Zip<br/>Code" HeaderStyle-CssClass="gridHeader" SettingsHeaderFilter-Mode="CheckedList" HeaderStyle-HorizontalAlign="Center" Width="50"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn FieldName="phone" Caption="Phone<br/>Number" HeaderStyle-CssClass="gridHeader" SettingsHeaderFilter-Mode="CheckedList" HeaderStyle-HorizontalAlign="Center"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn FieldName="email" Caption="Email<br/>Address" HeaderStyle-CssClass="gridHeader" SettingsHeaderFilter-Mode="CheckedList" HeaderStyle-HorizontalAlign="Center"></dx:GridViewDataColumn>
                    <dx:GridViewDataCheckColumn FieldName="isPaid" Caption="Paid by AP?"  HeaderStyle-CssClass="gridHeader" SettingsHeaderFilter-Mode="CheckedList" HeaderStyle-HorizontalAlign="Center"></dx:GridViewDataCheckColumn>
                    <dx:GridViewDataComboBoxColumn FieldName="partyTypeId" PropertiesComboBox-DataSourceID="dsPartyTypes" Caption="Party Types" HeaderStyle-CssClass="gridHeader" SettingsHeaderFilter-Mode="CheckedList">
                        <PropertiesComboBox TextField="partyType" ValueField="partyTypeId"></PropertiesComboBox>
                    </dx:GridViewDataComboBoxColumn>
                    <dx:GridViewDataColumn FieldName="requestedBy" Caption="Requestor" HeaderStyle-CssClass="gridHeader" SettingsHeaderFilter-Mode="CheckedList" HeaderStyle-HorizontalAlign="Center"></dx:GridViewDataColumn>
                    <dx:GridViewDataDateColumn FieldName="requestDate" Caption="Request<br/>Date" CellStyle-HorizontalAlign="Right" Settings-FilterMode="DisplayText" HeaderStyle-CssClass="gridHeader" HeaderStyle-HorizontalAlign="Center" PropertiesDateEdit-DisplayFormatString="MM/dd/yyyy"></dx:GridViewDataDateColumn>
                    <dx:GridViewDataColumn FieldName="reviewer" Caption="Reviewer" HeaderStyle-CssClass="gridHeader" SettingsHeaderFilter-Mode="CheckedList" HeaderStyle-HorizontalAlign="Center"></dx:GridViewDataColumn>
                    <dx:GridViewDataDateColumn FieldName="reviewedDate" Caption="Review<br/>Date" CellStyle-HorizontalAlign="Center" Settings-FilterMode="DisplayText" HeaderStyle-CssClass="gridHeader" HeaderStyle-HorizontalAlign="Center" PropertiesDateEdit-DisplayFormatString="MM/dd/yyyy" Width="70"></dx:GridViewDataDateColumn>
                    <dx:GridViewDataColumn FieldName="requestedById" Visible="false"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn FieldName="requestedByEmail" Visible="false"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Documents" HeaderStyle-CssClass="gridHeader"></dx:GridViewDataColumn>
                </Columns>
                <SettingsEditing Mode="Batch" BatchEditSettings-StartEditAction="Click"></SettingsEditing>
                <SettingsDataSecurity AllowEdit="true" />
                <Settings ShowHeaderFilterButton="true" />
            </dx:ASPxGridView>
            <asp:SqlDataSource ID="dsPartyRequests" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                UpdateCommand="usp_savePartyRequest" UpdateCommandType="StoredProcedure" OnUpdated="dsPartyRequests_Updated"
                SelectCommand="usp_getPartyRequests" SelectCommandType="StoredProcedure">
                <UpdateParameters>
                    <asp:Parameter Name="requestId" />
                    <asp:Parameter Name="businessName" />
                    <asp:Parameter Name="contactName" />
                    <asp:Parameter Name="address" />
                    <asp:Parameter Name="city" />
                    <asp:Parameter Name="state" />
                    <asp:Parameter Name="zip" />
                    <asp:Parameter Name="phone" />
                    <asp:Parameter Name="email" />
                    <asp:Parameter Name="partyTypeId" />
                    <asp:Parameter Name="requestStatusId" />
                    <asp:SessionParameter Name="reviewedBy" SessionField="UserID" />
                    <asp:Parameter Name="isCorporation" />
                    <asp:Parameter Name="isCorporationVerified" />
                    <asp:Parameter Name="corporationVerifiedDate" />
                    <asp:Parameter Name="isPaid" />
                    <asp:Parameter Name="newPartyId" Direction="Output" DbType="Int16" Size="4" />
                </UpdateParameters>
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="dsPartyRequestStatus" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                SelectCommand="usp_getPartyRequetTypes" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
            <asp:SqlDataSource ID="dsPartyTypes" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                SelectCommand="usp_getPartyTypes" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </div>
</asp:Content>