﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="diagramViewer.aspx.cs" Inherits="Magnolia.app.diagramViewer" %>
<%@ Register Assembly="DevExpress.Web.v20.2, Version=20.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxDiagram.v20.2, Version=20.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxDiagram" TagPrefix="dx" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="overflow:auto;height:100%">
            <dx:ASPxLabel ID="lblTest" ClientInstanceName="lblTest" runat="server" ClientVisible="false"></dx:ASPxLabel>
                <dx:ASPxDiagram ID="Diagram" ClientInstanceName="Diagram" runat="server" NodeDataSourceID="SqlDataSource1" AutoZoom="FitContent" ReadOnly="true" SimpleView="true" EnableTheming="true" Theme="Material">
                    <Mappings>
                        <Node Key="roleId" Text="roleName" ParentKey="parentId" TextStyle="{font-size:10pt,text-wrap:normal;word-wrap: break-word;}" />
                    </Mappings>
                    <SettingsToolbox Visibility="Disabled"></SettingsToolbox>
                    <SettingsSidePanel Visibility="Disabled" />
                    <SettingsAutoLayout Type="Auto" Orientation="Vertical"/>
                    <SettingsGrid Visible="false" />
            </dx:ASPxDiagram>
            <br /><br />
            <div style="width:50%;left:25%;text-align:center;">
                <dx:ASPxLabel ID="lblDescription" runat="server"></dx:ASPxLabel>
            </div>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                SelectCommand="usp_getRoute" SelectCommandType="StoredProcedure">
                <SelectParameters>
                    <asp:QueryStringParameter QueryStringField="id" Name="routingId" />
                </SelectParameters>
            </asp:SqlDataSource>
        </div>
    </form>
</body>
</html>
