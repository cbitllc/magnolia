﻿using System;
using DevExpress.Web;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Magnolia.app
{
    public partial class adminRoutingManagement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ((Master)Page.Master).SetHeader = "Admin Routing Configuration";
            //((Master)Page.Master).SetHelpURL = "admin.aspx";
            ((Master)Page.Master).SetHelpVisible = false;
            if (Session["isAdmin"] == null || Session["isAdmin"] == System.DBNull.Value || Convert.ToBoolean(Session["isAdmin"]) == false)
            {
                Response.Redirect("default.aspx");
            }
        }

        protected void gvConfig_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            ASPxGridView gridView = sender as ASPxGridView;
            if (gridView.IsEditing && e.Column.FieldName == "configurationId")
            {
                ASPxComboBox comboConfigName = e.Editor as ASPxComboBox;
                comboConfigName.Callback += comboConfigName_OnCallback;
                var currentConfigType = gridView.GetRowValues(e.VisibleIndex, "configurationType");
                if (hasValidationErrors)
                    FIllConfigNameCombo(comboConfigName, lastValidConfigType);
                else
                    if (e.KeyValue != DBNull.Value && e.KeyValue != null && currentConfigType != null && currentConfigType != DBNull.Value)
                {
                    FIllConfigNameCombo(comboConfigName, currentConfigType.ToString());
                }
                else
                {
                    comboConfigName.DataSourceID = null;
                    comboConfigName.Items.Clear();
                }
            }
        }

        protected void FIllConfigNameCombo(ASPxComboBox cmb, string configType)
        {
            if (string.IsNullOrEmpty(configType)) return;

            cmb.DataSourceID = null;
            DataView dv = (DataView)dsConfigNames.Select(DataSourceSelectArguments.Empty);
            //DataView dv = ds.Tables[0].DefaultView;
            dv.RowFilter = "configurationType='" + configType + "'";
            cmb.DataSource = dv;
            cmb.DataBindItems();
        }
        void comboConfigName_OnCallback(object source, CallbackEventArgsBase e)
        {
            FIllConfigNameCombo(source as ASPxComboBox, e.Parameter);
        }

        string lastValidConfigType = null;
        bool hasValidationErrors = false;

        protected void gvConfig_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            ASPxGridView gridView = sender as ASPxGridView;
            if (gridView.IsNewRowEditing)
            {
                ((GridViewDataComboBoxColumn)gridView.Columns["configurationType"]).EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.True;
                ((GridViewDataComboBoxColumn)gridView.Columns["configurationName"]).EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.True;
            }
            else
            {
                ((GridViewDataComboBoxColumn)gridView.Columns["Type"]).EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;
                ((GridViewDataComboBoxColumn)gridView.Columns["Name"]).EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;
            }
        }

        private void Page_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError();
            if (exc.Message.Contains("Violation"))
            {
                Message.Alert("The selected Type, Name and Route configuration already exists. Please edit the existing route.");
            }
            else
            {
                Message.Alert(exc.Message);

                // Handle specific exception.
                if (exc is System.Web.HttpUnhandledException)
                {
                    Message.LogError("adminRoutingManagement", "Page_Error", Session["UserID"].ToString(), exc.Message);
                    Message.Alert("An error occurred on this page. Please verify your information to resolve the issue.");
                }
                // Clear the error from the server.
            }
            Server.ClearError();
        }

        protected void gvConfig_RowInserted(object sender, DevExpress.Web.Data.ASPxDataInsertedEventArgs e)
        {
            if (e.Exception != null)
            {
                if (e.Exception.Message.Contains("Violation"))
                {
                    Message.Alert("The selected Type, Name and Route configuration already exists. Please edit the existing route.");
                }
                else
                {
                    Message.Alert(e.Exception.Message);
                }
                Server.ClearError();
            }
        }

        protected void gvConfig_RowUpdated(object sender, DevExpress.Web.Data.ASPxDataUpdatedEventArgs e)
        {
            if (e.Exception != null)
            {
                if (e.Exception.Message.Contains("Violation"))
                {
                    Message.Alert("The selected Type, Name and Route configuration already exists. Please edit the existing route.");
                }
                else
                {
                    Message.Alert(e.Exception.Message);
                }
                Server.ClearError();
            }
        }


        //protected void gvConfig_HtmlEditFormCreated(object sender, ASPxGridViewEditFormEventArgs e)
        //{
        //    ASPxGridView gridView = sender as ASPxGridView;
        //    if (gridView.IsNewRowEditing)
        //    {
        //        int idx = gridView.EditingRowVisibleIndex;
        //        gridView.Columns[""]
        //    }
        //}
        //protected void grid_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
        //{
        //    string newCustomerName = e.NewValues["CustomerName"].ToString();
        //    if (newCustomerName.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries).Length != 2)
        //    {
        //        e.Errors[(sender as ASPxGridView).Columns["CustomerName"]] = "The customer name should be in the following format: <First Name> <Last Name>";
        //        hasValidationErrors = true;
        //        lastValidConfigType = e.NewValues["configurationType"].ToString();
        //    }
        //}
    }
}