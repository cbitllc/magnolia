﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="adminDocuments.aspx.cs" Inherits="Magnolia.app.adminDocuments" MasterPageFile="~/app/Master.master" %>
<%@ MasterType VirtualPath="~/app/Master.master" %>
<%@ Register Assembly="DevExpress.Web.v20.2, Version=20.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.Bootstrap.v20.2, Version=20.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.Bootstrap" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div style="overflow:auto">
    <style>
        input {text-transform:uppercase;}
        textarea {text-transform:uppercase;}
        span {text-transform:uppercase;}
        .spanTitle{text-transform:capitalize;}
        .gridHeader {  
            background-color: white;  
            color:black;
        }  
        .gridHeader a {  
            color: black;  
        }  
    </style>
    <script type="text/javascript">
        function AdjustSize(s, e) {
            gvDocuments.SetHeight(window.innerHeight * .75);
            gvDocuments.SetWidth(window.innerWidth * .90)
        }
        var isResetRequired = false;
        function onSelectedContractTypeChanged(s, e) {
            isResetRequired = true;
            gvDocuments.GetEditor("contractId").PerformCallback(s.GetValue());
        }
        function onFolderEndCallback(s, e) {
            if (isResetRequired) {
                isResetRequired = false;
                s.SetSelectedIndex(0);
            }
        }
    </script>
    <dx:ASPxGridView ID="gvDocuments" ClientInstanceName="gvDocuments" runat="server" DataSourceID="dsDocuments" KeyFieldName="documentId" EditFormLayoutProperties-EncodeHtml="false"><%-- OnCellEditorInitialize="gvDocuments_CellEditorInitialize"--%>
        <ClientSideEvents Init="AdjustSize" />
        <SettingsAdaptivity AdaptivityMode="HideDataCells" AllowOnlyOneAdaptiveDetailExpanded="true" AllowHideDataCellsByColumnMinWidth="true"></SettingsAdaptivity>
        <Columns>
            <dx:BootstrapGridViewCommandColumn ShowEditButton="true" Width="40"></dx:BootstrapGridViewCommandColumn>
            <dx:GridViewDataColumn FieldName="documentId" Visible="false"></dx:GridViewDataColumn>
            <dx:GridViewDataComboBoxColumn FieldName="contractTypeId" Caption="Contract Type" PropertiesComboBox-DataSourceID="dsContractTypes" PropertiesComboBox-TextField="contractType" PropertiesComboBox-ValueField="contractTypeId">
                <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
            </dx:GridViewDataComboBoxColumn><%-- PropertiesComboBox-ClientSideEvents-SelectedIndexChanged="onSelectedContractTypeChanged"--%>
            <dx:GridViewDataSpinEditColumn FieldName="contractId" Caption="Folder" Width="75" Settings-SortMode="Value">
                <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
            </dx:GridViewDataSpinEditColumn><%-- PropertiesComboBox-DataSourceID="dsFolders" PropertiesComboBox-TextField="contractId" PropertiesComboBox-ValueType="System.Int32" PropertiesComboBox-ValueField="contractId" PropertiesComboBox-ClientSideEvents-EndCallback="onFolderEndCallback"></dx:GridViewDataSpinEditColumn>--%>
            <dx:GridViewDataHyperLinkColumn FieldName="url" Caption="File Name" EditFormSettings-Visible="False">
                <PropertiesHyperLinkEdit TextField="fileName" Target="_blank"></PropertiesHyperLinkEdit>
                <Settings FilterMode="DisplayText" />
                <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
            </dx:GridViewDataHyperLinkColumn>
            <dx:GridViewDataColumn FieldName="newPath" Caption="Path" Visible="false" EditFormSettings-Visible="True"></dx:GridViewDataColumn>
            <dx:GridViewDataComboBoxColumn FieldName="documentTypeId" Caption="File<br>Type" Width="60" PropertiesComboBox-DataSourceID="dsDocumentType" PropertiesComboBox-TextField="documentType" PropertiesComboBox-ValueField="documentTypeId"></dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataMemoColumn FieldName="description" Caption="Description" AllowTextTruncationInAdaptiveMode="true"></dx:GridViewDataMemoColumn>
            <dx:GridViewDataDateColumn FieldName="uploadDate" Caption="Upload Date"></dx:GridViewDataDateColumn>
            <dx:GridViewDataComboBoxColumn FieldName="uploadId" Caption="Upload User" PropertiesComboBox-DataSourceID="dsUsers" PropertiesComboBox-TextField="fullName" PropertiesComboBox-ValueField="userId">
                <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn FieldName="uploadDocumentTypeId" Caption="Document Type" PropertiesComboBox-DataSourceID="dsUploadDocumentType" PropertiesComboBox-TextField="uploadDocumentType" PropertiesComboBox-ValueField="uploadDocumentTypeId">
                <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn FieldName="partyId" Caption="Vendor" MinWidth="200" PropertiesComboBox-DataSourceID="dsParties" PropertiesComboBox-TextField="businessName" PropertiesComboBox-ValueField="partyId">
                <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewCommandColumn ShowDeleteButton="true" CellStyle-HorizontalAlign="Left" Caption=" " HeaderStyle-CssClass="gridHeader" Width="60"></dx:GridViewCommandColumn>
        </Columns>
        <Settings ShowHeaderFilterButton="true" VerticalScrollBarMode="Auto" />
        <SettingsBehavior ConfirmDelete="true" AllowEllipsisInText="true"/>
        <SettingsDataSecurity AllowDelete="true" AllowEdit="true" />
        <SettingsEditing Mode="PopupEditForm" UseFormLayout="true" EditFormColumnCount="1"></SettingsEditing>
        <SettingsResizing ColumnResizeMode="Control" Visualization="Live" />
        <SettingsPager PageSize="25"></SettingsPager>
        <SettingsPopup>
            <EditForm>
                <SettingsAdaptivity Mode="Always" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" MinWidth="800" MinHeight="400" />
            </EditForm>
        </SettingsPopup>
        <SettingsText ConfirmDelete="Are you sure you want to delete this document?" />
        <Styles Header-Font-Bold="true"></Styles>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="dsDocuments" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
        SelectCommand="usp_getDocumentsAdmin" SelectCommandType="StoredProcedure"
        UpdateCommand="usp_updateDocument" UpdateCommandType="StoredProcedure" OnUpdating="dsDocuments_Updating"
        InsertCommand="usp_addDocument" InsertCommandType="StoredProcedure"
        DeleteCommand="usp_deleteDocument" DeleteCommandType="StoredProcedure">
        <UpdateParameters>
            <asp:Parameter Name="uploadDocumentTypeId" />
            <asp:SessionParameter Name="uploadId" SessionField="UserID" />
            <asp:Parameter Name="documentId" />
            <asp:Parameter Name="contractTypeId" />
            <asp:Parameter Name="partyId" />
            <asp:Parameter Name="newPath" />
            <asp:Parameter Name="contractId" DefaultValue="0" />
            <asp:Parameter Name="documentTypeId" />
            <asp:Parameter Name="description" />
            <asp:Parameter Name="folder" Direction="Output" DbType="String" Size="20" />
            <asp:Parameter Name="prevFolder" Direction="Output" DbType="String" Size="20" />
        </UpdateParameters>
        <InsertParameters>
            <asp:Parameter Name="fileName" />
            <asp:Parameter Name="path" />
            <asp:Parameter Name="uploadDocumentTypeId" />
            <asp:Parameter Name="contractId" />
            <asp:QueryStringParameter Name="contractId" QueryStringField="id" />
            <asp:SessionParameter Name="uploadId" SessionField="UserID" />
            <asp:Parameter Name="partyId" />
            <asp:Parameter Name="contractTypeId" />
        </InsertParameters>
        <DeleteParameters>
            <asp:Parameter Name="documentId" />
        </DeleteParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="dsContractTypes" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" SelectCommand="usp_getContractTypes" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
    <asp:SqlDataSource ID="dsFolders" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
        SelectCommand="select distinct contractId from documents where contractTypeId = @contractTypeId" SelectCommandType="Text" OnSelecting="dsFolders_Selecting">
        <SelectParameters>
            <asp:Parameter Name="contractTypeId" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="dsDocumentType" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" SelectCommand="select *  from documentTypes" SelectCommandType="Text"></asp:SqlDataSource>
    <asp:SqlDataSource ID="dsUsers" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" SelectCommand="select *  from users" SelectCommandType="Text"></asp:SqlDataSource>
    <asp:SqlDataSource ID="dsUploadDocumentType" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" SelectCommand="select *  from uploadDocumentTypes" SelectCommandType="Text"></asp:SqlDataSource>
    <asp:SqlDataSource ID="dsParties" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" SelectCommand="select *  from parties" SelectCommandType="Text"></asp:SqlDataSource>
</div>    
</asp:Content>