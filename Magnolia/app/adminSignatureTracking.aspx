﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="adminSignatureTracking.aspx.cs" Inherits="Magnolia.app.adminSignatureTracking" MasterPageFile="~/app/Master.master" %>
<%@ Register Assembly="DevExpress.Web.v20.2, Version=20.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div>
        <dx:ASPxFormLayout ID="formDocusignConfig" runat="server" DataSourceID="dsDocusignConfig" EnableTheming="true" Theme="Metropolis">
            <Items>
                <dx:LayoutGroup Caption="DocuSign Configuration" GroupBoxDecoration="Box" ColumnCount="3">
                    <Items>
                        <dx:LayoutItem ColSpan="1" Caption="Maximum Signatures" FieldName="maxSignatureCount">
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer runat="server">
                                    <dx:ASPxSpinEdit ID="spinSignatureCount" runat="server"></dx:ASPxSpinEdit>
                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>
                        </dx:LayoutItem>
                        <dx:LayoutItem ColSpan="1" Caption="Maximum Dates" FieldName="maxDateCount">
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer runat="server">
                                    <dx:ASPxSpinEdit ID="spinDateCount" runat="server"></dx:ASPxSpinEdit>
                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>
                        </dx:LayoutItem>
                        <dx:LayoutItem ColSpan="1" Caption="Maximum Initials" FieldName="maxInitialsCount">
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer runat="server">
                                    <dx:ASPxSpinEdit ID="spinInitialsCount" runat="server"></dx:ASPxSpinEdit>
                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>
                        </dx:LayoutItem>
                        <dx:LayoutItem ColSpan="1" Caption="Signature Keyword" FieldName="signatureKeyword">
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer runat="server">
                                    <dx:ASPxTextBox ID="txtSignatureWord" runat="server"></dx:ASPxTextBox>
                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>
                        </dx:LayoutItem>
                        <dx:LayoutItem ColSpan="1" Caption="Date Keyword" FieldName="dateKeyword">
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer runat="server">
                                    <dx:ASPxTextBox ID="txtDateWord" runat="server"></dx:ASPxTextBox>
                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>
                        </dx:LayoutItem>
                        <dx:LayoutItem ColSpan="1" Caption="Initials Keyword" FieldName="initialsKeyword">
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer runat="server">
                                    <dx:ASPxTextBox ID="txtInitialsWord" runat="server"></dx:ASPxTextBox>
                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>
                        </dx:LayoutItem>
                        <dx:LayoutItem ColSpan="3" HorizontalAlign="Right" Caption=" ">
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer runat="server">
                                    <dx:ASPxButton ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save"></dx:ASPxButton>
                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>
                        </dx:LayoutItem>
                    </Items>
                </dx:LayoutGroup>
            </Items>
        </dx:ASPxFormLayout>
        <asp:SqlDataSource ID="dsDocusignConfig" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
            SelectCommand="usp_getDocusignConfig" SelectCommandType="StoredProcedure"
            UpdateCommand="usp_updateDocusignConfig" UpdateCommandType="StoredProcedure">
            <UpdateParameters>
                <asp:ControlParameter Name="maxSignatureCount"  ControlID="formDocusignConfig$spinSignatureCount" PropertyName="Value"  />
                <asp:ControlParameter Name="maxDateCount"  ControlID="formDocusignConfig$spinDateCount" PropertyName="Value"  />
                <asp:ControlParameter Name="maxInitialsCount"  ControlID="formDocusignConfig$spinInitialsCount" PropertyName="Value"  />
                <asp:ControlParameter Name="signatureKeyword"  ControlID="formDocusignConfig$txtSignatureWord" PropertyName="Value"  />
                <asp:ControlParameter Name="dateKeyword"  ControlID="formDocusignConfig$txtDateWord" PropertyName="Value"  />
                <asp:ControlParameter Name="initialsKeyword"  ControlID="formDocusignConfig$txtInitialsWord" PropertyName="Value"  />
            </UpdateParameters>
        </asp:SqlDataSource>
    </div>
    <br /><br />
    <dx:ASPxGridView ID="gvTracking" runat="server" DataSourceID="dsAlerts" KeyFieldName="contractTypeId">
        <Columns>
            <dx:GridViewDataHyperLinkColumn FieldName="contractId" Caption="Contract Number">
                <PropertiesHyperLinkEdit TextField="contractNumber" NavigateUrlFormatString="contractDetails.aspx?id={0}&tab=Documents" Target="_blank"></PropertiesHyperLinkEdit>
            </dx:GridViewDataHyperLinkColumn>
            <dx:GridViewDataDateColumn FieldName="emailDate" Caption="Email Date"></dx:GridViewDataDateColumn>
            <dx:GridViewDataColumn FieldName="fileName" Caption="File Name"></dx:GridViewDataColumn>
            <dx:GridViewDataCheckColumn FieldName="isEmailSuccess" Caption="Sent Successfully?"></dx:GridViewDataCheckColumn>
            <dx:GridViewDataCheckColumn FieldName="isEmailSigned" Caption="Signed?"></dx:GridViewDataCheckColumn>
            <dx:GridViewDataDateColumn FieldName="emailSignedDate" Caption="Signed Date"></dx:GridViewDataDateColumn>
            <dx:GridViewDataColumn FieldName="envelopeId" Visible="false"></dx:GridViewDataColumn>
            <dx:GridViewDataColumn FieldName="envelopeId" Caption=" ">
                <DataItemTemplate>
                    <dx:ASPxButton ID="btnResend" runat="server" Text="Resend" OnClick="btnResend_Click" OnInit="btnResend_Init" CommandArgument='<%# Bind("envelopeId")%>'></dx:ASPxButton>
                </DataItemTemplate>
            </dx:GridViewDataColumn>
            <dx:GridViewDataColumn FieldName="envelopeId" Caption=" ">
                <DataItemTemplate>
                    <dx:ASPxButton ID="btnRetrieve" runat="server" Text="Retrieve" OnClick="btnRetrieve_Click" OnInit="btnRetrieve_Init" CommandArgument='<%# Bind("envelopeId")%>'></dx:ASPxButton>
                </DataItemTemplate>
            </dx:GridViewDataColumn>
            <%--<dx:GridViewCommandColumn ShowEditButton="true"></dx:GridViewCommandColumn>--%>
<%--            <dx:GridViewDataColumn>
                <DataItemTemplate>
                    <dx:ASPxButton ID="btnResend" runat="server" Text="Resend"></dx:ASPxButton>
                </DataItemTemplate>
            </dx:GridViewDataColumn>
            <dx:GridViewDataColumn>
                <DataItemTemplate>
                    <dx:ASPxButton ID="btnPull" runat="server" Text="Pull"></dx:ASPxButton>
                </DataItemTemplate>
            </dx:GridViewDataColumn>--%>
        </Columns>
        <SettingsDataSecurity AllowEdit="true" />
        <SettingsEditing></SettingsEditing>
        <SettingsCommandButton>
            <EditButton Text="Resend" RenderMode="Button"></EditButton>
        </SettingsCommandButton>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="dsAlerts" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
        SelectCommand="usp_getSignatureTracking" SelectCommandType="StoredProcedure">
    </asp:SqlDataSource> 
     <asp:SqlDataSource ID="dsAllDocuments" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
        SelectCommand="usp_getSignatureTracking" SelectCommandType="StoredProcedure">
    </asp:SqlDataSource> 
</asp:Content>