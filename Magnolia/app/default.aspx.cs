﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using System.Data.SqlClient;

namespace Magnolia.app
{
    public partial class _default : System.Web.UI.Page
    {
        List<Document> ContractDocuments = new List<Document>();

        protected void Page_Init(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                Session["dsDocuments"] = null;
                //Message.LogError("login.aspx", "btnlogin_ServerClick-Log login", Session["UserName"].ToString(), "Default Init");
            }
            //gvContracts.DataBind();
            if (Session["dsDocuments"] == null)
            {
                using (SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cmsConn"].ToString()))
                {
                    string query = "usp_getAllDocuments";
                    DataSet ds = null;
                    using (SqlCommand myCommand = new SqlCommand(query, myConnection))
                    {
                        //myCommand.Parameters.Add(new SqlParameter("contractId", key));
                        myCommand.CommandType = CommandType.StoredProcedure;
                        myCommand.CommandTimeout = 0;
                        using (SqlDataAdapter mAdapter = new SqlDataAdapter(myCommand))
                        {
                            ds = new DataSet();
                            myConnection.Open();
                            mAdapter.Fill(ds);
                        }
                    }
                    myConnection.Close();
                    Session["dsDocuments"] = ds;
                }
            }
            ((GridViewDataColumn)gvContracts.Columns["Documents"]).DataItemTemplate = new CustomTemplate();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Master.SetHeader = "Default";
            Master.SetHelpVisible = false;
            if (!Page.IsPostBack)
            {
                //gvContracts.FilterExpression = "(myContracts = 1 or overDueContracts > 0)";
                //dsContracts.SelectParameters["isAdmin"].DefaultValue = Session["isAdmin"].ToString();
                //dsContracts.SelectParameters["userFullName"].DefaultValue = Session["UserName"].ToString();
                //dsContracts.SelectParameters["myContracts"].DefaultValue = "1";
                ////gvContracts.SortBy(gvContracts.Columns["statusDate"], DevExpress.Data.ColumnSortOrder.Descending);
                //dsContracts.Select(DataSourceSelectArguments.Empty);
                //gvContracts.DataSource = "dsContracts";
                gvContracts.DataBind();
                if (gvContracts.VisibleRowCount == 0)
                {
                    gvContracts.Visible = false;
                    divLegend.Visible = false;
                    lblMessage.Text = "You have no outstanding contracts to review.";
                }
                else
                {
                    gvContracts.Visible = true;
                    divLegend.Visible = true;
                    lblMessage.Text = "Outstanding and Upcoming Contracts";
                }
            }
        }

        protected void gvContracts_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (Convert.ToBoolean(Session["isAdmin"]) == false)
            {
                if (e.DataColumn.FieldName == "entryUserId")
                {
                    DevExpress.Web.Internal.HyperLinkDisplayControl hl = (DevExpress.Web.Internal.HyperLinkDisplayControl)e.Cell.Controls[0];
                    DevExpress.Web.ASPxLabel lbl = new DevExpress.Web.ASPxLabel();
                    lbl.Text = hl.Text;
                    e.Cell.Controls.Clear();
                    e.Cell.Controls.Add(lbl);
                }
            }
        }

        protected void gvContracts_SearchPanelEditorInitialize(object sender, ASPxGridViewSearchPanelEditorEventArgs e)
        {
            e.Editor.Width = Unit.Pixel(900);
        }

        protected void dsContracts_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.CommandTimeout = 0;
        }

        private void Page_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError();
            Message.Alert(exc.Message);

            // Handle specific exception.
            if (exc is System.Web.HttpUnhandledException)
            {
                Message.LogError("default", "Page_Error", Session["UserID"].ToString(), exc.Message);
                Message.Alert("An error occurred on this page. " + exc.Message);
            }
            // Clear the error from the server.
            Server.ClearError();
        }
    }

}