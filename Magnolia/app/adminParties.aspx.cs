﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Net;
using System.Collections.Generic;
using DevExpress.Web;
using System.Web.UI.WebControls;

namespace Magnolia.app
{
    public partial class adminParties : System.Web.UI.Page
    {
        public DataSet dsExclusions;

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            UploadControlHelper.RemoveOldStorages();
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Session["dsDocuments"] = null;
            }
            if (Session["dsDocuments"] == null)
            {
                DataSet ds = DataAccess.GetDataSet("usp_getAllDocuments", new string[] { "@folder" }, new string[] { "PARTY" });
                Session["dsDocuments"] = ds;
            }
            ((GridViewDataColumn)gvParties.Columns["Documents"]).DataItemTemplate = new PartyDocumentemplate();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ((Master)Page.Master).SetHeader = "Admin Party/Vendor";
            ((Master)Page.Master).SetHelpURL = "admin.aspx";
            if (Session["isAdmin"] == null || Session["isAdmin"] == System.DBNull.Value || Convert.ToBoolean(Session["isAdmin"]) == false)
            {
                Response.Redirect("default.aspx");
            }
            if (!Page.IsPostBack)
            {
                //DataSet ds = DataAccess.GetDataSet2("select distinct contractId,partyId from contracts", new string[] { }, new string[] { });
                //Session["dsContractIds"] = ds;
                if (Session["dsExclusions"] != null)
                {
                    gvExclusions.DataSource = (DataSet)Session["dsExclusions"];
                    gvExclusions.DataBind();
                }
                SubmissionID = UploadControlHelper.GenerateUploadedFilesStorageKey();
                UploadControlHelper.AddUploadedFilesStorage(SubmissionID);
            }
        }

        protected void cbParties_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            try
            {
                if (e.Parameter == "submit")
                {
                    gvParties.DataBind();
                }
                else
                {
                    if (dsExclusions != null)
                    {
                        gvExclusions.DataSource = (DataSet)Session["dsExclusions"];
                        gvExclusions.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Message.LogError("adminParties", "cbParties_Callback", Session["UserName"].ToString(), ex.Message);
            }
        }

        protected void gvParties_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            try
            {
                DevExpress.Web.ASPxTextBox txtNewBusinessName = (DevExpress.Web.ASPxTextBox)gvParties.FindEditRowCellTemplateControl(gvParties.DataColumns["businessName"], "txtNewBusinessName");
                e.NewValues["businessName"] = txtBusinessName.Text;
                DevExpress.Web.ASPxTextBox txtNewContactName = (DevExpress.Web.ASPxTextBox)gvParties.FindEditRowCellTemplateControl(gvParties.DataColumns["contactName"], "txtNewContactName");
                e.NewValues["contactName"] = txtNewContactName.Text;
            }
            catch(Exception ex)
            {
                Message.LogError("adminParties", "gvParties_RowInserting", Session["UserName"].ToString(), ex.Message);
            }
        }

        protected void btnExclusionSave_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet dsExclusions = (DataSet)Session["dsExclusions"];
                int oigCount = dsExclusions == null || dsExclusions.Tables.Count == 0 ? 0 : dsExclusions.Tables[0].Rows.Count;
                string isAdded = hdnOIGCheck.Get("check").ToString() == "true" ? "1" : "0";
                DataSet ds = DataAccess.GetDataSet("usp_logOIG", new string[] { "@contractId", "@searchDate", "@searchTerm", "@searchResults", "@isAdded", "@addedBy" }, new string[] { "0", DateTime.Now.ToString(), txtBusinessName.Text.ToUpper(), oigCount.ToString(), isAdded, Session["UserID"].ToString() });
                gvParties.AddNewRow();
                DevExpress.Web.ASPxTextBox txtNewContactName = (DevExpress.Web.ASPxTextBox)gvParties.FindEditRowCellTemplateControl(gvParties.DataColumns["contactName"], "txtNewContactName");
                txtNewContactName.Focus();
                popExclusions.ShowOnPageLoad = false;
            }
            catch (Exception ex)
            {
                Message.LogError("adminParties", "btnExclusionsSave_Click", Session["UserName"].ToString(), ex.Message);
            }
        }

        protected void btnSearchExclusions_Click(object sender, EventArgs e)
        {
            try
            {
                dsExclusions = null;
                Session["dsExclusions"] = null;
                string exactSearch = @"<exactName>" + txtBusinessName.Text.Replace("&", "").ToUpper() + "</exactName>";
                string nameSearch = @"<name>" + txtBusinessName.Text.Replace("&", "").ToUpper() + "</name>";
                string partialName = @"<partialName>" + txtBusinessName.Text.Replace("&", "").ToUpper() + "</partialName>";
                ParseSoap(exactSearch);
                ParseSoap(nameSearch);
                ParseSoap(partialName);
                DataSet dsTableExclusions = DataAccess.GetDataSet("usp_getExclusions", new string[] { "@businessName", "@isFuzzy" }, new string[] { txtBusinessName.Text.ToUpper(), cbFuzzy.Checked.ToString() });
                if (dsTableExclusions != null && dsTableExclusions.Tables.Count > 0 && dsTableExclusions.Tables[0].Rows.Count > 0)
                {
                    if (dsExclusions == null || dsExclusions.Tables.Count == 0 || dsExclusions.Tables[0].Rows.Count == 0)
                    {
                        dsExclusions = new DataSet();
                        DataTable dt = dsTableExclusions.Tables[0].Clone();
                        dsExclusions.Tables.Add(dt);
                    }
                    //else
                    //{
                    dsExclusions.Tables[0].Merge(dsTableExclusions.Tables[0], true, MissingSchemaAction.Ignore);
                    //}
                }
                if (hdnOIGCheck.Count > 0 && hdnOIGCheck.Contains("check"))
                {
                    hdnOIGCheck.Remove("check");
                }
                hdnOIGCheck.Add("check", "true");
                if (dsExclusions != null && dsExclusions.Tables.Count > 0)
                {
                    DataTable dt = dsExclusions.Tables[0].DefaultView.ToTable(true, "Classification", "EntityName", "FirstName", "MiddleInitial", "LastName", "Address", "City", "State", "ZipCode", "ExclusionType", "ActiveDate");
                    dsExclusions.Tables.RemoveAt(0);
                    dsExclusions.Tables.Add(dt);
                    gvExclusions.DataSource = dsExclusions;
                    gvExclusions.DataBind();
                    Session["dsExclusions"] = dsExclusions;
                    divExclusionResults.Visible = true;
                }
                else
                {
                    gvParties.AddNewRow();
                    DevExpress.Web.ASPxTextBox txtNewContactName = (DevExpress.Web.ASPxTextBox)gvParties.FindEditRowCellTemplateControl(gvParties.DataColumns["contactName"], "txtNewContactName");
                    txtNewContactName.Focus();
                    popExclusions.ShowOnPageLoad = false;
                }
                int oigCount = dsExclusions == null || dsExclusions.Tables.Count == 0 ? 0 : dsExclusions.Tables[0].Rows.Count;
                string isAdded = hdnOIGCheck.Get("check").ToString() == "true" ? "1" : "0";
                System.Data.DataSet dsLog = DataAccess.GetDataSet("usp_logOIG",
                    new string[] { "@contractId", "@searchDate", "@searchTerm", "@searchResults", "@isAdded", "@addedBy" },
                    new string[] { "0", DateTime.Now.ToString(), txtBusinessName.Text.ToUpper(), oigCount.ToString(), "0", Session["UserID"].ToString() });
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                {
                    throw new Exception("Business Name already exists.");
                }
                else
                {
                    Message.LogError("adminParties", "btnSearchExclusions_Click", Session["UserName"].ToString(), ex.Message);
                }
            }
        }

        protected void gvParties_CustomErrorText(object sender, DevExpress.Web.ASPxGridViewCustomErrorTextEventArgs e)
        {
            if (e.ErrorText.StartsWith("Violation of UNIQUE KEY constraint"))
            {
                e.ErrorText = "Business Name already exists. Business Names must be unique.";
            }
        }

        public string GetSearchResults(string xml)
        {
            string result = "";
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                   | SecurityProtocolType.Tls11
                   | SecurityProtocolType.Tls12
                   | SecurityProtocolType.Ssl3;
                HttpWebRequest ws = (HttpWebRequest)WebRequest.Create("https://gw.sam.gov/SAMWS/1.0/ExclusionSearch");
                ws.ContentType = "text/xml;charset=\"utf-8\"";
                ws.Accept = "text/xml";
                ws.Method = "POST";
                XmlDocument soapEnvelopeXml = new XmlDocument();
                soapEnvelopeXml.LoadXml(xml);
                using (Stream stream = ws.GetRequestStream())
                {
                    soapEnvelopeXml.Save(stream);
                }
                using (WebResponse response = ws.GetResponse())
                {
                    using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                    {
                        result = rd.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            {
                Message.LogError("adminParties", "GetSearchResults", Session["UserName"].ToString(), ex.Message);
            }
            return result;
        }

        public int ParseSoap(string searchType)
        {
            try
            {
                string xml = @"<soapenv:Envelope xmlns:soapenv = ""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:sam=""http://www.sam.gov"">";
                xml += @"<soapenv:Header></soapenv:Header><soapenv:Body><sam:doSearch><exclusionSearchCriteria>";
                xml += @"%SearchTerms%";
                xml += @"</exclusionSearchCriteria></sam:doSearch></soapenv:Body></soapenv:Envelope>";
                string respString = GetSearchResults(xml.Replace("%SearchTerms%", searchType));
                XDocument xDoc = XDocument.Parse(respString);
                IEnumerable<XElement> responses = xDoc.Elements().Elements().Elements().Elements();
                IEnumerable<XElement> elements = from c in xDoc.Descendants("excludedEntity") select c;
                IEnumerable<XElement> successful = from c in xDoc.Descendants("successful") select c;
                IEnumerable<XElement> count = from c in xDoc.Descendants("count") select c;
                IEnumerable<XElement> exclusionType = from c in xDoc.Descendants("exclusionType") select c;
                if (elements.Count() > 0)
                {
                    AddExclusionRows(elements);
                }
                if (successful.FirstOrDefault().Value == "true" && Convert.ToInt16(count.FirstOrDefault().Value) > 0)
                {
                    if (exclusionType.FirstOrDefault().Value != "")
                    {
                        return 1;
                    }
                }
            }
            catch(Exception ex)
            {
                Message.LogError("adminParties", "ParseSoap", Session["UserName"].ToString(), ex.Message);
            }
            return 0;
        }

        public void AddExclusionRows(IEnumerable<XElement> elements)
        {
            try
            {
                if (dsExclusions == null)
                {
                    dsExclusions = new DataSet();
                    DataTable dt = new DataTable();
                    dt.Columns.Add(new DataColumn() { ColumnName = "Classification", DataType = typeof(String) });
                    dt.Columns.Add(new DataColumn() { ColumnName = "EntityName", DataType = typeof(String) });
                    dt.Columns.Add(new DataColumn() { ColumnName = "FirstName", DataType = typeof(String) });
                    dt.Columns.Add(new DataColumn() { ColumnName = "MiddleInitial", DataType = typeof(String) });
                    dt.Columns.Add(new DataColumn() { ColumnName = "LastName", DataType = typeof(String) });
                    dt.Columns.Add(new DataColumn() { ColumnName = "Address", DataType = typeof(String) });
                    dt.Columns.Add(new DataColumn() { ColumnName = "City", DataType = typeof(String) });
                    dt.Columns.Add(new DataColumn() { ColumnName = "State", DataType = typeof(String) });
                    dt.Columns.Add(new DataColumn() { ColumnName = "ZipCode", DataType = typeof(String) });
                    dt.Columns.Add(new DataColumn() { ColumnName = "ExclusionType", DataType = typeof(String) });
                    dt.Columns.Add(new DataColumn() { ColumnName = "ActiveDate", DataType = typeof(String) });
                    //dt.Columns.Add(new DataColumn() { ColumnName = "TerminationDate", DataType = typeof(String) });
                    dsExclusions.Tables.Add(dt);
                }
                foreach (XElement element in elements)
                {
                    DataRow dr = dsExclusions.Tables[0].NewRow();
                    XElement classification = (from c in element.Descendants("classification") select c).FirstOrDefault();
                    dr["Classification"] = classification == null ? "" : classification.Value.ToString();
                    XElement name = (from c in element.Descendants("name") select c).FirstOrDefault();
                    dr["EntityName"] = name == null ? "" : name.Value.ToString();
                    XElement first = (from c in element.Descendants("first") select c).FirstOrDefault();
                    dr["FirstName"] = first == null ? "" : first.Value.ToString();
                    XElement middle = (from c in element.Descendants("middle") select c).FirstOrDefault();
                    dr["MiddleInitial"] = middle == null ? "" : middle.Value.ToString();
                    XElement last = (from c in element.Descendants("last") select c).FirstOrDefault();
                    dr["LastName"] = last == null ? "" : last.Value.ToString();
                    XElement street = (from c in element.Descendants("street1") select c).FirstOrDefault();
                    dr["Address"] = street == null ? "" : street.Value.ToString();
                    XElement city = (from c in element.Descendants("city") select c).FirstOrDefault();
                    dr["City"] = city == null ? "" : city.Value.ToString();
                    XElement state = (from c in element.Descendants("state") select c).FirstOrDefault();
                    dr["State"] = state == null ? "" : state.Value.ToString();
                    XElement zip = (from c in element.Descendants("zip") select c).FirstOrDefault();
                    dr["ZipCode"] = zip == null ? "" : zip.Value.ToString();
                    XElement exclusionType = (from c in element.Descendants("exclusionType") select c).FirstOrDefault();
                    dr["ExclusionType"] = exclusionType == null ? "" : exclusionType.Value.ToString();
                    XElement activeDate = (from c in element.Descendants("activeDate") select c).FirstOrDefault();
                    dr["ActiveDate"] = activeDate == null || activeDate.Value.ToString() == "" ? "" : Convert.ToDateTime(activeDate.Value).ToString("MM/dd/yyyy");
                    dsExclusions.Tables[0].Rows.Add(dr);
                }
            }
            catch(Exception ex)
            {
                Message.LogError("adminParties", "AddExclusionRows", Session["UserName"].ToString(), ex.Message);
            }
        }

        protected void gvParties_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableDataCellEventArgs e)
        {
            try
            {
                if (e.DataColumn.Caption == "Existing Contracts" && e.KeyValue != null)
                {
                    DataSet ds = DataAccess.GetDataSet2("select distinct contractId from contracts where partyId = @partyId", new string[] { "@partyId" }, new string[] { e.KeyValue.ToString() });
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        int i = 0;
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {

                            DevExpress.Web.ASPxHyperLink hl = new DevExpress.Web.ASPxHyperLink();
                            hl.Text = dr["contractId"].ToString();
                            hl.NavigateUrl = "contractDetails.aspx?from=admin&id=" + dr["contractId"].ToString();
                            hl.Target = "_blank";
                            e.Cell.Controls.Add(hl);
                            if (i < ds.Tables[0].Rows.Count)
                            {
                                System.Web.UI.HtmlControls.HtmlGenericControl span = new System.Web.UI.HtmlControls.HtmlGenericControl("span");
                                if ((i + 1) % 3 == 0)
                                {
                                    span.InnerHtml = "<br/>";
                                }
                                else
                                {
                                    span.InnerHtml = "&nbsp;";
                                }
                                e.Cell.Controls.Add(span);
                            }
                            i++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Message.Alert(ex.Message);
                Message.LogError("adminParties", "gvParties_HtmlDataCellPrepared", Session["UserName"].ToString(), ex.Message);
            }
        }

        protected void gvParties_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
        {
            try
            {
                if (e.Exception != null && e.Exception.Message.Contains("existing contracts"))
                {
                    gvParties.JSProperties["cpMessage"] = e.Exception.Message;
                    e.ExceptionHandled = true;
                    gvParties.DataBind();
                }
            }
            catch(Exception ex)
            {
                Message.LogError("adminParties", "gvParites_RowDeleted", Session["UserName"].ToString(), ex.Message);
            }
        }

        protected void dsParty_Inserting(object sender, System.Web.UI.WebControls.SqlDataSourceCommandEventArgs e)
        {
            String x = GetQuery(e.Command.CommandText, e.Command.Parameters);
        }

        protected void dsParty_Deleted(object sender, System.Web.UI.WebControls.SqlDataSourceStatusEventArgs e)
        {
            try
            {
                if (e.Command.Parameters["@count"].Value.ToString() == "0")
                {
                    throw new Exception("This party cannot be deleted because it is associated with existing contracts. This party has been automatically marked inactive.");
                }
            }
            catch (Exception ex)
            {
                Message.LogError("adminParties", "dsParty_Deleted", Session["UserName"].ToString(), ex.Message);
            }
        }

        protected void hyperLink_Init(object sender, EventArgs e)
        {
            ASPxHyperLink link = (ASPxHyperLink)sender;
            GridViewDataItemTemplateContainer templateContainer = (GridViewDataItemTemplateContainer)link.NamingContainer;
            link.NavigateUrl = "javascript:void(0);";
            link.ClientSideEvents.Click = "function(s, e) {{ gvParties.GetRowValues(" + templateContainer.VisibleIndex + ",'partyId',OnGetRowValues); popAddDocument.Show(); }}";
        }

        private string GetQuery(string CommandQuery, System.Data.Common.DbParameterCollection parmCollection)
        {
            string query = CommandQuery;
            string parms = "";
            foreach (System.Data.SqlClient.SqlParameter parm in parmCollection)
            {
                parms += parm.ParameterName + " = ";
                if (parm.Value == null)
                {
                    parms += "null,";

                }
                else
                {
                    if (parm.DbType == System.Data.DbType.String || parm.DbType == System.Data.DbType.DateTime)
                    {
                        parms += "'" + parm.Value.ToString() + "',";
                    }
                    else
                    {
                        parms += parm.Value.ToString() + ",";
                    }
                }
            }
            if (parms.Length > 0)
                query = query + " " + parms.Substring(0, parms.Length - 1);
            return query;
        }

        protected void dsDocuments_Deleting(object sender, SqlDataSourceCommandEventArgs e)
        {
            //Int32 id = Convert.ToInt32(e.Command.Parameters["@documentId"].Value);
            //Int32 idx = gvDocuments.FindVisibleIndexByKeyValue(id);
            //string[] fields = { "path", "fileName" };
            //object[] values = gvDocuments.GetRowValues(idx, fields) as object[];
            //if (values != null)
            //{
            //    try
            //    {
            //        File.Delete(Server.MapPath(values[0] + "/" + values[1]));
            //    }
            //    catch { }
            //    DataSet ds = DataAccess.GetDataSet("usp_deleteDocument", new string[] { "@documentId" }, new string[] { id.ToString() });
            //}
        }

        protected void dsDocuments_Updating(object sender, SqlDataSourceCommandEventArgs e)
        {
            string query = GetQuery(e.Command.CommandText, e.Command.Parameters);
        }

        protected void dsUploadDocumentTypes_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            string x = GetQuery(e.Command.CommandText, e.Command.Parameters);
        }

        protected string SubmissionID
        {
            get
            {
                return HiddenField.Get("SubmissionID").ToString();
            }
            set
            {
                HiddenField.Set("SubmissionID", value);
            }
        }

        UploadedFilesStorage UploadedFilesStorage
        {
            get { return UploadControlHelper.GetUploadedFilesStorageByKey(SubmissionID); }
        }

        protected void ProcessSubmit(List<UploadedFileInfo> fileInfos)
        {
            //DescriptionLabel.Value = Server.HtmlEncode(description);
            try
            {
                foreach (UploadedFileInfo fileInfo in fileInfos)
                {
                    // process uploaded files here
                    //DataSet ds = DataAccess.GetDataSet2("select * from parties where partyId = @partyId", new string[] { "@partyId" }, new string[] { Session["partyId"].ToString() });
                    string partyId = hdnPartyId.Get("partyId").ToString();
                    string folder = "PARTY";
                    string dir = System.Configuration.ConfigurationManager.AppSettings["documentPath"] + folder + "\\" + partyId;
                    if (!Directory.Exists(dir))
                    {
                        Directory.CreateDirectory(dir);
                    }
                    byte[] fileContent = File.ReadAllBytes(fileInfo.FilePath);
                    string fileName = fileInfo.OriginalFileName;
                    try {
                        using (var stream = File.Open(fileInfo.FilePath, FileMode.Open))
                        {
                            // Use stream
                            //FileStream fc = new FileStream(fileInfo.FilePath, FileMode.OpenOrCreate);
                            if (stream.Length > 0)
                            {
                                bool fileExists = File.Exists(dir + "/" + fileName);
                                if (fileExists == false || (fileExists == true && Convert.ToBoolean(Session["isAdmin"]) == true && cbReplaceFile.Checked))
                                {
                                    if (fileExists)
                                    {
                                        File.Delete(dir + "/" + fileName);
                                    }
                                    using (var fc = File.Create(dir + "\\" + fileName))
                                    {
                                        stream.Seek(0, SeekOrigin.Begin);
                                        stream.CopyTo(fc);
                                    }
                                    if (fileExists)
                                    {
                                        try
                                        {
                                            dsDocuments.UpdateParameters["uploadDocumentTypeId"].DefaultValue = ddlUploadDocumentType.Value.ToString();
                                            dsDocuments.UpdateParameters["fileName"].DefaultValue = fileName;
                                            dsDocuments.UpdateParameters["path"].DefaultValue = System.Configuration.ConfigurationManager.AppSettings["documentVirtualPath"] + folder + "\\" + partyId;
                                            dsDocuments.Update();
                                        }
                                        catch(Exception exUpdate)
                                        {
                                            Message.LogError("adminParties", "ProcessSubmit-dsDocumentUpdate", Session["UserName"].ToString(), exUpdate.Message);
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            dsDocuments.InsertParameters["fileName"].DefaultValue = fileName;
                                            dsDocuments.InsertParameters["path"].DefaultValue = System.Configuration.ConfigurationManager.AppSettings["documentVirtualPath"] + folder + "\\" + partyId;
                                            dsDocuments.InsertParameters["uploadDocumentTypeId"].DefaultValue = ddlUploadDocumentType.Value.ToString();
                                            dsDocuments.InsertParameters["folder"].DefaultValue = partyId;
                                            dsDocuments.InsertParameters["partyId"].DefaultValue = partyId;
                                            dsDocuments.Insert();
                                        }
                                        catch(Exception exInsert)
                                        {
                                            Message.LogError("adminParties", "ProcessSubmit-dsDocumentInsert", Session["UserName"].ToString(), exInsert.Message);
                                        }
                                    }
                                    DataSet ds = DataAccess.GetDataSet("usp_getAllDocuments", new string[] { "@folder" }, new string[] { "PARTY" });
                                    Session["dsDocuments"] = ds;
                                }
                                else
                                {
                                    UploadedFilesTokenBox.IsValid = false;
                                    UploadedFilesTokenBox.ErrorText = "File already exists.";
                                    Message.Alert("File already exists.");
                                }
                                //fc.Write(fileContent, 0, fileContent.Length);
                                //File.WriteAllBytes(dir, fileContent);
                            }
                        }
                    }
                    catch(Exception ex)
                    {
                        Message.LogError("adminParties", "ProcessSubmit-Upload File", Session["UserName"].ToString(), ex.Message);
                    }
                }
                //SubmittedFilesListBox.DataSource = fileInfos;
                //SubmittedFilesListBox.DataBind();
                //FormLayout.FindItemOrGroupByName("ResultGroup").Visible = true;
            }
            catch (Exception ex)
            {
                Message.LogError("adminParties", "ProcessSubmit", Session["UserName"].ToString(), ex.Message);
            }
        }

        protected void DocumentsUploadControl_FileUploadComplete(object sender, DevExpress.Web.FileUploadCompleteEventArgs e)
        {
            bool isSubmissionExpired = false;
            if (UploadedFilesStorage == null)
            {
                isSubmissionExpired = true;
                UploadControlHelper.AddUploadedFilesStorage(SubmissionID);
            }
            UploadedFileInfo tempFileInfo = UploadControlHelper.AddUploadedFileInfo(SubmissionID, e.UploadedFile.FileName);
            e.UploadedFile.SaveAs(tempFileInfo.FilePath);
            if (e.IsValid)
                e.CallbackData = tempFileInfo.UniqueFileName + "|" + isSubmissionExpired;
        }

        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            try
            {
                ValidateInputData();
                List<UploadedFileInfo> resultFileInfos = new List<UploadedFileInfo>();
                bool allFilesExist = true;
                if (UploadedFilesStorage == null)
                    UploadedFilesTokenBox.Tokens = new TokenCollection();
                foreach (string fileName in UploadedFilesTokenBox.Tokens)
                {
                    UploadedFileInfo demoFileInfo = UploadControlHelper.GetDemoFileInfo(SubmissionID, fileName);
                    FileInfo fileInfo = new FileInfo(demoFileInfo.FilePath);
                    if (fileInfo.Exists)
                    {
                        //demoFileInfo.FileSize = Utils.FormatSize(fileInfo.Length);
                        resultFileInfos.Add(demoFileInfo);
                    }
                    else
                        allFilesExist = false;
                }

                if (allFilesExist && resultFileInfos.Count > 0)
                {
                    ProcessSubmit(resultFileInfos);
                    cbParties_Callback(SubmitButton, new CallbackEventArgs("submit"));
                    popAddDocument.ShowOnPageLoad = false;
                    UploadControlHelper.RemoveUploadedFilesStorage(SubmissionID);
                    ASPxEdit.ClearEditorsInContainer(FormLayout, true);
                }
                else
                {
                    UploadedFilesTokenBox.ErrorText = "Submit failed because files have been removed from the server due to the 5 minute timeout.";
                    UploadedFilesTokenBox.IsValid = false;
                }
            }
            catch(Exception ex)
            {
                Message.LogError("adminParties", "SubmitButton_Click", Session["UserName"].ToString(), ex.Message);
            }
        }

        void ValidateInputData()
        {
            bool isInvalid = ddlUploadDocumentType.Value == null || UploadedFilesTokenBox.Tokens.Count == 0;
            if (isInvalid)
                throw new Exception("Document Type and File Name are required.");
        }


        private void Page_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError();
            Message.Alert(exc.Message);
            Message.LogError("adminParties", "Page_Error", Session["UserID"].ToString(), exc.Message);
            // Handle specific exception.
            if (exc is System.Web.HttpUnhandledException)
            {
                Message.Alert("An error occurred on this page. Please verify your information to resolve the issue.");
            }
            // Clear the error from the server.
            Server.ClearError();
        }

    }

    public class PartyDocumentemplate : ITemplate
    {
        public void InstantiateIn(Control container)
        {
            try
            {

                GridViewDataItemTemplateContainer gcontainer = (GridViewDataItemTemplateContainer)container;
                int key = Convert.ToInt32(DataBinder.Eval(gcontainer.DataItem, "partyId"));
                DataSet ds = (DataSet)System.Web.HttpContext.Current.Session["dsDocuments"];
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    string prevURL = "";
                    DataView dv = ds.Tables[0].DefaultView;
                    dv.RowFilter = "partyId = " + key.ToString();
                    DataTable dt = dv.ToTable();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        //    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        //{
                        string thisURL = "";
                        if (dt.Rows[i]["url"] != null)
                        {
                            thisURL = dt.Rows[i]["url"].ToString();
                        }
                        if (prevURL != thisURL && (dt.Rows[i]["partyId"].ToString() == key.ToString()))
                        {
                            ASPxHyperLink hl = new ASPxHyperLink();
                            //hl.NavigateUrl = @"DocumentViewer.aspx?url=" + System.Web.HttpContext.Current.Server.UrlEncode(dt.Rows[i]["url"].ToString());
                            hl.NavigateUrl = @"DocumentViewer.aspx?documentId=" + dt.Rows[i]["documentId"].ToString(); //System.Web.HttpContext.Current.Server.UrlEncode(dt.Rows[i]["url"].ToString());
                            hl.ImageHeight = Unit.Pixel(30);
                            hl.ImageWidth = Unit.Pixel(30);
                            hl.ToolTip = dt.Rows[i]["fileName"].ToString() + ' ' + dt.Rows[i]["uploadDate"].ToString();
                            hl.Target = "_blank";
                            switch (dt.Rows[i]["documentTypeId"])
                            {
                                case 1:
                                    hl.ImageUrl = "../images/Word.png";
                                    break;
                                case 2:
                                    hl.ImageUrl = "../images/PDF.png";
                                    break;
                                case 3:
                                    hl.ImageUrl = "../images/Excel.png";
                                    break;
                                default:
                                    hl.ImageUrl = "../images/Other.png";
                                    break;
                            }
                            gcontainer.Controls.Add(hl);
                        }
                        prevURL = thisURL;
                    }
                }
                //if (count > 0)
                //{
                //    ASPxLabel lbl = new ASPxLabel();
                //    lbl.Text = "<br/>";
                //    lbl.EncodeHtml = true;
                //    gcontainer.Controls.Add(lbl);
                //}
                //ASPxHyperLink hlUpload = new ASPxHyperLink();
                //hlUpload.Text = "Upload";
                //hlUpload.Style.Add("cursor", "pointer");
                //hlUpload.ClientSideEvents.Click = "OnLinkClicked";
                //gcontainer.Controls.Add(hlUpload);
            }
            catch (Exception ex)
            {
                string x = ex.Message;
            }
        }
    }

}