﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="adminOIGLog.aspx.cs" Inherits="Magnolia.app.adminOIGLog" MasterPageFile="~/app/Master.master" %>
<%@ Register Assembly="DevExpress.Web.v20.2, Version=20.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
         .flex {
          display: inline-flex; 
          flex-direction: row;
          flex: 1 1 auto;
          width: 100%;
        }
    </style>
    <br />
    <style>
        .gridHeader {  
            background-color: white;  
            color:black;
        }  
        .gridHeader a {  
            color: black;  
        }  
    </style>
    <div class="flex">
        <dx:ASPxCallbackPanel ID="cbAPI" runat="server" ClientInstanceName="cbAPI" OnCallback="cbAPI_Callback" Styles-Panel-Wrap="False">
            <ClientSideEvents EndCallback="function (s,e){ if (s.cppdfxx != null && s.cppdfxx != '') { alert(s.cppdfxx); } }" />
            <PanelCollection>
                <dx:PanelContent>
                    <table>
                        <tr>
                            <td style="padding:5px"><dx:ASPxLabel ID="lblOIG" runat="server" Text="API Key:" Wrap="False"></dx:ASPxLabel></td>
                            <td style="padding:5px"><dx:ASPxTextBox ID="txtAPI" ClientInstanceName="txtAPI" runat="server" Width="400"></dx:ASPxTextBox></td>
                            <td style="padding:5px">
                                <dx:ASPxButton ID="btnAPI" runat="server" Text="Update API Key">
                                    <ClientSideEvents Click="function (s,e) {cbAPI.PerformCallback(txtAPI.GetText());}" />
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxCallbackPanel>
    </div>
    <br /><br />
    <dx:ASPxGridView ID="gvOIG" runat="server" AutoGenerateColumns="false" DataSourceID="dsOIG" OnHtmlDataCellPrepared="gvOIG_HtmlDataCellPrepared">
        <Columns>
            <dx:GridViewDataHyperLinkColumn FieldName="contractId" Caption="Party" Width="200" Settings-FilterMode="DisplayText" CellStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true">
                <PropertiesHyperLinkEdit NavigateUrlFormatString="contractDetails.aspx?id={0}" TextField="businessName"></PropertiesHyperLinkEdit>
                <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
            </dx:GridViewDataHyperLinkColumn>
            <dx:GridViewDataDateColumn FieldName="searchDate" Caption="Search Date" PropertiesDateEdit-DisplayFormatString="MM/dd/yyyy hh:mm tt" HeaderStyle-Font-Bold="true"></dx:GridViewDataDateColumn>
            <dx:GridViewDataColumn FieldName="searchTerm" Caption="Search Term" SettingsHeaderFilter-Mode="CheckedList" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
            <dx:GridViewDataColumn FieldName="searchResults" Caption="Search Results(#)" SettingsHeaderFilter-Mode="CheckedList" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
            <dx:GridViewDataCheckColumn FieldName="isAdded" Caption="Added?" SettingsHeaderFilter-Mode="CheckedList" HeaderStyle-Font-Bold="true"></dx:GridViewDataCheckColumn>
            <dx:GridViewDataColumn FieldName="fullName" Caption="User" SettingsHeaderFilter-Mode="CheckedList" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
        </Columns>
        <SettingsBehavior AllowHeaderFilter="true" />
        <Settings ShowHeaderFilterButton="true" />
        <SettingsPager PageSize="20"></SettingsPager>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="dsOIG" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
        SelectCommand="usp_getOIGLog" SelectCommandType="StoredProcedure"></asp:SqlDataSource> 
</asp:Content>