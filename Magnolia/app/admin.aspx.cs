﻿using System;
using System.Web.UI;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Linq;

namespace Magnolia.app
{
    public partial class admin : System.Web.UI.Page
    {
        public DataSet dsExclusions;

        protected void Page_Load(object sender, EventArgs e)
        {
            ((Master)Page.Master).SetHeader = "Admin";
            ((Master)Page.Master).SetHelpURL = "admin.aspx";
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["tab"] != null)
                {
                    tabs.ActiveTabIndex = tabs.TabPages.FindByName(Request.QueryString["tab"]).Index;
                }
                else
                {
                    tabs.ActiveTabIndex = 2;
                }
                this.Form.DefaultButton = "";
                Session["dsExlusions"] = null;
                //Session["addParty"] = false;
            }
            DataSet ds = DataAccess.GetDataSet2("select distinct contractId,partyId from contracts", new string[] { }, new string[] { });
            Session["dsContractIds"] = ds;
            if (Session["dsExclusions"] != null)
            {
                gvExclusions.DataSource = (DataSet)Session["dsExclusions"];
                gvExclusions.DataBind();
            }
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["userId"] != null)
                {
                    ddlUsers.DataBind();
                    ddlUsers.SelectedIndex = ddlUsers.Items.FindByValue(Request.QueryString["userId"]).Index;
                    cbUsers_Callback(ddlUsers, new DevExpress.Web.CallbackEventArgsBase(Request.QueryString["userId"]));
                }
            }
        }

        protected void ddlTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            gvContractTypes.Visible = false;
            gvDepartments.Visible = false;
            gvUploadDocumentTypes.Visible = false;
            gvEOC.Visible = false;
            gvPartyTypes.Visible = false;
            gvPurchaseGroups.Visible = false;
            gvStageTypes.Visible = false;
            gvStatusTypes.Visible = false;
            switch (ddlTypes.Value)
            {
                case "contract":
                    gvContractTypes.Visible = true;
                    break;
                case "department":
                    gvDepartments.Visible = true;
                    break;
                case "document":
                    gvUploadDocumentTypes.Visible = true;
                    break;
                case "EOC":
                    gvEOC.Visible = true;
                    break;
                case "party":
                    gvPartyTypes.Visible = true;
                    break;
                case "purchaseGroups":
                    gvPurchaseGroups.Visible = true;
                    break;
                case "status":
                    gvStatusTypes.Visible = true;
                    break;
            }
        }

        protected void cbUsers_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            dsUser.SelectParameters["userId"].DefaultValue = e.Parameter;
            formUsers.DataBind();
        }

        protected void btnSaveUser_Click(object sender, EventArgs e)
        {
            if (ddlUsers.Value.ToString() == "0")
            {
                try
                {
                    if (deBypassStart.Date < Convert.ToDateTime("1/1/1753") || deBypassStart.Date > Convert.ToDateTime("12/31/9999"))
                    {
                        dsUser.InsertParameters.Remove(dsUser.InsertParameters["bypassStart"]);
                    }
                    if (deBypassEnd.Date < Convert.ToDateTime("1/1/1753") || deBypassEnd.Date > Convert.ToDateTime("12/31/9999"))
                    {
                        dsUser.InsertParameters.Remove(dsUser.InsertParameters["bypassEnd"]);
                    }
                    dsUser.Insert();
                    ddlUsers.DataBind();
                    ddlUsers.SelectedIndex = ddlUsers.Items.FindByText(txtUserFullName.Text).Index;
                    Message.Alert("User has been added.");
                }
                catch (Exception ex)
                {
                    Message.Alert(ex.Message);
                }
            }
            else
            {
                try
                {
                    if (deBypassStart.Date > Convert.ToDateTime("1/1/1753") || deBypassStart.Date < Convert.ToDateTime("12/31/9999"))
                    {
                        dsUser.UpdateParameters.Remove(dsUser.UpdateParameters["bypassStart"]);
                    }
                    if (deBypassEnd.Date < Convert.ToDateTime("1/1/1753") || deBypassEnd.Date > Convert.ToDateTime("12/31/9999"))
                    {
                        dsUser.UpdateParameters.Remove(dsUser.UpdateParameters["bypassEnd"]);
                    }
                    dsUser.Update();
                    Message.Alert("User has been updated.");
                }
                catch (Exception ex)
                {
                    Message.Alert(ex.Message);
                }
            }
        }

        protected void btnDeleteUser_Click(object sender, EventArgs e)
        {
            try
            {
                dsUser.Delete();
                ddlUsers.SelectedIndex = 0;
                cbUsers_Callback(ddlUsers, new DevExpress.Web.CallbackEventArgsBase("0"));
                formUsers.DataBind();
            }
            catch(Exception ex)
            {
                Message.Alert(ex.Message);
                Server.ClearError();
                formUsers.DataBind();
            }
        }

        protected void cbUserRoles_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            dsUserRole.SelectParameters["roleId"].DefaultValue = e.Parameter;
            formUserRoles.DataBind();
        }

        protected void btnSaveUserRole_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlUserRolesEdit.Value.ToString() == "0")
                {
                    try
                    {
                        dsUserRole.Insert();
                        ddlUserRolesEdit.DataBind();
                        ddlUserRolesEdit.SelectedIndex = ddlUserRolesEdit.Items.FindByText(txtRoleName.Text).Index;
                        Message.Alert("User Role has been added.");
                    }
                    catch (Exception ex)
                    {
                        Message.Alert(ex.Message);
                    }
                }
                else
                {
                    try
                    {
                        dsUserRole.Update();
                        Message.Alert("User Role has been updated.");
                    }
                    catch (Exception ex)
                    {
                        Message.Alert(ex.Message);
                    }
                }
                ddlUserRoles.DataBind();
            }
            catch(Exception ex)
            {
                Message.Alert(ex.Message);
            }
        }

        protected void btnDeleteUserRole_Click(object sender, EventArgs e)
        {
            try
            {
                dsUserRole.Delete();
                ddlUserRolesEdit.SelectedIndex = 0;
                cbUserRoles_Callback(ddlUserRolesEdit, new DevExpress.Web.CallbackEventArgsBase("0"));
                ddlUserRoles.DataBind();
            }
            catch (Exception ex)
            {
                Message.Alert(ex.Message);
                Server.ClearError();
                cbUserRoles_Callback(ddlUserRolesEdit, new DevExpress.Web.CallbackEventArgsBase(ddlUserRolesEdit.Value.ToString()));
            }
        }

        //protected void cbRouting_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        //{
        //    //dsRoute.SelectParameters["routingId"].DefaultValue = e.Parameter.ToString();
        //    //gvRoutes.DataBind();
        //    //gvRoutes.ClientVisible = true;
        //    //SqlDataSource1.SelectParameters["routingId"].DefaultValue = e.Parameter.ToString();
        //    //Diagram.DataBind();
        //    //Diagram.Visible = true;
        //}

        //protected void SqlDataSource1_Updating(object sender, System.Web.UI.WebControls.SqlDataSourceCommandEventArgs e)
        //{
        //    string x = GetQuery(e.Command.CommandText, e.Command.Parameters);
        //}

        private string GetQuery(string CommandQuery, System.Data.Common.DbParameterCollection parmCollection)
        {
            string query = CommandQuery;
            string parms = "";
            foreach (System.Data.SqlClient.SqlParameter parm in parmCollection)
            {
                parms += parm.ParameterName + " = ";
                if (parm.Value == null)
                {
                    parms += "null,";

                }
                else
                {
                    if (parm.DbType == System.Data.DbType.String || parm.DbType == System.Data.DbType.DateTime)
                    {
                        parms += "'" + parm.Value.ToString() + "',";
                    }
                    else
                    {
                        parms += parm.Value.ToString() + ",";
                    }
                }
            }
            query = query + " " + parms.Substring(0, parms.Length - 1);
            return query;
        }

        protected void gvRoutes_Init(object sender, EventArgs e)
        {
            SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cmsConn"].ToString());
            string query = "usp_getMaxRoleCount";
            SqlCommand myCommand = new SqlCommand(query, myConnection);
            myCommand.CommandType = CommandType.StoredProcedure;
            myCommand.CommandTimeout = 0;
            SqlDataAdapter mAdapter = new SqlDataAdapter(myCommand);
            myConnection.Open();
            DataSet ds = new DataSet();
            mAdapter.Fill(ds);
            myConnection.Close();
            int max = 0;
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                max = Convert.ToInt16(ds.Tables[0].Rows[0]["roleCount"]);
            }
            DevExpress.Web.ASPxGridView grid = sender as DevExpress.Web.ASPxGridView;
            (grid.Columns["Sequence"] as DevExpress.Web.GridViewDataSpinEditColumn).PropertiesSpinEdit.MaxValue = max;
        }

        protected void ddlRoutes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlRoutes.Value != null)
            {
                SqlDataSource1.SelectParameters["routingId"].DefaultValue = ddlRoutes.Value.ToString();
                Diagram.DataBind();
                dsRoute.SelectParameters["routingId"].DefaultValue = ddlRoutes.Value.ToString();
                dsRoute.InsertParameters["routingId"].DefaultValue = ddlRoutes.Value.ToString();
                gvRoutes.DataBind();
                if (gvRoutes.VisibleRowCount > 0)
                {
                    try
                    {
                        string description = gvRoutes.GetRowValues(0, new string[] { "description" }).ToString();
                        txtRouteDescription.Text = description;
                    }
                    catch(Exception ex)
                    {
                        Message.Alert("Error getting routing description: " + ex.Message);
                    }
                }
            }
        }

        protected void SqlDataSource1_Deleting(object sender, System.Web.UI.WebControls.SqlDataSourceCommandEventArgs e)
        {
            string x = GetQuery(e.Command.CommandText, e.Command.Parameters);
            //gvRoutes.DataBind();
        }

        protected void btnCloneRoute_Click(object sender, EventArgs e)
        {
            int routingId = Convert.ToInt32(ddlCloneRoutes.Value);
            SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cmsConn"].ToString());
            string query = "usp_addRoute";
            SqlCommand myCommand = new SqlCommand(query, myConnection);
            myCommand.Parameters.Add(new SqlParameter("routingId", routingId));
            myCommand.Parameters.Add(new SqlParameter("routingName", txtName.Text.ToUpper()));
            SqlParameter parm = new SqlParameter();
            parm.ParameterName = "newRoutingId";
            parm.DbType = DbType.Int32;
            parm.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parm);
            myCommand.CommandType = CommandType.StoredProcedure;
            myCommand.CommandTimeout = 0;
            SqlDataAdapter mAdapter = new SqlDataAdapter(myCommand);
            myConnection.Open();
            myCommand.ExecuteNonQuery();
            myConnection.Close();
            dsRoute.SelectParameters["routingId"].DefaultValue = parm.Value.ToString();
            gvRoutes.DataBind();
            ddlRoutes.DataBind();
            DevExpress.Web.ListEditItem obj = ddlRoutes.Items.FindByValue(parm.Value.ToString());
            if (obj != null)
            {
                ddlRoutes.SelectedIndex = obj.Index;
            }
            popNewRoute.ShowOnPageLoad = false;
            Diagram.DataBind();
            //gvNewRoute.ClientVisible = true;
        }

        protected void dsRoute_Deleting(object sender, System.Web.UI.WebControls.SqlDataSourceCommandEventArgs e)
        {
            //e.Command.Parameters["@routingId"].Value = gvRoutes.GetRowValues(0, new string[] { "routingId" });
            //string x = GetQuery(e.Command.CommandText, e.Command.Parameters);
            Diagram.DataBind();
        }

        protected void dsRoute_Deleted(object sender, System.Web.UI.WebControls.SqlDataSourceStatusEventArgs e)
        {
            if (e.Exception != null)
            {
                string sqlErrorMessage = e.Exception.Message;
                //e.ExceptionHandled = true;
                ClientScript.RegisterClientScriptBlock(Page.GetType(), "error", "<script>alert('Error Deleting Sequence.');window.location='contracts.aspx';</script>");
            }
            else
            {
                Diagram.DataBind();
            }
        }

        protected void dsRoute_Updating(object sender, System.Web.UI.WebControls.SqlDataSourceCommandEventArgs e)
        {
            //e.Command.Parameters["@routingId"].Value = gvRoutes.GetRowValues(0, new string[] { "routingId" });
            //string x = GetQuery(e.Command.CommandText, e.Command.Parameters);
        }

        protected void dsRoute_Updated(object sender, System.Web.UI.WebControls.SqlDataSourceStatusEventArgs e)
        {
            //if (e.Exception != null)
            //{
            //    string sqlErrorMessage = e.Exception.Message;
            //    ClientScript.RegisterClientScriptBlock(Page.GetType(), "error", "<script>alert('Error Deleting Sequence.');window.location='contracts.aspx';</script>");
            //}
            //else
            //{
            //    Diagram.DataBind();
            //}
        }

        protected void gvRoutes_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            try
            {
                dsRoute.UpdateParameters["rid"].DefaultValue = e.NewValues["rid"].ToString();
                dsRoute.UpdateParameters["parentId"].DefaultValue = e.NewValues["parentId"].ToString();
                dsRoute.UpdateParameters["roleId"].DefaultValue = e.NewValues["roleId"].ToString();
                dsRoute.Update();
                e.Cancel = true;
                gvRoutes.CancelEdit();
                Diagram.DataBind();
            }
            catch (Exception ex)
            {
                Message.Alert(ex.Message);
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            gvRoutes.DataBind();
            Diagram.DataBind();
        }

        protected void btnDeleteRoute_Click(object sender, EventArgs e)
        {
            try
            {
                dsRoutes.Delete();
                ddlRoutes.SelectedIndex = -1;
                ddlRoutes.DataBind();
                gvRoutes.DataBind();
                dsRoute.SelectParameters["@routingId"].DefaultValue = "0";
                Diagram.DataBind();
            }
            catch (Exception ex)
            {
                Message.Alert(ex.Message);
            }
        }

        //protected void dsParty_Inserting(object sender, System.Web.UI.WebControls.SqlDataSourceCommandEventArgs e)
        //{
        //    //try
        //    //{
        //    //    dsExclusions = null;
        //    //    if (Convert.ToBoolean(Session["addParty"]) == false)
        //    //    {
        //    //        string exactSearch = @"<exactName>" + e.Command.Parameters["@businessName"].Value.ToString().ToUpper() + "</exactName>";
        //    //        string nameSearch = @"<name>" + e.Command.Parameters["@businessName"].Value.ToString().ToUpper() + "</name>";
        //    //        string partialName = @"<partialName>" + e.Command.Parameters["@businessName"].Value.ToString().ToUpper() + "</partialName>";
        //    //        if (ParseSoap(exactSearch) > 0 || ParseSoap(nameSearch) > 0 || ParseSoap(partialName) > 0)
        //    //        {
        //    //            int count = 0;
        //    //            if (dsExclusions != null && dsExclusions.Tables.Count > 0)
        //    //            {
        //    //                count = dsExclusions.Tables[0].Rows.Count;
        //    //                gvExclusions.DataSource = dsExclusions;
        //    //                gvExclusions.DataBind();
        //    //                popExclusions.ShowOnPageLoad = true;
        //    //                Session["isExclusions"] = true;
        //    //                e.Cancel = true;
        //    //            }
        //    //            throw new Exception("Vendor/Party is on the exclusion list." + count + " records returned.");
        //    //        }
        //    //        else
        //    //        {
        //    //            //
        //    //        }
        //    //        DataSet ds = DataAccess.GetDataSet2("select Classification,Name,First,Middle,Last,[Address 1],City,[State / Province],[Zip Code],[Exclusion Program],[Active Date] from SAMExclusions where (First + ' ' + Last = @businessName) or (Name = @businessName)", new string[] { "@businessName" }, new string[] { e.Command.Parameters["@businessName"].Value.ToString().ToUpper() });
        //    //        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        //    //        {
        //    //            throw new Exception("Vendor/Party is in the exclusion database.");
        //    //        }
        //    //    }
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint"))
        //    //    {
        //    //        throw new Exception("Business Name already exists.");
        //    //    }
        //    //    else
        //    //    {
        //    //        throw;
        //    //    }
        //    //}
        //}

        public string GetSearchResults(string xml)
        {
            HttpWebRequest ws = (HttpWebRequest)WebRequest.Create("https://gw.sam.gov/SAMWS/1.0/ExclusionSearch");
            ws.ContentType = "text/xml;charset=\"utf-8\"";
            ws.Accept = "text/xml";
            ws.Method = "POST";
            XmlDocument soapEnvelopeXml = new XmlDocument();
            //xml = @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:sam=""http://www.sam.gov""> <soapenv:Header/> <soapenv:Body> <sam:doSearch> <exclusionSearchCriteria> <exactName>Bad Medicine Group, Inc.</exactName> </exclusionSearchCriteria> </sam:doSearch> </soapenv:Body> </soapenv:Envelope>";
            soapEnvelopeXml.LoadXml(xml);
            using (Stream stream = ws.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }
            string result;
            using (WebResponse response = ws.GetResponse())
            {
                using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                {
                    result = rd.ReadToEnd();
                }
            }
            return result;
        }

        public int ParseSoap(string searchType)
        {
            string xml = @"<soapenv:Envelope xmlns:soapenv = ""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:sam=""http://www.sam.gov"">";
            xml += @"<soapenv:Header></soapenv:Header><soapenv:Body><sam:doSearch><exclusionSearchCriteria>";
            xml += @"%SearchTerms%";
            xml += @"</exclusionSearchCriteria></sam:doSearch></soapenv:Body></soapenv:Envelope>";
            string respString = GetSearchResults(xml.Replace("%SearchTerms%", searchType));
            XDocument xDoc = XDocument.Parse(respString);
            IEnumerable<XElement> responses = xDoc.Elements().Elements().Elements().Elements();
            IEnumerable<XElement> elements = from c in xDoc.Descendants("excludedEntity") select c;
            IEnumerable < XElement > successful = from c in xDoc.Descendants("successful") select c;
            IEnumerable<XElement> count = from c in xDoc.Descendants("count") select c;
            IEnumerable<XElement> exclusionType = from c in xDoc.Descendants("exclusionType") select c;
            if (elements.Count() > 0)
            {
                AddExclusionRows(elements);
            }
            if (successful.FirstOrDefault().Value == "true" && Convert.ToInt16(count.FirstOrDefault().Value) > 0)
            {
                if (exclusionType.FirstOrDefault().Value != "")
                {
                    return 1;
                }
            }
            return 0;
        }

        public void AddExclusionRows(IEnumerable<XElement> elements)
        {
            if (dsExclusions == null)
            {
                dsExclusions = new DataSet();
                DataTable dt = new DataTable();
                dt.Columns.Add(new DataColumn() { ColumnName = "Classification", DataType = typeof(String) });
                dt.Columns.Add(new DataColumn() { ColumnName = "EntityName", DataType = typeof(String) });
                dt.Columns.Add(new DataColumn() { ColumnName = "FirstName", DataType = typeof(String) });
                dt.Columns.Add(new DataColumn() { ColumnName = "MiddleInitial", DataType = typeof(String) });
                dt.Columns.Add(new DataColumn() { ColumnName = "LastName", DataType = typeof(String) });
                dt.Columns.Add(new DataColumn() { ColumnName = "Address", DataType = typeof(String) });
                dt.Columns.Add(new DataColumn() { ColumnName = "City", DataType = typeof(String) });
                dt.Columns.Add(new DataColumn() { ColumnName = "State", DataType = typeof(String) });
                dt.Columns.Add(new DataColumn() { ColumnName = "ZipCode", DataType = typeof(String) });
                dt.Columns.Add(new DataColumn() { ColumnName = "ExclusionType", DataType = typeof(String) });
                dt.Columns.Add(new DataColumn() { ColumnName = "ActiveDate", DataType = typeof(String) });
                //dt.Columns.Add(new DataColumn() { ColumnName = "TerminationDate", DataType = typeof(String) });
                dsExclusions.Tables.Add(dt);
            }
            foreach (XElement element in elements)
            {
                DataRow dr = dsExclusions.Tables[0].NewRow();
                XElement classification = (from c in element.Descendants("classification") select c).FirstOrDefault();
                dr["Classification"] = classification == null ? "" : classification.Value.ToString();
                XElement name = (from c in element.Descendants("name") select c).FirstOrDefault();
                dr["EntityName"] = name == null ? "" : name.Value.ToString();
                XElement first = (from c in element.Descendants("first") select c).FirstOrDefault();
                dr["FirstName"] = first == null ? "" : first.Value.ToString();
                XElement middle = (from c in element.Descendants("middle") select c).FirstOrDefault();
                dr["MiddleInitial"] = middle == null ? "" : middle.Value.ToString();
                XElement last = (from c in element.Descendants("last") select c).FirstOrDefault();
                dr["LastName"] = last == null ? "" : last.Value.ToString();
                XElement street = (from c in element.Descendants("street1") select c).FirstOrDefault();
                dr["Address"] = street == null ? "" : street.Value.ToString();
                XElement city = (from c in element.Descendants("city") select c).FirstOrDefault();
                dr["City"] = city == null ? "" : city.Value.ToString();
                XElement state = (from c in element.Descendants("state") select c).FirstOrDefault();
                dr["State"] = state == null ? "" : state.Value.ToString();
                XElement zip = (from c in element.Descendants("zip") select c).FirstOrDefault();
                dr["ZipCode"] = zip == null ? "" : zip.Value.ToString();
                XElement exclusionType = (from c in element.Descendants("exclusionType") select c).FirstOrDefault();
                dr["ExclusionType"] = exclusionType == null ? "" : exclusionType.Value.ToString();
                XElement activeDate = (from c in element.Descendants("activeDate") select c).FirstOrDefault();
                dr["ActiveDate"] = activeDate == null || activeDate.Value.ToString() == "" ? "" : Convert.ToDateTime(activeDate.Value).ToString("MM/dd/yyyy");
                dsExclusions.Tables[0].Rows.Add(dr);
            }
        }

        //protected void gvParties_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        //{
        //    //dsExclusions = null;
        //    gvParties.JSProperties["cpIsUpdating"] = true;
        //    gvParties.DoRowValidation();
        //    if(dsExclusions == null) { 
        //        gvParties.UpdateEdit();
        //    }
        //}

        protected void btnExclusionSave_Click(object sender, EventArgs e)
        {
            //Session["addParty"] = true;
            //try {
            //    //dsParty.Insert();
            //    gvParties.UpdateEdit();
            //}
            //catch (Exception ex)
            //{
            //    if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint"))
            //    {
            //        throw new Exception("Business Name already exists.");
            //    }
            //    else
            //    {
            //        throw;
            //    }
            //}
            DataSet dsExclusions = (DataSet)Session["dsExclusions"];
            int oigCount = dsExclusions == null || dsExclusions.Tables.Count == 0 ? 0 : dsExclusions.Tables[0].Rows.Count;
            string isAdded = hdnOIGCheck.Get("check").ToString() == "true" ? "1" : "0";
            DataSet ds = DataAccess.GetDataSet("usp_logOIG", new string[] { "@contractId", "@searchDate", "@searchTerm", "@searchResults", "@isAdded", "@addedBy" }, new string[] { "0", DateTime.Now.ToString(), txtBusinessName.Text.ToUpper(), oigCount.ToString(), isAdded, Session["UserID"].ToString() });
            gvParties.AddNewRow();
            //DevExpress.Web.ASPxTextBox txtNewBusinessName = (DevExpress.Web.ASPxTextBox)gvParties.FindEditRowCellTemplateControl(gvParties.DataColumns["businessName"], "txtNewBusinessName");
            //txtNewBusinessName.Text = txtBusinessName.Text;
            DevExpress.Web.ASPxTextBox txtNewContactName = (DevExpress.Web.ASPxTextBox)gvParties.FindEditRowCellTemplateControl(gvParties.DataColumns["contactName"], "txtNewContactName");
            txtNewContactName.Focus();
            popExclusions.ShowOnPageLoad = false;
        }

        protected void gvParties_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            DevExpress.Web.ASPxTextBox txtNewBusinessName = (DevExpress.Web.ASPxTextBox)gvParties.FindEditRowCellTemplateControl(gvParties.DataColumns["businessName"], "txtNewBusinessName");
            e.NewValues["businessName"] = txtBusinessName.Text;
            DevExpress.Web.ASPxTextBox txtNewContactName = (DevExpress.Web.ASPxTextBox)gvParties.FindEditRowCellTemplateControl(gvParties.DataColumns["contactName"], "txtNewContactName");
            e.NewValues["contactName"] = txtNewContactName.Text;
        }

        protected void cbParties_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            if (dsExclusions != null)
            {
                gvExclusions.DataSource = (DataSet)Session["dsExclusions"];
                gvExclusions.DataBind();
            }
        }

        //protected void gvParties_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
        //{
        //    try
        //    {
        //        dsExclusions = null;
        //        Session["dsExclusions"] = null;
        //        if (Convert.ToBoolean(Session["addParty"]) == false)
        //        {
        //            string exactSearch = @"<exactName>" + e.NewValues["businessName"].ToString().ToUpper() + "</exactName>";
        //            string nameSearch = @"<name>" + e.NewValues["businessName"].ToString().ToUpper() + "</name>";
        //            string partialName = @"<partialName>" + e.NewValues["businessName"].ToString().ToUpper() + "</partialName>";
        //            if (ParseSoap(exactSearch) > 0 || ParseSoap(nameSearch) > 0 || ParseSoap(partialName) > 0)
        //            {
        //                if (dsExclusions != null && dsExclusions.Tables.Count > 0)
        //                {
        //                    //gvExclusions.DataSource = dsExclusions;
        //                    //gvExclusions.DataBind();
        //                    //popExclusions.ShowOnPageLoad = true;
        //                    Session["dsExclusions"] = dsExclusions;
        //                    Session["isExclusions"] = true;
        //                    gvParties.JSProperties["cpHasExclusions"] = true;
        //                    e.Errors[gvParties.Columns["businessName"]] = "Name matches Exclusions";
        //                }
        //                //throw new Exception("Vendor/Party is on the exclusion list." + count + " records returned.");
        //            }
        //            else
        //            {
        //                //
        //            }
        //            gvParties.JSProperties["cpHasErrors"] = e.HasErrors;
        //            DataSet ds = DataAccess.GetDataSet2("select Classification,Name,First,Middle,Last,[Address 1],City,[State / Province],[Zip Code],[Exclusion Program],[Active Date] from SAMExclusions where (First + ' ' + Last = @businessName) or (Name = @businessName)",
        //                new string[] { "@businessName" },
        //                new string[] { e.NewValues["businessName"].ToString().ToUpper() });
        //            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        //            {
        //                throw new Exception("Vendor/Party is in the exclusion database.");
        //            }
        //        }
        //        else
        //        {
        //            gvParties.UpdateEdit();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint"))
        //        {
        //            throw new Exception("Business Name already exists.");
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }
        //}

        protected void btnSearchExclusions_Click(object sender, EventArgs e)
        {
            try
            {
                dsExclusions = null;
                Session["dsExclusions"] = null;
                string exactSearch = @"<exactName>" + txtBusinessName.Text.Replace("&", "").ToUpper() + "</exactName>";
                string nameSearch = @"<name>" + txtBusinessName.Text.Replace("&", "").ToUpper() + "</name>";
                string partialName = @"<partialName>" + txtBusinessName.Text.Replace("&", "").ToUpper() + "</partialName>";
                ParseSoap(exactSearch);
                ParseSoap(nameSearch);
                ParseSoap(partialName);
                DataSet dsTableExclusions = DataAccess.GetDataSet("usp_getExclusions", new string[] { "@businessName", "@isFuzzy" }, new string[] { txtBusinessName.Text.ToUpper(), cbFuzzy.Checked.ToString() });
                if (dsTableExclusions != null && dsTableExclusions.Tables.Count > 0 && dsTableExclusions.Tables[0].Rows.Count > 0)
                {
                    if (dsExclusions == null || dsExclusions.Tables.Count == 0 || dsExclusions.Tables[0].Rows.Count == 0)
                    {
                        dsExclusions = new DataSet();
                        DataTable dt = dsTableExclusions.Tables[0].Clone();
                        dsExclusions.Tables.Add(dt);
                    }
                    //else
                    //{
                        dsExclusions.Tables[0].Merge(dsTableExclusions.Tables[0], true, MissingSchemaAction.Ignore);
                    //}
                }
                if (hdnOIGCheck.Count > 0 && hdnOIGCheck.Contains("check"))
                {
                    hdnOIGCheck.Remove("check");
                }
                hdnOIGCheck.Add("check", "true");
                if (dsExclusions != null && dsExclusions.Tables.Count > 0)
                {
                    DataTable dt = dsExclusions.Tables[0].DefaultView.ToTable(true, "Classification", "EntityName", "FirstName", "MiddleInitial", "LastName", "Address", "City", "State", "ZipCode", "ExclusionType", "ActiveDate");
                    dsExclusions.Tables.RemoveAt(0);
                    dsExclusions.Tables.Add(dt);
                    gvExclusions.DataSource = dsExclusions;
                    gvExclusions.DataBind();
                    Session["dsExclusions"] = dsExclusions;
                    divExclusionResults.Visible = true;
                }      
                else
                {
                    gvParties.AddNewRow();
                    //DevExpress.Web.ASPxTextBox txtNewBusinessName = (DevExpress.Web.ASPxTextBox)gvParties.FindEditRowCellTemplateControl(gvParties.DataColumns["businessName"], "txtNewBusinessName");
                    //txtNewBusinessName.Text = txtBusinessName.Text;
                    DevExpress.Web.ASPxTextBox txtNewContactName = (DevExpress.Web.ASPxTextBox)gvParties.FindEditRowCellTemplateControl(gvParties.DataColumns["contactName"], "txtNewContactName");
                    txtNewContactName.Focus();
                    popExclusions.ShowOnPageLoad = false;
                }
                int oigCount = dsExclusions == null || dsExclusions.Tables.Count == 0 ? 0 : dsExclusions.Tables[0].Rows.Count;
                string isAdded = hdnOIGCheck.Get("check").ToString() == "true" ? "1" : "0";
                System.Data.DataSet dsLog = DataAccess.GetDataSet("usp_logOIG", 
                    new string[] { "@contractId", "@searchDate", "@searchTerm", "@searchResults", "@isAdded", "@addedBy"}, 
                    new string[] { "0", DateTime.Now.ToString(), txtBusinessName.Text.ToUpper(), oigCount.ToString(), "0", Session["UserID"].ToString() });
//                usp_logOIG
//@contractId int,
//@searchDate datetime,
//@searchTerm varchar(255),
//@searchResults int,
//@isAdded int,
//@addedBy int
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                {
                    throw new Exception("Business Name already exists.");
                }
                else
                {
                    throw;
                }
            }
        }

        protected void gvParties_CustomErrorText(object sender, DevExpress.Web.ASPxGridViewCustomErrorTextEventArgs e)
        {
            if (e.ErrorText.StartsWith("Violation of UNIQUE KEY constraint"))
            {
                e.ErrorText = "Business Name already exists. Business Names must be unique.";
            }
        }

        protected void gvOIG_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.Caption == "Party")
            {
                string company = gvOIG.GetRowValues(e.VisibleIndex, new string[] { "businessName" }).ToString();
                if (company == "Admin" || e.CellValue.ToString() == "0")
                {
                    e.Cell.Controls.Clear();
                    DevExpress.Web.ASPxLabel lbl = new DevExpress.Web.ASPxLabel();
                    lbl.Text = "Admin";
                    e.Cell.Controls.Add(lbl);
                }
            }
        }

        protected void dsRoute_Inserting(object sender, System.Web.UI.WebControls.SqlDataSourceCommandEventArgs e)
        {
            e.Command.Parameters["@routingId"].Value = gvRoutes.GetRowValues(0, new string[] { "routingId" });
        }

        protected void dsParty_Inserting(object sender, System.Web.UI.WebControls.SqlDataSourceCommandEventArgs e)
        {
            String x = GetQuery(e.Command.CommandText, e.Command.Parameters);
        }

        protected void dsContractTypes_Deleted(object sender, System.Web.UI.WebControls.SqlDataSourceStatusEventArgs e)
        {
            try
            {
                if (e.Command.Parameters["@count"].Value.ToString() == "0")
                {
                    throw new Exception("This contract type cannot be deleted because it is associated with existing contracts. This contract type has been automatically marked inactive.");
                }
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void dsDepartments_Deleted(object sender, System.Web.UI.WebControls.SqlDataSourceStatusEventArgs e)
        {
            try
            {
                if (e.Command.Parameters["@count"].Value.ToString() == "0")
                {
                    throw new Exception("This department cannot be deleted because it is associated with existing contracts. This department has been automatically marked inactive.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void dsUploadDocType_Deleted(object sender, System.Web.UI.WebControls.SqlDataSourceStatusEventArgs e)
        {
            try
            {
                if (e.Command.Parameters["@count"].Value.ToString() == "0")
                {
                    throw new Exception("This document type cannot be deleted because it is associated with existing contracts. This document type has been automatically marked inactive.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void dsPartyTypes_Deleted(object sender, System.Web.UI.WebControls.SqlDataSourceStatusEventArgs e)
        {
            try
            {
                if (e.Command.Parameters["@count"].Value.ToString() == "0")
                {
                    throw new Exception("This party type cannot be deleted because it is associated with existing contracts. This party type has been automatically marked inactive.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void dsUser_Deleted(object sender, System.Web.UI.WebControls.SqlDataSourceStatusEventArgs e)
        {
            try
            {
                if (e.Command.Parameters["@count"].Value.ToString() == "0")
                {
                    throw new Exception("This user cannot be deleted because it is associated with existing contracts. This user has been automatically marked inactive.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void dsUserRole_Deleted(object sender, System.Web.UI.WebControls.SqlDataSourceStatusEventArgs e)
        {
            try
            {
                if (e.Command.Parameters["@count"].Value.ToString() == "0")
                {
                    throw new Exception("This user role cannot be deleted because it is associated with existing contracts. This user role has been automatically marked inactive.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void dsParty_Deleted(object sender, System.Web.UI.WebControls.SqlDataSourceStatusEventArgs e)
        {
            try
            {
                if (e.Command.Parameters["@count"].Value.ToString() == "0")
                {
                    throw new Exception("This party cannot be deleted because it is associated with existing contracts. This party has been automatically marked inactive.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void Page_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError();
            Message.Alert(exc.Message);

            // Handle specific exception.
            if (exc is System.Web.HttpUnhandledException)
            {
                Message.Alert("An error occurred on this page. Please verify your information to resolve the issue.");
            }
            // Clear the error from the server.
            Server.ClearError();
        }

        protected void dsPurchaseGroups_Deleted(object sender, System.Web.UI.WebControls.SqlDataSourceStatusEventArgs e)
        {
            try
            {
                if (e.Command.Parameters["@count"].Value.ToString() == "0")
                {
                    throw new Exception("This purchase group cannot be deleted because it is associated with existing contracts. This purchase group has been automatically marked inactive.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void dsEOC_Deleted(object sender, System.Web.UI.WebControls.SqlDataSourceStatusEventArgs e)
        {
            try
            {
                if (e.Command.Parameters["@count"].Value.ToString() == "0")
                {
                    throw new Exception("This EOC cannot be deleted because it is associated with existing contracts. This EOC has been automatically marked inactive.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void btnSaveDescription_Click(object sender, EventArgs e)
        {
            try
            {
                dsRoutes.Update();
                Message.Alert("Route description has been updated.");
            }
            catch (Exception ex)
            {
                Message.Alert(ex.Message);
            }
        }

        protected void gvContractTypes_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
        {
            if (e.Exception != null && e.Exception.Message.Contains("existing contracts"))
            {
                gvContractTypes.JSProperties["cpMessage"] = e.Exception.Message;
                e.ExceptionHandled = true;
                gvContractTypes.DataBind();
            }
        }

        protected void gvEOC_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
        {
            if (e.Exception != null && e.Exception.Message.Contains("existing contracts"))
            {
                gvEOC.JSProperties["cpMessage"] = e.Exception.Message;
                e.ExceptionHandled = true;
                gvEOC.DataBind();
            }
        }

        protected void gvPartyTypes_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
        {
            if (e.Exception != null && e.Exception.Message.Contains("existing contracts"))
            {
                gvPartyTypes.JSProperties["cpMessage"] = e.Exception.Message;
                e.ExceptionHandled = true;
                gvPartyTypes.DataBind();
            }
        }

        protected void gvPurchaseGroups_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
        {
            if (e.Exception != null && e.Exception.Message.Contains("existing contracts"))
            {
                gvPurchaseGroups.JSProperties["cpMessage"] = e.Exception.Message;
                e.ExceptionHandled = true;
                gvPurchaseGroups.DataBind();
            }
        }

        protected void gvDepartments_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
        {
            if (e.Exception != null && e.Exception.Message.Contains("existing contracts"))
            {
                gvDepartments.JSProperties["cpMessage"] = e.Exception.Message;
                e.ExceptionHandled = true;
                gvDepartments.DataBind();
            }
        }

        protected void gvParties_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
        {
            if (e.Exception != null && e.Exception.Message.Contains("existing contracts"))
            {
                gvParties.JSProperties["cpMessage"] = e.Exception.Message;
                e.ExceptionHandled = true;
                gvParties.DataBind();
            }
        }

        protected void dsUser_Inserting(object sender, System.Web.UI.WebControls.SqlDataSourceCommandEventArgs e)
        {
            string x = GetQuery(e.Command.CommandText, e.Command.Parameters);
        }

        protected void dsUser_Updating(object sender, System.Web.UI.WebControls.SqlDataSourceCommandEventArgs e)
        {
            string x = GetQuery(e.Command.CommandText, e.Command.Parameters);
        }

        protected void gvUploadDocumentTypes_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
        {
            if (e.Exception != null && e.Exception.Message.Contains("existing contracts"))
            {
                gvUploadDocumentTypes.JSProperties["cpMessage"] = e.Exception.Message;
                e.ExceptionHandled = true;
                gvUploadDocumentTypes.DataBind();
            }
        }

        protected void gvParties_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableDataCellEventArgs e)
        {
            try
            {
                if (e.DataColumn.Caption == "Existing Contracts" && e.KeyValue != null)
                {
                    DataSet ds = DataAccess.GetDataSet2("select distinct contractId from contracts where partyId = @partyId", new string[] { "@partyId" }, new string[] { e.KeyValue.ToString() });
                    //DataSet ds = null;
                    //if (Session["dsContractIds"] != null)
                    //{
                    //    ds = (DataSet)Session["dsContractIds"];
                    //}
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        //DataView dv = ds.Tables[0].DefaultView;
                        //dv.RowFilter = "partyId = " + e.KeyValue.ToString();
                        //dv.Sort = "contractId";
                        int i = 0;
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            
                            DevExpress.Web.ASPxHyperLink hl = new DevExpress.Web.ASPxHyperLink();
                            hl.Text = dr["contractId"].ToString();
                            hl.NavigateUrl = "contractDetails.aspx?from=admin&id=" + dr["contractId"].ToString();
                            hl.Target = "_blank";
                            e.Cell.Controls.Add(hl);
                            if (i < ds.Tables[0].Rows.Count)
                            {
                                System.Web.UI.HtmlControls.HtmlGenericControl span = new System.Web.UI.HtmlControls.HtmlGenericControl("span");
                                if ((i+1) % 3 == 0)
                                {
                                    span.InnerHtml = "<br/>";
                                } else
                                {
                                    span.InnerHtml = "&nbsp;";
                                }
                                e.Cell.Controls.Add(span);
                            }
                            i++;
                        }
                    }
                }
            }
            catch (Exception  ex)
            {
                Message.Alert(ex.Message);
            }
        }

        protected void gvPartyRequests_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e)
        {
            if (e.VisibleIndex >= 0 && e.GetValue("requestStatusId").ToString() == "2")
                
                foreach (System.Web.UI.WebControls.TableCell cell in e.Row.Cells)
                {
                    cell.Attributes.Add("onclick", "event.cancelBubble = true");
                    cell.Attributes.Add("style", "background-color:lightgray");
                }
        }
    }

}