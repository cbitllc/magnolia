﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Magnolia.app
{
    public partial class adminUsers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ((Master)Page.Master).SetHeader = "Admin Users/User Roles";
            ((Master)Page.Master).SetHelpURL = "admin.aspx";
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["userId"] != null)
                {
                    ddlUsers.DataBind();
                    ddlUsers.SelectedIndex = ddlUsers.Items.FindByValue(Request.QueryString["userId"]).Index;
                    cbUsers_Callback(ddlUsers, new DevExpress.Web.CallbackEventArgsBase(Request.QueryString["userId"]));
                }
            }
        }

        protected void cbUsers_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            dsUser.SelectParameters["userId"].DefaultValue = e.Parameter;
            formUsers.DataBind();
        }

        protected void btnSaveUser_Click(object sender, EventArgs e)
        {
            if (ddlUsers.Value.ToString() == "0")
            {
                try
                {
                    if (deBypassStart.Date < Convert.ToDateTime("1/1/1753") || deBypassStart.Date > Convert.ToDateTime("12/31/9999"))
                    {
                        dsUser.InsertParameters.Remove(dsUser.InsertParameters["bypassStart"]);
                    }
                    if (deBypassEnd.Date < Convert.ToDateTime("1/1/1753") || deBypassEnd.Date > Convert.ToDateTime("12/31/9999"))
                    {
                        dsUser.InsertParameters.Remove(dsUser.InsertParameters["bypassEnd"]);
                    }
                    dsUser.Insert();
                    ddlUsers.DataBind();
                    ddlUsers.SelectedIndex = ddlUsers.Items.FindByText(txtUserFullName.Text.ToUpper()).Index;
                    Message.Alert("User has been added.");
                }
                catch (Exception ex)
                {
                    Message.Alert(ex.Message);
                }
            }
            else
            {
                try
                {
                    if (deBypassStart.Date > Convert.ToDateTime("1/1/1753") || deBypassStart.Date < Convert.ToDateTime("12/31/9999"))
                    {
                        dsUser.UpdateParameters.Remove(dsUser.UpdateParameters["bypassStart"]);
                    }
                    if (deBypassEnd.Date < Convert.ToDateTime("1/1/1753") || deBypassEnd.Date > Convert.ToDateTime("12/31/9999"))
                    {
                        dsUser.UpdateParameters.Remove(dsUser.UpdateParameters["bypassEnd"]);
                    }
                    dsUser.Update();
                    Message.Alert("User has been updated.");
                }
                catch (Exception ex)
                {
                    Message.Alert(ex.Message);
                }
            }
        }

        protected void btnDeleteUser_Click(object sender, EventArgs e)
        {
            try
            {
                dsUser.Delete();
                ddlUsers.SelectedIndex = 0;
                cbUsers_Callback(ddlUsers, new DevExpress.Web.CallbackEventArgsBase("0"));
                formUsers.DataBind();
            }
            catch (Exception ex)
            {
                Message.Alert(ex.Message);
                Server.ClearError();
                formUsers.DataBind();
            }
        }

        protected void cbUserRoles_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            dsUserRole.SelectParameters["roleId"].DefaultValue = e.Parameter;
            formUserRoles.DataBind();
        }

        protected void btnSaveUserRole_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlUserRolesEdit.Value.ToString() == "0")
                {
                    try
                    {
                        dsUserRole.Insert();
                        ddlUserRolesEdit.DataBind();
                        DevExpress.Web.ListEditItem obj = ddlUserRolesEdit.Items.FindByText(txtRoleName.Text.ToUpper());
                        if (obj != null)
                        {
                            ddlUserRolesEdit.SelectedIndex = obj.Index;
                        }
                        Message.Alert("User Role has been added.");
                    }
                    catch (Exception ex)
                    {
                        Message.Alert(ex.Message);
                    }
                }
                else
                {
                    try
                    {
                        dsUserRole.Update();
                        Message.Alert("User Role has been updated.");
                    }
                    catch (Exception ex)
                    {
                        Message.Alert(ex.Message);
                    }
                }
                ddlUserRoles.DataBind();
            }
            catch (Exception ex)
            {
                Message.Alert(ex.Message);
            }
        }

        protected void btnDeleteUserRole_Click(object sender, EventArgs e)
        {
            try
            {
                dsUserRole.Delete();
                ddlUserRolesEdit.SelectedIndex = 0;
                cbUserRoles_Callback(ddlUserRolesEdit, new DevExpress.Web.CallbackEventArgsBase("0"));
                ddlUserRoles.DataBind();
            }
            catch (Exception ex)
            {
                Message.Alert(ex.Message);
                Server.ClearError();
                cbUserRoles_Callback(ddlUserRolesEdit, new DevExpress.Web.CallbackEventArgsBase(ddlUserRolesEdit.Value.ToString()));
            }
        }

        protected void dsUser_Deleted(object sender, System.Web.UI.WebControls.SqlDataSourceStatusEventArgs e)
        {
            try
            {
                if (e.Command.Parameters["@count"].Value.ToString() == "0")
                {
                    throw new Exception("This user cannot be deleted because it is associated with existing contracts. This user has been automatically marked inactive.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void dsUserRole_Deleted(object sender, System.Web.UI.WebControls.SqlDataSourceStatusEventArgs e)
        {
            try
            {
                if (e.Command.Parameters["@count"].Value.ToString() == "0")
                {
                    throw new Exception("This user role cannot be deleted because it is associated with existing contracts. This user role has been automatically marked inactive.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void dsUser_Inserting(object sender, System.Web.UI.WebControls.SqlDataSourceCommandEventArgs e)
        {
            string x = GetQuery(e.Command.CommandText, e.Command.Parameters);
        }

        protected void dsUser_Updating(object sender, System.Web.UI.WebControls.SqlDataSourceCommandEventArgs e)
        {
            string x = GetQuery(e.Command.CommandText, e.Command.Parameters);
        }

        private string GetQuery(string CommandQuery, System.Data.Common.DbParameterCollection parmCollection)
        {
            string query = CommandQuery;
            string parms = "";
            foreach (System.Data.SqlClient.SqlParameter parm in parmCollection)
            {
                parms += parm.ParameterName + " = ";
                if (parm.Value == null)
                {
                    parms += "null,";

                }
                else
                {
                    if (parm.DbType == System.Data.DbType.String || parm.DbType == System.Data.DbType.DateTime)
                    {
                        parms += "'" + parm.Value.ToString() + "',";
                    }
                    else
                    {
                        parms += parm.Value.ToString() + ",";
                    }
                }
            }
            query = query + " " + parms.Substring(0, parms.Length - 1);
            return query;
        }

        private void Page_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError();
            Message.Alert(exc.Message);

            // Handle specific exception.
            if (exc is System.Web.HttpUnhandledException)
            {
                Message.Alert("An error occurred on this page. Please verify your information to resolve the issue.");
            }
            // Clear the error from the server.
            Server.ClearError();
        }
    }
}