﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="adminUpdates.aspx.cs" Inherits="Magnolia.app.adminUpdates" MasterPageFile="~/app/Master.master" %>
<%@ Register Assembly="DevExpress.Web.v20.2, Version=20.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <style>
        input {text-transform:uppercase;}
        textarea {text-transform:uppercase;}
        span {text-transform:uppercase;}
        .spanTitle{text-transform:capitalize;}
        .gridHeader {  
            background-color: white;  
            color:black;
        }  
        .gridHeader a {  
            color: black;  
        }  
        .dxgvTable caption  
        {  
            color:White;  
            background-color:DarkGreen;  
        } 
    </style>
    <div style="width:50%; top:5px; left:0px; height:100%; position:relative; max-width:1000px; display:inline-block; overflow:auto; vertical-align:top;">
        <dx:ASPxGridView ID="gvSAMExclusions" runat="server" DataSourceID="dsSamExclusions">
            <Columns>
                <dx:GridViewDataColumn FieldName="fileName" Caption="File Name"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="updateDate" Caption="Update Date"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="recordCount" Caption="Record Count"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="success" Caption="Success?"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="errorMessage" Caption="Error Message"></dx:GridViewDataColumn>
            </Columns>
            <Settings ShowTitlePanel="true" />
            <SettingsText Title="SAM Exclusion Data" />
            <Styles>
                <Header Font-Bold="true"></Header>
                <TitlePanel BackColor="DarkGreen" ForeColor="White"></TitlePanel>
            </Styles>
        </dx:ASPxGridView>
        <br /><br />
        <dx:ASPxLabel ID="lblRestoreSAM" runat="server" Text="Restore SAM Data from Backup"></dx:ASPxLabel>
        &nbsp;&nbsp;&nbsp;
        <dx:ASPxButton ID="btnRestoreSAM" runat="server" Text="Restore SAM Exclusions" OnClick="btnRestoreSAM_Click"></dx:ASPxButton>
    </div>
    <div style="width:48%; top:20px; height:100%; left:2%; position:relative; overflow:auto; max-width:1000px; display:inline-block; vertical-align:top; ">
        <dx:ASPxGridView ID="gvMeditechUpdates" runat="server" DataSourceID="dsMeditech">
            <Columns>
                <dx:GridViewDataColumn FieldName="fileName" Caption="File Name"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="fileDate" Caption="File Date"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="updateDate" Caption="Update Date"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="recordCount" Caption="Record Count"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="success" Caption="Success?"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="errorMessage" Caption="Error Message"></dx:GridViewDataColumn>
            </Columns>
            <Settings ShowTitlePanel="true" />
            <SettingsText Title="Meditech Files" />
            <Styles>
                <Header Font-Bold="true"></Header>
                <TitlePanel BackColor="DarkGreen" ForeColor="White"></TitlePanel>
            </Styles>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="dsSamExclusions" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                SelectCommand="usp_getSamExlusionTracking" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        <asp:SqlDataSource ID="dsMeditech" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                SelectCommand="usp_getMeditechFileInfo" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        <br />
        <table style="padding:10px;">
            <tr style="padding:10px">
                <td style="padding:10px"><dx:ASPxLabel ID="lblRestoreEOC" runat="server" Text="Restore EOC Data from Backup"></dx:ASPxLabel></td>
                <td style="padding:10px"><dx:ASPxButton ID="btnRestoreEOC" runat="server" Text="Restore EOCs" OnClick="btnRestoreEOC_Click"></dx:ASPxButton></td>
            </tr>
            <tr style="padding:10px">
                <td style="padding:10px"><dx:ASPxLabel ID="lblRestoreDept" runat="server" Text="Restore Department Data from Backup"></dx:ASPxLabel></td>
                <td style="padding:10px"><dx:ASPxButton ID="btnRestoreDept" runat="server" Text="Restore Departments" OnClick="btnRestoreDept_Click"></dx:ASPxButton></td>
            </tr>
            <tr style="padding:10px">
                <td style="padding:10px"><dx:ASPxLabel ID="lblRestoreVendor" runat="server" Text="Restore Vendor Data from Backup"></dx:ASPxLabel></td>
                <td style="padding:10px"><dx:ASPxButton ID="btnRestoreVendor" runat="server" Text="Restore Vendors" OnClick="btnRestoreVendor_Click"></dx:ASPxButton></td>
            </tr>
        </table>
    </div>
</asp:Content>