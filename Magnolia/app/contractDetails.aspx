﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="contractDetails.aspx.cs" Inherits="Magnolia.app.contractDetails" MasterPageFile="~/app/Master.master" %>
<%@ MasterType VirtualPath="~/app/Master.master" %>
<%@ Register Assembly="DevExpress.Web.v20.2, Version=20.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.Bootstrap.v20.2, Version=20.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.Bootstrap" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        input {text-transform:uppercase;}
        textarea {text-transform:uppercase;}
        span {text-transform:uppercase;}
        .spanTitle{text-transform:capitalize;}
        .centerHeader{text-align:center;}
        .hint{width:400px;}
        .flex {
          display: inline-flex; 
          flex-direction: row;
          flex: 1 1 auto;
          width: 100%;
        }
    </style>
    <script type="text/javascript">
        function OnActionChanged(s, e) {
            if (ddlContractTypes.GetText() == 'MAINTENANCE' && (ddlActions.GetText() == 'RENEWAL' || ddlActions.GetText() == 'ANNUAL REVIEW')) {
                alert('For maintenance contract renewal requests, obtain a 12 month service history from our vendor and attach to the Contract Summary Sheet. As your request moves up the approval ladder, reviewers can see the value in the requested contract based on the difference between actual services and keeping the equipment on the maintenance contract.');
            }
            if (ddlContractTypes.GetValue() == '7') {
                formContract.GetItemByName('EACApproval').SetVisible(true);
                formContract.GetItemByName('EACApprovalDate').SetVisible(true);
            }
            else {
                formContract.GetItemByName('EACApproval').SetVisible(false);
                formContract.GetItemByName('EACApprovalDate').SetVisible(false);
            }
            if (ddlContractTypes.GetValue() == '6') {
                formContract.GetItemByName('BOTApproval').SetVisible(true);
                formContract.GetItemByName('BOTApprovalDate').SetVisible(true);
                formContract.GetItemByName('MPSApproval').SetVisible(true);
                formContract.GetItemByName('MPSApprovalDate').SetVisible(true);
            }
            else {
                formContract.GetItemByName('BOTApproval').SetVisible(false);
                formContract.GetItemByName('BOTApprovalDate').SetVisible(false);
                formContract.GetItemByName('MPSApproval').SetVisible(false);
                formContract.GetItemByName('MPSApprovalDate').SetVisible(false);
            }
            if (ddlActions.GetValue() == "1") {
                //ddlMasterContracts.SetVisible(false);
                ddlMasterContracts.SetVisible(false);
                formContract.GetItemByName('masterContractNumber').SetCaption(" ");
                document.getElementById('aMasterContract').style.visibility = 'hidden';
            }
            else {
                //ddlMasterContracts.SetVisible(true);
                if (ddlActions.GetValue() == "5") {
                    document.getElementById('aEndDate').style.visibility = 'visible';
                }
                else {
                    document.getElementById('aEndDate').style.visibility = 'hidden';
                }
                ddlMasterContracts.SetVisible(true);
                formContract.GetItemByName('masterContractNumber').SetCaption("Master Contract:");
                document.getElementById('aMasterContract').style.visibility = 'visible';
            }
        }
        function OnPurchasingGroupChanged(s, e) {
            if (ddlPurchasingGroup.GetValue() == '1') {
                //formContract.GetItemByName('purchaseGroup').SetVisible(true);
                ddlPurchaseGroups.SetVisible(true);
                formContract.GetItemByName('purchaseGroup').SetCaption("Purchase Group");
                document.getElementById('aPurchaseGroups').style.visibility = 'visible';
            } else {
                //formContract.GetItemByName('purchaseGroup').SetVisible(false);
                ddlPurchaseGroups.SetVisible(false);
                formContract.GetItemByName('purchaseGroup').SetCaption(" ");
                document.getElementById('aPurchaseGroups').style.visibility = 'hidden';
            }
            OnDateChanged();
        }
        function OnDateChanged(s, e) {
            deEndDate.SetIsValid(true);
            deStartDate.SetIsValid(true);
            if (deStartDate.GetDate() != null && deEndDate.GetDate() != null) {
                if (deStartDate.GetDate() > deEndDate.GetDate()) {
                    alert('The Contract End Date cannnot come before the Contract Start Date.');
                    deEndDate.SetIsValid(false);
                    deStartDate.SetIsValid(false);
                } else {
                    var length = Math.floor((new Date(deEndDate.GetDate()) - new Date(deStartDate.GetDate())) / (1000 * 60 * 60 * 24));
                    if (length > 1826) {
                        alert('The Contract length cannot be greater than five years. The dates you have entered are greater than five years.');
                        deEndDate.SetIsValid(false);
                        deStartDate.SetIsValid(false);
                    } else {
                        deEndDate.SetIsValid(true);
                        deStartDate.SetIsValid(true);
                    }
                    diff = new Date(
                        deEndDate.GetDate().getFullYear() - deStartDate.GetDate().getFullYear(),
                        deEndDate.GetDate().getMonth() - deStartDate.GetDate().getMonth(),
                        deEndDate.GetDate().getDate() - deStartDate.GetDate().getDate()
                    );
                    var answer = '';
                    var years = diff.getYear();
                    if (years > 0) {
                        answer = years + ((years == 1) ? ' Year' : ' Years');
                    }
                    var months = diff.getMonth();
                    if (months > 0) {
                        if (answer.length > 0) {
                            answer = answer + ' ';
                        }
                        answer += months + ((months == 1) ? ' Month' : ' Months');
                    }
                    var days = diff.getDate();
                    if (days > 0) {
                        if (answer.length > 0) {
                            answer = answer + ' ';
                        }
                        answer += days + ((days == 1) ? ' Day' : ' Days');
                    }
                    if (deStartDate.GetDate() == deEndDate.GetDate()) {
                        answer = '0';
                    }
                    else {
                        answer = ((deEndDate.GetDate() - deStartDate.GetDate()) / 365 / (24 * 60 * 60 * 1000)).toFixed(2);
                    }
                    txtContractLength.SetText(answer);
                    lblContractLength.SetText(years);
                    CalculateTotalCost();
                }
            }
        }
        function CalculateTotalCost(s, e) {
            var length = txtContractLength.GetText();
            var totalCost = spinAnnualCost.GetValue();
            if (length >= 1) {
                totalCost = totalCost * length;
            }
            spinEstimatedTotalCost.SetValue(totalCost);
        }
        function OutclauseChanged(s, e) {
            if (s.GetValue() == '1') {
                formContract.GetItemByName('notificationDaysBreach').SetVisible(true);
                formContract.GetItemByName('notificationDaysNoBreach').SetVisible(true);
            } else {
                //if (confirm('Choosing No Outclause will require Chief Officer initials before it can be submitted for signatures.')){
                formContract.GetItemByName('notificationDaysBreach').SetVisible(false);
                formContract.GetItemByName('notificationDaysNoBreach').SetVisible(false);
                //}
            }
        }
        function ShowBreach(s, e) {
            if (s.GetText() == 'Yes') {
                formContract.GetItemByName('notificationDaysBreach').SetVisible(true);
                formContract.GetItemByName('notificationDaysNoBreach').SetVisible(true);
            } else {
                formContract.GetItemByName('notificationDaysBreach').SetVisible(false);
                formContract.GetItemByName('notificationDaysNoBreach').SetVisible(false);
            }
            //BreachDaysCheck(s, e);
        }
        function BreachDaysCheck(s, e) {
            if (spinDaysBreach.GetValue() != null && spinDaysNoBreach.GetValue() != null) {
                if (spinDaysBreach.GetValue() > spinDaysNoBreach.GetValue()) {
                    alert('Notification Days Breach is more than Notification Days No Breach. Please verify your entries.');
                }
            }
        }
        function AnnualCostChanged(s, e) {
            if ((spinEstimatedTotalCost.GetValue() != null && spinEstimatedTotalCost.GetValue() >= 50000) || (spinAnnualCost.GetValue() !== null && spinAnnualCost.GetValue() >= 50000)) {
                //alert('Annual Cost > $50,000 will require Chief Officer initials before it can be submitted for signatures.');
                //if (confirm('Annual Cost > $50,000 will require Chief Officer initials before it can be submitted for signatures.') == false) {
                //    spinAnnualCost.SetNumber(0.00);
                //}
                //else {
                formContract.GetItemByName('BOTApproval').SetVisible(true);
                formContract.GetItemByName('BOTApprovalDate').SetVisible(true);
                //formContract.GetItemByName('trusteeApproval').SetVisible(true);
                //formContract.GetItemByName('trusteeApprovalDate').SetVisible(true);
                //}
            }
            else {
                formContract.GetItemByName('BOTApproval').SetVisible(false);
                formContract.GetItemByName('BOTApprovalDate').SetVisible(false);
                //formContract.GetItemByName('trusteeApproval').SetVisible(false);
                //formContract.GetItemByName('trusteeApprovalDate').SetVisible(false);
            }
            CalculateDifference(s, e);
        }
        function InitAnnualCost(s, e) {
            if ((spinEstimatedTotalCost.GetValue() != null && spinEstimatedTotalCost.GetValue() >= 50000) || (spinAnnualCost.GetValue() != null && spinAnnualCost.GetValue() >= 50000)) {
                formContract.GetItemByName('BOTApproval').SetVisible(true);
                formContract.GetItemByName('BOTApprovalDate').SetVisible(true);
                //formContract.GetItemByName('trusteeApproval').SetVisible(true);
                //formContract.GetItemByName('trusteeApprovalDate').SetVisible(true);
            }
            else {
                formContract.GetItemByName('BOTApproval').SetVisible(false);
                formContract.GetItemByName('BOTApprovalDate').SetVisible(false);
                //formContract.GetItemByName('trusteeApproval').SetVisible(false);
                //formContract.GetItemByName('trusteeApprovalDate').SetVisible(false);
            }
            CalculateDifference(s, e);
        }
        function CalculateDifference(s, e) {
            if (spinAnnualCost.GetValue() != null) {
                if (spinPriorYearCost.GetValue() != null) {
                    var annual = spinAnnualCost.GetNumber();
                    var prior = spinPriorYearCost.GetNumber();
                    var difference = parseFloat(annual - prior);
                    spinCostDifference.SetNumber(parseFloat(difference).toFixed(2));
                }
                else {
                    spinCostDifference.SetNumber(parseFloat(spinAnnualCost.GetNumber()).toFixed(2));
                }
            }
        }
        function OnPaymentTermChange(s, e) {
            if (s.GetValue() == 'Other') {
                formContract.GetItemByName('OtherTerm').SetVisible(true);
                formContract.GetItemByName('paymentOtherSpace').SetVisible(false);
                //txtPaymenetOther.SetFocus();
            } else {
                formContract.GetItemByName('OtherTerm').SetVisible(false);
                formContract.GetItemByName('paymentOtherSpace').SetVisible(true);
            }
        }
        function OnReferenceChange(s, e) {
            //formContract.GetItemByName('BooksReference').SetCaption("");
            //txtBooksReference.SetVisible(false);
            //formContract.GetItemByName('MedicareAdjustmentsReference').SetCaption("");
            //txtMedicareReference.SetVisible(false);
            //txtHipaaReference.SetVisible(false);
            //formContract.GetItemByName('HIPAAReference').SetCaption("");
            //formContract.GetItemByName('GoverningLawReference').SetCaption("");
            //txtGoverningLawReference.SetVisible(false);
            //if (ddlAccessToBooks.GetText() == "Yes") {
            //    txtBooksReference.SetVisible(true);
            //    formContract.GetItemByName('BooksReference').SetCaption("Books/Records Reference");
            //} else {
            //    txtBooksReference.SetVisible(false);
            //    formContract.GetItemByName('BooksReference').SetCaption(" ");
            //}
            //if (ddlMedicareAdjustments.GetText() == "Yes") {
            //    formContract.GetItemByName('MedicareAdjustmentsReference').SetCaption("Medicare Reference");
            //    txtMedicareReference.SetVisible(true);
            //} else {
            //    formContract.GetItemByName('MedicareAdjustmentsReference').SetCaption(" ");
            //    txtMedicareReference.SetVisible(false);
            //}
            //if (ddlAccessToBooks.GetText() == "Yes" || ddlMedicareAdjustments.GetText() == "Yes") {
            //    formContract.GetItemByName('BooksReference').SetVisible(true);
            //    formContract.GetItemByName('MedicareAdjustmentsReference').SetVisible(true);
            //} else {
            //    formContract.GetItemByName('BooksReference').SetVisible(false);
            //    formContract.GetItemByName('MedicareAdjustmentsReference').SetVisible(false);
            //}
            //if (ddlHipaaBA.GetText() == "Yes") {
            //    txtHipaaReference.SetVisible(true);
            //    formContract.GetItemByName('HIPAAReference').SetCaption("HIPAA/BAA Reference");
            //} else {
            //    txtHipaaReference.SetVisible(false);
            //    formContract.GetItemByName('HIPAAReference').SetCaption(" ");
            //}
            //if (ddlGoverningLaw.GetText() == "Yes") {
            //    formContract.GetItemByName('GoverningLawReference').SetCaption("Governing Law Reference");
            //    txtGoverningLawReference.SetVisible(true);
            //} else {
            //    formContract.GetItemByName('GoverningLawReference').SetCaption(" ");
            //    txtGoverningLawReference.SetVisible(false);
            //}
            //if (ddlHipaaBA.GetText() == "Yes" || ddlGoverningLaw.GetText() == "Yes") {
            //    formContract.GetItemByName('HIPAAReference').SetVisible(true);
            //    formContract.GetItemByName('GoverningLawReference').SetVisible(true);
            //} else {
            //    formContract.GetItemByName('HIPAAReference').SetVisible(false);
            //    formContract.GetItemByName('GoverningLawReference').SetVisible(false);
            //}
        }
        function OnSave(s, e) {
            var message = "";
            deEndDate.SetIsValid(true);
            deStartDate.SetIsValid(true);
            if (deStartDate.GetDate() > deEndDate.GetDate()) {
                message = 'The Contract End Date cannnot come before the Contract Start Date.';
                deEndDate.SetIsValid(false);
                deStartDate.SetIsValid(false);
            }
            var length = Math.floor((new Date(deEndDate.GetDate()) - new Date(deStartDate.GetDate())) / (1000 * 60 * 60 * 24));
            if (length > 1826) {
                if (message != "") {
                    message = message + "\r\n";
                }
                message = message + 'The Contract length cannot be greater than five years.';
                deEndDate.SetIsValid(false);
                deStartDate.SetIsValid(false);
            }
            if (ddlOutclause.GetValue() == "1") {
                if (spinDaysBreach.GetValue() == null && spinDaysNoBreach.GetValue() == null) {
                    if (message != "") {
                        message = message + "\r\n";
                    }
                    message = message + "Notification days are required when there is an Outclause.";
                    spinDaysBreach.SetIsValid(false);
                    spinDaysNoBreach.SetIsValid(false);
                }
            }
            else {
                spinDaysBreach.SetIsValid(true);
                spinDaysNoBreach.SetIsValid(true);
            }
            if (ddlAccessToBooks.GetText() == "Yes" && (txtBooksReference.GetText() == null || txtBooksReference.GetText() == "")) {
                if (message != "") {
                    message = message + "\r\n";
                }
                message = message + "Books/Records Reference Page/Paragraph is required.";
                txtBooksReference.SetIsValid(false);
            }
            else {
                txtBooksReference.SetIsValid(true);
            }
            if (ddlMedicareAdjustments.GetText() == "Yes" && (txtMedicareReference.GetText() == null || txtMedicareReference.GetText() == "")) {
                if (message != "") {
                    message = message + "\r\n";
                }
                message = message + "Medicare Reference Page/Paragraph is required.";
                txtMedicareReference.SetIsValid(false);
            }
            else {
                txtMedicareReference.SetIsValid(true);
            }
            if (ddlHipaaBA.GetText() == "Yes" && (txtHipaaReference.GetText() == null || txtHipaaReference.GetText() == "")) {
                if (message != "") {
                    message = message + "\r\n";
                }
                message = message + "HIPAA/BAA Reference Page/Paragraph is required.";
                txtHipaaReference.SetIsValid(false);
            }
            else {
                txtHipaaReference.SetIsValid(true);
            }
            if (ddlGoverningLaw.GetText() == "Yes" && (txtGoverningLawReference.GetText() == null || txtGoverningLawReference.GetText() == "")) {
                if (message != "") {
                    message = message + "\r\n";
                }
                message = message + "Governing Law Reference Page/Paragraph is required.";
                txtGoverningLawReference.SetIsValid(false);
            }
            else {
                txtGoverningLawReference.SetIsValid(true);
            }
            if (rbCorporation.GetValue() == null) {
                if (message != "") {
                    message = message + "\r\n";
                }
                message = message + "Is Party a Corporation? is required.";
            }
            if (rbCorporation.GetValue() == "1" && (rbCorporationStanding.GetValue() == null)) {
                if (message != "") {
                    message = message + "\r\n";
                }
                message = message + "Corporation Standing is required for all corporations.";
            }
            if (memoDetails.GetText() == null || memoDetails.GetText() == '') {
                if (message != "") {
                    message = message + "\r\n";
                }
                message = message + "Details is required.";
            }
            //if (ddlMasterContracts.GetValue() != null) {
            //    const params = new URLSearchParams(window.location.search);
            //    var contractId = params.get('id');
            //    if (ddlMasterContracts.GetValue() == contractId) {
            //        if (message != "") {
            //            message = message + "\r\n";
            //        }
            //        message = message + "Master Contract Number cannot be the same as the Contract Number.";
            //        ddlMasterContracts.SetIsValid(false);
            //    }
            //    else {
            //        ddlMasterContracts.SetIsValid(true);
            //    }
            //}
            //else {
            //    ddlMasterContracts.SetIsValid(true);
            //}
            if (message != "") {
                alert(message);
                e.processOnServer = false;
            }
            else {
                try {
                    if (s.name.indexOf("btnApprove") >= 0 && s.GetText() == "Approve") {
                        e.processOnServer = confirm('Approving will automatically save any changes.\nAre you ready to save this contract?');
                    }
                    else {
                        e.processOnServer = confirm('Are you ready to save this contract?');
                    }
                }
                catch (err) {
                    alert(err);
                }
            }
        }
        function ShowHint(hint, hintStyle) {
            switch (hint) {
                case 'hintParties': hintParties.Show(hintStyle);
                    break;
                case 'hintContractType': hintContractType.Show(hintStyle);
                    break;
                case 'hintActions': hintActions.Show(hintStyle);
                    break;
                case 'hintMasterContract': hintMasterContract.Show(hintStyle);
                    break;
                case 'hintPurchaseGroup': hintPurchaseGroup.Show(hintStyle);
                    break;
                case 'hintPurchaseGroup2': hintPurchaseGroup2.Show(hintStyle);
                    break;
                case 'hintAutoRenewal': hintAutoRenewal.Show(hintStyle);
                    break;
                case 'hintBudgeted': hintBudgeted.Show(hintStyle);
                    break;
                case 'hintOutclause': hintOutclause.Show(hintStyle);
                    break;
                case 'hintNotificationDays': hintNotificationDays.Show(hintStyle);
                    break;
                case 'hintNotificationDaysNoBreach': hintNotificationDaysNoBreach.Show(hintStyle);
                    break;
                case 'hintDetails': hintDetails.Show(hintStyle);
                    break;
                case 'hintEstimatedTotalCost': hintEstimatedTotalCost.Show(hintStyle);
                    break;
                case 'hintAnnualCost': hintAnnualCost.Show(hintStyle);
                    break;
                case 'hintPriorYearCost': hintPriorYearCost.Show(hintStyle);
                    break;
                case 'hintExclusivity': hintExclusivity.Show(hintStyle);
                    break;
                case 'hintGlEOC': hintGlEOC.Show(hintStyle);
                    break;
                case 'hintAccessToBooks': hintAccessToBooks.Show(hintStyle);
                    break;
                case 'hintMedicareAdjustments': hintMedicareAdjustments.Show(hintStyle);
                    break;
                case 'hintHipaaBAA': hintHipaaBAA.Show(hintStyle);
                    break;
                case 'hintGoverningLaw': hintGoverningLaw.Show(hintStyle);
                    break;
                case 'hintGSA': hintGSA.Show(hintStyle);
                    break;
                case 'hintPhysicianRationale': hintPhysicianRationale.Show(hintStyle);
                    break;
                case 'hintContractEmployees': hintContractEmployees.Show(hintStyle);
                    break;
                case 'hintVendorSigned': hintVendorSigned.Show(hintStyle);
                    break;
                case 'hintElectronicAgreement': hintElectronicAgreement.Show(hintStyle);
                    break;
                case 'hintCorporation': hintCorporation.Show(hintStyle);
                    break;
                case 'hintCorporationStanding': hintCorporationStanding.Show(hintStyle);
                    break;
                case 'hintEndDate': hintEndDate.Show(hintStyle);
                    break;
                default: break;
            }
        }
    </script>
    <div style="max-width:50%; top:5px; left:0px; height:100%; position:relative; display:inline-block; overflow:visible; vertical-align:top; z-index:1">
        <dx:ASPxCallbackPanel ID="cbContract" ClientInstanceName="cbContract" runat="server" OnCallback="cbContract_Callback" Width="100%" DefaultButton="btnSave">
            <PanelCollection>
                <dx:PanelContent>
                    <dx:ASPxFormLayout ID="formContract" ClientInstanceName="formContract" runat="server" DataSourceID="dsContract" EnableTheming="true" Theme="Metropolis" EncodeHtml="false" Paddings-Padding="10">
                        <SettingsAdaptivity AdaptivityMode="SingleColumnWindowLimit" SwitchToSingleColumnAtWindowInnerWidth="1000" />
                        <ClientSideEvents Init="OnReferenceChange" />
                        <Items>
                            <dx:LayoutGroup ShowCaption="False" GroupBoxDecoration="None" ColumnCount="1" HorizontalAlign="Center">
                                <Items>
                                    <dx:LayoutItem ColSpan="1" FieldName="contractId" Caption="Select Related Contract">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <dx:ASPxComboBox ID="ddlContracts" ClientInstanceName="ddlContracts" runat="server" DataSourceID="dsRelatedContracts" ValueField="contractId" TextField="contractDisplay" OnSelectedIndexChanged="ddlContracts_SelectedIndexChanged" AutoPostBack="true" SelectedIndex="0"></dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="dsRelatedContracts" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>"
                                                        SelectCommand="usp_getContractsByMasterId" SelectCommandType="StoredProcedure">
                                                        <SelectParameters>
                                                            <asp:QueryStringParameter Name="contractId" QueryStringField="id" />
                                                            <asp:Parameter Name="includeSelectedContract" DefaultValue="1" />
                                                        </SelectParameters>
                                                    </asp:SqlDataSource>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                </Items>
                            </dx:LayoutGroup>
                            <dx:LayoutGroup Caption="Enter Contract Details" ColumnCount="2">
                                <Items>
                                    <%--<dx:LayoutItem ColSpan="1" FieldName="contractId" Visible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <dx:ASPxLabel ID="lblContractId" runat="server"></dx:ASPxLabel>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>--%>
                                    <dx:LayoutItem ColSpan="1" Caption="Department" FieldName="departmentId">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="width:95%">
                                                    <dx:ASPxComboBox ID="ddlDepartments" runat="server" DataSourceID="dsDepartments" ValueField="departmentId" TextField="department" AllowNull="true" NullText=""></dx:ASPxComboBox>
                                                </div>
                                                <asp:SqlDataSource ID="dsDepartments" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                                                    SelectCommand="usp_getDepartments" SelectCommandType="StoredProcedure">
                                                    <SelectParameters>
                                                        <asp:Parameter Name="includeInactive" DefaultValue="1" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Contract Type" FieldName="contractTypeId">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div class="flex">
                                                    <div style="width:95%">
                                                        <dx:ASPxComboBox ID="ddlContractTypes" ClientInstanceName="ddlContractTypes" runat="server" DataSourceID="dsContractTypes" ValueField="contractTypeId" TextField="contractType">
                                                            <ClientSideEvents SelectedIndexChanged="OnActionChanged" />
                                                        </dx:ASPxComboBox>
                                                    </div>
                                                    <span>&nbsp;&nbsp;&nbsp;</span>
                                                    <a href="javascript:ShowHint('hintContractType','.contractTypeHint');" id="aContractType"><i class="fa fa-info-circle contractTypeHint"></i></a>
                                                </div>
                                                <dx:ASPxHint ID="hintContractType" ClientInstanceName="hintContractType" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Contract Type" Content="<dl><dt>Vendor</dt><dd>Vendor provides a service (i.e. Clean linens, mobile MRIs), good, supplies, maintenance, or equipment. For maintenance contract renewal requests, obtain a 12 month service history from the vendor and upload to the contract request. Can also be consulting, professional fees, schools, management fees, etc.</dd><dt>Administrative</dt><dd>Administrative, Consulting, NDA, Managed Care</dd><dt>Education</dt><dd>Education Assistance requests.</dd><dt>Employment</dt><dd>Employment Agreement.</dd><dt>Lease/Rent</dt><dd>Space or Property</dd><dt>PHO<dd>Managed Care</dd></dl>" TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                                <asp:SqlDataSource ID="dsContractTypes" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                                                    SelectCommand="usp_getContractTypes" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Party/Vendor" FieldName="partyId">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div class="flex">
                                                    <div style="width:95%">
                                                        <dx:ASPxComboBox ID="ddlParties" runat="server" DataSourceID="dsParties" TextField="businessName" ValueField="partyId" OnDataBound="ddlParties_DataBound"></dx:ASPxComboBox>
                                                    </div>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a href="javascript:ShowHint('hintParties','.partyHint');" id="aParty"><i class="fa fa-info-circle partyHint"></i></a>
                                                </div>
                                                <dx:ASPxHint ID="hintParties" ClientInstanceName="hintParties" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Party/Vendor" Content="Select the LEGAL NAME of the party/vendor.<br/>If not available in the list, select “Add Party” button and complete the form, entering the LEGAL NAME of the party/vendor." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                                <asp:SqlDataSource ID="dsParties" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                                                    SelectCommand="usp_getParties" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Action" FieldName="actionId">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div class="flex">
                                                    <div style="width:95%">
                                                        <dx:ASPxComboBox ID="ddlActions" ClientInstanceName="ddlActions" runat="server" DataSourceID="dsActions" ValueField="actionId" TextField="action">
                                                            <ClientSideEvents SelectedIndexChanged="OnActionChanged" Init="OnActionChanged" />
                                                        </dx:ASPxComboBox>
                                                    </div>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a href="javascript:ShowHint('hintActions','.actionsHint');" id="aActions"><i class="fa fa-info-circle actionsHint"></i></a>
                                                </div>
                                                <dx:ASPxHint ID="hintActions" ClientInstanceName="hintActions" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Action" Content="<dl><dt>New</dt><dd>New contract that has not existed or replaces previous contract with new terms</dd><dt>Renewal</dt><dd>Review and Renewal of an existing contract that is ending, no changes in terms.</dd><dt>Amendment</dt><dd>Changes to an existing contract</dd><dt>Auto Renewal Review</dt><dd>Review of a contract annually that automatically renews each year with no contract end date. The review is good for one year. Verify contract is still in good standing and add any details of information that may be needed in the future. If any terms or amounts change, you must enter as a NEW contract.</dd><dt>Cancel/Inactivate</dt><dd>The contract is expiring or is being terminated.</dd><dt>Other</dt><dd>Any action not identified above.</dd></dl>" TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                                <asp:SqlDataSource ID="dsActions" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                                                    SelectCommand="usp_getActions">
                                                    <SelectParameters>
                                                        <asp:Parameter Name="inactive" DefaultValue="1" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Start Date" FieldName="startDate">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="width:95%">
                                                    <dx:ASPxDateEdit ID="deStartDate" ClientInstanceName="deStartDate" runat="server">
                                                        <ClientSideEvents DateChanged="OnDateChanged" Init="OnDateChanged"/>
                                                    </dx:ASPxDateEdit>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColumnSpan="1" Caption="Master Contract" FieldName="masterContractNumber" Name="masterContractNumber">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div class="flex">
                                                    <div style="width:95%">
                                                        <dx:ASPxComboBox ID="ddlMasterContracts" ClientInstanceName="ddlMasterContracts" runat="server" ValueField="masterContractNumber" TextField="businessName" DataSourceID="dsMasterContracts" DropDownWidth="100" AllowNull="true"></dx:ASPxComboBox>
                                                    </div>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a href="javascript:ShowHint('hintMasterContract','.masterContractHint');" id="aMasterContract"><i class="fa fa-info-circle masterContractHint"></i></a>
                                                </div>
                                                <dx:ASPxHint ID="hintMasterContract" ClientInstanceName="hintMasterContract" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Master Contract" Content="If this contract is a subordinate or amendment to a previously entered contract, select the master contract from this list.<br/><br/>If you are inactivating a contract, select the contract you wish to inactivate." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                                <asp:SqlDataSource ID="dsMasterContracts" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                                                    SelectCommand="usp_getMasterContracts" SelectCommandType="StoredProcedure">
                                                    <SelectParameters>
                                                        <asp:ControlParameter Name="partyId" ControlID="formContract$ddlParties" PropertyName="Value" />
                                                        <asp:QueryStringParameter Name="contractId" QueryStringField="id" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="End Date" FieldName="endDate">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div class="flex">
                                                    <div style="width:95%">
                                                        <dx:ASPxDateEdit ID="deEndDate" ClientInstanceName="deEndDate" runat="server">
                                                            <ClientSideEvents DateChanged="OnDateChanged" />
                                                        </dx:ASPxDateEdit>
                                                    </div>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a href="javascript:ShowHint('hintEndDate','.endDateHint');" id="aEndDate"><i class="fa fa-info-circle endDateHint"></i></a>
                                                </div>
                                                <dx:ASPxHint ID="hintEndDate" ClientInstanceName="hintEndDate" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="End Date" Content="The End Date must be a future date or the contract will automatically be placed in an Inactive/Canceled State.<br/><br/>If you are inactivating a contract, the end date must be equal to the date the contract will end.<br/>You will need to caluclate that date using the Notification Breach/No Breach values or set this to the end date of the existing contract." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Purchasing Group" FieldName="isPurchasingGroup">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div class="flex">
                                                    <div style="width:95%">
                                                        <dx:ASPxComboBox ID="ddlPurchasingGroup" ClientInstanceName="ddlPurchasingGroup" runat="server" AllowNull="true" NullText="">
                                                            <Items>
                                                                <dx:ListEditItem Text="Yes" Value="True" />
                                                                <dx:ListEditItem Text="No" Value="False" />
                                                            </Items>
                                                            <ClientSideEvents SelectedIndexChanged="OnPurchasingGroupChanged" />
                                                        </dx:ASPxComboBox>
                                                    </div>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a href="javascript:ShowHint('hintPurchaseGroup','.purchaseGroupHint');"><i class="fa fa-info-circle purchaseGroupHint"></i></a>
                                                </div>
                                                <dx:ASPxHint ID="hintPurchaseGroup" ClientInstanceName="hintPurchaseGroup" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Purchase Group" Content="Select the appropriate purchase group to which the contract is affiliated. If affiliated to a purchase group, select none.<br/>If not on purchase group, please ensure the contract adheres to proper quote and bid purchasing procedures.<br/>(Over $10K requires multiple quotes and over $50K requires a bid)" TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Contract Length" FieldName="contractLength">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="width:95%">
                                                    <dx:ASPxTextBox ID="txtContractLength" ClientInstanceName="txtContractLength" runat="server" ReadOnly="true" BackColor="WhiteSmoke"></dx:ASPxTextBox>
                                                    <dx:ASPxLabel ID="lblContractLength" ClientInstanceName="lblContractLength" ClientVisible="false" runat="server"></dx:ASPxLabel>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Purchase Group" FieldName="purchasingGroup" Name="purchaseGroup">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div class="flex">
                                                    <div style="width:95%">
                                                        <dx:ASPxComboBox ID="ddlPurchaseGroups" ClientInstanceName="ddlPurchaseGroups" runat="server" DataSourceID="dsPurchasingGroups" TextField="groupName" ValueField="purchaseGroupid">
                                                            <ClientSideEvents SelectedIndexChanged="OnPurchasingGroupChanged" Init="OnPurchasingGroupChanged" />
                                                        </dx:ASPxComboBox>
                                                    </div>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a id="aPurchaseGroups" href="javascript:ShowHint('hintPurchaseGroup','.purchaseGroupHint2');"><i class="fa fa-info-circle purchaseGroupHint2"></i></a>
                                                </div>
                                                <dx:ASPxHint ID="hintPurchaseGroup2" ClientInstanceName="hintPurchaseGroup2" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Purchase Group" Content="Select the appropriate purchase group to which the contract is affiliated. If affiliated to a purchase group, select none.<br/>If not on purchase group, please ensure the contract adheres to proper quote and bid purchasing procedures.<br/>(Over $10K requires multiple quotes and over $50K requires a bid)" TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                                <asp:SqlDataSource ID="dsPurchasingGroups" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" SelectCommand="usp_getPurchasingGroupsAdmin" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Auto Renewal" FieldName="isAutoRenewal">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div class="flex">
                                                    <div style="width:95%">
                                                        <dx:ASPxComboBox ID="ddlAutoRenewal" runat="server">
                                                            <Items>
                                                                <dx:ListEditItem Text="Yes" Value="True" />
                                                                <dx:ListEditItem Text="No" Value="False" />
                                                            </Items>
                                                            <%--<ClientSideEvents SelectedIndexChanged="function(s,e){if (s.GetValue() == '1'){alert('AutoRenewal will require Chief Officer initials before it can be submitted for signatures.');}}" />--%>
                                                        </dx:ASPxComboBox>
                                                    </div>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a href="javascript:ShowHint('hintAutoRenewal','.autoRenewalHint');"><i class="fa fa-info-circle autoRenewalHint"></i></a>
                                                </div>
                                                <dx:ASPxHint ID="hintAutoRenewal" ClientInstanceName="hintAutoRenewal" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Auto Renewal" Content="Does the contract contain terms that whereby the contract will renewal automatically at the end of the term without MRHC actions?" TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Exclusivity" FieldName="isExclusivity">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div class="flex">
                                                    <div style="width:95%">
                                                        <dx:ASPxComboBox ID="ddlExclusivity" runat="server">
                                                            <Items>
                                                                <dx:ListEditItem Text="Yes" Value="True" />
                                                                <dx:ListEditItem Text="No" Value="False" />
                                                            </Items>
                                                            <%--<ClientSideEvents SelectedIndexChanged="function(s,e){if (s.GetValue() == '1'){alert('Exclusivity will require Chief Officer initials before it can be submitted for signatures.');}}" />--%>
                                                        </dx:ASPxComboBox>
                                                    </div>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a href="javascript:ShowHint('hintExclusivity','.exclusivityHint');"><i class="fa fa-info-circle exclusivityHint"></i></a>
                                                </div>
                                                <dx:ASPxHint ID="hintExclusivity" ClientInstanceName="hintExclusivity" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Exclusivity" Content="Does this contract limit who MRHC can do business with?" TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Budgeted" FieldName="isBudgeted">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div class="flex">
                                                    <div style="width:95%">
                                                        <dx:ASPxComboBox ID="ddlBudgeted" ClientInstanceName="ddlBudgeted" runat="server">
                                                            <Items>
                                                                <dx:ListEditItem Text="Yes" Value="True" />
                                                                <dx:ListEditItem Text="No" Value="False" />
                                                            </Items>
                                                        </dx:ASPxComboBox>
                                                    </div>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a href="javascript:ShowHint('hintBudgeted','.budgetedHint');"><i class="fa fa-info-circle budgetedHint"></i></a>
                                                </div>
                                                <dx:ASPxHint ID="hintBudgeted" ClientInstanceName="hintBudgeted" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Budgeted" Content="Was this purchase budgeted in the current year’s budget?" TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Budgeted Amount" FieldName="budgetedAmount">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="width:95%">
                                                    <dx:ASPxSpinEdit ID="spinBudgetedAmount" ClientInstanceName="spinBudgetedAmount" runat="server"></dx:ASPxSpinEdit>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Outclause" FieldName="isOutClause">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div class="flex">
                                                    <div style="width:95%">
                                                        <dx:ASPxComboBox ID="ddlOutclause" ClientInstanceName="ddlOutclause" runat="server">
                                                            <Items>
                                                                <dx:ListEditItem Text="Yes" Value="True" />
                                                                <dx:ListEditItem Text="No" Value="False" />
                                                            </Items>
                                                            <ClientSideEvents SelectedIndexChanged="OutclauseChanged" Init="ShowBreach" />
                                                        </dx:ASPxComboBox>
                                                    </div>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a href="javascript:ShowHint('hintOutclause','.outclauseHint');"><i class="fa fa-info-circle outclauseHint"></i></a>
                                                </div>
                                                <dx:ASPxHint ID="hintOutclause" ClientInstanceName="hintOutclause" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Outclause" Content="Review the terms of the contract and select if the contract contains an out clause." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Vendor/Party Signed" FieldName="isPartySigned">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div class="flex">
                                                    <div style="width:95%">
                                                        <dx:ASPxComboBox ID="ddlPartySigned" ClientInstanceName="ddlPartySigned" runat="server">
                                                            <Items>
                                                                <dx:ListEditItem Text="Yes" Value="Y" />
                                                                <dx:ListEditItem Text="No" Value="N" />
                                                                <dx:ListEditItem Text="N/A" Value="NA" />
                                                            </Items>
                                                        </dx:ASPxComboBox>
                                                    </div>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a href="javascript:ShowHint('hintVendorSigned','.vendorSignedHint');"><i class="fa fa-info-circle vendorSignedHint"></i></a>
                                                </div>
                                                <dx:ASPxHint ID="hintVendorSigned" ClientInstanceName="hintVendorSigned" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Is Party/Vendor Signed" Content="If the Vendor has already signed the contract, select Yes. Otherwise, select No." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Notification Days Breach" FieldName="notificationDaysBreach" ClientVisible="false" Name="notificationDaysBreach">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div class="flex">
                                                    <div style="width:95%">
                                                        <dx:ASPxSpinEdit ID="spinDaysBreach" ClientInstanceName="spinDaysBreach" runat="server" DecimalPlaces="0" AllowNull="true">
                                                            <ClientSideEvents NumberChanged="BreachDaysCheck" />
                                                        </dx:ASPxSpinEdit>
                                                    </div>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a href="javascript:ShowHint('hintNotificationDays','.notificationDaysBreachHint');"><i class="fa fa-info-circle notificationDaysBreachHint"></i></a>
                                                </div>
                                                <dx:ASPxHint ID="ASPxHint1" ClientInstanceName="hintNotificationDays" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Notification Days Breach" Content="Enter the number of days MRHC must provide a notification prior to terminating the contract for a non-cured breach." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Notification Days<br/>No Breach" FieldName="notificationDaysNoBreach" ClientVisible="false" Name="notificationDaysNoBreach">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div class="flex">
                                                    <div style="width:95%">
                                                        <dx:ASPxSpinEdit ID="spinDaysNoBreach" ClientInstanceName="spinDaysNoBreach" runat="server" DecimalPlaces="0" AllowNull="true">
                                                            <ClientSideEvents NumberChanged="BreachDaysCheck" />
                                                        </dx:ASPxSpinEdit>
                                                    </div>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a href="javascript:ShowHint('hintNotificationDaysNoBreach','.notificationDaysNoBreach');"><i class="fa fa-info-circle notificationDaysNoBreach"></i></a>
                                                </div>
                                                <dx:ASPxHint ID="hintNotificationDaysNoBreach" ClientInstanceName="hintNotificationDaysNoBreach" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Notificaiton Days No Breach" Content="Enter the number of days MRHC must provide a notification prior to terminating the contract for any cause." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Education Assistance<br/>Committee Approval" FieldName="eacApproval" Name="EACApproval" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="width:95%">
                                                    <dx:ASPxComboBox ID="ddlEACApproval" runat="server">
                                                        <Items>
                                                            <dx:ListEditItem Text="Yes" Value="True" />
                                                            <dx:ListEditItem Text="No" Value="False" />
                                                        </Items>
                                                    </dx:ASPxComboBox>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="EAC Approval Date" FieldName="eacApprovalDate" Name="EACApprovalDate" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="width:95%">
                                                    <dx:ASPxDateEdit ID="deEACApprovalDate" ClientinstanceName="deEACApprovalDate" runat="server"></dx:ASPxDateEdit>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="BOT Approval" FieldName="botApproval" Name="BOTApproval" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="width:95%">
                                                    <dx:ASPxComboBox ID="ddlBOTApproval" runat="server">
                                                        <Items>
                                                            <dx:ListEditItem Text="Yes" Value="True" />
                                                            <dx:ListEditItem Text="No" Value="False" />
                                                        </Items>
                                                    </dx:ASPxComboBox>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="BOT Approval Date" FieldName="botApprovalDate" Name="BOTApprovalDate" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="width:95%">
                                                    <dx:ASPxDateEdit ID="deBOTApprovalDate" ClientinstanceName="deBOTApprovalDate" runat="server"></dx:ASPxDateEdit>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="MPS Approval" FieldName="mpsApproval" Name="MPSApproval" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="width:95%">
                                                    <dx:ASPxComboBox ID="ddlMPSApproval" runat="server">
                                                        <Items>
                                                            <dx:ListEditItem Text="Yes" Value="True" />
                                                            <dx:ListEditItem Text="No" Value="False" />
                                                        </Items>
                                                    </dx:ASPxComboBox>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="MPS Approval Date" FieldName="mpsApprovalDate" Name="MPSApprovalDate" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="width:95%">
                                                    <dx:ASPxDateEdit ID="deMPSApprovalDate" ClientinstanceName="deMPSApprovalDate" runat="server"></dx:ASPxDateEdit>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="ElectronicAgreement?" FieldName="isElectronicAgreement" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="display:flex; flex-direction:row;">
                                                    <div style="width:20px">
                                                        <a href="javascript:ShowHint('hintElectronicAgreement','.electronicAgreementHint');"><i class="fa fa-info-circle electronicAgreementHint"></i></a>
                                                    </div>
                                                    <dx:ASPxComboBox ID="ddlElectronicAgreement" runat="server">
                                                        <Items>
                                                            <dx:ListEditItem Text="Yes" Value="True" />
                                                            <dx:ListEditItem Text="No" Value="False" />
                                                        </Items>
                                                    </dx:ASPxComboBox>
                                                </div>
                                                <dx:ASPxHint ID="hintElectronicAgreement" ClientInstanceName="hintElectronicAgreement" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Is Contract an Electronic Agreement" Content="If the contract must be electronically signed outside of the CMS System, select Yes. Otherwise select No." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColumnSpan="1" Caption="Is Party a Corporation?" FieldName="isCorporation" Name="corporation">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <div style="display:flex;">
                                                    <div style="width:95%">
                                                        <dx:ASPxComboBox ID="rbCorporation" ClientInstanceName="rbCorporation" runat="server" ValidationSettings-RequiredField-IsRequired="true" ValidationSettings-ErrorDisplayMode="ImageWithTooltip" ValidationSettings-RequiredField-ErrorText="Is Party a Corporation? is a required field.">
                                                            <Items>
                                                                <dx:ListEditItem Text="Yes" Value="True" />
                                                                <dx:ListEditItem Text="No" Value="False" />
                                                            </Items>
                                                        </dx:ASPxComboBox>
                                                    </div>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a href="javascript:ShowHint('hintCorporation','.corporationHint');"><i class="fa fa-info-circle corporationHint"></i></a>
                                                </div>
                                                <dx:ASPxHint ID="hintCorporation" ClientInstanceName="hintCorporation" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Is Party a Corporation?" Content="Perform a corporation check by searching the secretary of state of the state of incorporation. Print and upload good standing documentation. If unsure how to do this, call the contract administrator." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColumnSpan="1" Caption="Corporation in Good Standing?" FieldName="isCorporationGoodStanding" Name="corporationStanding">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <div style="display:flex;">
                                                    <div style="width:95%">
                                                        <dx:ASPxComboBox ID="rbCorporationStanding" ClientInstanceName="rbCorporationStanding" runat="server">
                                                            <Items>
                                                                <dx:ListEditItem Text="Yes" Value="True" />
                                                                <dx:ListEditItem Text="No" Value="False" />
                                                            </Items>
                                                        </dx:ASPxComboBox>
                                                    </div>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a href="javascript:ShowHint('hintCorporationStanding','.corporationStandingHint');" id="aCorporationStanding"><i class="fa fa-info-circle corporationStandingHint"></i></a>
                                                </div>
                                                <dx:ASPxHint ID="hintCorporationStanding" ClientInstanceName="hintCorporationStanding" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Corporation in Good Standing?" Content="Perform a corporation check by searching the secretary of state of the state of incorporation. Print and upload good standing documentation. If unsure how to do this, call the contract administrator." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="2" Caption="Details" FieldName="details" CaptionSettings-Location="Top">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div class="flex">
                                                    <div style="width:95%">
                                                            <%--function OnKeyDown(s, e) {  
                                                                var textArea = s.GetInputElement();  
                                                                if (textArea.scrollHeight > s.GetHeight() - 2) {  
                                                                    s.SetHeight(textArea.scrollHeight + 10);  
                                                                } 
                                                                }   --%>
                                                        <dx:ASPxMemo ID="memoDetails" ClientInstanceName="memoDetails" runat="server" ValidationSettings-RequiredField-IsRequired="true" ValidationSettings-ErrorDisplayMode="ImageWithTooltip" ValidationSettings-RequiredField-ErrorText="Details is a required field." MaxLength="2000">
                                                            <ClientSideEvents Init="function OnMemoInit(s, e) { s.SetHeight(s.GetInputElement().scrollHeight + 10); }" />
                                                        </dx:ASPxMemo>
                                                    </div>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a href="javascript:ShowHint('hintDetails','.detailsHint');"><i class="fa fa-info-circle detailsHint"></i></a>
                                                </div>
                                            <dx:ASPxHint ID="hintDetails" ClientInstanceName="hintDetails" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Contract Details" Content="Summary of goods or services provided and the business case for the contract.<ul><li>Why do we need it?</li><li>What is ROI?</li><li>It is a complementary service to a valued service?</li><li>New offering to better serve the community?</li><li>It is required by a regulatory agency? provide documentation.</li><li>What are the alternatives?</li><li>How does this purchase align with the current strategic plan?</li></ul>" TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Total Cost" FieldName="estimatedTotalCost">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div class="flex">
                                                    <div style="width:95%">
                                                        <dx:ASPxSpinEdit ID="spinEstimatedTotalCost" ClientInstanceName="spinEstimatedTotalCost" runat="server" DecimalPlaces="2" AllowNull="true" ReadOnly="true" BackColor="WhiteSmoke">
                                                            <ClientSideEvents NumberChanged="AnnualCostChanged" Init="InitAnnualCost" />
                                                        </dx:ASPxSpinEdit>
                                                    </div>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a href="javascript:ShowHint('hintEstimatedTotalCost','.estimatedTotalCostHint');"><i class="fa fa-info-circle estimatedTotalCostHint"></i></a>
                                                </div>
                                            <dx:ASPxHint ID="hintEstimatedTotalCost" ClientInstanceName="hintEstimatedTotalCost" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Total Cost" Content="Fill in the PROPOSED ANNUAL COST in the appropriate box.<br/>Fill in LAST YEARS COST in the appropriate box.<br/>Verify through accounts payable your ACTUAL spend.<br/>Cost difference is automatically calculated. If Total Cost is $50K or more, the contract will go to the Board of Trustees for approval. " TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="EOC" FieldName="glEOC">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div class="flex">
                                                    <div style="width:95%">
                                                        <dx:ASPxComboBox ID="ddlGlEOC" ClientInstanceName="ddlGlEOC" runat="server" DataSourceID="dsEOC" TextField="eocDescription" ValueField="eocId"></dx:ASPxComboBox>
                                                    </div>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a href="javascript:ShowHint('hintGlEOC','.glEOCHint');"><i class="fa fa-info-circle glEOCHint"></i></a>
                                                </div>
                                                <dx:ASPxHint ID="hintGlEOC" ClientInstanceName="hintGlEOC" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="EOC" Content="Choose the EOC category to which the charges will be accrued for the contract. When multiple EOCs, select the largest EOC to be used" TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                                <asp:SqlDataSource ID="dsEOC" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" SelectCommand="usp_getEocs" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Annual Cost" FieldName="annualCost">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div class="flex">
                                                    <div style="width:95%">
                                                         <dx:ASPxSpinEdit ID="spinAnnualCost" ClientInstanceName="spinAnnualCost" runat="server" DecimalPlaces="2" AllowNull="true">
                                                             <ClientSideEvents ValueChanged="CalculateTotalCost" />
                                                         </dx:ASPxSpinEdit>
                                                    </div>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a href="javascript:ShowHint('hintAnnualCost','.annualCostHint');"><i class="fa fa-info-circle annualCostHint"></i></a>
                                                </div>
                                                <dx:ASPxHint ID="hintAnnualCost" ClientInstanceName="hintAnnualCost" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Annual Cost" Content="Enter the annual costs associated with the contract." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Payment Term" FieldName="paymentTerm">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="width:95%">
                                                    <dx:ASPxComboBox ID="ddlPaymentTerm" runat="server" DataSourceID="dsPaymentTypes" TextField="paymentType" ValueField="paymentType">
                                                        <ClientSideEvents SelectedIndexChanged="OnPaymentTermChange" Init="OnPaymentTermChange" />
                                                    </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="dsPaymentTypes" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" SelectCommand="usp_getPaymentTypes" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Last Year's Cost" FieldName="priorYearCost">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div class="flex">
                                                    <div style="width:95%">
                                                        <dx:ASPxSpinEdit ID="spinPriorYearCost" ClientInstanceName="spinPriorYearCost" runat="server" DecimalPlaces="2" AllowNull="true">
                                                            <ClientSideEvents NumberChanged="CalculateDifference" />
                                                        </dx:ASPxSpinEdit>
                                                    </div>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a href="javascript:ShowHint('hintPriorYearCost','.priorYearHint');"><i class="fa fa-info-circle priorYearHint"></i></a>
                                                </div>
                                                <dx:ASPxHint ID="hintPriorYearCost" ClientInstanceName="hintPriorYearCost" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Last Year's Cost" Content="Fill in the PROPOSED ANNUAL COST in the appropriate box.<br/>Fill in LAST YEAR'S COST in the appropriate box.<br/>Verify through accounts payable your ACTUAL spend.<br/>Cost difference is automatically calculated." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Other Term" FieldName="paymentOther" Name="OtherTerm">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="width:95%">
                                                    <dx:ASPxTextBox ID="txtPaymentOther" ClientInstanceName="txtPaymenetOther" runat="server"></dx:ASPxTextBox>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:EmptyLayoutItem Name="paymentOtherSpace"></dx:EmptyLayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Cost Difference" >
                                    <LayoutItemNestedControlCollection>
                                        <dx:LayoutItemNestedControlContainer runat="server">
                                            <div style="width:95%">
                                                <dx:ASPxSpinEdit ID="spinCostDifference" ClientInstanceName="spinCostDifference" runat="server" DecimalPlaces="2" AllowNull="true" ReadOnly="true" BackColor="WhiteSmoke"></dx:ASPxSpinEdit>
                                            </div>
                                        </dx:LayoutItemNestedControlContainer>
                                    </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="GSA/HHS/OIG" FieldName="isGsa">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div class="flex">
                                                    <div style="width:95%">
                                                        <dx:ASPxComboBox ID="ddlGSA" runat="server">
                                                            <Items>
                                                                <dx:ListEditItem Text="Yes" Value="True" />
                                                                <dx:ListEditItem Text="No" Value="False" />
                                                            </Items>
                                                        </dx:ASPxComboBox>
                                                    </div>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a href="javascript:ShowHint('hintGSA','.gsaHint');"><i class="fa fa-info-circle gsaHint"></i></a>
                                                </div>
                                                <dx:ASPxHint ID="hintGSA" ClientInstanceName="hintGSA" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="GSA/HHS/OIG" Content="Magnolia Regional Health Center vendors must undergo an OIG investigation BEFORE we can conduct business with them. This is done when new vendors are selected and annually thereafter for all MRHC vendors.<br/><br/>If an OIG/GSA/HHS exclusion investigation shows that a vendor or individual has been excluded from the Medicare program, we cannot do business with them.<br/><br/>In no event will Magnolia Regional Health Center hire, engage as a contractor or otherwise do business with an individual or entity who<br/><br><ol><li> is currently excluded, suspended, debarred or otherwise ineligible to participate in the federal Medicare and Medicaid programs and/or </li><li> has been convicted of a criminal offense related to the provision of healthcare items and service without being reinstated in the federal Medicare program.</li></ol><br/>OIG checks will automatically be performed against the contracting party entered into the “Party” field and this box will automatically be checked if the party did not return a finding against the database search.<br/><br/>See Policy: Magnolia Regional Health Center Screening of Employees, Contractors and Medical Staff under the Policies section on the MRHC intranet home page for more information." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Board of Trustee Approval" FieldName="trusteeApproval" Name="trusteeApproval" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="width:95%">
                                                    <dx:ASPxComboBox ID="ddlTrusteeApproval" ClientInstanceName="ddlTrusteeApproval" runat="server">
                                                        <Items>
                                                            <dx:ListEditItem Text="Yes" Value="True" />
                                                            <dx:ListEditItem Text="No" Value="False" />
                                                        </Items>
                                                    </dx:ASPxComboBox>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Board of Trustee<br/>Approval Date" FieldName="trusteeApprovalDate" Name="trusteeApprovalDate" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="width:95%">
                                                    <dx:ASPxDateEdit ID="deTrusteeApprovalDate" ClientinstanceName="deTrusteeApprovalDate" runat="server"></dx:ASPxDateEdit>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Access to Books/Records" FieldName="isAccessToBooks">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div class="flex">
                                                    <div style="width:95%">
                                                        <dx:ASPxComboBox ID="ddlAccessToBooks" ClientInstanceName="ddlAccessToBooks" runat="server">
                                                            <Items>
                                                                <dx:ListEditItem Text="Yes" Value="True" />
                                                                <dx:ListEditItem Text="No" Value="False" />
                                                            </Items>
                                                            <ClientSideEvents SelectedIndexChanged="OnReferenceChange" />
                                                        </dx:ASPxComboBox>
                                                    </div>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a href="javascript:ShowHint('hintAccessToBooks','.accessToBooksHint');"><i class="fa fa-info-circle accessToBooksHint"></i></a>
                                                </div>
                                                <dx:ASPxHint ID="hintAccessToBooks" ClientInstanceName="hintAccessToBooks" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Access To Books" Content="If the contract cost will be greater than $10,000 per year we are required to have certain language in the contract.<br/>If this is not stated in the contract provided to you by your vendor, ask them to include it before you route your contract request through for approval.<br/>WHAT THE CONTRACT NEEDS TO SAY - <br/><br/>&quot;____________________________ (Vendor Name) agrees that until the expiration of four (4) years after the furnishing of services pursuant to this agreement,<br/>it shall make available upon request of the Secretary of the Department of Health and Human Services, or upon request of the Comptroller General, or any of their duly authorized representatives,<br/>the contract, books, documents and records that are necessary to certify the nature and extent of payments to __________________________ (Vendor Name).&quot;<br/><br/>If yes, please enter the page and paragraph (page-paragraph) of the associated language in the reference field." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Adjustments for<br/>Medicare Changes" FieldName="isMedicareAdjustments">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div class="flex">
                                                    <div style="width:95%">
                                                        <dx:ASPxComboBox ID="ddlMedicareAdjustments" ClientInstanceName="ddlMedicareAdjustments" runat="server">
                                                            <Items>
                                                                <dx:ListEditItem Text="Yes" Value="True" />
                                                                <dx:ListEditItem Text="No" Value="False" />
                                                            </Items>
                                                            <ClientSideEvents SelectedIndexChanged="OnReferenceChange" />
                                                        </dx:ASPxComboBox>
                                                    </div>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a href="javascript:ShowHint('hintMedicareAdjustments','.medicareAdjustmentsHint');"><i class="fa fa-info-circle medicareAdjustmentsHint"></i></a>
                                                </div>
                                                <dx:ASPxHint ID="hintMedicareAdjustments" ClientInstanceName="hintMedicareAdjustments" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Medicare Adjustments" Content="If the service or supply could be affected by Medicare reimbursement, regulations, etc., the contract needs to include the following statement:<br/><br/>&ldquo;This agreement can be amended if Medicare or Medicaid issues regulations which are in conflict with this existing agreement.&rdquo;:<br/><br/>If yes, please enter the page and paragraph (page-paragraph) of the associated language in the reference field." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Books/Records Reference" FieldName="accessToBooksReference" Name="BooksReference">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="width:95%">
                                                    <dx:ASPxTextBox ID="txtBooksReference" ClientInstanceName="txtBooksReference" runat="server"></dx:ASPxTextBox>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Medicare Reference" FieldName="medicareAdjustmentsReference" Name="MedicareAdjustmentsReference">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="width:95%">
                                                    <dx:ASPxTextBox ID="txtMedicareReference" ClientInstanceName="txtMedicareReference" runat="server"></dx:ASPxTextBox>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="HIPAA Clause/<br/>Business Agreement" FieldName="isHipaaBA">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div class="flex">
                                                    <div style="width:95%">
                                                        <dx:ASPxComboBox ID="ddlHipaaBA" ClientInstanceName="ddlHipaaBA" runat="server">
                                                            <Items>
                                                                <dx:ListEditItem Text="Yes" Value="True" />
                                                                <dx:ListEditItem Text="No" Value="False" />
                                                            </Items>
                                                            <ClientSideEvents SelectedIndexChanged="OnReferenceChange" />
                                                        </dx:ASPxComboBox>
                                                    </div>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a href="javascript:ShowHint('hintHipaaBAA','.hipaaBAAHint');"><i class="fa fa-info-circle hipaaBAAHint"></i></a>
                                                </div>
                                                <dx:ASPxHint ID="hintHipaaBAA" ClientInstanceName="hintHipaaBAA" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="HIPAA Clause/Business Agreement" Content="If the vendor you are contracting with has access to PROTECTED HEALTH INFORMATION in any form, the vendor must have a signed Business Associates Agreement with Magnolia Regional Health Center.<br/>The BAA form can be found in the forms section of Magnet:<br/> <a href='https://magnet.mrhc.org/leadership/deptlead/Business%20Associate%20Agreement/Forms/AllItems.aspx' target='_blank'>Business Associate Agreement Form</a>" TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Governing Law (MS)" FieldName="isGoverningLaw">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div class="flex">
                                                    <div style="width:95%">
                                                        <dx:ASPxComboBox ID="ddlGoverningLaw" ClientInstanceName="ddlGoverningLaw" runat="server">
                                                            <Items>
                                                                <dx:ListEditItem Text="Yes" Value="YES" />
                                                                <dx:ListEditItem Text="No" Value="NO" />
                                                                <dx:ListEditItem Text="Implied" Value="IMPLIED" />
                                                            </Items>
                                                            <ClientSideEvents SelectedIndexChanged="OnReferenceChange" />
                                                        </dx:ASPxComboBox>
                                                    </div>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a href="javascript:ShowHint('hintGoverningLaw','.governingLawHint');"><i class="fa fa-info-circle governingLawHint"></i></a>
                                                </div>
                                                <dx:ASPxHint ID="hintGoverningLaw" ClientInstanceName="hintGoverningLaw" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Governing Law (MS)" Content="All MRHC contracts must state the governing law as the State of Mississippi.<br/>Most contracts are written with governing law in a different state.<br/>We must ask the vendor to change this to the State of Mississippi.<br/>If yes, please enter the page and paragraph (page-paragraph) of the associated language in the reference field.<br/>If a STATE is not mentioned, check the IMPLIED box." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="HIPAA/BAA Reference" FieldName="hipaaReference" Name="HIPAAReference">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="width:95%">
                                                    <dx:ASPxTextBox ID="txtHipaaReference" ClientInstanceName="txtHipaaReference" runat="server"></dx:ASPxTextBox>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Governing Law Reference" FieldName="governingLawReference" Name="GoverningLawReference">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="width:95%">
                                                    <dx:ASPxTextBox ID="txtGoverningLawReference" ClientInstanceName="txtGoverningLawReference" runat="server"></dx:ASPxTextBox>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>                                
                                    <dx:LayoutItem ColSpan="1" Caption="Physician Rationale" FieldName="isPhysicianRationale">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div class="flex">
                                                    <div style="width:95%">
                                                        <dx:ASPxComboBox ID="ddlPhysicianRationale" runat="server">
                                                            <Items>
                                                                <dx:ListEditItem Text="Yes" Value="True" />
                                                                <dx:ListEditItem Text="No" Value="False" />
                                                            </Items>
                                                        </dx:ASPxComboBox>
                                                    </div>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a href="javascript:ShowHint('hintPhysicianRationale','.physicianRationaleHint');"><i class="fa fa-info-circle physicianRationaleHint"></i></a>
                                                </div>
                                                <dx:ASPxHint ID="hintPhysicianRationale" ClientInstanceName="hintPhysicianRationale" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Physician Rationale" Content="If a physician rationale is required, please upload to the documents section of the contract." TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Contracted Employees" FieldName="isContractEmployees">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div class="flex">
                                                    <div style="width:95%">
                                                        <dx:ASPxComboBox ID="ddlContractEmployees" runat="server">
                                                            <Items>
                                                                <dx:ListEditItem Text="Yes" Value="True" />
                                                                <dx:ListEditItem Text="No" Value="False" />
                                                            </Items>
                                                        </dx:ASPxComboBox>
                                                    </div>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a href="javascript:ShowHint('hintContractEmployees','.contractEmployeesHint');"><i class="fa fa-info-circle contractEmployeesHint"></i></a>
                                                </div>
                                                <dx:ASPxHint ID="hintContractEmployees" ClientInstanceName="hintContractEmployees" runat="server" CssClass="hint" ShowCallout="true" Theme="Metropolis" Position="Right" Title="Contracted Employees" Content="Does the contract include contract staff or labor?" TriggerAction="Click" EncodeHtml="false"></dx:ASPxHint>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Restricted Data" FieldName="securityLevelId" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="width:95%">
                                                    <dx:ASPxComboBox ID="ddlSecurityLevel" runat="server">
                                                        <Items>
                                                            <dx:ListEditItem Text="Yes" Value="True" />
                                                            <dx:ListEditItem Text="No" Value="False" />
                                                        </Items>
                                                    </dx:ASPxComboBox>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Routing Name" FieldName="routingId">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="width:95%">
                                                    <dx:ASPxComboBox ID="ddlRouting" runat="server" DataSourceID="dsRouting" ValueField="routingId" TextField="routingName" ReadOnly="true" BackColor="WhiteSmoke"></dx:ASPxComboBox>
                                                </div>
                                                <asp:SqlDataSource ID="dsRouting" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                                                    SelectCommand="usp_getRoutes" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Current Status" FieldName="statusId" CaptionCellStyle-CssClass="spanTitle" CaptionStyle-CssClass="spanTitle">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="width:95%">
                                                    <dx:ASPxComboBox ID="lblStatus" runat="server" DataSourceID="dsStatusType" ValueField="statusTypeId" TextField="statusType"></dx:ASPxComboBox>
                                                    <asp:SqlDataSource ID="dsStatusType" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                                                    SelectCommand="select * from statusTypes" SelectCommandType="Text"></asp:SqlDataSource>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Status Date" FieldName="statusDate">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="width:95%">
                                                    <dx:ASPxDateEdit ID="deStatusDate" runat="server" ReadOnly="true" BackColor="WhiteSmoke"></dx:ASPxDateEdit>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Entry User" FieldName="entryUserId">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="width:95%">
                                                    <%--<dx:ASPxTextBox ID="txtEntryName" runat="server" ReadOnly="true" BackColor="WhiteSmoke"></dx:ASPxTextBox>--%>
                                                    <dx:ASPxComboBox ID="ddlEntryUser" runat="server" ClientInstanceName="ddlEntryUser" DataSourceID="dsEntryUser" ValueField="userId" TextField="fullName"></dx:ASPxComboBox>
                                                    <asp:SqlDataSource ID="dsEntryUser" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                                                        SelectCommand="select userId, fullName from users order by fullName" SelectCommandType="Text"></asp:SqlDataSource>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>                                    
                                    <dx:LayoutItem ColSpan="1" Caption="Entry Date" FieldName="entryDate">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <div style="width:95%">
                                                    <dx:ASPxDateEdit ID="deEntryDate" runat="server" ReadOnly="true" BackColor="WhiteSmoke"></dx:ASPxDateEdit>
                                                </div>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Contract Folder" FieldName="contractFolder" ClientVisible="false" Name="contractFolder">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <dx:ASPxSpinEdit ID="spinContractFolder" runat="server"></dx:ASPxSpinEdit>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" FieldName="routingSequence" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <dx:ASPxLabel ID="lblRoutingSequence" runat="server"></dx:ASPxLabel>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan ="1" FieldName="statusFullName" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxLabel ID="lblFullName" runat="server"></dx:ASPxLabel>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan ="1" FieldName="sequenceOrder" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxLabel ID="lblNextSequence" runat="server"></dx:ASPxLabel>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <%--<dx:LayoutItem ColSpan="1" FieldName="statusId" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <dx:ASPxLabel ID="lblStatusId" runat="server"></dx:ASPxLabel>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>--%>
                                    <dx:LayoutItem ColSpan ="1" FieldName="canOverride" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxLabel ID="lblCanOverride" runat="server"></dx:ASPxLabel>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan ="1" FieldName="overDueContracts" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxLabel ID="lblOverdueContracts" runat="server"></dx:ASPxLabel>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan ="1" FieldName="minReturned" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxLabel ID="lblMinReturned" runat="server"></dx:ASPxLabel>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <%--<dx:LayoutItem ColSpan="1" Caption="Entry User Id" FieldName="entryUserId" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <dx:ASPxTextBox ID="txtEntryUserId" runat="server"></dx:ASPxTextBox>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>--%>
                                    <dx:LayoutItem ColSpan="1" Caption="Contract Number" FieldName="contractNumber" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <dx:ASPxTextBox ID="txtContractNumber" runat="server"></dx:ASPxTextBox>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Master Contract" FieldName="masterContractNumber" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <dx:ASPxComboBox ID="ddlMasterContractsDetail" ClientInstanceName="ddlMasterContractsDetail" runat="server" ValueField="masterContractNumber" TextField="businessName" DataSourceID="dsMasterContractsDetail"></dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="dsMasterContractsDetail" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                                                    SelectCommand="usp_getMasterContracts" SelectCommandType="StoredProcedure" OnSelecting="dsMasterContractsDetail_Selecting">
                                                    <SelectParameters>
                                                        <asp:ControlParameter Name="partyId" ControlID="formContract$ddlParties" PropertyName="Value" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Current Role" FieldName="roleName" ClientVisible="false">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <dx:ASPxTextBox ID="lblRoleName" runat="server"></dx:ASPxTextBox>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Assigned To Role" FieldName="currentRoleName" ClientVisible="false" Name="currentRoleName">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <dx:ASPxComboBox ID="ddlRoles" ClientInstanceName="ddlRoles" runat="server" ValueField="roleId" TextField="roleName" DataSourceID="dsRoles" AllowNull="true"></dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="dsRoles" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                                                    SelectCommand="usp_getRoute" SelectCommandType="StoredProcedure">
                                                    <SelectParameters>
                                                        <asp:ControlParameter Name="routingId" ControlID="formContract$ddlRouting" PropertyName="Value" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ColSpan="1" Caption="Assigned To" FieldName="currentFullName" ClientVisible="false" Name="currentFullName">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer runat="server">
                                                <dx:ASPxComboBox ID="ddlUsers" ClientInstanceName="ddlUsers" runat="server" ValueField="userId" TextField="fullName" DataSourceID="dsUsers" AllowNull="true"></dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="dsUsers" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                                                    SelectCommand="usp_getUsers" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                               </Items>
                            </dx:LayoutGroup>
                            <dx:LayoutGroup GroupBoxDecoration="None" ColumnCount="6" Name="Buttons">
                                <Items>
                                    <dx:LayoutItem ShowCaption="False" HorizontalAlign="Right">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxButton ID="btnCancel" runat="server" Text="Inactivate" OnClick="btnCancel_Click" CausesValidation="false">
                                                    <ClientSideEvents Click="function (s,e) { e.processOnServer = confirm('Are you sure you want to cancel this contract?\r\n\r\nClick OK to create a new contract that will initiate the Cancel/Inactivate process, otherwise click CANCEL.'); }" />
                                                </dx:ASPxButton>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ShowCaption="False" HorizontalAlign="Right">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxButton ID="btnDelete" runat="server" OnClick="btnDelete_Click" Text="Delete" AutoPostBack="true" CausesValidation="false">
                                                    <ClientSideEvents Click="function(s, e) { e.processOnServer = confirm('Are you sure you want to delete this contract?');}" />
                                                </dx:ASPxButton>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ShowCaption="False" HorizontalAlign="Right">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxButton ID="btnReturn" runat="server" Text="Return" AutoPostBack="false" CausesValidation="false">
                                                    <ClientSideEvents Click="function(s,e){popReturn.Show();}" />
                                                </dx:ASPxButton>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ShowCaption="False" HorizontalAlign="Right">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxButton ID="btnReject" runat="server" Text="Reject" AutoPostBack="false" CausesValidation="false">
                                                    <ClientSideEvents Click="function(s,e){popReject.Show();}" />
                                                </dx:ASPxButton>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ShowCaption="False" HorizontalAlign="Right">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxButton ID="btnApprove" ClientInstanceName="btnApprove" runat="server" OnClick="btnApprove_Click" Text="Approve" CausesValidation="false">
                                                    <ClientSideEvents Click="OnSave" />
                                                </dx:ASPxButton>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ShowCaption="False" HorizontalAlign="Right">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxButton ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save">
                                                    <ClientSideEvents Click="OnSave" />
                                                </dx:ASPxButton>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                </Items>
                            </dx:LayoutGroup>
                            <dx:LayoutGroup GroupBoxDecoration="None" ColumnCount="1" Name="PendingNotice" ClientVisible="false">
                                <Items>
                                    <dx:LayoutItem ShowCaption="False">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxLabel ID="lblPendingNotice" ClientInstanceName="lblPendingNotice" runat="server" Text="*Contract is Pending awaiting Chief Officer approval." Font-Size="Medium" Font-Bold="true" ForeColor="Red"></dx:ASPxLabel>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                </Items>
                            </dx:LayoutGroup>
                            <dx:LayoutGroup GroupBoxDecoration="None" ColumnCount="1" Name="DeniedNotice" ClientVisible="false">
                                <Items>
                                    <dx:LayoutItem ShowCaption="False">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="*Contract has been denied." Font-Size="Medium" Font-Bold="true" ForeColor="Red"></dx:ASPxLabel>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                </Items>
                            </dx:LayoutGroup>
                        </Items>
                    </dx:ASPxFormLayout>
                    <asp:SqlDataSource ID="dsContract" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                        OnUpdating="dsContract_Updating" 
                        DeleteCommand="usp_deleteContract" DeleteCommandType="StoredProcedure" OnDeleting="dsContract_Deleting" 
                        SelectCommand="usp_getContractsEnhanced" SelectCommandType="StoredProcedure"
                        InsertCommand="usp_addContract" InsertCommandType="StoredProcedure"
                        UpdateCommand="usp_updateContract" UpdateCommandType="StoredProcedure"><%--OnSelecting="dsContract_Selecting"--%>
                        <InsertParameters>
                            <asp:ControlParameter ControlID="formContract$ddlParties" PropertyName="Value" Name="partyId" />
                            <asp:ControlParameter ControlID="formContract$ddlContractTypes" PropertyName="Value" Name="contractTypeId" />
                            <asp:ControlParameter ControlID="formContract$deStartDate" PropertyName="Date" Name="startDate" DefaultValue="<%= DateTime.Now%>" />
                            <asp:ControlParameter ControlID="formContract$deEndDate" PropertyName="Date" Name="endDate" DefaultValue="<%= DateTime.Now%>" />
                            <asp:ControlParameter ControlID="formContract$lblRoutingSequence" PropertyName="Text" Name="routingSequence" />
                            <asp:ControlParameter ControlID="formContract$lblStatus" PropertyName="Value" Name="statusId" DefaultValue="1" />
                            <asp:ControlParameter ControlID="formContract$ddlEntryUser" PropertyName="Value" Name="entryUserId" />
                            <asp:ControlParameter ControlID="formContract$deEntryDate" PropertyName="Text" Name="entryDate" DefaultValue="<%= DateTime.Now%>" />
                            <asp:ControlParameter ControlID="formContract$ddlActions" PropertyName="Value" Name="actionId" />
                            <asp:ControlParameter ControlID="formContract$ddlPurchasingGroup" PropertyName="Value" Name="isPurchasingGroup" />
                            <asp:ControlParameter ControlID="formContract$ddlPurchaseGroups" PropertyName="Value" Name="purchasingGroup" />
                            <asp:ControlParameter ControlID="formContract$ddlAutoRenewal" PropertyName="Value" Name="isAutoRenewal" />
                            <asp:ControlParameter ControlID="formContract$ddlOutclause" PropertyName="Value" Name="isOutClause" />
                            <asp:ControlParameter ControlID="formContract$spinDaysBreach" PropertyName="Value" Name="notificationDaysBreach" />
                            <asp:ControlParameter ControlID="formContract$spinDaysNoBreach" PropertyName="Value" Name="notificationDaysNoBreach" DefaultValue="0" />
                            <asp:ControlParameter ControlID="formContract$ddlExclusivity" PropertyName="Value" Name="isExclusivity" />
                            <asp:ControlParameter ControlID="formContract$memoDetails" PropertyName="Text" Name="details" />
                            <asp:ControlParameter ControlID="formContract$spinAnnualCost" PropertyName="Value" Name="annualCost" DefaultValue="0" />
                            <asp:ControlParameter ControlID="formContract$spinPriorYearCost" PropertyName="Value" Name="priorYearCost" DefaultValue="0" />
                            <asp:ControlParameter ControlID="formContract$spinEstimatedTotalCost" PropertyName="Value" Name="estimatedTotalCost" DefaultValue="0" />
                            <asp:ControlParameter ControlID="formContract$ddlPaymentTerm" PropertyName="Value" Name="paymentTerm" />
                            <asp:ControlParameter ControlID="formContract$ddlGlEOC" PropertyName="Value" Name="glEOC" />
                            <asp:ControlParameter ControlID="formContract$ddlAccessToBooks" PropertyName="Value" Name="isAccessToBooks" />
                            <asp:ControlParameter ControlID="formContract$txtBooksReference" PropertyName="Text" Name="accessToBooksReference" />
                            <asp:ControlParameter ControlID="formContract$ddlMedicareAdjustments" PropertyName="Value" Name="isMedicareAdjustments" />
                            <asp:ControlParameter ControlID="formContract$ddlHipaaBA" PropertyName="Value" Name="isHipaaBA" />
                            <asp:ControlParameter ControlID="formContract$ddlGsa" PropertyName="Value" Name="isGsa" />
                            <asp:ControlParameter ControlID="formContract$ddlPhysicianRationale" PropertyName="Value" Name="isPhysicianRationale" />
                            <asp:ControlParameter ControlID="formContract$ddlGoverningLaw" PropertyName="Value" Name="isGoverningLaw" />
                            <asp:ControlParameter ControlID="formContract$ddlContractEmployees" PropertyName="Value" Name="isContractEmployees" />
                            <asp:ControlParameter ControlID="formContract$deStatusDate" PropertyName="Date" Name="statusDate" DefaultValue="<%= DateTime.Now%>" />
                            <asp:ControlParameter ControlID="formContract$ddlSecurityLevel" PropertyName="Value" Name="securityLevelId" DefaultValue="0" />
                            <asp:ControlParameter ControlID="formContract$lblContractLength" PropertyName="Text" Name="contractLength" />
                            <asp:ControlParameter ControlID="formContract$ddlMasterContracts" PropertyName="Value" Name="masterContractId" />
                            <asp:ControlParameter ControlID="formContract$ddlDepartments" PropertyName="Value" Name="departmentId" />
                            <asp:ControlParameter ControlID="formContract$txtMedicareReference" PropertyName="Text" Name="medicareAdjustmentsReference" />
                            <asp:ControlParameter ControlID="formContract$txtHipaaReference" PropertyName="Text" Name="hipaaReference" />
                            <asp:ControlParameter ControlID="formContract$txtGoverningLawReference" PropertyName="Text" Name="governingLawReference" />
                            <asp:ControlParameter ControlID="formContract$txtPaymentOther" PropertyName="Text" Name="paymentOther" />
                            <asp:ControlParameter ControlID="formContract$ddlBudgeted" PropertyName="Value" Name="isBudgeted" />
                            <asp:ControlParameter ControlID="formContract$spinBudgetedAmount" PropertyName="Value" Name="budgetedAmount" DefaultValue="0" />
                            <asp:ControlParameter ControlID="formContract$ddlEACApproval" PropertyName="Value" Name="eacApproval" />
                            <asp:ControlParameter ControlID="formContract$deEACApprovalDate" PropertyName="Date" Name="eacApprovalDate" />
                            <asp:ControlParameter ControlID="formContract$ddlBOTApproval" PropertyName="Value" Name="botApproval" />
                            <asp:ControlParameter ControlID="formContract$deBOTApprovalDate" PropertyName="Date" Name="botApprovalDate" />
                            <asp:ControlParameter ControlID="formContract$ddlMPSApproval" PropertyName="Value" Name="mpsApproval" />
                            <asp:ControlParameter ControlID="formContract$deMPSApprovalDate" PropertyName="Date" Name="mpsApprovalDate" />
                            <asp:ControlParameter ControlID="formContract$ddlTrusteeApproval" PropertyName="Value" Name="trusteeApproval" />
                            <asp:ControlParameter ControlID="formContract$deTrusteeApprovalDate" PropertyName="Date" Name="trusteeApprovalDate" />
                            <asp:ControlParameter ControlID="formContract$ddlPartySigned" PropertyName="Value" Name="isPartySigned" />
                            <asp:ControlParameter ControlID="formContract$ddlElectronicAgreement" PropertyName="Value" Name="isElectronicAgreement" />
                            <asp:ControlParameter ControlID="formContract$rbCorporation" PropertyName="Value" Name="isCorporation" />
                            <asp:ControlParameter ControlID="formContract$rbCorporationStanding" PropertyName="Value" Name="isCorporationGoodStanding" />
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:ControlParameter ControlID="formContract$ddlParties" PropertyName="Value" Name="partyId" />
                            <asp:ControlParameter ControlID="formContract$ddlContractTypes" PropertyName="Value" Name="contractTypeId" />
                            <asp:ControlParameter ControlID="formContract$deStartDate" PropertyName="Date" Name="startDate" DefaultValue="<%= DateTime.Now%>" />
                            <asp:ControlParameter ControlID="formContract$deEndDate" PropertyName="Date" Name="endDate" DefaultValue="<%= DateTime.Now%>" />
                            <asp:ControlParameter ControlID="formContract$lblRoutingSequence" PropertyName="Text" Name="routingSequence" />
                            <asp:ControlParameter ControlID="formContract$lblStatus" PropertyName="Value" Name="statusId" DefaultValue="1" />
                            <asp:ControlParameter ControlID="formContract$ddlEntryUser" PropertyName="Value" Name="entryUserId" />
                            <asp:ControlParameter ControlID="formContract$deEntryDate" PropertyName="Text" Name="entryDate" DefaultValue="<%= DateTime.Now%>" />
                            <asp:ControlParameter ControlID="formContract$ddlActions" PropertyName="Value" Name="actionId" />
                            <asp:ControlParameter ControlID="formContract$ddlPurchasingGroup" PropertyName="Value" Name="isPurchasingGroup" />
                            <asp:ControlParameter ControlID="formContract$ddlPurchaseGroups" PropertyName="Value" Name="purchasingGroup" />
                            <asp:ControlParameter ControlID="formContract$ddlAutoRenewal" PropertyName="Value" Name="isAutoRenewal" />
                            <asp:ControlParameter ControlID="formContract$ddlOutclause" PropertyName="Value" Name="isOutClause" />
                            <asp:ControlParameter ControlID="formContract$ddlPartySigned" PropertyName="Value" Name="isPartySigned" />
                            <asp:ControlParameter ControlID="formContract$spinDaysBreach" PropertyName="Value" Name="notificationDaysBreach" />
                            <asp:ControlParameter ControlID="formContract$spinDaysNoBreach" PropertyName="Value" Name="notificationDaysNoBreach" DefaultValue="0" />
                            <asp:ControlParameter ControlID="formContract$ddlExclusivity" PropertyName="Value" Name="isExclusivity" />
                            <asp:ControlParameter ControlID="formContract$memoDetails" PropertyName="Text" Name="details" />
                            <asp:ControlParameter ControlID="formContract$spinAnnualCost" PropertyName="Value" Name="annualCost" DefaultValue="0" />
                            <asp:ControlParameter ControlID="formContract$spinPriorYearCost" PropertyName="Value" Name="priorYearCost" DefaultValue="0" />
                            <asp:ControlParameter ControlID="formContract$spinEstimatedTotalCost" PropertyName="Value" Name="estimatedTotalCost" DefaultValue="0" />
                            <asp:ControlParameter ControlID="formContract$ddlPaymentTerm" PropertyName="Value" Name="paymentTerm" />
                            <asp:ControlParameter ControlID="formContract$ddlGlEOC" PropertyName="Value" Name="glEOC" />
                            <asp:ControlParameter ControlID="formContract$ddlAccessToBooks" PropertyName="Value" Name="isAccessToBooks" />
                            <asp:ControlParameter ControlID="formContract$txtBooksReference" PropertyName="Text" Name="accessToBooksReference" />
                            <asp:ControlParameter ControlID="formContract$ddlMedicareAdjustments" PropertyName="Value" Name="isMedicareAdjustments" />
                            <asp:ControlParameter ControlID="formContract$ddlHipaaBA" PropertyName="Value" Name="isHipaaBA" />
                            <asp:ControlParameter ControlID="formContract$ddlGsa" PropertyName="Value" Name="isGsa" />
                            <asp:ControlParameter ControlID="formContract$ddlPhysicianRationale" PropertyName="Value" Name="isPhysicianRationale" />
                            <asp:ControlParameter ControlID="formContract$ddlGoverningLaw" PropertyName="Value" Name="isGoverningLaw" />
                            <asp:ControlParameter ControlID="formContract$ddlContractEmployees" PropertyName="Value" Name="isContractEmployees" />
                            <asp:ControlParameter ControlID="formContract$deStatusDate" PropertyName="Date" Name="statusDate" DefaultValue="<%= DateTime.Now%>" />
                            <asp:ControlParameter ControlID="formContract$ddlSecurityLevel" PropertyName="Value" Name="securityLevelId" DefaultValue="0" />
                            <asp:ControlParameter ControlID="formContract$lblContractLength" PropertyName="Text" Name="contractLength" />
                            <asp:QueryStringParameter Name="contractId" QueryStringField="id" />
                            <asp:ControlParameter ControlID="formContract$ddlDepartments" PropertyName="Value" Name="departmentId" />
                            <asp:ControlParameter ControlID="formContract$txtPaymentOther" PropertyName="Text" Name="paymentOther" />
                            <asp:ControlParameter ControlID="formContract$ddlMasterContracts" PropertyName="Value" Name="masterContractId" />
                            <asp:ControlParameter ControlID="formContract$txtMedicareReference" PropertyName="Text" Name="medicareAdjustmentsReference" />
                            <asp:ControlParameter ControlID="formContract$txtHipaaReference" PropertyName="Text" Name="hipaaReference" />
                            <asp:ControlParameter ControlID="formContract$txtGoverningLawReference" PropertyName="Text" Name="governingLawReference" />
                            <asp:ControlParameter ControlID="formContract$ddlBudgeted" PropertyName="Value" Name="isBudgeted" />
                            <asp:ControlParameter ControlID="formContract$spinBudgetedAmount" PropertyName="Value" Name="budgetedAmount" DefaultValue="0" />
                            <asp:ControlParameter ControlID="formContract$ddlEACApproval" PropertyName="Value" Name="eacApproval" />
                            <asp:ControlParameter ControlID="formContract$deEACApprovalDate" PropertyName="Date" Name="eacApprovalDate" />
                            <asp:ControlParameter ControlID="formContract$ddlBOTApproval" PropertyName="Value" Name="botApproval" />
                            <asp:ControlParameter ControlID="formContract$deBOTApprovalDate" PropertyName="Date" Name="botApprovalDate" />
                            <asp:ControlParameter ControlID="formContract$ddlMPSApproval" PropertyName="Value" Name="mpsApproval" />
                            <asp:ControlParameter ControlID="formContract$deMPSApprovalDate" PropertyName="Date" Name="mpsApprovalDate" />
                            <asp:ControlParameter ControlID="formContract$ddlTrusteeApproval" PropertyName="Value" Name="trusteeApproval" />
                            <asp:ControlParameter ControlID="formContract$deTrusteeApprovalDate" PropertyName="Date" Name="trusteeApprovalDate" />
                            <asp:ControlParameter ControlID="formContract$ddlElectronicAgreement" PropertyName="Value" Name="isElectronicAgreement" />
                            <asp:ControlParameter ControlID="formContract$rbCorporation" PropertyName="Value" Name="isCorporation" />
                            <asp:ControlParameter ControlID="formContract$rbCorporationStanding" PropertyName="Value" Name="isCorporationGoodStanding" />
                            <asp:ControlParameter ControlID="formContract$spinContractFolder" PropertyName="Value" Name="contractFolder" />
                            <asp:ControlParameter ControlID="formContract$ddlRoles" PropertyName="Text" Name="currentRoleName" />
                            <asp:ControlParameter ControlID="formContract$ddlUsers" PropertyName="Text" Name="currentFullName" />
                            <asp:Parameter Name="isValid" Direction="Output" Size="4" Type="Boolean" />
                        </UpdateParameters>
                        <DeleteParameters>
                            <asp:QueryStringParameter Name="contractId" QueryStringField="id" />
                        </DeleteParameters>
                        <SelectParameters>
                            <%--<asp:ControlParameter Name="contractId" ControlID="formContract$ddlContracts" PropertyName="Value" />--%>
                            <asp:QueryStringParameter Name="contractId" QueryStringField="id" />
                            <%--<asp:SessionParameter Name="isAdmin" SessionField="isAdmin" />--%>
                            <asp:Parameter Name="isAdmin" DefaultValue="1" />
                            <asp:SessionParameter Name="userFullName" SessionField="UserName" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="dsUpdateStatus" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                        UpdateCommand="usp_updateContractStatus" UpdateCommandType="StoredProcedure" OnUpdated="dsUpdateStatus_Updated" OnUpdating="dsUpdateStatus_Updating">
                        <UpdateParameters>
                            <asp:QueryStringParameter Name="contractId" QueryStringField="id" />
                            <asp:Parameter Name="approvalStatus" Type="Boolean" />
                            <asp:SessionParameter Name="userId" SessionField="UserID" />
                            <asp:ControlParameter Name="routingSequence" ControlID="formContract$lblNextSequence" PropertyName="Text" />
                            <asp:Parameter Name="isBypass" DefaultValue="False" />
                        </UpdateParameters>
                    </asp:SqlDataSource>
                    <dx:ASPxPopupControl ID="popBypass" ClientInstanceName="popBypass" runat="server" Width="600px" Left="150" PopupVerticalAlign="WindowCenter" ShowHeader="false" ShowCloseButton="true">
                        <ContentCollection>
                             <dx:PopupControlContentControl>
                                <dx:ASPxRoundPanel ID="pnlBypassReaspon" runat="server" HeaderText="Bypass User" Width="100%">
                                    <PanelCollection>
                                        <dx:PanelContent>
                                            <div id="divBypassReason" runat="server" style="width:100%;">
                                                <dx:ASPxLabel ID="lblBypassReason" runat="server" Text="Reason for Bypass:"></dx:ASPxLabel>
                                                <br /><br />
                                                <dx:ASPxMemo ID="memoBypassReason" runat="server" Width="100%" Height="100"></dx:ASPxMemo>
                                            </div>
                                            <br /><br />
                                            <div style="text-align:center;width:100%;">
                                                <dx:ASPxButton ID="btnBypassCancel" runat="server" Text="Cancel" AutoPostBack="false">
                                                    <ClientSideEvents Click="function(s,e){popBypass.Hide();}" />
                                                </dx:ASPxButton>
                                                &nbsp;&nbsp;&nbsp;
                                                <dx:ASPxButton ID="btnBypassReason" runat="server" Text="Bypass" AutoPostBack="true" OnClick="btnBypassReason_Click"></dx:ASPxButton>
                                            </div>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                </dx:ASPxRoundPanel>
                            </dx:PopupControlContentControl>
                        </ContentCollection>
                    </dx:ASPxPopupControl>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxCallbackPanel>
    </div>
    <div style="max-width:48%; top:20px; height:100%; position:relative; overflow:auto; display:inline-block; vertical-align:top; z-index:0 ">
        <dx:ASPxCallbackPanel ID="cbDocuments" runat="server" OnCallback="cbDocuments_Callback">
            <PanelCollection>
                <dx:PanelContent>
                    <script type="text/javascript">
                        function AdjustHeight(s, e) {
                            s.SetHeight(window.innerHeight * .75);
                            s.SetWidth(window.innerWidth * .35);
                        }
                        function ShowBulkEmail(s, e) {
                            if ('<%=Session["isAdmin"] %>' == 'True') {
                                if (e.tab.name == 'Documents') {
                                    btnDocusignBulk.SetVisible(true);
                                    btnGetDocusign.SetVisible(true);
                                    btnEmailRequestor.SetVisible(true);
                                } else {
                                    btnDocusignBulk.SetVisible(false);
                                    btnGetDocusign.SetVisible(false);
                                    btnEmailRequestor.SetVisible(false);
                                }
                            } else {
                                btnDocusignBulk.SetVisible(false);
                                btnGetDocusign.SetVisible(false);
                                btnEmailRequestor.SetVisible(false);
                            }
                        }
                    </script>
                    <asp:SqlDataSource ID="dsUploadDocumentTypes" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>"
                        SelectCommand="usp_getUploadDocumentTypes" SelectCommandType="StoredProcedure" OnSelecting="dsUploadDocumentTypes_Selecting"></asp:SqlDataSource>
                    <dx:ASPxPageControl ID="pcContract" ClientInstanceName="pcContract" runat="server" TabPosition="Left" EncodeHtml="false" Width="100%">
                        <ClientSideEvents ActiveTabChanging="ShowBulkEmail" />
                        <TabPages>
                            <dx:TabPage Name="AllContracts" Text="Related<br/>Contracts">
                                <ContentCollection>
                                   <dx:ContentControl>
                                       <dx:ASPxRoundPanel ID="pnlAllContracts" runat="server" ShowHeader="false" ShowCollapseButton="false" BorderLeft-BorderStyle="Solid" BorderLeft-BorderWidth="1px" BorderLeft-BorderColor="LightGray" BorderRight-BorderStyle="Solid" BorderRight-BorderWidth="1px" BorderRight-BorderColor="LightGray">
                                           <ClientSideEvents Init="AdjustHeightAllContracts" />
                                           <PanelCollection>
                                               <dx:PanelContent runat="server">
                                                   <script type="text/javascript">
                                                       function AdjustHeightAllContracts(s, e) {
                                                           gvAllContracts.SetHeight(window.innerHeight * .75);
                                                           gvAllContracts.SetWidth(window.innerWidth * .35);
                                                       }
                                                   </script>
                                                   <dx:ASPxGridView ID="gvAllContracts" ClientInstanceName="gvAllContracts" runat="server" DataSourceID="dsAllContracts" KeyFieldName="contractId">
                                                        <SettingsAdaptivity AdaptivityMode="HideDataCells" AllowOnlyOneAdaptiveDetailExpanded="true" AllowHideDataCellsByColumnMinWidth="true"></SettingsAdaptivity>
                                                       <SettingsBehavior AllowEllipsisInText="true"/>
                                                        <SettingsResizing ColumnResizeMode="Control" Visualization="Live" />
                                                        <EditFormLayoutProperties>
                                                            <SettingsAdaptivity AdaptivityMode="SingleColumnWindowLimit" SwitchToSingleColumnAtWindowInnerWidth="1000" />
                                                        </EditFormLayoutProperties>
                                                       <ClientSideEvents Init="AdjustHeightAllContracts" />
                                                       <Columns>
                                                           <dx:GridViewDataColumn FieldName="contractId" Visible="false"></dx:GridViewDataColumn>
                                                           <dx:BootstrapGridViewDataColumn Visible="false" FieldName="contractNumber" Caption="Contract #"></dx:BootstrapGridViewDataColumn>
                                                           <dx:GridViewDataHyperLinkColumn FieldName="contractId" Caption="Vendor Name" CellStyle-HorizontalAlign="Left" AdaptivePriority="1">
                                                               <PropertiesHyperLinkEdit TextField="businessName" NavigateUrlFormatString="contractDetails.aspx?id={0}"></PropertiesHyperLinkEdit>
                                                           </dx:GridViewDataHyperLinkColumn>
                                                           <dx:GridViewDataColumn FieldName="startDate" Caption="Start Date" AdaptivePriority="2" MinWidth="30"></dx:GridViewDataColumn>
                                                           <dx:GridViewDataColumn FieldName="endDate" Caption="End Date" AdaptivePriority="2" MinWidth="30"></dx:GridViewDataColumn>
                                                           <dx:BootstrapGridViewDataColumn FieldName="statusType" Caption="Status" AdaptivePriority="3" MinWidth="50"></dx:BootstrapGridViewDataColumn>
                                                           <dx:BootstrapGridViewCheckColumn FieldName="isMaster" Caption="Master?" AdaptivePriority="3" MinWidth="50"></dx:BootstrapGridViewCheckColumn>
                                                       </Columns>
                                                       <FormatConditions>
                                                           <dx:GridViewFormatConditionHighlight ApplyToRow="true" Format="LightGreenFill" FieldName="isMaster" Expression="isMaster = 1"></dx:GridViewFormatConditionHighlight>
                                                       </FormatConditions>
                                                   </dx:ASPxGridView>
                                                    <asp:SqlDataSource ID="dsAllContracts" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>"
                                                        SelectCommand="usp_getContractsByMasterId" SelectCommandType="StoredProcedure">
                                                        <SelectParameters>
                                                            <asp:QueryStringParameter Name="contractId" QueryStringField="id" />
                                                            <asp:Parameter Name="includeSelectedContract" DefaultValue="0" />
                                                        </SelectParameters>
                                                    </asp:SqlDataSource>
                                               </dx:PanelContent>
                                           </PanelCollection>
                                       </dx:ASPxRoundPanel>
                                   </dx:ContentControl>
                                </ContentCollection>
                            </dx:TabPage>
                            <dx:TabPage Name="Documents" Text="Documents">
                                <ContentCollection>
                                    <dx:ContentControl>
                                        <dx:ASPxRoundPanel ID="pnlDocuments" runat="server" ShowHeader="false" ShowCollapseButton="false" BorderLeft-BorderStyle="Solid" BorderLeft-BorderWidth="1px" BorderLeft-BorderColor="LightGray" BorderRight-BorderStyle="Solid" BorderRight-BorderWidth="1px" BorderRight-BorderColor="LightGray">
                                            <PanelCollection>
                                                <dx:PanelContent runat="server">
                                                    <dx:ASPxGridView ID="gvDocuments" ClientInstanceName="gvDocuments" runat="server" DataSourceID="dsDocuments" KeyFieldName="documentId" OnHtmlDataCellPrepared="gvDocuments_HtmlDataCellPrepared" OnRowUpdating="gvDocuments_RowUpdating" Caption="Contract Documents"><%-- OnInit="gvDocuments_Init"--%>
                                                        <SettingsAdaptivity AdaptivityMode="HideDataCells" AllowOnlyOneAdaptiveDetailExpanded="true"
                                                            AllowHideDataCellsByColumnMinWidth="true"></SettingsAdaptivity>
                                                       <SettingsBehavior AllowEllipsisInText="true"/>
                                                        <EditFormLayoutProperties>
                                                            <SettingsAdaptivity AdaptivityMode="SingleColumnWindowLimit" SwitchToSingleColumnAtWindowInnerWidth="1000" />
                                                        </EditFormLayoutProperties>
                                                       <ClientSideEvents Init="AdjustHeight" />
                                                        <Columns>
                                                            <dx:GridViewDataColumn FieldName="fileName" Caption="File Name" Visible="false"></dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn FieldName="documentType" Caption="Type" Visible="false"></dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn FieldName="path" Caption="Path" Visible="false"></dx:GridViewDataColumn>
                                                            <dx:GridViewDataHyperLinkColumn FieldName="url" Caption=" " AdaptivePriority="1" ReadOnly="true" EditFormSettings-Visible="False">
                                                                <PropertiesHyperLinkEdit NavigateUrlFormatString="DocumentViewer.aspx?url={0}"  ImageUrl="{0}" ImageUrlField="ImageUrl" ImageHeight="30" ImageWidth="30" Target="_blank" ></PropertiesHyperLinkEdit>
                                                                <DataItemTemplate>
                                                                    <dx:ASPxHyperlink ID="btnURL" runat="server" Text="Select" AutoPostBack="false" Cursor="pointer" ForeColor="Blue" Font-Underline="true" OnInit="btnURL_Init" RenderMode="Link" EnableViewState="false"></dx:ASPxHyperlink>
                                                                </DataItemTemplate>
                                                            </dx:GridViewDataHyperLinkColumn>
                                                            <dx:GridViewDataComboBoxColumn FieldName="uploadDocumentTypeId" Caption="Document Type" AdaptivePriority="2" PropertiesComboBox-DataSourceID="dsUploadDocumentTypes" PropertiesComboBox-TextField="uploadDocumentType" PropertiesComboBox-ValueField="uploadDocumentTypeId"></dx:GridViewDataComboBoxColumn>
                                                            <dx:GridViewDataColumn FieldName="documentId" Visible="false"></dx:GridViewDataColumn>
                                                            <dx:GridViewDataDateColumn FieldName="uploadDate" Caption="Upload Date" AdaptivePriority="3" ReadOnly="true" EditFormSettings-Visible="False"></dx:GridViewDataDateColumn>
                                                            <dx:GridViewDataColumn FieldName="fullName" Caption="User" AdaptivePriority="3" EditFormSettings-Visible="False"></dx:GridViewDataColumn>
                                                            <dx:GridViewCommandColumn Caption=" " ShowDeleteButton="true" ShowEditButton="true" AdaptivePriority="1" Name="Delete"></dx:GridViewCommandColumn>
                                                            <dx:GridViewDataColumn FieldName="folder" Visible="false"></dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn FieldName="envelopeId" Visible="false"></dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn FieldName="partyId" Visible="false"></dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn FieldName="isEmailSigned" Visible="false"></dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn FieldName="mainFolder" Visible="false"></dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn FieldName="contractFolder" Caption="Contract Folder" Visible="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True"></dx:GridViewDataColumn>
<%--                                                            <dx:GridViewDataCheckColumn Caption="Bulk<br/>Email" HeaderStyle-HorizontalAlign="Center" Name="isToSend" PropertiesCheckEdit-NullDisplayText="false"></dx:GridViewDataCheckColumn>--%>
                                                            <dx:GridViewCommandColumn ShowSelectCheckbox="true" Caption="Bulk<br/>Email" HeaderStyle-HorizontalAlign="Center" Name="isToSend"></dx:GridViewCommandColumn>
                                                            <%--<dx:GridViewDataColumn Name="EmailDocusign" AdaptivePriority="1" EditFormSettings-Visible="False">
                                                                <DataItemTemplate>
                                                                    <dx:ASPxButton ID="btnDocusign" runat="server" Text="Email CEO" OnInit="btnDocusign_Init">
                                                                        <ClientSideEvents Click="function(s,e){gvDocuments.PerformCallback();}" />
                                                                    </dx:ASPxButton><%--OnClick="btnDocusign_Click"
                                                                </DataItemTemplate>
                                                            </dx:GridViewDataColumn>--%>
                                                        </Columns>
                                                        <SettingsDataSecurity AllowDelete="true" AllowEdit="true" />
                                                        <SettingsBehavior ConfirmDelete="true" />
                                                        <SettingsEditing></SettingsEditing>
                                                        <SettingsText ConfirmDelete="Are you sure you want to delete this document?" />
                                                        <SettingsResizing ColumnResizeMode="Control" Visualization="Live" />
                                                        <Styles Header-Font-Bold="true"></Styles>
                                                    </dx:ASPxGridView>
                                                    <asp:SqlDataSource ID="dsDocuments" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>"
                                                        SelectCommand="usp_getDocuments" SelectCommandType="StoredProcedure"
                                                        InsertCommand="usp_addDocument" InsertCommandType="StoredProcedure"
                                                        UpdateCommand="usp_updateDocument" UpdateCommandType="StoredProcedure" OnUpdating="dsDocuments_Updating" OnUpdated="dsDocuments_Updated"
                                                        DeleteCommand="usp_deleteDocument" DeleteCommandType="StoredProcedure" OnDeleting="dsDocuments_Deleting">
                                                        <SelectParameters>
                                                            <asp:QueryStringParameter Name="contractId" QueryStringField="id" />
                                                        </SelectParameters>
                                                        <InsertParameters>
                                                            <asp:Parameter Name="fileName" />
                                                            <asp:Parameter Name="path" />
                                                            <asp:Parameter Name="uploadDocumentTypeId" />
                                                            <asp:Parameter Name="folder" />
                                                            <asp:QueryStringParameter Name="contractId" QueryStringField="id" />
                                                            <asp:SessionParameter Name="uploadId" SessionField="UserID" />
                                                            <asp:Parameter Name="partyId" />
                                                            <asp:Parameter Name="contractTypeId" />
                                                        </InsertParameters>
                                                        <UpdateParameters>
                                                            <asp:Parameter Name="uploadDocumentTypeId" />
                                                            <asp:SessionParameter Name="uploadId" SessionField="UserID" />
                                                            <asp:Parameter Name="documentId" />
                                                            <asp:Parameter Name="contractTypeId" />
                                                            <asp:Parameter Name="partyId" />
                                                            <asp:Parameter Name="contractId" />
                                                            <asp:Parameter Name="folder" Direction="Output" DbType="String" Size="20" />
                                                            <asp:Parameter Name="prevFolder" Direction="Output" DbType="String" Size="20" />
                                                        </UpdateParameters>
                                                        <DeleteParameters>
                                                            <asp:Parameter Name="documentId" />
                                                        </DeleteParameters>
                                                    </asp:SqlDataSource>
                                                    <br />
                                                    <dx:ASPxGridView ID="gvEsignatures" runat="server" ClientInstanceName="gvEsignatures" Caption="Electronic Agreements" DataSourceID="dsEsignatures" KeyFieldName="linkId" Visible="false">
                                                        <SettingsAdaptivity AdaptivityMode="HideDataCells" AllowOnlyOneAdaptiveDetailExpanded="true"
                                                            AllowHideDataCellsByColumnMinWidth="true"></SettingsAdaptivity>
                                                       <SettingsBehavior AllowEllipsisInText="true"/>
                                                        <EditFormLayoutProperties>
                                                            <SettingsAdaptivity AdaptivityMode="SingleColumnWindowLimit" SwitchToSingleColumnAtWindowInnerWidth="1000" />
                                                        </EditFormLayoutProperties>
                                                       <ClientSideEvents Init="AdjustHeight" />
                                                        <Columns>
                                                            <dx:GridViewCommandColumn ShowEditButton="true" Width="50">
                                                                <HeaderTemplate>
                                                                    <a href="javascript:gvEsignatures.AddNewRow()" style="text-decoration:underline;">New</a>
                                                                </HeaderTemplate>
                                                            </dx:GridViewCommandColumn>
                                                            <dx:GridViewDataColumn FieldName="linkId" Visible="false"></dx:GridViewDataColumn>
                                                            <dx:GridViewDataHyperLinkColumn FieldName="url" PropertiesHyperLinkEdit-Target="_blank" Caption="Electronic Agreement Link">
                                                                <EditFormSettings ColumnSpan="3" />
                                                            </dx:GridViewDataHyperLinkColumn>
                                                            <dx:GridViewDataColumn FieldName="fullName" Caption="User" EditFormSettings-Visible="False"></dx:GridViewDataColumn>
                                                            <dx:GridViewDataDateColumn FieldName="dateAdded" Caption="Date" EditFormSettings-Visible="False"></dx:GridViewDataDateColumn>
                                                            <dx:GridViewCommandColumn ShowDeleteButton="true" Width="70" Caption=" "></dx:GridViewCommandColumn>
                                                        </Columns>
                                                        <SettingsDataSecurity AllowDelete="true" AllowEdit="true" AllowInsert="true" />
                                                        <SettingsBehavior ConfirmDelete="true" />
                                                        <SettingsText ConfirmDelete="Are you sure you want to delete this link?" />
                                                        <SettingsResizing ColumnResizeMode="Control" Visualization="Live" />
                                                        <Styles Header-Font-Bold="true"></Styles>
                                                    </dx:ASPxGridView>
                                                    <asp:SqlDataSource ID="dsEsignatures" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>"
                                                        SelectCommand="select e.*, u.fullName from esignatureLinks e join users u on e.userId = u.userId and e.contractId = @contractId" SelectCommandType="Text"
                                                        DeleteCommand="delete from esignatureLinks where linkId = @linkId" DeleteCommandType="Text"
                                                        InsertCommand="insert into esignatureLinks(contractId,url,userId,dateAdded) values(@contractId,@url,@userId,GetDate())" InsertCommandType="Text"
                                                        UpdateCommand="update esignatureLinks set contractId = @contractId, url=@url, userId = @userId, dateAdded = GetDate() where linkId = @linkId" UpdateCommandType="Text">
                                                        <SelectParameters>
                                                            <asp:QueryStringParameter QueryStringField="id" Name="contractId" />
                                                        </SelectParameters>
                                                        <DeleteParameters>
                                                            <asp:Parameter Name="linkId" />
                                                        </DeleteParameters>
                                                        <InsertParameters>
                                                            <asp:QueryStringParameter QueryStringField="id" Name="contractId" />
                                                            <asp:Parameter Name="url" />
                                                            <asp:SessionParameter Name="userId" SessionField="UserID" />
                                                        </InsertParameters>
                                                        <UpdateParameters>
                                                            <asp:QueryStringParameter QueryStringField="id" Name="contractId" />
                                                            <asp:Parameter Name="url" />
                                                            <asp:SessionParameter Name="userId" SessionField="UserID" />
                                                        </UpdateParameters>
                                                    </asp:SqlDataSource>
                                                </dx:PanelContent>
                                            </PanelCollection>
                                        </dx:ASPxRoundPanel>
                                    </dx:ContentControl>
                                </ContentCollection>
                            </dx:TabPage>
                            <dx:TabPage Name="Upload" Text="Upload<br/>Documents">
                                <ContentCollection>
                                    <dx:ContentControl>
                                        <script type="text/javascript">
                                            var uploadInProgress = false,
                                                submitInitiated = false,
                                                uploadErrorOccurred = false;
                                            uploadedFiles = [];
                                            function onFileUploadComplete(s, e) {
                                                var callbackData = e.callbackData.split("|"),
                                                    uploadedFileName = callbackData[0],
                                                    isSubmissionExpired = callbackData[1] === "True";
                                                uploadedFiles.push(uploadedFileName);
                                                if (e.errorText.length > 0 || !e.isValid) {
                                                    uploadErrorOccurred = true;
                                                    UploadedFilesTokenBox.isValid = false;
                                                    alert("Error uploading file(s)");
                                                }
                                                if (isSubmissionExpired && UploadedFilesTokenBox.GetText().length > 0) {
                                                    var removedAfterTimeoutFiles = UploadedFilesTokenBox.GetTokenCollection().join("\n");
                                                    alert("The following files have been removed from the server due to the defined 5 minute timeout: \n\n" + removedAfterTimeoutFiles);
                                                    UploadedFilesTokenBox.ClearTokenCollection();
                                                }
                                            }
                                            function onFileUploadStart(s, e) {
                                                uploadInProgress = true;
                                                uploadErrorOccurred = false;
                                                UploadedFilesTokenBox.SetIsValid(true);
                                            }
                                            function onFilesUploadComplete(s, e) {
                                                uploadInProgress = false;
                                                for (var i = 0; i < uploadedFiles.length; i++)
                                                    UploadedFilesTokenBox.AddToken(uploadedFiles[i]);
                                                updateTokenBoxVisibility();
                                                uploadedFiles = [];
                                                if (submitInitiated) {
                                                    SubmitButton.SetEnabled(true);
                                                    SubmitButton.DoClick();
                                                }
                                            }
                                            function onSubmitButtonInit(s, e) {
                                                s.SetEnabled(true);
                                            }
                                            function onSubmitButtonClick(s, e) {
                                                ASPxClientEdit.ValidateGroup();
                                                if (!formIsValid()) {
                                                    e.processOnServer = false;
                                                }
                                                else if (uploadInProgress) {
                                                    s.SetEnabled(false);
                                                    submitInitiated = true;
                                                    e.processOnServer = false;
                                                }
                                            }
                                            function onTokenBoxValidation(s, e) {
                                                var isValid = DocumentsUploadControl.GetText().length > 0 || UploadedFilesTokenBox.GetText().length > 0;
                                                e.isValid = isValid;
                                                if (!isValid) {
                                                    e.errorText = "No files have been uploaded. Upload at least one file.";
                                                }
                                            }
                                            function onTokenBoxValueChanged(s, e) {
                                                updateTokenBoxVisibility();
                                            }
                                            function updateTokenBoxVisibility() {
                                                var isTokenBoxVisible = UploadedFilesTokenBox.GetTokenCollection().length > 0;
                                                UploadedFilesTokenBox.SetVisible(isTokenBoxVisible);
                                            }
                                            function formIsValid() {
                                                return !ValidationSummary.IsVisible() && ddlUploadDocumentType.GetIsValid() && UploadedFilesTokenBox.GetIsValid() && !uploadErrorOccurred;
                                            }
                                        </script>
                                        <dx:ASPxHiddenField runat="server" ID="HiddenField" ClientInstanceName="HiddenField" />
                                        <dx:ASPxFormLayout ID="FormLayout" runat="server" Width="100%" ColCount="1" UseDefaultPaddings="false">
                                            <ClientSideEvents Init="AdjustHeight" />
                                            <Items>
                                                <dx:LayoutGroup ShowCaption="False" GroupBoxDecoration="None" Width="100%" UseDefaultPaddings="false" ColumnCount="1">
                                                    <Items>
                                                        <dx:LayoutItem Caption="Document Type" CaptionStyle-Font-Bold="true" CaptionSettings-Location="Top" ColumnSpan="1">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer>
                                                                    <dx:ASPxComboBox ID="ddlUploadDocumentType" ClientInstanceName="ddlUploadDocumentType" runat="server" DataSourceID="dsUploadDocumentTypes" TextField="uploadDocumentType" ValueField="uploadDocumentTypeId">
                                                                        <ValidationSettings  Display="Dynamic" ErrorDisplayMode="Text" ErrorTextPosition="Bottom" ValidationGroup="DocumentTypeValidation">
                                                                            <RequiredField IsRequired="True" ErrorText="Document Type is required" />
                                                                        </ValidationSettings>
                                                                    </dx:ASPxComboBox>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                        <dx:LayoutItem Caption="Replace Existing File" CaptionStyle-Font-Bold="true" CaptionSettings-Location="Left" ColumnSpan="1" Name="ReplaceFile" ClientVisible="false">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer>
                                                                    <dx:ASPxCheckBox ID="cbReplaceFile" runat="server" Checked="false"></dx:ASPxCheckBox>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                        <dx:LayoutGroup Caption="Documents" ColumnCount="1">
                                                            <Items>
                                                                <dx:LayoutItem ShowCaption="False" ColumnSpan="1">
                                                                    <LayoutItemNestedControlCollection>
                                                                        <dx:LayoutItemNestedControlContainer>
                                                                            <div id="dropZone">
                                                                                <dx:ASPxUploadControl runat="server" ID="DocumentsUploadControl" ClientInstanceName="DocumentsUploadControl" Width="100%"
                                                                                    AutoStartUpload="true" ShowProgressPanel="True" ShowTextBox="false" BrowseButton-Text="Add documents" FileUploadMode="OnPageLoad"
                                                                                    OnFileUploadComplete="DocumentsUploadControl_FileUploadComplete">
                                                                                    <AdvancedModeSettings EnableMultiSelect="true" EnableDragAndDrop="true" ExternalDropZoneID="dropZone" />
                                                                                    <ValidationSettings ShowErrors="true" GeneralErrorText="Error uploading file(s)"></ValidationSettings>
                                                                                    <ClientSideEvents
                                                                                        FileUploadComplete="onFileUploadComplete"
                                                                                        FilesUploadComplete="onFilesUploadComplete"
                                                                                        FilesUploadStart="onFileUploadStart" />
                                                                                </dx:ASPxUploadControl>
                                                                                <br />
                                                                                <dx:ASPxTokenBox runat="server" Width="100%" ID="UploadedFilesTokenBox" ClientInstanceName="UploadedFilesTokenBox"
                                                                                    NullText="Select the documents to submit" AllowCustomTokens="false" ClientVisible="false">
                                                                                    <ClientSideEvents Init="updateTokenBoxVisibility" ValueChanged="onTokenBoxValueChanged" Validation="onTokenBoxValidation" />
                                                                                    <ValidationSettings EnableCustomValidation="true"/><%-- ValidationGroup="UploadValidation" --%>
                                                                                </dx:ASPxTokenBox>
                                                                                <br />
                                                                                <%--<p class="Note">
                                                                                    <dx:ASPxLabel ID="MaxFileSizeLabel" runat="server" Text="Maximum file size: 4 MB." Font-Size="8pt" />
                                                                                </p>--%>
                                                                                <dx:ASPxValidationSummary runat="server" ID="ValidationSummary" ClientInstanceName="ValidationSummary" RenderMode="BulletedList" Width="250px" ShowErrorAsLink="false" ForeColor="Red" Font-Bold="true" /><%-- ValidationGroup="UploadValidation"--%>
                                                                            </div>
                                                                        </dx:LayoutItemNestedControlContainer>
                                                                    </LayoutItemNestedControlCollection>
                                                                </dx:LayoutItem>
                                                            </Items>
                                                        </dx:LayoutGroup>
                                                        <dx:LayoutItem ShowCaption="False" HorizontalAlign="Right" ColumnSpan="1">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer>
                                                                    <dx:ASPxButton runat="server" ID="SubmitButton" ClientInstanceName="SubmitButton" Text="Submit" AutoPostBack="True"
                                                                        OnClick="SubmitButton_Click"  ClientEnabled="false" ValidateInvisibleEditors="true">
                                                                        <ClientSideEvents Init="onSubmitButtonInit" Click="onSubmitButtonClick" />
                                                                    </dx:ASPxButton>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                        <dx:EmptyLayoutItem Height="5" />
                                                    </Items>
                                                </dx:LayoutGroup>
                                                <dx:LayoutGroup GroupBoxDecoration="None" ShowCaption="False" Name="ResultGroup" Visible="false" Width="50%" UseDefaultPaddings="false">
                                                    <Items>
                                                        <dx:LayoutItem ShowCaption="False">
                                                            <LayoutItemNestedControlCollection>
                                                                <dx:LayoutItemNestedControlContainer>
                                                                    <dx:ASPxRoundPanel ID="RoundPanel" runat="server" HeaderText="Uploaded files" Width="100%">
                                                                        <PanelCollection>
                                                                            <dx:PanelContent>
                                                                                <b>Description:</b>
                                                                                <dx:ASPxLabel runat="server" ID="DescriptionLabel" />
                                                                                <br />
                                                                                <br />
                                                                                <dx:ASPxListBox ID="SubmittedFilesListBox" runat="server" Width="100%" Height="150px">
                                                                                    <ItemStyle CssClass="ResultFileName" />
                                                                                    <Columns>
                                                                                        <dx:ListBoxColumn FieldName="OriginalFileName" />
                                                                                        <dx:ListBoxColumn FieldName="FileSize" Width="15%"/>
                                                                                    </Columns>
                                                                                </dx:ASPxListBox>
                                                                            </dx:PanelContent>
                                                                        </PanelCollection>
                                                                    </dx:ASPxRoundPanel>
                                                                </dx:LayoutItemNestedControlContainer>
                                                            </LayoutItemNestedControlCollection>
                                                        </dx:LayoutItem>
                                                    </Items>
                                                </dx:LayoutGroup>
                                            </Items>
                                            <SettingsAdaptivity AdaptivityMode="SingleColumnWindowLimit" SwitchToSingleColumnAtWindowInnerWidth="1000" />
                                        </dx:ASPxFormLayout>                        
                                    </dx:ContentControl>
                                </ContentCollection>
                            </dx:TabPage>
                            <dx:TabPage Name="Bypass" Text="Bypass<br/>Tracking">
                                <ContentCollection>
                                    <dx:ContentControl>
                                        <dx:ASPxRoundPanel ID="pnlBypass" runat="server" ShowHeader="false" BorderLeft-BorderStyle="Solid" BorderLeft-BorderWidth="1px" BorderLeft-BorderColor="LightGray" BorderRight-BorderStyle="Solid" BorderRight-BorderWidth="1px" BorderRight-BorderColor="LightGray">
                                            <PanelCollection>
                                                <dx:PanelContent>
                                                    <dx:ASPxGridView ID="gvBypass" ClientInstanceName="gvBypass" runat="server" DataSourceID="dsBypass">
                                                        <SettingsAdaptivity AdaptivityMode="HideDataCells" AllowOnlyOneAdaptiveDetailExpanded="true"
                                                            AllowHideDataCellsByColumnMinWidth="true"></SettingsAdaptivity>
                                                       <SettingsBehavior AllowEllipsisInText="true"/>
                                                        <SettingsResizing ColumnResizeMode="Control" Visualization="Live" />
                                                        <EditFormLayoutProperties>
                                                            <SettingsAdaptivity AdaptivityMode="SingleColumnWindowLimit" SwitchToSingleColumnAtWindowInnerWidth="1000" />
                                                        </EditFormLayoutProperties>
                                                       <ClientSideEvents Init="AdjustHeight" />
                                                        <Columns>
                                                            <dx:GridViewDataDateColumn FieldName="statusDate" Caption="Date" AdaptivePriority="2"></dx:GridViewDataDateColumn>
                                                            <dx:GridViewDataColumn FieldName="fullName" Caption="Bypassed By" AdaptivePriority="1"></dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn FieldName="bypassRole" Caption="Bypassed Role" AdaptivePriority="1"></dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn FieldName="byPassReason" Caption="Reason" AdaptivePriority="3"></dx:GridViewDataColumn>
                                                        </Columns>
                                                    </dx:ASPxGridView>
                                                    <asp:SqlDataSource ID="dsBypass" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>"
                                                        SelectCommand="usp_getBypassTracking" SelectCommandType="StoredProcedure">
                                                        <SelectParameters>
                                                            <asp:QueryStringParameter Name="contractId" QueryStringField="id" />
                                                        </SelectParameters>
                                                    </asp:SqlDataSource>
                                                </dx:PanelContent>
                                            </PanelCollection>
                                        </dx:ASPxRoundPanel>
                                    </dx:ContentControl>
                                </ContentCollection>
                            </dx:TabPage>
                            <dx:TabPage Name="Return" Text="Return<br/>Tracking">
                                <ContentCollection>
                                    <dx:ContentControl>
                                        <dx:ASPxRoundPanel ID="pnlReturnData" runat="server" ShowHeader="false" BorderLeft-BorderStyle="Solid" BorderLeft-BorderWidth="1px" BorderLeft-BorderColor="LightGray" BorderRight-BorderStyle="Solid" BorderRight-BorderWidth="1px" BorderRight-BorderColor="LightGray">
                                            <PanelCollection>
                                                <dx:PanelContent>
                                                    <dx:ASPxGridView ID="gvReturn" runat="server" DataSourceID="dsReturnData">
                                                        <SettingsAdaptivity AdaptivityMode="HideDataCells" AllowOnlyOneAdaptiveDetailExpanded="true"
                                                            AllowHideDataCellsByColumnMinWidth="true"></SettingsAdaptivity>
                                                       <SettingsBehavior AllowEllipsisInText="false"/>
                                                        <SettingsResizing ColumnResizeMode="Control" Visualization="Live" />
                                                        <EditFormLayoutProperties>
                                                            <SettingsAdaptivity AdaptivityMode="SingleColumnWindowLimit" SwitchToSingleColumnAtWindowInnerWidth="1000" />
                                                        </EditFormLayoutProperties>
                                                       <ClientSideEvents Init="AdjustHeight" />
                                                        <Columns>
                                                            <dx:GridViewDataDateColumn FieldName="statusDate" Caption="Return Date" AdaptivePriority="2"></dx:GridViewDataDateColumn>
                                                            <dx:GridViewDataColumn FieldName="fullName" Caption="Returned By" AdaptivePriority="1"></dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn FieldName="returnReason" Caption="Return Reason" AdaptivePriority="3"></dx:GridViewDataColumn>
                                                        </Columns>
                                                    </dx:ASPxGridView>
                                                    <asp:SqlDataSource ID="dsReturnData" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>"
                                                        SelectCommand="usp_getReturnTracking" SelectCommandType="StoredProcedure">
                                                        <SelectParameters>
                                                            <asp:QueryStringParameter Name="contractId" QueryStringField="id" />
                                                        </SelectParameters>
                                                    </asp:SqlDataSource>
                                                </dx:PanelContent>
                                            </PanelCollection>
                                        </dx:ASPxRoundPanel>
                                    </dx:ContentControl>
                                </ContentCollection>
                            </dx:TabPage>
                            <dx:TabPage Name="Return" Text="Reject/<br/>Restore<br/>Reason">
                                <ContentCollection>
                                    <dx:ContentControl>
                                        <dx:ASPxRoundPanel ID="ASPxRoundPanel2" runat="server" ShowHeader="false" BorderLeft-BorderStyle="Solid" BorderLeft-BorderWidth="1px" BorderLeft-BorderColor="LightGray" BorderRight-BorderStyle="Solid" BorderRight-BorderWidth="1px" BorderRight-BorderColor="LightGray">
                                            <PanelCollection>
                                                <dx:PanelContent>
                                                    <dx:ASPxGridView ID="gvReject" runat="server" DataSourceID="dsReject">
                                                        <SettingsAdaptivity AdaptivityMode="HideDataCells" AllowOnlyOneAdaptiveDetailExpanded="true"
                                                            AllowHideDataCellsByColumnMinWidth="true"></SettingsAdaptivity>
                                                       <SettingsBehavior AllowEllipsisInText="false"/>
                                                        <SettingsResizing ColumnResizeMode="Control" Visualization="Live" />
                                                        <EditFormLayoutProperties>
                                                            <SettingsAdaptivity AdaptivityMode="SingleColumnWindowLimit" SwitchToSingleColumnAtWindowInnerWidth="1000" />
                                                        </EditFormLayoutProperties>
                                                       <ClientSideEvents Init="AdjustHeight" />
                                                        <Columns>
                                                            <dx:GridViewDataDateColumn FieldName="statusDate" Caption="Date" AdaptivePriority="2"></dx:GridViewDataDateColumn>
                                                            <dx:GridViewDataColumn FieldName="fullName" Caption="User" AdaptivePriority="1"></dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn FieldName="action" Caption="Action" AdaptivePriority="2"></dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn FieldName="reason" Caption="Reason" AdaptivePriority="3"></dx:GridViewDataColumn>
                                                        </Columns>
                                                    </dx:ASPxGridView>
                                                    <asp:SqlDataSource ID="dsReject" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>"
                                                        SelectCommand="usp_getRejectReason" SelectCommandType="StoredProcedure">
                                                        <SelectParameters>
                                                            <asp:QueryStringParameter Name="contractId" QueryStringField="id" />
                                                        </SelectParameters>
                                                    </asp:SqlDataSource>
                                                </dx:PanelContent>
                                            </PanelCollection>
                                        </dx:ASPxRoundPanel>
                                    </dx:ContentControl>
                                </ContentCollection>
                            </dx:TabPage>
                            <dx:TabPage Name="OIG" Text="OIG<br/>Tracking">
                                <ContentCollection>
                                    <dx:ContentControl>
                                        <dx:ASPxRoundPanel ID="pnlOIG" runat="server" ShowHeader="false" BorderLeft-BorderStyle="Solid" BorderLeft-BorderWidth="1px" BorderLeft-BorderColor="LightGray" BorderRight-BorderStyle="Solid" BorderRight-BorderWidth="1px" BorderRight-BorderColor="LightGray">
                                            <PanelCollection>
                                                <dx:PanelContent>
                                                    <dx:ASPxGridView ID="gvOIG" runat="server" DataSourceID="dsOIG">
                                                        <SettingsAdaptivity AdaptivityMode="HideDataCells" AllowOnlyOneAdaptiveDetailExpanded="true"
                                                            AllowHideDataCellsByColumnMinWidth="true"></SettingsAdaptivity>
                                                       <SettingsBehavior AllowEllipsisInText="true"/>
                                                        <SettingsResizing ColumnResizeMode="Control" Visualization="Live" />
                                                        <EditFormLayoutProperties>
                                                            <SettingsAdaptivity AdaptivityMode="SingleColumnWindowLimit" SwitchToSingleColumnAtWindowInnerWidth="1000" />
                                                        </EditFormLayoutProperties>
                                                       <ClientSideEvents Init="AdjustHeight" />
                                                        <Columns>
                                                            <dx:GridViewDataDateColumn FieldName="searchDate" Caption="Date" AdaptivePriority="2"></dx:GridViewDataDateColumn>
                                                            <dx:GridViewDataColumn FieldName="searchTerm" Caption="Party Searched" AdaptivePriority="1"></dx:GridViewDataColumn>
                                                            <dx:BootstrapGridViewDataColumn FieldName="searchResults" Caption="Hit Count" AdaptivePriority="1"></dx:BootstrapGridViewDataColumn>
                                                            <dx:GridViewDataCheckColumn FieldName="isAdded" Caption="Added Party" AdaptivePriority="2"></dx:GridViewDataCheckColumn>
                                                        </Columns>
                                                    </dx:ASPxGridView>
                                                    <asp:SqlDataSource ID="dsOIG" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>"
                                                        SelectCommand="usp_getOIGTracking" SelectCommandType="StoredProcedure">
                                                        <SelectParameters>
                                                            <asp:QueryStringParameter Name="contractId" QueryStringField="id" />
                                                        </SelectParameters>
                                                    </asp:SqlDataSource>
                                                </dx:PanelContent>
                                            </PanelCollection>
                                        </dx:ASPxRoundPanel>
                                    </dx:ContentControl>
                                </ContentCollection>
                            </dx:TabPage>
                            <dx:TabPage Name="ContractData" Text="Additional<br/>Data">
                                <ContentCollection>
                                    <dx:ContentControl>
                                        <dx:ASPxRoundPanel ID="pnlContractData" runat="server" ShowHeader="false" BorderLeft-BorderStyle="Solid" BorderLeft-BorderWidth="1px" BorderLeft-BorderColor="LightGray" BorderRight-BorderStyle="Solid" BorderRight-BorderWidth="1px" BorderRight-BorderColor="LightGray">
                                            <PanelCollection>
                                                <dx:PanelContent runat="server">
                                                    <dx:ASPxGridView ID="gvContractData" ClientInstanceName="gvContractData" runat="server" DataSourceID="dsContractData" KeyFieldName="contractDataId">
                                                        <SettingsAdaptivity AdaptivityMode="HideDataCells" AllowOnlyOneAdaptiveDetailExpanded="true"
                                                            AllowHideDataCellsByColumnMinWidth="true"></SettingsAdaptivity>
                                                       <SettingsBehavior AllowEllipsisInText="true"/>
                                                        <EditFormLayoutProperties>
                                                            <SettingsAdaptivity AdaptivityMode="SingleColumnWindowLimit" SwitchToSingleColumnAtWindowInnerWidth="1000" />
                                                        </EditFormLayoutProperties>
                                                       <ClientSideEvents Init="AdjustHeight" />
                                                        <Columns>
                                                            <dx:GridViewCommandColumn ShowEditButton="true" CellStyle-HorizontalAlign="Left" Width="20%">
                                                                <HeaderTemplate>
                                                                    <a href="javascript:gvContractData.AddNewRow()" style="text-decoration:underline;">New</a>
                                                                </HeaderTemplate>
                                                            </dx:GridViewCommandColumn>
                                                            <dx:GridViewDataColumn FieldName="dataType" Caption="Type" AdaptivePriority="1"></dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn FieldName="dataValue" Caption="Value" AdaptivePriority="2"></dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn FieldName="contractDataId" Visible="false"></dx:GridViewDataColumn>
                                                            <dx:GridViewCommandColumn Caption=" " ShowDeleteButton="true" AdaptivePriority="1"></dx:GridViewCommandColumn>
                                                        </Columns>
                                                        <SettingsDataSecurity AllowDelete="true" AllowInsert="true" AllowEdit="true" />
                                                        <SettingsBehavior ConfirmDelete="true" />
                                                        <SettingsResizing ColumnResizeMode="Control" Visualization="Live" />
                                                        <SettingsText ConfirmDelete="Are you sure you want to delete this line item?" PopupEditFormCaption="Additional Data" />
                                                        <SettingsEditing Mode="PopupEditForm" UseFormLayout="true" EditFormColumnCount="1"></SettingsEditing>
                                                        <Styles Header-Font-Bold="true"></Styles>
                                                        <SettingsPopup EditForm-HorizontalAlign="LeftSides"></SettingsPopup>
                                                    </dx:ASPxGridView>
                                                    <asp:SqlDataSource ID="dsContractData" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>"
                                                        SelectCommand="usp_getAdditionalData" SelectCommandType="StoredProcedure"
                                                        InsertCommand="usp_addAdditionalData" InsertCommandType="StoredProcedure"
                                                        DeleteCommand="usp_deleteAdditionalData" DeleteCommandType="StoredProcedure"
                                                        UpdateCommand="usp_updateAdditionalData" UpdateCommandType="StoredProcedure">
                                                        <SelectParameters>
                                                            <asp:QueryStringParameter Name="contractId" QueryStringField="id" />
                                                        </SelectParameters>
                                                        <InsertParameters>
                                                            <asp:QueryStringParameter Name="contractId" QueryStringField="id" />
                                                            <asp:Parameter Name="dataType" />
                                                            <asp:Parameter Name="dataValue" />
                                                        </InsertParameters>
                                                        <DeleteParameters>
                                                            <asp:Parameter Name="contractDataId" />
                                                        </DeleteParameters>
                                                        <UpdateParameters>
                                                            <asp:Parameter Name="dataType" />
                                                            <asp:Parameter Name="dataValue" />
                                                            <asp:Parameter Name="contractDataId" />
                                                        </UpdateParameters>
                                                    </asp:SqlDataSource>
                                                </dx:PanelContent>
                                            </PanelCollection>
                                        </dx:ASPxRoundPanel>
                                    </dx:ContentControl>
                                </ContentCollection>
                            </dx:TabPage>
                            <dx:TabPage Name="AnnualReview" Text="Annual<br/>Review">
                                <ContentCollection>
                                    <dx:ContentControl>
                                        <dx:ASPxRoundPanel ID="pnlAnnualReview" runat="server" ShowHeader="false" BorderLeft-BorderStyle="Solid" BorderLeft-BorderWidth="1px" BorderLeft-BorderColor="LightGray" BorderRight-BorderStyle="Solid" BorderRight-BorderWidth="1px" BorderRight-BorderColor="LightGray">
                                            <PanelCollection>
                                                <dx:PanelContent>
                                                    <dx:ASPxGridView ID="gvAnnualReview" runat="server" DataSourceID="dsAnnualReview" KeyFieldName="contractId;reviewByDate">
                                                        <SettingsAdaptivity AdaptivityMode="HideDataCells" AllowOnlyOneAdaptiveDetailExpanded="true"
                                                            AllowHideDataCellsByColumnMinWidth="true"></SettingsAdaptivity>
                                                       <SettingsBehavior AllowEllipsisInText="true"/>
                                                        <SettingsResizing ColumnResizeMode="Control" Visualization="Live" />
                                                        <EditFormLayoutProperties>
                                                            <SettingsAdaptivity AdaptivityMode="SingleColumnWindowLimit" SwitchToSingleColumnAtWindowInnerWidth="1000" />
                                                        </EditFormLayoutProperties>
                                                       <ClientSideEvents Init="AdjustHeight" />
                                                        <Columns>
                                                            <dx:GridViewDataDateColumn FieldName="reviewByDate" Caption="Review By Date" EditFormSettings-Visible="False"></dx:GridViewDataDateColumn> 
                                                            <dx:GridViewDataDateColumn FieldName="reviewDate" Caption="Review Date" EditFormSettings-Visible="False"></dx:GridViewDataDateColumn>
                                                            <dx:GridViewDataTextColumn FieldName="reviewer" Caption="Reviewer" EditFormSettings-Visible="False"></dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataComboBoxColumn FieldName="isApproved" Caption="Approved?">
                                                                <PropertiesComboBox>
                                                                    <Items>
                                                                        <dx:ListEditItem Text="YES" Value="True" />
                                                                        <dx:ListEditItem Text="NO" Value="False" />
                                                                    </Items>
                                                                </PropertiesComboBox>
                                                            </dx:GridViewDataComboBoxColumn>
                                                            <dx:GridViewCommandColumn ShowEditButton="true" ButtonRenderMode="Link" Caption="Approve/Deny" Name="approve"></dx:GridViewCommandColumn>
                                                            <dx:GridViewDataColumn FieldName="contractId" Visible="false"></dx:GridViewDataColumn>
                                                        </Columns>
                                                        <SettingsDataSecurity AllowEdit="true" />
                                                        <SettingsCommandButton EditButton-Text="Review"></SettingsCommandButton>
                                                    </dx:ASPxGridView>
                                                    <asp:SqlDataSource ID="dsAnnualReview" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>"
                                                        SelectCommand="usp_getContractReviewTracking" SelectCommandType="StoredProcedure"
                                                        UpdateCommand="usp_updateContractReviewTracking" UpdateCommandType="StoredProcedure" OnUpdating="dsAnnualReview_Updating">
                                                        <SelectParameters>
                                                            <asp:QueryStringParameter Name="contractId" QueryStringField="id" />
                                                        </SelectParameters>
                                                        <UpdateParameters>
                                                            <asp:QueryStringParameter Name="contractId" QueryStringField="id" />
                                                            <asp:SessionParameter SessionField="userID" Name="userId" />
                                                            <asp:Parameter Name="isApproved" />
                                                            <asp:Parameter Name="reviewByDate" />
                                                        </UpdateParameters>
                                                    </asp:SqlDataSource>
                                                    <br /><br />
                                                    <div style="text-align:center">
                                                        <dx:ASPxButton ID="btnOIG" runat="server" Text="OIG Check" OnClick="btnOIG_Click" CausesValidation="false"></dx:ASPxButton>
                                                    </div>
                                                </dx:PanelContent>
                                            </PanelCollection>
                                        </dx:ASPxRoundPanel>
                                    </dx:ContentControl>
                                </ContentCollection>
                            </dx:TabPage>
                            <dx:TabPage Name="Routing" Text="Routing">
                                <ContentCollection>
                                    <dx:ContentControl>
                                        <dx:ASPxRoundPanel ID="pnlRouting" runat="server" ShowHeader="false" BorderLeft-BorderStyle="Solid" BorderLeft-BorderWidth="1px" BorderLeft-BorderColor="LightGray" BorderRight-BorderStyle="Solid" BorderRight-BorderWidth="1px" BorderRight-BorderColor="LightGray">
                                            <PanelCollection>
                                                <dx:PanelContent>
                                                    <dx:ASPxCallbackPanel ID="cbRouting" ClientInstanceName="cbRouting" runat="server">
                                                        <PanelCollection>
                                                            <dx:PanelContent>
                                                                <script type="text/javascript">
                                                                    function OnEndCallBack(s, e) {
                                                                        if (s.cpIsUpdated != '') {
                                                                            if (s.cpIsUpdated.includes('bypassed')) {
                                                                                alert(s.cpIsUpdated);
                                                                            }
                                                                            //alert('yo');
                                                                            //gvRouting.Refresh();
                                                                            gvBypass.Refresh();
                                                                        }
                                                                        if (s.cpIsDeleted != '') {
                                                                            cbContract.PerformCallback();
                                                                        }
                                                                    }
                                                                    function AdjustHeightRouting(s, e) {
                                                                        gvRouting.SetHeight(window.innerHeight * .75);
                                                                        gvRouting.SetWidth(window.innerWidth * .35);
                                                                    }
                                                                </script>
                                                                <dx:ASPxGridView ID="gvRouting" ClientInstanceName="gvRouting" runat="server" DataSourceID="dsContractRoute" KeyFieldName="row_num" EditFormLayoutProperties-EncodeHtml="false" 
                                                                    OnHtmlDataCellPrepared="gvRouting_HtmlDataCellPrepared" OnCommandButtonInitialize="gvRouting_CommandButtonInitialize" OnCancelRowEditing="gvRouting_CancelRowEditing" OnRowUpdated="gvRouting_RowUpdated" OnRowDeleted="gvRouting_RowDeleted"><%--ClientSideEvents-EndCallback="function (s, e) { cbRouting.PerformCallback(); }"--%>
                                                                    <ClientSideEvents EndCallback="OnEndCallBack" />
                                                                    <SettingsAdaptivity AdaptivityMode="HideDataCells" AllowOnlyOneAdaptiveDetailExpanded="true"
                                                                        AllowHideDataCellsByColumnMinWidth="true"></SettingsAdaptivity>
                                                                   <SettingsBehavior AllowEllipsisInText="true"/>
                                                                    <EditFormLayoutProperties>
                                                                        <SettingsAdaptivity AdaptivityMode="SingleColumnWindowLimit" SwitchToSingleColumnAtWindowInnerWidth="1000" />
                                                                    </EditFormLayoutProperties>
                                                                    <ClientSideEvents Init="AdjustHeightRouting" />
                                                                    <Columns>
                                                                        <dx:GridViewDataColumn FieldName="roleId" Visible="false"></dx:GridViewDataColumn>
                                                                        <dx:GridViewDataColumn FieldName="roleName" Caption="Role" HeaderStyle-VerticalAlign="Bottom" AdaptivePriority="1">
                                                                            <EditFormSettings Visible="False" />
                                                                        </dx:GridViewDataColumn>
                                                                        <dx:GridViewDataColumn FieldName="approver" Caption="Approver" HeaderStyle-VerticalAlign="Bottom" AdaptivePriority="3">
                                                                            <EditFormSettings Visible="False" />
                                                                        </dx:GridViewDataColumn>
                                                                        <dx:GridViewDataColumn FieldName="statusDate" Caption="Approval<br/>Date" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Bottom" AdaptivePriority="1">
                                                                            <EditFormSettings Visible="False" />
                                                                        </dx:GridViewDataColumn>
                                                                        <dx:GridViewDataCheckColumn FieldName="isApproved" Caption="Is<br/>Approved" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Bottom" AdaptivePriority="1">
                                                                            <EditFormSettings Visible="False" />
                                                                        </dx:GridViewDataCheckColumn>
                                                                        <dx:GridViewDataColumn FieldName="approvedBy" Caption="Processed<br/>By" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Bottom" AdaptivePriority="2">
                                                                            <EditFormSettings Visible="False" />
                                                                        </dx:GridViewDataColumn>
                                                                        <dx:GridViewDataColumn FieldName="routingSequence" Visible="false">
                                                                            <EditFormSettings Visible="False" />
                                                                        </dx:GridViewDataColumn>
                                                                        <dx:GridViewCommandColumn ShowEditButton="true" Caption="Bypass" ButtonRenderMode="Link" HeaderStyle-VerticalAlign="Bottom" ShowDeleteButton="true"></dx:GridViewCommandColumn>
                                                                        <dx:GridViewDataColumn FieldName="isBypass" Visible="false"></dx:GridViewDataColumn>
                                                                        <dx:GridViewDataColumn FieldName="byPassReason" Caption="Bypass<br/>Reason" Visible="false" HeaderStyle-VerticalAlign="Bottom">
                                                                            <EditFormSettings Visible="True" />
                                                                        </dx:GridViewDataColumn>
                                                                        <dx:GridViewDataColumn FieldName="row_num" Visible="false"></dx:GridViewDataColumn>
                                                                        <dx:GridViewDataColumn FieldName="canByPass" Visible="false"></dx:GridViewDataColumn>
                                                                    </Columns>
                                                                    <%--<Templates>
                                                                        <EditForm>
                                                                            <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors" runat="server"></dx:ASPxGridViewTemplateReplacement>
                                                                            <br />
                                                                            <div style="text-align: right; padding: 2px">
                                                                                <dx:ASPxButton ID="btnRoutingCancel" runat="server" Text="Cancel">
                                                                                    <ClientSideEvents Click="function (s, e) { gvRouting.CancelEdit(); }" />
                                                                                </dx:ASPxButton>
                                                                                &nbsp;&nbsp;&nbsp;
                                                                                <dx:ASPxButton ID="btnRoutingUpdate" runat="server" Text="Update">
                                                                                    <ClientSideEvents Click="function (s, e) { gvRouting.UpdateEdit(); }" />
                                                                                </dx:ASPxButton>
                                                                            </div>
                                                                        </EditForm>
                                                                    </Templates>--%>
                                                                    <FormatConditions>
                                                                        <dx:GridViewFormatConditionHighlight ApplyToRow="true" Expression="isBypass=1" RowStyle-BackColor="LightGray" Format="Custom"></dx:GridViewFormatConditionHighlight>
                                                                    </FormatConditions>
                                                                    <SettingsBehavior AllowSort="false" />
                                                                    <SettingsResizing ColumnResizeMode="Control" Visualization="Live" />
                                                                    <SettingsDataSecurity AllowEdit="true" AllowDelete="true" />
                                                                    <%--<SettingsText PopupEditFormCaption="Enter Bypass Reason" />--%>
                                                                    <SettingsCommandButton EditButton-Text="Bypass" EditButton-Styles-Style-HorizontalAlign="Left" CancelButton-Styles-Style-HorizontalAlign="Left"></SettingsCommandButton>
                                                                    <SettingsEditing Mode="PopupEditForm" UseFormLayout="true"></SettingsEditing>
                                                                    <SettingsPager Mode="ShowAllRecords"></SettingsPager>
                                                                </dx:ASPxGridView>
                                                                <asp:SqlDataSource ID="dsContractRoute" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>"
                                                                    SelectCommand="usp_getAllRoutingByContractId" SelectCommandType="StoredProcedure"
                                                                    UpdateCommand="usp_updateContractStatus" UpdateCommandType="StoredProcedure" OnUpdating="dsContractRoute_Updating"
                                                                    DeleteCommand="usp_deleteContractStatus" DeleteCommandType="StoredProcedure" OnDeleting="dsContractRoute_Deleting">
                                                                    <UpdateParameters>
                                                                        <asp:QueryStringParameter Name="contractId" QueryStringField="id" />
                                                                        <asp:Parameter Name="approvalStatus" Type="Boolean" DefaultValue="True" />
                                                                        <asp:SessionParameter Name="userId" SessionField="UserID" />
                                                                        <asp:Parameter Name="routingSequence" />
                                                                        <asp:SessionParameter Name="bypassUserId" SessionField="UserID" />
                                                                        <asp:Parameter Name="isBypass" Type="Boolean" />
                                                                        <asp:Parameter Name="byPassReason" />
                                                                    </UpdateParameters>
                                                                    <SelectParameters>
                                                                        <asp:QueryStringParameter Name="contractId" QueryStringField="id" />
                                                                        <asp:SessionParameter Name="username" SessionField="UserID" />
                                                                    </SelectParameters>
                                                                    <DeleteParameters>
                                                                        <asp:QueryStringParameter Name="contractId" QueryStringField="id" />
                                                                        <asp:Parameter Name="routingSequence" />
                                                                        <asp:Parameter Name="statusDate" />
                                                                    </DeleteParameters>
                                                                </asp:SqlDataSource>
                                                            </dx:PanelContent>
                                                        </PanelCollection>
                                                    </dx:ASPxCallbackPanel>
                                                </dx:PanelContent>
                                            </PanelCollection>
                                        </dx:ASPxRoundPanel>
                                    </dx:ContentControl>
                                </ContentCollection>
                            </dx:TabPage>
                        </TabPages>
                    </dx:ASPxPageControl>
                    <div style="width:100%; top: 80%; height: 20%; position:relative; display:inline-block; overflow:auto; vertical-align:top; text-align:center;">
                        <dx:ASPxButton ID="btnDocusignBulk" ClientInstanceName="btnDocusignBulk" runat="server" Text="Send Signature Email" ClientVisible="false" OnClick="btnDocusignBulk_Click" CausesValidation="false">
                            <%--<ClientSideEvents Click="function(s,e){gvDocuments.PerformCallback();}" />--%>
                        </dx:ASPxButton><%----%>
                        &nbsp;&nbsp;&nbsp;
                        <dx:ASPxButton ID="btnGetDocusign" ClientInstanceName="btnGetDocusign" runat="server" Text="Get Signed Documents" ClientVisible="false" OnClick="btnGetDocusign_Click" CausesValidation="false"></dx:ASPxButton>
                        &nbsp;&nbsp;&nbsp;
                        <dx:ASPxButton ID="btnEmailRequestor" ClientInstanceName="btnEmailRequestor" runat="server" Text="Email Requestor" ClientVisible="false" OnClick="btnEmailRequestor_Click" ToolTip="Send fully executed contract to requestor." CausesValidation="false"></dx:ASPxButton>
                    </div>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxCallbackPanel>
    </div>
    <dx:ASPxPopupControl ID="popReturn" ClientInstanceName="popReturn" runat="server" ShowHeader="false" ShowCloseButton="true">
        <SettingsAdaptivity Mode="Always" MaxWidth="600px" VerticalAlign="WindowCenter" />
        <ContentCollection>
                <dx:PopupControlContentControl>
                <dx:ASPxRoundPanel ID="pnlReturn" runat="server" HeaderText="Return Contract to User" Width="100%">
                    <PanelCollection>
                        <dx:PanelContent>
                            <div id="divReturn" runat="server" style="display:flex; flex-direction:row;width:100%;">
                                <dx:ASPxLabel ID="lblReturn" runat="server" Text="Return to the Following User:"></dx:ASPxLabel>
                                &nbsp;&nbsp;&nbsp;
                                <dx:ASPxComboBox ID="ddlReturn" ClientInstanceName="ddlReturn" runat="server" DataSourceID="dsReturn" TextField="roleName" ValueField="routingSequence">
                                    <%--<ValidationSettings Display="Static" ErrorDisplayMode="Text" RequiredField-IsRequired="true" RequiredField-ErrorText="Return to User is Required" ValidationGroup="ReturnValidation"></ValidationSettings>--%>
                                </dx:ASPxComboBox>
                                <asp:SqlDataSource ID="dsReturn" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                                        SelectCommand="usp_getReturnRoles" SelectCommandType="StoredProcedure">
                                    <SelectParameters>
                                        <asp:QueryStringParameter Name="contractId" QueryStringField="id" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </div>
                            <br /><br />
                            <div id="divReason" runat="server" style="width:100%;">
                                <dx:ASPxLabel ID="lblReason" runat="server" Text="Reason for Returning:"></dx:ASPxLabel>
                                <br /><br />
                                <dx:ASPxMemo ID="memoReason" runat="server" Width="100%" Height="100"></dx:ASPxMemo>
                            </div>
                            <br /><br />
                            <div style="text-align:center;width:100%;">
                                <dx:ASPxButton ID="btnReturnCancel" runat="server" Text="Cancel" AutoPostBack="false">
                                    <ClientSideEvents Click="function(s,e){popReturn.Hide();}" />
                                </dx:ASPxButton>
                                &nbsp;&nbsp;&nbsp;
                                <dx:ASPxButton ID="btnReturnUser" runat="server" Text="Return" AutoPostBack="true" OnClick="btnReturnUser_Click" CausesValidation="false">
                                    <ClientSideEvents Click="function(s, e) {if(ddlReturn.GetValue() != null && ddlReturn.GetValue() >= 0){popReturn.Hide();} else { e.processOnServer = false; alert('You must select the return user for the contract.');}}" />
                                </dx:ASPxButton>
                            </div>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxRoundPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <dx:ASPxPopupControl ID="popReject" ClientInstanceName="popReject" runat="server" Width="600px" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ShowHeader="false" ShowCloseButton="true" Modal="true" AllowDragging="true" AllowResize="true">
        <ContentCollection>
                <dx:PopupControlContentControl>
                <dx:ASPxRoundPanel ID="pnlReject" runat="server" HeaderText="Reject Contract" Width="100%">
                    <PanelCollection>
                        <dx:PanelContent>
                            <div id="div1" runat="server" style="width:100%;"><%--z-index:999;position:relative;left:0px;top:0px;--%>
                                <dx:ASPxLabel ID="lblReject" runat="server" Text="Reason for Rejecting:"></dx:ASPxLabel>
                                <br /><br />
                                <dx:ASPxMemo ID="memoRejectReason" runat="server" Width="100%" Height="100"></dx:ASPxMemo>
                            </div>
                            <br /><br />
                            <div style="text-align:center;width:100%;">
                                <dx:ASPxButton ID="btnCancelReject" runat="server" Text="Cancel" AutoPostBack="false">
                                    <ClientSideEvents Click="function(s,e){popReject.Hide();}" />
                                </dx:ASPxButton>
                                &nbsp;&nbsp;&nbsp;
                                <dx:ASPxButton ID="btnRejectReason" runat="server" Text="Reject" AutoPostBack="true" OnClick="btnRejectReason_Click"></dx:ASPxButton>
                            </div>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxRoundPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <dx:ASPxPopupControl ID="popExclusions" ClientInstanceName="popExclusions" runat="server" Modal="true" HeaderText="" Left="75" Top="100" ShowCloseButton="true" AllowDragging="true" AllowResize="true">
        <SettingsAdaptivity Mode="Always" VerticalAlign="WindowBottom" HorizontalAlign="WindowCenter" MinHeight="80%" MinWidth="90%" />
        <ContentCollection>
            <dx:PopupControlContentControl>
                <div class="flex">
                    <dx:ASPxLabel id="lblBusinessName" runat="server" Text="Business Name:"></dx:ASPxLabel>
                    &nbsp;&nbsp;&nbsp;
                    <dx:ASPxTextBox ID="txtBusinessName" ClientInstanceName="txtBusinessName" runat="server" ValidationSettings-RequiredField-IsRequired="true" Width="200" ValidationSettings-RequiredField-ErrorText="Business Name is required." ValidationSettings-ValidationGroup="BusinessNameValidatoin"></dx:ASPxTextBox>
                    &nbsp;&nbsp;&nbsp;
                    <dx:ASPxCheckBox ID="cbFuzzy" runat="server" Checked="false" Text="Include Fuzzy Search Results?"></dx:ASPxCheckBox>
                    &nbsp;&nbsp;&nbsp;
                    <dx:ASPxButton ID="btnSearchExclusions" runat="server" Text="Search Exclusions" OnClick="btnSearchExclusions_Click"></dx:ASPxButton>
                    <br /><br />
                </div>
                <div id="divExclusionResults" runat="server" visible="false" style="width:100%;">
                    <dx:ASPxLabel ID="lblExclusions" ClientInstanceName="lblExclusions" runat="server" Text="The party/vendor entered matches one or more on the exclusions list.<br/>If you are sure your party/vendor is not the same as one listed below, click Add Party below." Font-Bold="true" ForeColor="Red" Font-Size="Small" EncodeHtml="false"></dx:ASPxLabel>
                    <br /><br />
                    <dx:ASPxGridView ID="gvExclusions" runat="server" ClientInstanceName="gvExclusions" Width="100%">
                        <Columns>
                            <dx:GridViewDataColumn FieldName="Classification" Caption="Classification"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="EntityName" Caption="Entity Name"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="FirstName" Caption="First Name"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="MiddleInitial" Caption="Middle Initial"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="LastName" Caption="Last Name"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="Address" Caption="Address"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="City" Caption="City"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="State" Caption="State"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="ZipCode" Caption="Zip Code"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="ExclusionType" Caption="Exclusion Type"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="ActiveDate" Caption="Active Date"></dx:GridViewDataColumn>
                        </Columns>
                        <Settings ShowHeaderFilterButton="true"  />
                        <SettingsPager Mode="ShowPager" PageSize="10"></SettingsPager>
                    </dx:ASPxGridView>
                    <br /><br />
                    <div style="width:100%;text-align:left;">
                        <dx:ASPxButton ID="btnExclusionCancel" runat="server" Text="Cancel">
                            <ClientSideEvents Click="function(s,e){popExclusions.Hide();}" />
                        </dx:ASPxButton>
                        &nbsp;&nbsp;&nbsp;
                        <dx:ASPxButton ID="btnExclusionSave" runat="server" Text="Add Party" AutoPostBack="true" OnClick="btnExclusionSave_Click"></dx:ASPxButton>
                    </div>
                    <br /><br />
                    <div style="width:100%;text-align:left;">
                        <dx:ASPxLabel ID="lblExclusionNote" ClientInstanceName="lblExclusionNote" runat="server" Font-Bold="true" ForeColor="Red" Text="*Only Add Party if you are sure your party is not the same as one listed above."></dx:ASPxLabel>
                    </div>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <dx:ASPxHiddenField ID="hdnOIGCheck" runat="server"></dx:ASPxHiddenField>
    <dx:ASPxHiddenField ID="hdnTotalCost" runat="server"></dx:ASPxHiddenField>
    <dx:ASPxHiddenField ID="hdnAnnualCost" runat="server"></dx:ASPxHiddenField>
</asp:Content>