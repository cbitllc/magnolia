﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocumentViewer.aspx.cs" Inherits="Magnolia.app.DocumentViewer" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width:100%;height:100%;">
            <% if (strFilename != "")
                {
                if (Request.QueryString["url"].ToString().Contains("pdf"))
                { %>
                    <object id="exPDF" type="application/pdf" data="<% Response.Write(strFilename);%>" style="width:100%;height:100vh">
                        <embed allowfullscreen="true" />
                    </object>
                 <% }
                else
                { %>
                    <iframe src="<%= Request.QueryString["url"] %>" frameborder="0" style="width:100%" height="1000"></iframe>
                <% }
               }%>
        </div>
    </form>
</body>
</html>