﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Magnolia.app
{
    public class DataAccess
    {
        public static string strCon = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["cmsConn"].ConnectionString;

        public static DataSet GetDataSet(string sp_name, string[] ParamNames, string[] ParamValues) {
            using (SqlConnection myConnection = new SqlConnection(strCon))
            {
                using (SqlCommand myCommand = new SqlCommand(sp_name, myConnection))
                {
                    myCommand.CommandType = CommandType.StoredProcedure;
                    myCommand.CommandTimeout = 0;
                    int ctr = 0;
                    string sqlDebug = "exec " + sp_name + " ";
                    if (ParamNames != null)
                    {
                        foreach (string pName in ParamNames)
                        {
                            if (pName != "DUMMY")
                            {
                                myCommand.Parameters.AddWithValue(pName, ParamValues[ctr]);
                                sqlDebug += pName + "='" + ParamValues[ctr] + "'";
                                if (ctr != ParamNames.Length - 1)
                                {
                                    sqlDebug += ",";
                                }
                            }
                            ctr = ctr + 1;
                        }
                    }
                    DataSet dst = new DataSet();
                    try
                    {
                        using (SqlDataAdapter mAdapter = new SqlDataAdapter(myCommand))
                        {
                            myConnection.Open();
                            mAdapter.Fill(dst);
                        }
                    }
                    catch (Exception ex)
                    {
                        Message.Alert(ex.Message);
                        Message.LogError("GetDataSet", "Page_Error", ex.Message, sqlDebug);
                        dst.ExtendedProperties.Add("DataBaseError", ex.Message);
                    }
                    return dst;
                }
            }
        }

        public static DataSet GetDataSet2(string query,string[] ParamNames,string[] ParamValues)
        { 
            using (SqlConnection myConnection = new SqlConnection(strCon))
            {
                using (SqlCommand myCommand = new SqlCommand(query, myConnection))
                {
                    myCommand.CommandType = CommandType.Text;
                    myCommand.CommandTimeout = 0;
                    int ctr = 0;
                    string sqlDebug = query + " ";
                    foreach (string pName in ParamNames)
                    {
                        if (pName != "DUMMY")
                        {
                            myCommand.Parameters.AddWithValue(pName, ParamValues[ctr]);
                            sqlDebug += pName + "='" + ParamValues[ctr] + "'";
                            if (ctr != ParamNames.Length - 1)
                            {
                                sqlDebug += ",";
                            }
                        }
                        ctr += 1;
                    }
                    DataSet ds = new DataSet();
                    try
                    {
                        using (SqlDataAdapter mAdapter = new SqlDataAdapter(myCommand))
                            {
                            myConnection.Open();
                            mAdapter.Fill(ds);
                        }
                    }
                    catch (Exception ex)
                    {
                        Message.Alert(ex.Message);
                        Message.LogError("GetDataSet", "Page_Error", ex.Message, sqlDebug);
                    }
                    return ds;
                }
            }
        }

    }
}