﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.IO;
using DevExpress.Pdf;
using System.Globalization;

namespace Magnolia.app
{
    public class ContractSummarySheet
    {
        public static MemoryStream Form(int ContractId)
        {
            MemoryStream stream = null;
            DataSet ds = DataAccess.GetDataSet("usp_getContractsEnhanced", new string[] { "@contractId", "@isAdmin", "@userFullName" }, new string[] { ContractId.ToString(), HttpContext.Current.Session["isAdmin"].ToString(), HttpContext.Current.Session["UserName"].ToString() });
            if (ds!= null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                try
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    using (PdfDocumentProcessor source = new PdfDocumentProcessor())
                    {
                        source.LoadDocument(HttpContext.Current.Server.MapPath("~/Templates/ContractSummaryInformationSheetV3.pdf"));
                        PdfFormData formData = source.GetFormData();
                        try
                        {
                            formData["Department"].Value = dr["department"].ToString();
                        }
                        catch { }
                        try
                        {
                            formData["Party_Vendor"].Value = dr["businessName"].ToString();
                        }
                        catch { }
                        try
                        {
                            formData["Start_Date"].Value = Convert.ToDateTime(dr["startDate"].ToString()).ToShortDateString();
                        }
                        catch { }
                        try
                        {
                            formData["End_Date"].Value = Convert.ToDateTime(dr["endDate"].ToString()).ToShortDateString();
                        }
                        catch { }
                        try
                        {
                            formData["Contract Length"].Value = dr["contractLength"].ToString() + " YEARS";
                        }
                        catch { }
                        try
                        {
                            if (dr["botApproval"] != null && dr["botApproval"].ToString().ToLower() == "true")
                            {
                                formData["BOT Approval"].Value = Convert.ToDateTime(dr["botApprovalDate"].ToString()).ToShortDateString();
                            }
                            else
                            {
                                formData["BOT Approval"].Value = dr["botApproval"].ToString().ToLower() == "false" ? "NO" : "";
                            }
                        }
                        catch { }
                        try
                        {
                            if (dr["eacApproval"] != null && dr["eacApproval"].ToString().ToLower() == "true")
                            {
                                formData["EAC Approval"].Value = Convert.ToDateTime(dr["eacApprovalDate"].ToString()).ToShortDateString();
                            }
                            else
                            {
                                formData["EAC Approval"].Value = dr["eacApproval"].ToString().ToLower() == "false" ? "NO" : "";
                            }
                        }
                        catch { }
                        try
                        {
                            formData["Contract Type"].Value = dr["contractType"].ToString();
                        }
                        catch { }
                        try
                        {
                            DataSet dsAction = DataAccess.GetDataSet2("select distinct action from actions where actionId = @actionId", new string[] { "@actionId" }, new string[] { dr["actionId"].ToString() });
                            if (dsAction != null && dsAction.Tables.Count > 0 && dsAction.Tables[0].Rows.Count > 0)
                            {
                                formData["Action"].Value = dsAction.Tables[0].Rows[0]["action"].ToString();
                            }
                        }
                        catch { }
                        try
                        {
                            formData["Budgeted"].Value = dr["isBudgeted"].ToString().ToLower() == "true" ? "YES" : "NO";
                        }
                        catch { }
                        try
                        {
                            formData["Budgeted Amount"].Value = string.Format("{0:C2}", Convert.ToDouble(dr["budgetedAmount"]));
                        }
                        catch { }
                         try
                        {
                            formData["Total Cost"].Value = string.Format("{0:C2}", Convert.ToDouble(dr["estimatedTotalCost"]));
                        }
                        catch { }
                        try
                        {
                           formData["Annual Cost"].Value = string.Format("{0:C2}", Convert.ToDouble(dr["annualCost"]));
                        }
                        catch { }
                       try
                        {
                            if (dr["mpsApproval"] != null && dr["mpsApproval"].ToString().ToLower() == "true")
                            {
                                formData["MPS Approval"].Value = Convert.ToDateTime(dr["mpsApprovalDate"].ToString()).ToShortDateString();
                            }
                            else
                            {
                                formData["MPS Approval"].Value = dr["mpsApproval"].ToString().ToLower() == "false" ? "NO" : "";
                            }
                        }
                        catch { }
                        try
                        {
                            formData["Details"].Value = dr["details"];
                        }
                        catch { }
                        DataSet dsRouting = DataAccess.GetDataSet("usp_getAllRoutingByContractId", new string[] { "@contractId", "@username" }, new string[] { ContractId.ToString(), HttpContext.Current.Session["UserID"].ToString() });
                        if (dsRouting!= null && dsRouting.Tables.Count > 0 && dsRouting.Tables[0].Rows.Count > 0)
                        {
                            int i = 1;
                            for (int x = 0; x < dsRouting.Tables[0].Rows.Count; x++)
                            {
                                DataRow row = dsRouting.Tables[0].Rows[x];
                                try
                                {
                                    if (row["isBypass"] != null && row["isBypass"].ToString() == "1")
                                    {
                                        formData["ApproverRow" + i].Value = "BP";
                                    }
                                    else
                                    {
                                        if (row["isReturned"] != null && row["isReturned"].ToString() == "1")
                                        {
                                            formData["ApproverRow" + i].Value = "RETURN";
                                        }
                                        else
                                        {
                                            formData["ApproverRow" + i].Value = row["approvedBy"].ToString(); //approvedBy? approver?
                                        }
                                    }
                                }
                                catch { }
                                try
                                {
                                    formData["Approval DateRow" + i].Value = Convert.ToDateTime(row["statusDate"].ToString()).ToShortDateString();
                                }
                                catch { }
                                try
                                {
                                    formData["Is ApprovedRow" + i].Value = row["isApproved"].ToString().ToLower() == "true" ? "YES" : "NO"; ;
                                }
                                catch { }
                                i++;
                            }
                        }
                        try
                        {
                            source.ApplyFormData(formData);
                            using (MemoryStream mem = new MemoryStream())
                            {
                                source.SaveDocument(mem);
                                stream = mem;
                            }
                        }
                        catch { }
                    }
                }
                catch(Exception ex)
                {
                    Message.LogError("ContractSummarySheet", "Form", HttpContext.Current.Session["UserName"].ToString(), ex.Message);
                }
            }
            return stream;
        }
    }
}