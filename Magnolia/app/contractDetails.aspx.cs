﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using DevExpress.Web;
using System.Data;
using System.Net;
using System.Xml;
using System.Xml.Linq;
using System.Net.Mail;
using System.Data.SqlClient;

namespace Magnolia.app
{
    public class UploadedFilesStorage
    {
        public string Path { get; set; }
        public string Key { get; set; }
        public DateTime LastUsageTime { get; set; }
        public IList<UploadedFileInfo> Files { get; set; }
    }

    public class UploadedFileInfo
    {
        public string UniqueFileName { get; set; }
        public string OriginalFileName { get; set; }
        public string FilePath { get; set; }
        public string FileSize { get; set; }
    }

    public partial class contractDetails : System.Web.UI.Page
    {
        public DataSet dsExclusions;
        bool isRejected = false;
        int byPassCount = 0;

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            UploadControlHelper.RemoveOldStorages();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (gvRouting.JSProperties.Count > 0)
                {
                    if (gvRouting.JSProperties["cpIsUpdated"].ToString() != "")
                    {
                        formContract.DataBind();
                    }
                }
                gvRouting.JSProperties["cpIsUpdated"] = "";
                if (!IsPostBack)
                {
                    Session["party"] = "";
                    Session["SaveType"] = "";
                    Session["referrer"] = "contracts.aspx";
                    hdnAnnualCost.Add("value", "0");
                    hdnTotalCost.Add("value", "0");
                    if (Request.UrlReferrer != null && Request.UrlReferrer.AbsoluteUri.Contains("default"))
                    {
                        Session["referrer"] = "default.aspx";
                    }
                    dsAnnualReview.Select(DataSourceSelectArguments.Empty);
                    gvAnnualReview.DataBind();
                    if (gvAnnualReview.VisibleRowCount == 0)
                    {
                        pcContract.TabPages.FindByName("AnnualReview").Visible = false;

                    }
                    gvAnnualReview.Columns["approve"].Visible = false;
                    btnOIG.Visible = false;
                    dsRelatedContracts.SelectParameters["contractId"].DefaultValue = Request.QueryString["id"];
                    dsRelatedContracts.Select(DataSourceSelectArguments.Empty);
                    ddlContracts.DataBind();
                    dsContract.SelectParameters["contractId"].DefaultValue = Request.QueryString["id"];
                    SubmissionID = UploadControlHelper.GenerateUploadedFilesStorageKey();
                    UploadControlHelper.AddUploadedFilesStorage(SubmissionID);
                    ((Master)Page.Master).SetHeader = "Edit Contract";
                    ((Master)Page.Master).SetHelpURL = "contractDetails.aspx";
                    ListEditItem obj = ddlContracts.Items.FindByValue(Request.QueryString["id"]);
                    if (obj != null)
                    {
                        ddlContracts.SelectedIndex = obj.Index;
                    }
                    if (Request.QueryString["tab"] != null)
                    {
                        pcContract.ActiveTabIndex = pcContract.TabPages.FindByName(Request.QueryString["tab"].ToString()).Index;
                    }
                    else
                    {
                        pcContract.ActiveTabIndex = pcContract.TabPages.FindByName("Routing").Index;
                    }
                    btnDelete.Visible = false;
                    btnApprove.Visible = false;
                    btnReject.Visible = false;
                    btnReturn.Visible = false;
                    btnSave.Visible = false;
                    btnCancel.Visible = false;
                    gvRouting.Columns["Bypass"].Visible = false;
                    if (Convert.ToBoolean(Session["isAdmin"]) == true)
                    {
                        btnDelete.Visible = true;
                        btnSave.Visible = true;
                        btnReturn.Visible = true;
                        btnCancel.Visible = true;
                        gvRouting.Columns["Bypass"].Visible = true;
                        ddlEntryUser.ReadOnly = false;
                        ddlEntryUser.Enabled = true;
                        lblStatus.ReadOnly = false;
                        //btnPDF.Visible = true;
                        FormLayout.FindItemOrGroupByName("ReplaceFile").ClientVisible = true;
                        formContract.FindItemOrGroupByName("contractFolder").ClientVisible = true;
                        formContract.FindItemOrGroupByName("currentRoleName").ClientVisible = true;
                        formContract.FindItemOrGroupByName("currentFullName").ClientVisible = true;
                        //pcContract.TabPages.FindByName("AnnualReview").Visible = true;
                        //gvAnnualReview.Columns["approve"].Visible = true;
                        int i = gvAnnualReview.VisibleRowCount - 1;
                        bool isApproved = false;
                        if(gvAnnualReview.GetRowValues(i, "isApproved") != DBNull.Value)
                        {
                            isApproved = Convert.ToBoolean(gvAnnualReview.GetRowValues(i, "isApproved"));
                            btnOIG.Visible = false;
                            gvAnnualReview.Columns["approve"].Visible = true;
                        }
                        else
                        {
                            btnOIG.Visible = true;
                            gvAnnualReview.Columns["approve"].Visible = false;
                        }
                    }
                    else
                    {
                        FormLayout.FindItemOrGroupByName("ReplaceFile").ClientVisible = false;
                        gvDocuments.Columns["Delete"].Visible = false;
                        btnDocusignBulk.ClientVisible = false;
                        btnGetDocusign.ClientVisible = false;
                        btnEmailRequestor.ClientVisible = false;
                        ddlEntryUser.ReadOnly = true;
                        ddlEntryUser.Enabled = false;
                        lblStatus.ReadOnly = true;
                        gvRouting.DataBind();
                        bool isApprover = false;
                        DataSet dsApprover = DataAccess.GetDataSet("usp_getCurrentRouting", new string[] { "@contractId", "@userName" }, new string[] { Request.QueryString["id"], Session["UserName"].ToString() });
                        if (dsApprover != null && dsApprover.Tables.Count > 0 && dsApprover.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow drApprover in dsApprover.Tables[0].Rows)
                            {
                                if (drApprover["approver"].ToString() == Session["UserName"].ToString())
                                    isApprover = true;
                            }
                        }
                        DataRow dr = null;
                        if (isApprover)
                        {
                            int j = gvAnnualReview.VisibleRowCount - 1;
                            bool isApproved = false;
                            if (gvAnnualReview.GetRowValues(j, "isApproved") != DBNull.Value)
                            {
                                isApproved = Convert.ToBoolean(gvAnnualReview.GetRowValues(j, "isApproved"));
                                btnOIG.Visible = false;
                                gvAnnualReview.Columns["approve"].Visible = true;
                            }
                            else
                            {
                                btnOIG.Visible = true;
                                gvAnnualReview.Columns["approve"].Visible = false;
                            }
                        }
                    }
                    formContract.FindItemOrGroupByName("PendingNotice").ClientVisible = false;
                    formContract.DataBind();
                    if (spinEstimatedTotalCost.Value != null)
                    {
                        hdnTotalCost["value"] = spinEstimatedTotalCost.Value.ToString();
                    }
                    if (spinAnnualCost.Value != null)
                    {
                        hdnAnnualCost["value"] = spinAnnualCost.Value.ToString();
                    }
                    dsAllContracts.SelectParameters["includeSelectedContract"].DefaultValue = "0";
                    System.Data.DataSet ds = DataAccess.GetDataSet("usp_getContractStatus", new string[] { "@contractId" }, new string[] { Request.QueryString["id"] });
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables[0].Rows[0]["statusType"].ToString() == "Denied")
                        {
                            isRejected = true;
                        }
                    }
                    //if (ddlEntryUser.Text.ToUpper() == Session["UserID"].ToString())
                    //{
                    //    //btnSave.Visible = true;
                    //    if (lblStatus.Text == "Pending")
                    //    {
                    //        btnDelete.Visible = true;
                    //    }
                    //}
                    if (isRejected == true)
                    {
                        btnDelete.Visible = false;
                        btnApprove.Visible = false;
                        btnSave.Visible = false;
                        btnReturn.Visible = false;
                        btnCancel.Visible = false;
                        gvRouting.Columns["Bypass"].Visible = false;
                        formContract.FindItemOrGroupByName("DeniedNotice").ClientVisible = true;
                        if (Convert.ToBoolean(Session["isAdmin"]) == true)
                        {
                            btnReject.Visible = true;
                            btnReject.Text = "Restore";
                            btnRejectReason.Text = "Restore";
                            pnlReject.HeaderText = "Restore Contract";
                            lblReject.Text = "Reason for Restoring:";
                        }
                        else
                        {
                            btnReject.Visible = false;
                        }
                    }
                    else
                    {
                        if (lblStatus.Text.ToUpper() == "PENDING")
                        {
                            formContract.FindItemOrGroupByName("PendingNotice").ClientVisible = true;
                            if (Session["role"].ToString().ToUpper().StartsWith("CHIEF") == true || (Convert.ToBoolean(Session["isAdmin"]) == true || lblFullName.Text.ToUpper() == Session["UserName"].ToString().ToUpper() && Session["role"].ToString().ToUpper() != "CONTRACT REQUESTOR"))
                            {
                                btnDelete.Visible = true;
                                btnApprove.Visible = true;
                                btnReject.Visible = true;
                                btnReturn.Visible = true;
                                btnSave.Visible = true;
                                btnCancel.Visible = true;
                            }
                            else
                            {
                                btnDelete.Visible = false;
                                btnApprove.Visible = false;
                                btnReject.Visible = false;
                                btnReturn.Visible = false;
                                btnSave.Visible = false;
                                btnCancel.Visible = false;
                            }
                        }
                        else
                        {
                            int countSignatures = 0;
                            System.Data.DataSet dsStatusCount = DataAccess.GetDataSet("usp_getCountSignatures", new string[] { "@contractId" }, new string[] { Request.QueryString["id"] });
                            if (dsStatusCount != null && dsStatusCount.Tables.Count > 0 && dsStatusCount.Tables[0].Rows.Count > 0)
                            {
                                countSignatures = Convert.ToInt16(dsStatusCount.Tables[0].Rows[0][0]);
                            }
                            if (lblFullName.Text.ToUpper() == Session["UserName"].ToString().ToUpper() || (countSignatures == 0 && ddlEntryUser.Text.ToUpper() == Session["UserName"].ToString().ToUpper()) || (lblRoleName.Text == "Contract Managers" && (Session["role"].ToString() == "Contract Managers") || Convert.ToBoolean(Session["isAdmin"]) == true))
                            {
                                if (lblCanOverride.Text == "1" || lblFullName.Text.ToUpper() == Session["UserName"].ToString().ToUpper() || (lblRoleName.Text == "Contract Managers" && (Session["role"].ToString() == "Contract Managers") || Convert.ToBoolean(Session["isAdmin"]) == true))
                                {
                                    btnReject.Visible = true;
                                    btnApprove.Visible = true;
                                    btnReturn.Visible = true;
                                    btnCancel.Visible = true;
                                    if (lblFullName.Text.ToUpper() != Session["UserName"].ToString().ToUpper() && !(lblRoleName.Text == "Contract Managers" && (Session["role"].ToString() == "Contract Managers") || Convert.ToBoolean(Session["isAdmin"]) == true))
                                    {
                                        btnApprove.Text = "Bypass";
                                    }
                                    else
                                    {
                                        btnApprove.Text = "Approve";
                                    }
                                }
                                if (countSignatures == 0 || lblMinReturned.Text == "1")
                                {
                                    btnSave.Visible = true;
                                    btnDelete.Visible = true;
                                    btnCancel.Visible = true;
                                    gvDocuments.Columns["Delete"].Visible = true;
                                }
                            }
                            if(lblOverdueContracts.Text != "" && Convert.ToInt16(lblOverdueContracts.Text) > 0)
                            {
                                btnCancel.Visible = true;
                            }
                        }
                    }
                    if (lblStatus.Text.ToUpper() == "APPROVED")
                    {
                        btnApprove.Visible = false;
                    }
                }
                if ((IsPostBack || IsCallback) && pcContract.ActiveTabIndex == 1 && Convert.ToBoolean(Session["isAdmin"]) == true)
                {
                    btnDocusignBulk.ClientVisible = true;
                    btnGetDocusign.ClientVisible = true;
                    btnEmailRequestor.ClientVisible = true;
                }
            }
            catch (Exception ex)
            {
                Message.LogError("contractDetails", "Page_Load", Session["UserID"].ToString(), ex.Message + " for contract " + Request.QueryString["id"].ToString());
                Message.Alert(ex.Message);
            }
        }

        protected string SubmissionID
        {
            get
            {
                return HiddenField.Get("SubmissionID").ToString();
            }
            set
            {
                HiddenField.Set("SubmissionID", value);
            }
        }

        UploadedFilesStorage UploadedFilesStorage
        {
            get { return UploadControlHelper.GetUploadedFilesStorageByKey(SubmissionID); }
        }

        protected void cbContract_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            dsContract.SelectParameters["contractId"].DefaultValue = e.Parameter;
            formContract.DataBind();
            if (lblStatus.Text.ToUpper() != "APPROVED" && Convert.ToBoolean(Session["isAdmin"]) == true)
            {
                btnApprove.Visible = true;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["party"] != null && Session["party"].ToString().ToUpper() != ddlParties.Text.ToUpper())
                {
                    SearchExclusions(ddlParties.Text.ToUpper().Replace("(INACTIVE) ",""));
                    DataSet dsExclusions = (DataSet)Session["dsExclusions"];
                    int oigCount = dsExclusions == null || dsExclusions.Tables.Count == 0 ? 0 : dsExclusions.Tables[0].Rows.Count;
                    string isAdded = hdnOIGCheck.Get("check").ToString() == "true" ? "1" : "0";
                    DataSet ds = DataAccess.GetDataSet("usp_logOIG", new string[] { "@contractId", "@searchDate", "@searchTerm", "@searchResults", "@isAdded", "@addedBy" }, new string[] { Request.QueryString["id"].ToString(), DateTime.Now.ToString(), ddlParties.Text.ToUpper(), oigCount.ToString(), isAdded, Session["UserID"].ToString() });
                    gvOIG.DataBind();
                }
                else
                {
                    if (deEndDate.Date > deStartDate.Date.AddYears(5))
                    {
                        Message.Alert("The Contract length cannot be greater than five years. The dates you have entered are greater than five years.");
                    }
                    else
                    {
                        if (Request.QueryString["id"] != null && Request.QueryString["id"] != "0")
                        {
                            dsContract.Update();
                            ddlContracts.DataBind();
                        }
                        else
                        {
                            dsContract.Insert();
                        }
                        if (Session["SaveType"].ToString() != "Autosave")
                        {
                            if (Request.QueryString["from"] != null && Request.QueryString["from"] == "admin")
                            {
                                Message.Alert("Contract has been saved.");
                            }
                            else
                            {
                                string url = "contracts.aspx";
                                if (Session["referrer"] != null && Session["referrer"].ToString() != "")
                                {
                                    url = Session["referrer"].ToString();
                                }
                                Message.AlertAndRedirect("Contract has been saved.", url);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Message.Alert(ex.Message);
                Message.LogError("contractDetails", "btnSave_Click", Session["UserName"].ToString(), ex.Message + " for contract " + Request.QueryString["id"].ToString());
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if ((Convert.ToDouble(spinEstimatedTotalCost.Value) >= 50000 || Convert.ToDouble(spinAnnualCost.Value) >= 50000) && ddlActions.Value.ToString() != "4")
                {
                    string body = "A Contract has been deleted that had an annual or total cost >= $50,000 by " + Session["UserName"] + ".";
                    Message.SendMail("", "", "Contract Deleted for " + ddlParties.Text + " with a High Annual Cost", body, null, "", "Contract Managers");
                }
                dsContract.Delete();
                string url = "contracts.aspx";
                if (Session["referrer"] != null && Session["referrer"].ToString() != "")
                {
                    url = Session["referrer"].ToString();
                }
                Message.AlertAndRedirect("Contract has been deleted.", url);
            }
            catch (Exception ex)
            {
                Message.Alert(ex.Message);
                Message.LogError("contractDetails", "btnDelete_Click", Session["UserName"].ToString(), ex.Message + " for contract " + Request.QueryString["id"].ToString());
            }
        }

        protected void dsContract_Updating(object sender, SqlDataSourceCommandEventArgs e)
        {
            if (Convert.ToDateTime(e.Command.Parameters["@eacApprovalDate"].Value) < Convert.ToDateTime("1/1/1900"))
            {
                e.Command.Parameters["@eacApprovalDate"].Value = null;
                //e.Command.Parameters.Remove(e.Command.Parameters["eacApprovalDate"]);
            }
            if (Convert.ToDateTime(e.Command.Parameters["@botApprovalDate"].Value) < Convert.ToDateTime("1/1/1900"))
            {
                e.Command.Parameters["@botApprovalDate"].Value = null;
                //e.Command.Parameters.Remove(dsUpdateStatus.UpdateParameters["botApprovalDate"]);
            }
            if (Convert.ToDateTime(e.Command.Parameters["@mpsApprovalDate"].Value) < Convert.ToDateTime("1/1/1900"))
            {
                e.Command.Parameters["@mpsApprovalDate"].Value = null;
                //e.Command.Parameters.Remove(dsUpdateStatus.UpdateParameters["mpsApprovalDate"]);
            }
            if (Convert.ToDateTime(e.Command.Parameters["@trusteeApprovalDate"].Value) < Convert.ToDateTime("1/1/1900"))
            {
                e.Command.Parameters["@trusteeApprovalDate"].Value = null;
                //e.Command.Parameters.Remove(dsUpdateStatus.UpdateParameters["mpsApprovalDate"]);
            }
            if (Convert.ToDateTime(e.Command.Parameters["@startDate"].Value) < Convert.ToDateTime("1/1/1900"))
            {
                e.Command.Parameters["@startDate"].Value = null;
            }
            if (Convert.ToDateTime(e.Command.Parameters["@endDate"].Value) < Convert.ToDateTime("1/1/1900"))
            {
                e.Command.Parameters["@endDate"].Value = null;
            }
            if (ddlMasterContracts.Visible == false || e.Command.Parameters["@actionId"].Value.ToString() == "1")
            {
                e.Command.Parameters["@masterContractId"].Value = null;
            }
            string query = GetQuery(e.Command.CommandText, e.Command.Parameters);
            if ( ddlActions.Value.ToString() != "4" && (hdnTotalCost.Count > 0 && hdnTotalCost.Contains("value") && Convert.ToDouble(hdnTotalCost["value"]) < 50000 && Convert.ToDouble(hdnTotalCost["value"]) != Convert.ToDouble(spinEstimatedTotalCost.Value) && Convert.ToDouble(spinEstimatedTotalCost.Value) >= 50000) || (hdnAnnualCost.Count > 0 && hdnAnnualCost.Contains("value") && Convert.ToDouble(hdnAnnualCost["value"]) < 50000 && Convert.ToDouble(hdnAnnualCost["value"]) != Convert.ToDouble(spinAnnualCost.Value) && Convert.ToDouble(spinAnnualCost.Value) >= 50000))
            {
                DataSet ds = DataAccess.GetDataSet("usp_logHighCostTracking", new string[] { "@contractId", "@userId", "@previousTotalCost", "@newTotalCost", "@previousAnnualCost", "@newAnnualCost" },
                    new string[] { Request.QueryString["id"], Session["UserID"].ToString(), hdnTotalCost["value"].ToString(), spinEstimatedTotalCost.Value.ToString(), hdnAnnualCost["value"].ToString(), spinAnnualCost.Value.ToString() });
                string body = "A Contract has been added with an annual or total cost >= $50,000 by " + Session["UserName"] + ".";
                body += "<br/><br/>You can view the current here: <a href='" + Request.Url.AbsoluteUri.Replace("Questionnaire", "Details") + "'>" + Request.Url.AbsoluteUri.Replace("Questionnaire", "Details") + "</a>";
                Message.SendMail("", "", "Contract for " + ddlParties.Text + " has a High Annual Cost", body, null,"", "Contract Managers");
            }
        }

        protected void dsContract_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            bool isAdmin = Convert.ToBoolean(Session["isAdmin"]);
            e.Command.Parameters["@isAdmin"].Value = isAdmin == true ? "1" : "0";

            string query = GetQuery(e.Command.CommandText, e.Command.Parameters);
        }

        protected void ProcessSubmit(List<UploadedFileInfo> fileInfos)
        {
            //DescriptionLabel.Value = Server.HtmlEncode(description);
            foreach (UploadedFileInfo fileInfo in fileInfos)
            {
                // process uploaded files here
                DataSet ds = DataAccess.GetDataSet2("select isnull(c.folder,c.contractId) folder,isnull(contractType,'VENDOR') contractType, c.contractTypeId, t.folder mainFolder from contracts c left join contractTypes t on c.contractTypeId = t.contractTypeId where c.contractId = @contractId", new string[] { "@contractId" }, new string[] { Request.QueryString["id"].ToString() });
                string subFolder = "";
                string contractType = "";
                int contractTypeId = 0;
                string folder = "";
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    subFolder = ds.Tables[0].Rows[0]["folder"].ToString();
                    contractType = ds.Tables[0].Rows[0]["contractType"].ToString();
                    contractTypeId = Convert.ToInt32(ds.Tables[0].Rows[0]["contractTypeId"].ToString());
                    folder = ds.Tables[0].Rows[0]["mainFolder"].ToString();
                }
                if (subFolder == null || subFolder == "" || subFolder == "0")
                {
                    subFolder = Request.QueryString["id"];
                }
                if (gvDocuments.VisibleRowCount > 0)
                {
                    try
                    {
                        subFolder = spinContractFolder.Value.ToString();
                    }
                    catch
                    { }
                }
                if (contractType == "")
                {
                    contractType = ddlContractTypes.Text == null || ddlContractTypes.Text == "" ? "VENDOR" : ddlContractTypes.Text;
                }
                if (contractTypeId == 0)
                {
                    contractTypeId = ddlContractTypes.Value == null || Convert.ToInt32(ddlContractTypes.Value) == 0 ? 1 : Convert.ToInt32(ddlContractTypes.Value);
                }
                //string folder = "";
                //switch (contractTypeId)
                //{
                //    case 7:
                //        folder = "EmployeeEducationAssistanceDB";
                //        break;
                //    case 5:
                //        folder = "LeaseDatabase";
                //        break;
                //    case 6:
                //        folder = "PhysicianContractDatabase";
                //        break;
                //    case 10:
                //        folder = "AdministrativeDatabase";
                //        break;
                //    case 11:
                //        folder = "PHODatabase";
                //        break;
                //    default:
                //        folder = "ContractDatabase";
                //        break;
                //}
                DataSet dsType = DataAccess.GetDataSet2("select * from uploadDocumentTypes where uploadDocumentTypeId = @uploadDocumentTypeId", new string[] { "@uploadDocumentTypeId" }, new string[] { ddlUploadDocumentType.Value.ToString() });
                if (dsType != null && dsType.Tables.Count>0 && dsType.Tables[0].Rows.Count>0)
                {
                    if(dsType.Tables[0].Rows[0]["folder"] != null && dsType.Tables[0].Rows[0]["folder"].ToString() == "PARTY")
                    {
                        folder = "PARTY";
                        subFolder = ddlParties.Value.ToString();
                    }
                }
                string dir = System.Configuration.ConfigurationManager.AppSettings["documentPath"] + folder + "\\" + subFolder;
                
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
                byte[] fileContent = File.ReadAllBytes(fileInfo.FilePath);
                string fileName = fileInfo.OriginalFileName;
                using (var stream = File.Open(fileInfo.FilePath, FileMode.Open))
                {
                    // Use stream
                    //FileStream fc = new FileStream(fileInfo.FilePath, FileMode.OpenOrCreate);
                    if (stream.Length > 0)
                    {
                        bool fileExists = File.Exists(dir + "/" + fileName);
                        string contractIdDupe = "";
                        DataSet dsDupe = DataAccess.GetDataSet2("select c.contractId from documents d join contracts c on d.contractId = c.folder and c.contractTypeId = d.contractTypeId and d.filename = @filename and c.contractId = @contractId", new string[] { "@filename", "@contractId" }, new string[] { fileName, Request.QueryString["id"].ToString() });
                        if (dsDupe != null && dsDupe.Tables.Count > 0 && dsDupe.Tables[0].Rows.Count> 0)
                        {
                            fileExists = true;
                            contractIdDupe = dsDupe.Tables[0].Rows[0]["contractId"].ToString();
                        }
                        if (fileExists == false || (fileExists == true && Convert.ToBoolean(Session["isAdmin"]) == true && cbReplaceFile.Checked))
                        {
                            if (fileExists)
                            {
                                File.Delete(dir + "/" + fileName);
                            }
                            using (var fc = File.Create(dir + "\\" + fileName))
                            {
                                stream.Seek(0, SeekOrigin.Begin);
                                stream.CopyTo(fc);
                            }
                            if (fileExists)
                            {
                                DataSet dsDocId = DataAccess.GetDataSet2("select * from documents where filename = @filename and path = @path", new string[] { "@filename", "@path" },new string[]{ fileName, System.Configuration.ConfigurationManager.AppSettings["documentVirtualPath"] + folder + "/" + subFolder + "/" });
                                string documentId = "";
                                string uploadUserId = "";
                                if (dsDocId != null && dsDocId.Tables.Count>0 && dsDocId.Tables[0].Rows.Count>0 )
                                {
                                    documentId = dsDocId.Tables[0].Rows[0]["documentId"].ToString();
                                    uploadUserId = dsDocId.Tables[0].Rows[0]["uploadId"].ToString();
                                }
                                dsDocuments.UpdateParameters["uploadDocumentTypeId"].DefaultValue = ddlUploadDocumentType.Value.ToString();
                                dsDocuments.UpdateParameters["documentId"].DefaultValue = documentId;
                                dsDocuments.UpdateParameters["uploadId"].DefaultValue = uploadUserId;
                                dsDocuments.UpdateParameters["contractTypeId"].DefaultValue = ddlContractTypes.Value.ToString();
                                dsDocuments.Update();
                                try
                                {
                                    string returnFolder = dsDocuments.UpdateParameters["folder"].DefaultValue;
                                    string returnPrevFolder = dsDocuments.UpdateParameters["prevFolder"].DefaultValue;
                                    if (returnFolder != returnPrevFolder)
                                    {
                                       //Move File 
                                    }
                                }
                                catch(Exception ex)
                                {
                                    Message.LogError("contractDetails", "ProcessSubmit", Session["UserName"].ToString(), ex.Message + " for contract " + Request.QueryString["id"].ToString());
                                }
                            }
                            else
                            {
                                dsDocuments.InsertParameters["fileName"].DefaultValue = fileName;
                                dsDocuments.InsertParameters["path"].DefaultValue = System.Configuration.ConfigurationManager.AppSettings["documentVirtualPath"] + folder + @"\" + subFolder + @"\";
                                dsDocuments.InsertParameters["uploadDocumentTypeId"].DefaultValue = ddlUploadDocumentType.Value.ToString();
                                dsDocuments.InsertParameters["folder"].DefaultValue = subFolder;
                                dsDocuments.InsertParameters["partyId"].DefaultValue = ddlParties.Value.ToString();
                                dsDocuments.InsertParameters["contractTypeId"].DefaultValue = ddlContractTypes.Value.ToString();
                                dsDocuments.Insert();
                            }
                            pcContract.ActiveTabIndex = pcContract.TabPages.FindByName("Documents").Index;
                            if (Convert.ToBoolean(Session["isAdmin"]) == true)
                            {
                                btnDocusignBulk.ClientVisible = true;
                                btnGetDocusign.ClientVisible = true;
                                btnEmailRequestor.ClientVisible = true;
                            }
                        }
                        else
                        {
                            UploadedFilesTokenBox.IsValid = false;
                            UploadedFilesTokenBox.ErrorText = "File already exists.";
                            if (contractIdDupe == "")
                            {
                                Message.Alert("This file already exists for this contract. If this is the correct document for the selected contract, please rename the file and try again.");
                            }
                            else
                            {
                                Message.Alert("This file name already exists in the database for contract " + contractIdDupe + ". If this is the correct document for the selected contract, please rename the file and try again.");
                            }
                        }
                        //fc.Write(fileContent, 0, fileContent.Length);
                        //File.WriteAllBytes(dir, fileContent);
                    }
                }
            }
            //SubmittedFilesListBox.DataSource = fileInfos;
            //SubmittedFilesListBox.DataBind();
            //FormLayout.FindItemOrGroupByName("ResultGroup").Visible = true;
        }

        protected void DocumentsUploadControl_FileUploadComplete(object sender, DevExpress.Web.FileUploadCompleteEventArgs e)
        {
            bool isSubmissionExpired = false;
            if (UploadedFilesStorage == null)
            {
                isSubmissionExpired = true;
                UploadControlHelper.AddUploadedFilesStorage(SubmissionID);
            }
            UploadedFileInfo tempFileInfo = UploadControlHelper.AddUploadedFileInfo(SubmissionID, e.UploadedFile.FileName);
            e.UploadedFile.SaveAs(tempFileInfo.FilePath);
            if (e.IsValid)
                e.CallbackData = tempFileInfo.UniqueFileName + "|" + isSubmissionExpired;
        }

        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            ValidateInputData();
            List<UploadedFileInfo> resultFileInfos = new List<UploadedFileInfo>();
            bool allFilesExist = true;
            if (UploadedFilesStorage == null)
                UploadedFilesTokenBox.Tokens = new TokenCollection();
            foreach (string fileName in UploadedFilesTokenBox.Tokens)
            {
                UploadedFileInfo demoFileInfo = UploadControlHelper.GetDemoFileInfo(SubmissionID, fileName);
                FileInfo fileInfo = new FileInfo(demoFileInfo.FilePath);
                if (fileInfo.Exists)
                {
                    //demoFileInfo.FileSize = Utils.FormatSize(fileInfo.Length);
                    resultFileInfos.Add(demoFileInfo);
                }
                else
                    allFilesExist = false;
            }

            if (allFilesExist && resultFileInfos.Count > 0)
            {
                ProcessSubmit(resultFileInfos);
                cbDocuments_Callback(SubmitButton, new CallbackEventArgs("submit"));
                UploadControlHelper.RemoveUploadedFilesStorage(SubmissionID);
                ASPxEdit.ClearEditorsInContainer(FormLayout, true);
            }
            else
            {
                UploadedFilesTokenBox.ErrorText = "Submit failed because files have been removed from the server due to the 5 minute timeout.";
                UploadedFilesTokenBox.IsValid = false;
            }
        }

        void ValidateInputData()
        {
            bool isInvalid = ddlUploadDocumentType.Value == null || UploadedFilesTokenBox.Tokens.Count == 0;
            if (isInvalid)
                throw new Exception("Document Type and File Name are required.");
        }

        protected void cbDocuments_Callback(object sender, CallbackEventArgsBase e)
        {
            gvDocuments.DataBind();
        }

        protected void dsDocuments_Deleting(object sender, SqlDataSourceCommandEventArgs e)
        {
            Int32 id = Convert.ToInt32(e.Command.Parameters["@documentId"].Value);
            Int32 idx = gvDocuments.FindVisibleIndexByKeyValue(id);
            string[] fields = { "path", "fileName" };
            object[] values = gvDocuments.GetRowValues(idx, fields) as object[];
            if (values != null)
            {
                try
                {
                    //File.Delete(Server.MapPath(values[0] + "/" + values[1]));
                }
                catch(Exception ex)
                {
                    Message.LogError("contractDetails", "dsDocuments_Deleting", Session["UserName"].ToString(), ex.Message + " for contract " + Request.QueryString["id"].ToString());
                }
                DataSet ds = DataAccess.GetDataSet("usp_deleteDocument", new string[] { "@documentId" }, new string[] { id.ToString() });
            }
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            UpdateStatus("Rejected");
            gvRouting.DataBind();
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                if (formContract.FindItemOrGroupByName("PendingNotice").ClientVisible == true)
                {
                    DataSet ds = DataAccess.GetDataSet2("update contracts set statusId=2,statusDate=GetDate(),chiefApprovalId=@userId,chiefApprovalDate=GetDate() where contractId = @contractId", new string[] { "@userId", "@contractId" }, new string[] { Session["UserID"].ToString(), Request.QueryString["id"].ToString() });
                    ds = DataAccess.GetDataSet2("insert into contractStatus (contractId,routingSequence,statusDate,isApproved,userId,isApprovePending) values (@contractId,1,GetDate(),1,@userId,1)", new string[] { "@contractId", "@userId" }, new string[] { Request.QueryString["id"], Session["UserId"].ToString() });
                    string url = "contracts.aspx";
                    if (Session["referrer"] != null && Session["referrer"].ToString() != "")
                    {
                        url = Session["referrer"].ToString();
                    }
                    ds = DataAccess.GetDataSet("usp_getRoutingAlert", new string[] { "@contractId" }, new string[] { Request.QueryString["id"].ToString() });
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        string body = "A Contract has been assigned to you for review.";
                        body += "<br/><br/>You can view the current status here: <a href='" + Request.Url.AbsoluteUri + "'>" + Request.Url.AbsoluteUri + "</a>";
                        Message.SendMail(ds.Tables[0].Rows[0]["userEmail"].ToString(), ds.Tables[0].Rows[0]["userName"].ToString(), "Contract for " + ddlParties.Text + " Action", body, null, "", ds.Tables[0].Rows[0]["userRole"].ToString());
                    }
                    Message.AlertAndRedirect("Contract has been approved.", url);
                }
                else
                {
                    if (btnApprove.Text == "Bypass")
                    {
                        popBypass.ShowOnPageLoad = true;
                        memoBypassReason.Focus();
                    }
                    else
                    {
                        Session["SaveType"] = "Autosave";
                        btnSave_Click(btnSave, new EventArgs());
                        UpdateStatus("Approved");
                    }
                }
            }
            catch(Exception ex)
            {
                Message.Alert(ex.Message);
                Message.LogError("contractDetails", "btnApprove_Click", Session["UserName"].ToString(), ex.Message + " for contract " + Request.QueryString["id"].ToString());
            }
        }

        private void UpdateStatus(string status, int routingSequence = 0)
        {
            try
            {
                bool isApproved = (status == "Rejected") ? false : true;
                if (status == "Bypassed")
                {
                    dsUpdateStatus.UpdateParameters["routingSequence"].DefaultValue = routingSequence.ToString();
                    Parameter parm = new Parameter() { Name = "bypassUserId", DefaultValue = Session["UserID"].ToString() };
                    dsUpdateStatus.UpdateParameters.Add(parm);
                    parm = new Parameter() { Name = "byPassReason", DefaultValue = memoBypassReason.Text.ToUpper() };
                    dsUpdateStatus.UpdateParameters.Add(parm);
                    dsUpdateStatus.UpdateParameters["isBypass"].DefaultValue = true.ToString();
                }
                if (status == "Rejected")
                {
                    Parameter parm = new Parameter() { Name = "rejectReason", DefaultValue = memoRejectReason.Text.ToUpper() };
                    dsUpdateStatus.UpdateParameters.Add(parm);
                }
                if (status == "Restored")
                {
                    lblNextSequence.Text = "1";
                    Parameter parm = new Parameter() { Name = "restoreReason", DefaultValue = memoRejectReason.Text.ToUpper() };
                    dsUpdateStatus.UpdateParameters.Add(parm);
                }
                dsUpdateStatus.UpdateParameters["approvalStatus"].DefaultValue = Convert.ToString(isApproved);
                dsUpdateStatus.Update();
                System.Data.DataSet ds = DataAccess.GetDataSet("usp_getRoutingAlert", new string[] { "@contractId" }, new string[] { Request.QueryString["id"].ToString() });
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    string body = "A Contract you Submitted for " + ddlParties.Text + " has been " + status + " at its current level by " + Session["UserName"] + ".";
                    body += "<br/><br/>You can view the current status here: <a href='" + Request.Url.AbsoluteUri + "'>" + Request.Url.AbsoluteUri + "</a>";
                    if (status == "Rejected")
                    {
                        Message.SendMail(ds.Tables[0].Rows[0]["entryEmail"].ToString(), ds.Tables[0].Rows[0]["entryName"].ToString(), "Contract for " + ddlParties.Text + " Action", body);
                    }
                    else 
                    { 
                        body = "A Contract has been assigned to you for review.";
                        if (status == "Bypassed")
                        {
                            body += "<br/><br/>The reason it was bypassed is: " + memoBypassReason.Text.ToUpper();
                        }
                        body += "<br/><br/>You can view the current status here: <a href='" + Request.Url.AbsoluteUri + "'>" + Request.Url.AbsoluteUri + "</a>";
                        Message.SendMail(ds.Tables[0].Rows[0]["userEmail"].ToString(), ds.Tables[0].Rows[0]["userName"].ToString(), "Contract for " + ddlParties.Text + " Action", body,null,"",ds.Tables[0].Rows[0]["userRole"].ToString());
                    }
                }
                if (status == "Bypassed")
                {
                    Message.Alert("Contract has been " + status + ".");
                }
                else
                {
                    string url = "contracts.aspx";
                    if (Session["referrer"] != null && Session["referrer"].ToString() != "")
                    {
                        url = Session["referrer"].ToString();
                    }
                    Message.AlertAndRedirect("Contract has been " + status + ".", url);
                }
            }
            catch(Exception ex)
            {
                Message.Alert(ex.Message);
                Message.LogError("contractDetails", "UpdateStatus", Session["UserName"].ToString(), ex.Message + " for contract " + Request.QueryString["id"].ToString());
            }
        }

        //protected void btnReturnUser_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (ddlReturn.SelectedIndex < 0)
        //        {
        //            Message.Alert("You must select the return user for the contract.");
        //        }
        //        else
        //        {
        //            dsUpdateStatus.UpdateParameters["routingSequence"].DefaultValue = ddlReturn.Items.Count == 0 ? "0" : ddlReturn.Value.ToString();
        //            Parameter parm = new Parameter() { Name = "isReturned", DefaultValue = true.ToString() };
        //            dsUpdateStatus.UpdateParameters.Add(parm);
        //            Parameter parm2 = new Parameter() { Name = "returnReason", DefaultValue = memoReason.Text.ToUpper() };
        //            dsUpdateStatus.UpdateParameters.Add(parm2);
        //            dsUpdateStatus.UpdateParameters["approvalStatus"].DefaultValue = true.ToString();
        //            lblNextSequence.Text = ddlReturn.Items.Count == 0 ? "0" : ddlReturn.Value.ToString();
        //            dsUpdateStatus.Update();
        //            System.Data.DataSet ds = DataAccess.GetDataSet("usp_getRoutingAlert", new string[] { "@contractId" }, new string[] { Request.QueryString["id"].ToString() });
        //            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        //            {
        //                string body = "A Contract you Submitted for " + ddlParties.Text + " has been returned from its current level by " + Session["UserName"] + ".";
        //                body += "<br/><br/>You can view the current status here: <a href='" + Request.Url.AbsoluteUri + "'>" + Request.Url.AbsoluteUri + "</a>";
        //                Message.SendMail(ds.Tables[0].Rows[0]["entryEmail"].ToString(), ds.Tables[0].Rows[0]["entryName"].ToString(), "Contract for " + ddlParties.Text + " Action", body);
        //                body = "A Contract has been assigned to you for review.";
        //                body += "<br/><br/>The reason it was returned is: " + memoReason.Text.ToUpper();
        //                body += "<br/><br/>You can view the current status here: <a href='" + Request.Url.AbsoluteUri + "&tab=Return'>" + Request.Url.AbsoluteUri + "&tab=Return</a>";
        //                Message.SendMail(ds.Tables[0].Rows[0]["userEmail"].ToString(), ds.Tables[0].Rows[0]["userName"].ToString(), "Contract for " + ddlParties.Text + " Action", body);
        //            }
        //            Message.Alert("Returned to " + ddlReturn.Text);
        //            popReturn.ShowOnPageLoad = false;
        //            formContract.DataBind();
        //            gvRouting.DataBind();
        //            gvReturn.DataBind();
        //        }
        //    }
        //    catch(Exception ex)
        //    {
        //        Message.Alert(ex.Message);
        //        Message.LogError("contractDetails", "btnReturnUser_Click", Session["UserName"].ToString(), ex.Message);

        //    }
        //}

        protected void gvRouting_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.VisibleIndex == 0)
            {
                byPassCount = 0;
            }
            if (e.DataColumn.FieldName == "isApproved")
            {
                if (DBNull.Value == e.CellValue)
                {
                    e.Cell.Controls.Clear();
                }
            }
        }

        private string GetQuery(string CommandQuery, System.Data.Common.DbParameterCollection parmCollection)
        {
            string query = CommandQuery;
            string parms = "";
            foreach (System.Data.SqlClient.SqlParameter parm in parmCollection)
            {
                parms += parm.ParameterName + " = ";
                if (parm.Value == null)
                {
                    parms += "null,";

                }
                else
                {
                    if (parm.DbType == System.Data.DbType.String || parm.DbType == System.Data.DbType.DateTime)
                    {
                        parms += "'" + parm.Value.ToString() + "',";
                    }
                    else
                    {
                        parms += parm.Value.ToString() + ",";
                    }
                }
            }
            if (parms.Length > 0)
                query = query + " " + parms.Substring(0, parms.Length - 1);
            return query;
        }

        protected void gvRouting_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {
            string approvedBy = gvRouting.GetRowValues(e.VisibleIndex, "approvedBy").ToString();
            bool isApproved = true;
            if (DBNull.Value != gvRouting.GetRowValues(e.VisibleIndex, "isApproved"))
            {
                isApproved = Convert.ToBoolean(gvRouting.GetRowValues(e.VisibleIndex, "isApproved"));
            }
            bool canByPass = false;
            if (byPassCount == 0)
            {
                if (DBNull.Value != gvRouting.GetRowValues(e.VisibleIndex, "canByPass"))
                {
                    canByPass = Convert.ToBoolean(gvRouting.GetRowValues(e.VisibleIndex, "canByPass"));
                }
            }
            if (e.ButtonType == ColumnCommandButtonType.Edit)
            {
                //e.Visible = false;
                if (canByPass == true)// && byPassCount == 0)
                {
                    e.Visible = true;
                    byPassCount += 1;
                }
                else
                {
                    e.Visible = false;
                }
            }
            if (e.ButtonType == ColumnCommandButtonType.Delete)
            {
                e.Visible = false;
                if (Convert.ToBoolean(Session["isAdmin"]) == true && gvRouting.GetRowValues(e.VisibleIndex, "roleName").ToString() == "Contract Managers" && isApproved == true && gvRouting.GetRowValues(e.VisibleIndex, "statusDate").ToString() != "")
                {
                    e.Visible = true;
                }
            }
        }

        protected void dsContractRoute_Updating(object sender, SqlDataSourceCommandEventArgs e)
        {
            if (e.Command.Parameters["@byPassReason"].ToString() == "")
            {
                e.Cancel = true;
            }
            else
            {
                if (!e.Command.Parameters.Contains("@isBypass"))
                {
                    var param = e.Command.CreateParameter();
                    param.ParameterName = "@isBypass";
                    param.Value = true.ToString();
                    e.Command.Parameters.Add(param);
                }
                else
                {
                    e.Command.Parameters["@isBypass"].Value = true.ToString();
                }
            }
        }

        protected void gvRouting_RowUpdated(object sender, DevExpress.Web.Data.ASPxDataUpdatedEventArgs e)
        {
            if (byPassCount > 0)
            {
                System.Data.DataSet ds = DataAccess.GetDataSet("usp_getRoutingAlert", new string[] { "@contractId" }, new string[] { Request.QueryString["id"].ToString() });
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    string body = "A Contract has been assigned to you for review.";
                    //body += "<br/><br/>The reason it was bypassed is: " + memoBypassReason.Text.ToUpper();
                    body += "<br/><br/>You can view the current status here: <a href='" + Request.Url.AbsoluteUri + "'>" + Request.Url.AbsoluteUri + "</a>";
                    Message.SendMail(ds.Tables[0].Rows[0]["userEmail"].ToString(), ds.Tables[0].Rows[0]["userName"].ToString(), "Contract for " + ddlParties.Text + " Action", body, null, "", ds.Tables[0].Rows[0]["userRole"].ToString());
                }
                Message.Alert("Contract has been bypassed.");
                ((ASPxGridView)sender).JSProperties["cpIsUpdated"] = "Contract has been bypassed.";
            }
            else
            {
                ((ASPxGridView)sender).JSProperties["cpIsUpdated"] = e.Keys[0];
            }
            byPassCount = 0;
       }


        protected void ddlParties_DataBound(object sender, EventArgs e)
        {
            if (Session["party"].ToString() == "")
            {
                Session["party"] = ddlParties.Text.ToUpper();
            }
        }

        public string GetSearchResults(string xml)
        {
            string result = "";
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                   | SecurityProtocolType.Tls11
                   | SecurityProtocolType.Tls12
                   | SecurityProtocolType.Ssl3; 
                HttpWebRequest ws = (HttpWebRequest)WebRequest.Create("https://gw.sam.gov/SAMWS/1.0/ExclusionSearch");
                ws.ContentType = "text/xml;charset=\"utf-8\"";
                ws.Accept = "text/xml";
                ws.Method = "POST";
                XmlDocument soapEnvelopeXml = new XmlDocument();
                //xml = @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:sam=""http://www.sam.gov""> <soapenv:Header/> <soapenv:Body> <sam:doSearch> <exclusionSearchCriteria> <exactName>Bad Medicine Group, Inc.</exactName> </exclusionSearchCriteria> </sam:doSearch> </soapenv:Body> </soapenv:Envelope>";
                soapEnvelopeXml.LoadXml(xml);
                try
                {
                    using (Stream stream = ws.GetRequestStream())
                    {
                        soapEnvelopeXml.Save(stream);
                    }
                    using (WebResponse response = ws.GetResponse())
                    {
                        using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                        {
                            result = rd.ReadToEnd();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Message.Alert(ex.Message);
                    string searchString = ddlParties.Text.ToUpper();
                    Message.SendMail("CMSAdministrators@mrhc.org", "IT Alerts", "Contract Management System - SAM Exclusion Connection Error", "There was an error when connecting to the SAM Exclusion Web Service for vendor " + searchString + " at https://gw.sam.gov/SAMWS/1.0/ExclusionSearch. <br/><br/>" + xml);
                }
            }
            catch (Exception ex)
            {
                Message.Alert(ex.Message);
                Message.LogError("contractDetails", "GetSearchResults", Session["UserName"].ToString(), ex.Message + " for contract " + Request.QueryString["id"].ToString());
            }
            return result;
        }

        public int ParseSoap(string searchType)
        {
            string xml = @"<soapenv:Envelope xmlns:soapenv = ""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:sam=""http://www.sam.gov"">";
            xml += @"<soapenv:Header></soapenv:Header><soapenv:Body><sam:doSearch><exclusionSearchCriteria>";
            xml += @"%SearchTerms%";
            xml += @"</exclusionSearchCriteria></sam:doSearch></soapenv:Body></soapenv:Envelope>";
            string respString = GetSearchResults(xml.Replace("%SearchTerms%", searchType));
            XDocument xDoc = XDocument.Parse(respString);
            IEnumerable<XElement> responses = xDoc.Elements().Elements().Elements().Elements();
            IEnumerable<XElement> elements = from c in xDoc.Descendants("excludedEntity") select c;
            IEnumerable<XElement> successful = from c in xDoc.Descendants("successful") select c;
            IEnumerable<XElement> count = from c in xDoc.Descendants("count") select c;
            IEnumerable<XElement> exclusionType = from c in xDoc.Descendants("exclusionType") select c;
            if (elements.Count() > 0)
            {
                AddExclusionRows(elements);
            }
            if (successful.FirstOrDefault().Value == "true" && Convert.ToInt16(count.FirstOrDefault().Value) > 0)
            {
                if (exclusionType.FirstOrDefault().Value != "")
                {
                    return 1;
                }
            }
            return 0;
        }

        public void AddExclusionRows(IEnumerable<XElement> elements)
        {
            if (dsExclusions == null)
            {
                dsExclusions = new DataSet();
                DataTable dt = new DataTable();
                dt.Columns.Add(new DataColumn() { ColumnName = "Classification", DataType = typeof(String) });
                dt.Columns.Add(new DataColumn() { ColumnName = "EntityName", DataType = typeof(String) });
                dt.Columns.Add(new DataColumn() { ColumnName = "FirstName", DataType = typeof(String) });
                dt.Columns.Add(new DataColumn() { ColumnName = "MiddleInitial", DataType = typeof(String) });
                dt.Columns.Add(new DataColumn() { ColumnName = "LastName", DataType = typeof(String) });
                dt.Columns.Add(new DataColumn() { ColumnName = "Address", DataType = typeof(String) });
                dt.Columns.Add(new DataColumn() { ColumnName = "City", DataType = typeof(String) });
                dt.Columns.Add(new DataColumn() { ColumnName = "State", DataType = typeof(String) });
                dt.Columns.Add(new DataColumn() { ColumnName = "ZipCode", DataType = typeof(String) });
                dt.Columns.Add(new DataColumn() { ColumnName = "ExclusionType", DataType = typeof(String) });
                dt.Columns.Add(new DataColumn() { ColumnName = "ActiveDate", DataType = typeof(String) });
                //dt.Columns.Add(new DataColumn() { ColumnName = "TerminationDate", DataType = typeof(String) });
                dsExclusions.Tables.Add(dt);
            }
            foreach (XElement element in elements)
            {
                DataRow dr = dsExclusions.Tables[0].NewRow();
                XElement classification = (from c in element.Descendants("classification") select c).FirstOrDefault();
                dr["Classification"] = classification == null ? "" : classification.Value.ToString();
                XElement name = (from c in element.Descendants("name") select c).FirstOrDefault();
                dr["EntityName"] = name == null ? "" : name.Value.ToString();
                XElement first = (from c in element.Descendants("first") select c).FirstOrDefault();
                dr["FirstName"] = first == null ? "" : first.Value.ToString();
                XElement middle = (from c in element.Descendants("middle") select c).FirstOrDefault();
                dr["MiddleInitial"] = middle == null ? "" : middle.Value.ToString();
                XElement last = (from c in element.Descendants("last") select c).FirstOrDefault();
                dr["LastName"] = last == null ? "" : last.Value.ToString();
                XElement street = (from c in element.Descendants("street1") select c).FirstOrDefault();
                dr["Address"] = street == null ? "" : street.Value.ToString();
                XElement city = (from c in element.Descendants("city") select c).FirstOrDefault();
                dr["City"] = city == null ? "" : city.Value.ToString();
                XElement state = (from c in element.Descendants("state") select c).FirstOrDefault();
                dr["State"] = state == null ? "" : state.Value.ToString();
                XElement zip = (from c in element.Descendants("zip") select c).FirstOrDefault();
                dr["ZipCode"] = zip == null ? "" : zip.Value.ToString();
                XElement exclusionType = (from c in element.Descendants("exclusionType") select c).FirstOrDefault();
                dr["ExclusionType"] = exclusionType == null ? "" : exclusionType.Value.ToString();
                XElement activeDate = (from c in element.Descendants("activeDate") select c).FirstOrDefault();
                dr["ActiveDate"] = activeDate == null || activeDate.Value.ToString() == "" ? "" : Convert.ToDateTime(activeDate.Value).ToString("MM/dd/yyyy");
                dsExclusions.Tables[0].Rows.Add(dr);
            }
        }

        protected void btnExclusionSave_Click(object sender, EventArgs e)
        {
            popExclusions.ShowOnPageLoad = false;
            dsContract.Update();
            Message.Alert("Contract has been saved.");
        }

        protected void btnSearchExclusions_Click(object sender, EventArgs e)
        {
            SearchExclusions(ddlParties.Text.ToUpper().Replace("(INACTIVE) ",""));
        }

        protected void SearchExclusions(string BusinessName, string searchType = "")
        {
            try
            {
                dsExclusions = null;
                Session["dsExclusions"] = null;
                txtBusinessName.Text = BusinessName;
                //string exactSearch = @"<exactName>" + BusinessName.Replace("&", "").ToUpper() + "</exactName>";
                //string nameSearch = @"<name>" + BusinessName.Replace("&", "").ToUpper() + "</name>";
                //string partialName = @"<partialName>" + BusinessName.Replace("&", "").ToUpper() + "</partialName>";
                //ParseSoap(exactSearch);
                //ParseSoap(nameSearch);
                //ParseSoap(partialName);
                DataSet dsTableExclusions = DataAccess.GetDataSet("usp_getExclusions", new string[] { "@businessName", "@isFuzzy" }, new string[] { BusinessName.ToUpper(), cbFuzzy.Checked.ToString() });
                if (dsTableExclusions != null && dsTableExclusions.Tables.Count > 0 && dsTableExclusions.Tables[0].Rows.Count > 0)
                {
                    if (dsExclusions == null || dsExclusions.Tables.Count == 0 || dsExclusions.Tables[0].Rows.Count == 0)
                    {
                        dsExclusions = new DataSet();
                        DataTable dt = dsTableExclusions.Tables[0].Clone();
                        dsExclusions.Tables.Add(dt);
                    }
                    //else
                    //{
                    dsExclusions.Tables[0].Merge(dsTableExclusions.Tables[0], true, MissingSchemaAction.Ignore);
                    //}
                }
                if (hdnOIGCheck.Count > 0 && hdnOIGCheck.Contains("check"))
                {
                    hdnOIGCheck.Remove("check");
                }
                hdnOIGCheck.Add("check", "true");
                ddlGSA.SelectedIndex = 0;
                if (dsExclusions != null && dsExclusions.Tables.Count > 0)
                {
                    DataTable dt = dsExclusions.Tables[0].DefaultView.ToTable(true, "Classification", "EntityName", "FirstName", "MiddleInitial", "LastName", "Address", "City", "State", "ZipCode", "ExclusionType", "ActiveDate");
                    dsExclusions.Tables.RemoveAt(0);
                    dsExclusions.Tables.Add(dt);
                    gvExclusions.DataSource = dsExclusions;
                    gvExclusions.DataBind();
                    Session["dsExclusions"] = dsExclusions;
                    divExclusionResults.Visible = true;
                    popExclusions.ShowOnPageLoad = true;
                }
                else
                {
                    popExclusions.ShowOnPageLoad = false;
                    if (searchType != "OIG")
                    {
                        dsContract.Update();
                        Message.Alert("Contract has been saved.");
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                {
                    throw new Exception("Business Name already exists.");
                }
                else
                {
                    Message.LogError("contractDetails", "SearchExclusions", Session["UserName"].ToString(), ex.Message + " for contract " + Request.QueryString["id"].ToString());
                    throw;
                }
            }
        }

        protected void btnBypassReason_Click(object sender, EventArgs e)
        {
            UpdateStatus("Bypassed");
            gvRouting.DataBind();
            gvBypass.DataBind();
            popBypass.ShowOnPageLoad = false;
            formContract.DataBind();
        }

        protected void btnRejectReason_Click(object sender, EventArgs e)
        {
            if (btnRejectReason.Text == "Restore")
            {
                UpdateStatus("Restored");
            }
            else
            {
                UpdateStatus("Rejected");
            }
        }

        protected void dsUpdateStatus_Updated(object sender, SqlDataSourceStatusEventArgs e)
        {

        }

        protected void dsContract_Deleting(object sender, SqlDataSourceCommandEventArgs e)
        {
            string x = GetQuery(e.Command.CommandText, e.Command.Parameters);
        }

        protected void dsMasterContractsDetail_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            string x = GetQuery(e.Command.CommandText, e.Command.Parameters);
        }

        protected void gvDocuments_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.FieldName == "url")
            {
                e.Cell.ToolTip = gvDocuments.GetRowValues(e.VisibleIndex, new string[] { "fileName" }).ToString();
            }
        }

        protected void ddlContracts_SelectedIndexChanged(object sender, EventArgs e)
        {
            //dsContract.SelectParameters["contractId"].DefaultValue = ddlContracts.Value.ToString();
            //dsContract.Select(DataSourceSelectArguments.Empty);
            //formContract.DataBind();
            //dsAllContracts.SelectParameters["contractId"].DefaultValue = ddlContracts.Value.ToString();
            //gvAllContracts.DataBind();
            Response.Redirect("contractDetails.aspx?id=" + ddlContracts.Value.ToString());
        }

        protected void btnDocusignBulk_Click(object sender, EventArgs e)
        {
            try
            {
                DocuSign docsign = new DocuSign();
                docsign.ContractID = Request.QueryString["id"];
                docsign.RecipientEmail = Session["email"].ToString();
                docsign.RecipientUserName = Session["UserName"].ToString();
                docsign.CEOEmail = Session["ceoEmail"].ToString();
                docsign.CEOName = "CEO";
                List<DocusignDocument> Documents = docsign.Documents;
                int docCount = 0;
                string msg = "";
                for (int i = 0; i < gvDocuments.VisibleRowCount; i++)
                {
                    bool isToSend = false;
                    try
                    {
                        isToSend = gvDocuments.Selection.IsRowSelected(i);
                    }
                    catch(Exception ex)
                    {
                        Message.LogError("contractDetails", "btnDocusignBulk_Click isRowSelected", Session["UserName"].ToString(), ex.Message + " for contract " + Request.QueryString["id"].ToString());
                    }
                    if (isToSend)
                    {
                        docCount++;
                        DocusignDocument thisDocument = new DocusignDocument();
                        thisDocument.RecipientEmail = Session["email"].ToString();
                        thisDocument.RecipientUserName = "TEST RECIPIENT";
                        thisDocument.CEOEmail = Session["ceoEmail"].ToString();
                        thisDocument.CEOName = "CEO";
                        string url = gvDocuments.GetRowValues(i, new string[] { "url" }).ToString();
                        if (System.Configuration.ConfigurationManager.AppSettings["system"] == "LOCALHOST" || Request.Url.Host.Contains("azure"))
                        {
                            url = url.Replace(@"//mrhc.org/pub/Public", @"P:\");
                            url = url.Replace("Documents/", "").Replace(" / ", "\\");
                            url = url.Replace(@"//Contract Database/",@"P://Contract Database/");
                        }
                        else
                        {
                            url = gvDocuments.GetRowValues(i, new string[] { "path" }).ToString() + @"\" + gvDocuments.GetRowValues(i, new string[] { "fileName" }).ToString(); //url.Replace("http://localhost//Documents", "P://").Replace("//", "/").Replace("/", "\\");
                        }
                        string dir = url;
                        thisDocument.DocumentName = gvDocuments.GetRowValues(i, new string[] { "fileName" }).ToString();
                        thisDocument.DocumentID = Convert.ToInt32(gvDocuments.GetRowValues(i, new string[] { "documentId" }));
                        try
                        {
                            using (var stream = File.Open(dir, FileMode.Open))
                            {
                                if (stream.Length > 0)
                                {
                                    using (var fc = new MemoryStream())
                                    {
                                        stream.Seek(0, SeekOrigin.Begin);
                                        stream.CopyTo(fc);
                                        thisDocument.DocumentStream = fc;
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            msg += ex.Message;
                            Message.LogError("contractDetails", "btnDocusignBulk_Click-Add Stream", Session["UserName"].ToString(), msg + " for contract " + Request.QueryString["id"].ToString());
                            Server.ClearError();
                        }
                        docsign.Documents.Add(thisDocument);
                    }
                }
                if (msg != "")
                {
                    Message.Alert(msg);
                }
                else
                {
                    if (docCount > 0)
                    {
                        docsign.EnvelopeSubject = "Contract Signature Required";
                        msg = "Email sent to CEO for Signature";
                        string status = docsign.CreateAndSend();
                        if (status == "0")
                        {
                            Message.Alert("Error sending email");
                        }
                        else
                        {
                            Message.Alert(msg);
                            btnGetDocusign.ClientVisible = true;
                            gvDocuments.Selection.UnselectAll();
                            gvDocuments.DataBind();
                        }
                    }
                    else
                    {
                        Message.Alert("No documents have been selected.");
                    }
                }
            }
            catch (Exception ex)
            {
                Message.LogError("contractDetails", "btnDocusignBulk_Click", Session["UserName"].ToString(), ex.Message + " for contract " + Request.QueryString["id"].ToString());
                Message.Alert(ex.Message);
            }
        }

        protected void btnGetDocusign_Click(object sender, EventArgs e)
        {
            try
            {
                //DataSet ds = DataAccess.GetDataSet("usp_getDocusignEnvelope", new string[] { "@contractId" }, new string[] { Request.QueryString["id"].ToString() });
                string prevEnvelopeId = "";
                int count = 0;
                List<string> envelopes = new  List<string>();
                for (int i = 0; i < gvDocuments.VisibleRowCount; i++)
                {
                    string envelopeId = gvDocuments.GetRowValues(i, new string[] { "envelopeId" }).ToString();
                    bool isEmailSigned = Convert.ToBoolean(gvDocuments.GetRowValues(i, new string[] { "isEmailSigned" }).ToString());
                    if (envelopeId != null && envelopeId != "" && prevEnvelopeId != envelopeId && isEmailSigned == false)
                    {
                        //foreach (DataRow row in ds.Tables[0].Rows)
                        //{
                        //envelopeId = row["envelopeId"] == null ? "" : row["envelopeId"].ToString();
                        //if (envelopeId != "")
                        //{
                        envelopes.Add(envelopeId);
                        
                        //}
                        //}
                    }
                    if (envelopeId != null && envelopeId != "")
                    {
                        prevEnvelopeId = envelopeId;
                        //envelopeId = "1";
                        //prevEnvelopeId = "";
                    }
                    //else
                    //{
                    //    prevEnvelopeId = envelopeId;
                    //}
                }
                if (envelopes.Count> 0)
                {
                    for(int j = 0; j< envelopes.Count;j++)
                    {
                        DocuSign doc = new DocuSign();
                        if (doc.CheckStatusByEnvelopeId(envelopes[j]))
                            count += 1;
                    }
                }
                Message.Alert(count + " documents imported from Docusign");
                gvDocuments.DataBind();
                gvDocuments.Selection.UnselectAll();
                pcContract.ActiveTabIndex = pcContract.TabPages.FindByName("Documents").Index;
                btnGetDocusign.ClientVisible = true;
            }
            catch (Exception ex)
            {
                Message.LogError("contractDetails", "btnGetDocusign_Click", Session["UserName"].ToString(), ex.Message + " for contract " + Request.QueryString["id"].ToString());
                Message.Alert(ex.Message);
            }
        }

        //protected void btnDocusign_Init(object sender, EventArgs e)
        //{
        //    ASPxButton btn = (ASPxButton)sender;
        //    GridViewDataItemTemplateContainer container = (GridViewDataItemTemplateContainer)btn.NamingContainer;
        //    //btn.ClientSideEvents.Click = String.Format("function(s, e) {{cbShowCheckoutDetail({0})}}", container.VisibleIndex)
        //    int index = container.VisibleIndex;
        //    string envelopeId = "";
        //    if(gvDocuments.GetRowValues(index,"envelopeId") != null)
        //    {
        //        envelopeId = gvDocuments.GetRowValues(index, "envelopeId").ToString();
        //    }
        //    btn.Visible = Convert.ToBoolean(Session["isAdmin"]) == true; // && envelopeId != "";
        //}

        //protected void gvDocuments_Init(object sender, EventArgs e)
        //{
        //    gvDocuments.Columns["EmailDocusign"].Visible = Convert.ToBoolean(Session["isAdmin"]);
        //}

        //protected void btnDocusign_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        DocuSign docsign = new DocuSign();
        //        List<DocusignDocument> Documents = docsign.Documents;
        //        DocusignDocument thisDocument = new DocusignDocument();
        //        thisDocument.RecipientEmail = "rigymmom@gmail.com";
        //        thisDocument.RecipientUserName = "TEST RECIPIENT";
        //        docsign.ContractID = Request.QueryString["id"];
        //        thisDocument.CEOEmail = "jodie@cbit-llc.com";
        //        thisDocument.CEOName = "TEST CEO";
        //        docsign.EnvelopeSubject = "Contract Signature Required";
        //        ASPxButton btn = (ASPxButton)sender;
        //        GridViewDataItemTemplateContainer container = (GridViewDataItemTemplateContainer)btn.NamingContainer;
        //        //btn.ClientSideEvents.Click = String.Format("function(s, e) {{cbShowCheckoutDetail({0})}}", container.VisibleIndex)
        //        int index = container.VisibleIndex;
        //        string url = gvDocuments.GetRowValues(index, "url").ToString();
        //        url = url.Replace("http://localhost/", "").Replace("/mrhc.org/pub/Public/", "").Replace("//", "/"); //.Replace("/", "\\");
        //        //string dir = "P:\\Contract Database\\ContractDatabase\\" + Request.QueryString["id"].ToString(); //Server.MapPath("/Documents/"
        //        //string dir = "P:\\Contract Database\\ContractDatabase\\" + Request.QueryString["id"].ToString() + "\\Test2.pdf"; //Server.MapPath("/Documents/"
        //        string dir = Server.MapPath(url);
        //        thisDocument.DocumentName = gvDocuments.GetRowValues(index, "path").ToString();// @"P:\\Contract Database\\ContractDatabase\\1\\CM_01-3M.pdf"; // dir;
        //        string status = "0", docMsg = "",msg = "Email sent to CEO for Signature";
        //        try
        //        {
        //            using (var stream = File.Open(dir, FileMode.Open))
        //            {
        //                if (stream.Length > 0)
        //                {
        //                    using (var fc = new MemoryStream())
        //                    {
        //                        stream.Seek(0, SeekOrigin.Begin);
        //                        stream.CopyTo(fc);
        //                        thisDocument.DocumentStream = fc;
        //                    }
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            docMsg = ex.Message;
        //        }
        //        if (thisDocument.DocumentStream != null)
        //        {
        //            docsign.Documents.Add(thisDocument);
        //            status = docsign.CreateAndSend();
        //        }
        //        else
        //        {
        //            docMsg = "The selected document could not be found.";
        //        }
        //        if (status == "0")
        //        {
        //            Message.Alert("Error sending email." + docMsg);
        //        }
        //        else
        //        {
        //            Message.Alert(msg);
        //        }
        //        }
        //        catch (Exception ex)
        //        {
        //            Message.Alert(ex.Message);
        //        }
        //}

        private void Page_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError();
            Message.Alert(exc.Message);

            // Handle specific exception.
            if (exc is System.Web.HttpUnhandledException)
            {
                Message.LogError("contractDetails", "Page_Error", Session["UserID"].ToString(), exc.Message);
                Message.Alert("An error occurred on this page. Please verify your information to resolve the issue.");
            }
            // Clear the error from the server.
            Server.ClearError();
        }

        protected void dsDocuments_Updating(object sender, SqlDataSourceCommandEventArgs e)
        {
            string query = GetQuery(e.Command.CommandText, e.Command.Parameters);
        }

        protected void dsUploadDocumentTypes_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            string x = GetQuery(e.Command.CommandText, e.Command.Parameters);
        }

        protected void gvDocuments_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            dsDocuments.UpdateParameters["uploadDocumentTypeId"].DefaultValue = e.NewValues["uploadDocumentTypeId"].ToString();
            dsDocuments.UpdateParameters["contractTypeId"].DefaultValue = ddlContractTypes.Value.ToString();
            dsDocuments.UpdateParameters["documentId"].DefaultValue = e.Keys["documentId"].ToString();
            dsDocuments.UpdateParameters["partyId"].DefaultValue = ddlParties.Value.ToString();
            dsDocuments.UpdateParameters["contractId"].DefaultValue = Request.QueryString["id"];
            dsDocuments.Update();
        }

        protected void dsDocuments_Updated(object sender, SqlDataSourceStatusEventArgs e)
        {
            string x = e.Command.CommandText;
            try
            {
                string returnFolder = "";
                if (e.Command.Parameters["@folder"].Value != null && e.Command.Parameters["@folder"].Value != System.DBNull.Value)
                {
                    returnFolder = e.Command.Parameters["@folder"].Value.ToString();
                }
                string returnPrevFolder = "";
                if (e.Command.Parameters["@prevFolder"].Value != null && e.Command.Parameters["@prevFolder"].Value != System.DBNull.Value)
                {
                    returnPrevFolder = e.Command.Parameters["@prevFolder"].Value.ToString();
                }
                if (returnFolder != returnPrevFolder)
                {
                    string docId = "";
                    if (e.Command.Parameters["@documentId"].Value != null && e.Command.Parameters["@documentId"].Value != System.DBNull.Value)
                    {
                        docId = e.Command.Parameters["@documentId"].Value.ToString();
                    }
                    if (docId != "")
                    {
                        string url = "";
                        if (gvDocuments.GetRowValuesByKeyValue(docId, "url") != null && gvDocuments.GetRowValuesByKeyValue(docId, "url") != System.DBNull.Value)
                        {
                            url = gvDocuments.GetRowValuesByKeyValue(docId, "url").ToString();
                        }
                        string partyId = "";
                        if (gvDocuments.GetRowValuesByKeyValue(docId, "partyId") != null && gvDocuments.GetRowValuesByKeyValue(docId, "partyId") != System.DBNull.Value)
                        {
                            partyId = gvDocuments.GetRowValuesByKeyValue(docId, "partyId").ToString();
                        }
                        string[] urlParts = url.Split('/');
                        string folder = "";
                        if (gvDocuments.GetRowValuesByKeyValue(docId, "mainFolder") != null && gvDocuments.GetRowValuesByKeyValue(docId, "mainFolder") != System.DBNull.Value)
                        {
                            folder = gvDocuments.GetRowValuesByKeyValue(docId, "mainFolder").ToString();
                        }
                        if (returnFolder == "PARTY")
                        {
                            urlParts[7] = partyId;
                            urlParts[6] = returnFolder;
                        }
                        else
                        {
                            urlParts[7] = Request.QueryString["id"];
                            urlParts[6] = folder;
                            //switch (e.Command.Parameters["@contractTypeId"].ToString())
                            //{
                            //    case "4":
                            //        urlParts[6] = "PhysicianContractDatabase";
                            //        break;
                            //    case "5":
                            //        urlParts[6] = "LeaseDatabase";
                            //        break;
                            //    case "7":
                            //        urlParts[6] = "EmployeeEducationAssistanceDB";
                            //        break;
                            //    case "10":
                            //        urlParts[6] = "AdministrativeDB";
                            //        break;
                            //    case "11":
                            //        urlParts[6] = "PHODatabase";
                            //        break;
                            //    default:
                            //        urlParts[6] = "ContractDatabase";
                            //        break;
                            //}
                        }
                        string destUrl = String.Join("/", urlParts);
                        string path = destUrl.Replace(urlParts[urlParts.Length - 1], "").Replace("/", @"\");
                        path = path.Substring(0, path.Length - 1);
                        try
                        {
                            if (File.Exists(url) && !File.Exists(destUrl))
                            {
                                File.Move(url, destUrl);
                                //DataSet ds = DataAccess.GetDataSet2("update documents set path = @path where documentId = @documentId", new string[] { "@path", "@documentId" }, new string[] { path, e.Command.Parameters["@documentId"].Value.ToString() });
                            }
                            else
                            {
                                string msgType = File.Exists(url) == false ? " Cannot locate original document." : " A document with that name already exists in the new location.";
                                Message.Alert("There was a problem moving the selected file." + msgType);
                            }
                        }
                        catch (Exception ex)
                        {
                            Message.LogError("contractDetails", "dsDocuments_Updated-Move File", Session["UserName"].ToString(), ex.Message + " for contract " + Request.QueryString["id"].ToString());
                            Message.Alert(ex.Message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var st = new System.Diagnostics.StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber();
                Message.LogError("contractDetails", "dsDocuments_Updated line: " + line, Session["UserName"].ToString(), ex.Message + " for contract " + Request.QueryString["id"].ToString());
                Message.Alert(ex.Message);
            }
        }

        protected void dsUpdateStatus_Updating(object sender, SqlDataSourceCommandEventArgs e)
        {
            string x = GetQuery(e.Command.CommandText, e.Command.Parameters);
        }

        protected void btnEmailRequestor_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlEntryUser.Text != "")
                {
                    HttpServerUtility Server = HttpContext.Current.Server;
                    int docCount = 0;
                    string msg = "";
                    List<string> urls = new List<string>();
                    List<string> files = new List<string>();
                    for (int i = 0; i < gvDocuments.VisibleRowCount; i++)
                    {
                        bool isToSend = false;
                        try
                        {
                            isToSend = gvDocuments.Selection.IsRowSelected(i);
                        }
                        catch (Exception ex)
                        {
                            Message.LogError("contractDetails", "btnEmailRequestor_Click isRowSelected", Session["UserName"].ToString(), ex.Message + " for contract " + Request.QueryString["id"].ToString());
                        }
                        if (isToSend)
                        {
                            docCount++;
                            string url = gvDocuments.GetRowValues(i, new string[] { "url" }).ToString();
                            if (System.Configuration.ConfigurationManager.AppSettings["system"] == "LOCALHOST" || Request.Url.Host.Contains("azure"))
                            {
                                url = url.Replace(@"\\mrhc.org\pub\Public", @"P:\");
                                url = url.Replace("Documents/", "").Replace(" / ", "\\");
                                url = url.Replace(@"//Contract Database/", @"P://Contract Database/");
                            }
                            else
                            {
                                url = gvDocuments.GetRowValues(i, new string[] { "path" }).ToString() + @"\" + gvDocuments.GetRowValues(i, new string[] { "fileName" }).ToString(); //url.Replace("http://localhost//Documents", "P://").Replace("//", "/").Replace("/", "\\");
                            }
                            urls.Add(url);
                            files.Add(gvDocuments.GetRowValues(i, new string[] { "fileName" }).ToString());
                        }
                    }
                    if (docCount > 0)
                    {
                        string entryUserId = ddlEntryUser.Value.ToString();
                        string toEmail = "";
                        string entryUserName = ddlEntryUser.Text;
                        DataSet ds = DataAccess.GetDataSet2("select * from users where userId = @userId", new string[] { "@userId" }, new string[] { entryUserId });
                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            toEmail = ds.Tables[0].Rows[0]["emailAddress"].ToString();
                            string body = "A Contract you Submitted for " + ddlParties.Text + " has been signed by all internal parties";
                            if (ddlPartySigned.Value.ToString() != "N")
                            {
                                body += ". Please send a fully exeucted copy to the Vendor for their records.";
                                body += "<br/><br/>You can obtain the fully executed contract here: <a href='" + System.Configuration.ConfigurationManager.AppSettings["site"].ToString() + "/app/contractDetails.aspx?id=" + Request.QueryString["id"].ToString() + "'>" + "https://cm/cms/app/contractDetails.aspx?id=" + Request.QueryString["id"].ToString() + "</a>";
                            }
                            else
                            {
                                body += " and is fully executed and ready for a copy to be sent to the vendor.";
                                body += "<br/><br/>You can view the current status here: <a href='" + System.Configuration.ConfigurationManager.AppSettings["site"].ToString() + "/app/contractDetails.aspx?id=" + Request.QueryString["id"].ToString() + "'>" + "https://cm/cms/app/contractDetails.aspx?id=" + Request.QueryString["id"].ToString() + "</a>";
                            }
                            using (MailMessage mail = new MailMessage())
                            {
                                if (System.Configuration.ConfigurationManager.AppSettings["system"] == "DEV")
                                {
                                    mail.To.Clear();
                                    mail.To.Add("CMSAdministrators@mrhc.org");
                                    mail.Subject = "TEST SITE - CMS Contract Request Completed";
                                }
                                else
                                {
                                    MailAddress to = new MailAddress(toEmail, entryUserName);
                                    mail.To.Add(to);
                                    mail.Subject = "CMS Contract Request Completed";
                                }
                                mail.From = new MailAddress("contracts@mrhc.org");
                                mail.Body = body;
                                mail.IsBodyHtml = true;
                                FileStream stream = null;
                                MemoryStream fc = null;
                                for (int i = 0; i < urls.Count; i++)
                                {
                                    try
                                    {
                                        stream = File.Open(urls[i], FileMode.Open);
                                        if (stream.Length > 0)
                                        {
                                            fc = new MemoryStream();
                                            stream.Seek(0, SeekOrigin.Begin);
                                            stream.CopyTo(fc);
                                            fc.Seek(0, SeekOrigin.Begin);
                                            Attachment Att = new Attachment(fc, files[i]); //*, "application/pdf"*/
                                            mail.Attachments.Add(Att);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        msg += ex.Message;
                                        Message.LogError("contractDetails", "btnEmailRequestor_Click-Add Stream", Session["UserName"].ToString(), msg + " for contract " + Request.QueryString["id"].ToString());
                                        Server.ClearError();
                                    }
                                }
                                SmtpClient smtp = new SmtpClient();
                                smtp.Port = 25;
                                smtp.Host = "relay.mrhc.org";
                                smtp.EnableSsl = true;
                                if (System.Configuration.ConfigurationManager.AppSettings["system"] == "LOCALHOST")
                                {
                                    smtp.PickupDirectoryLocation = @"c:\emails\";
                                    smtp.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                                    smtp.EnableSsl = false;
                                }
                                //System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls
                                try
                                {
                                    if (System.Configuration.ConfigurationManager.AppSettings["sendMail"].ToString() == "TRUE")
                                    {
                                        DataSet dsEmail = DataAccess.GetDataSet("usp_logEmail", new string[] { "@emailTo", "@subject", "@body" }, new string[] { mail.To.ToString(), mail.Subject, mail.Body });
                                        smtp.Send(mail);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Message.LogError("Message", "SendMail", HttpContext.Current.Session["userName"].ToString(), ex.Message + " for contract " + Request.QueryString["id"].ToString());
                                    Message.Alert(ex.Message);
                                    Server.ClearError();
                                }
                                finally
                                {
                                    if (stream != null)
                                    {
                                        stream.Close();
                                        stream.Dispose();
                                    }
                                    if (fc != null)
                                    {
                                        fc.Close();
                                        fc.Dispose();
                                    }
                                    if (smtp != null) { smtp.Dispose(); }
                                    if (mail != null) { mail.Dispose(); }
                                }
                            }
                            msg = "Email sent to Contract Requestor";
                            Message.Alert(msg);
                        }
                        else
                        {
                            Message.Alert("No user data found for entry user.");
                        }
                    }
                    else
                    {
                        Message.Alert("No documents have been selected.");
                    }
                }
                else
                {
                    Message.Alert("Please select an entry user.");
                }
            }
            catch (Exception ex)
            {
                Message.LogError("contractDetails", "btnEmailRequestor_Click", Session["UserName"].ToString(), ex.Message + " for contract " + Request.QueryString["id"].ToString());
                Message.Alert(ex.Message);
            }

        }

        protected void gvRouting_CancelRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            //gvRouting_CommandButtonInitialize(sender,new ASPxGridViewCommandButtonEventArgs())
            byPassCount = 0;
            //gvRouting.DataBind();
            ((ASPxGridView)sender).JSProperties["cpIsUpdated"] = 1;
        }

        protected void btnURL_Init(object sender, EventArgs e)
        {
            ASPxHyperLink link = (ASPxHyperLink)sender;

            GridViewDataItemTemplateContainer templateContainer = (GridViewDataItemTemplateContainer)link.NamingContainer;

            int rowVisibleIndex = templateContainer.VisibleIndex;
            string url = templateContainer.Grid.GetRowValues(rowVisibleIndex, "url").ToString();
            string imageUrl = templateContainer.Grid.GetRowValues(rowVisibleIndex, "ImageUrl").ToString();
            string documentId = templateContainer.Grid.GetRowValues(rowVisibleIndex, "documentId").ToString();
            link.ImageUrl = imageUrl;
            link.ImageHeight = Unit.Pixel(30);
            link.ImageWidth = Unit.Pixel(30);
            link.Target = "_blank";
            //Session["documentURL"] = Server.UrlEncode(url);
            //link.NavigateUrl = string.Format("DocumentViewer.aspx?url={0}",Server.UrlEncode(url));
            link.NavigateUrl = string.Format("DocumentViewer.aspx?documentId={0}", documentId);
        }

        protected void btnPDF_Click(object sender, EventArgs e)
        {
            Response.Redirect("DocumentViewer.aspx?id=" + Request.QueryString["id"]);
        }

        protected void btnReturnUser_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlReturn.SelectedIndex < 0)
                {
                    Message.Alert("You must select the return user for the contract.");
                }
                else
                {
                    int returnFromSequence = 0;
                    for (int i = 0; i < gvRouting.VisibleRowCount; i ++)
                    {
                        if (gvRouting.GetRowValues(i,"statusDate") == null || gvRouting.GetRowValues(i,"statusDate").ToString() == "")
                        {
                            returnFromSequence = Convert.ToInt32(gvRouting.GetRowValues(i, "routingSequence"));
                            break;
                        }
                    }
                    if (returnFromSequence != 0)
                    {
                        Parameter parmSequence = new Parameter() { Name = "returnFromSequence", DefaultValue = returnFromSequence.ToString() };
                        dsUpdateStatus.UpdateParameters.Add(parmSequence);
                    }
                    dsUpdateStatus.UpdateParameters["routingSequence"].DefaultValue = ddlReturn.Items.Count == 0 ? "0" : ddlReturn.Value.ToString();
                    Parameter parm = new Parameter() { Name = "isReturned", DefaultValue = true.ToString() };
                    dsUpdateStatus.UpdateParameters.Add(parm);
                    Parameter parm2 = new Parameter() { Name = "returnReason", DefaultValue = memoReason.Text.ToUpper() };
                    dsUpdateStatus.UpdateParameters.Add(parm2);
                    dsUpdateStatus.UpdateParameters["approvalStatus"].DefaultValue = true.ToString();
                    lblNextSequence.Text = ddlReturn.Items.Count == 0 ? "0" : ddlReturn.Value.ToString();
                    dsUpdateStatus.Update();
                    System.Data.DataSet ds = DataAccess.GetDataSet("usp_getRoutingAlert", new string[] { "@contractId" }, new string[] { Request.QueryString["id"].ToString() });
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        string body = "";
                        if (ds.Tables[0].Rows[0]["entryEmail"].ToString() != ds.Tables[0].Rows[0]["userEmail"].ToString())
                        {
                            body = "A Contract you Submitted for " + ddlParties.Text + " has been returned from its current level by " + Session["UserName"] + ".";
                            body += "<br/><br/>The reason it was returned is: " + memoReason.Text.ToUpper();
                            body += "<br/><br/>You can view the current status here: <a href='" + Request.Url.AbsoluteUri + "&tab=Return'>" + Request.Url.AbsoluteUri + "</a>";
                            Message.SendMail(ds.Tables[0].Rows[0]["entryEmail"].ToString(), ds.Tables[0].Rows[0]["entryName"].ToString(), "Contract for " + ddlParties.Text + " Returned", body);
                        }
                        body = "A Contract has been returned  to you for action.";
                        body += "<br/><br/>The reason it was returned is: " + memoReason.Text.ToUpper();
                        body += "<br/><br/>You can view the current status here: <a href='" + Request.Url.AbsoluteUri + "&tab=Return'>" + Request.Url.AbsoluteUri + "&tab=Return</a>";
                        Message.SendMail(ds.Tables[0].Rows[0]["userEmail"].ToString(), ds.Tables[0].Rows[0]["userName"].ToString(), "Contract Returned For Action - " + ddlParties.Text, body);
                    }
                    Message.Alert("Returned to " + ds.Tables[0].Rows[0]["userName"].ToString());
                    popReturn.ShowOnPageLoad = false;
                    formContract.DataBind();
                    gvRouting.DataBind();
                    gvReturn.DataBind();
                }
            }
            catch (Exception ex)
            {
                Message.Alert(ex.Message);
                Message.LogError("contractDetails", "btnReturnUser_Click", Session["UserName"].ToString(), ex.Message + " for contract " + Request.QueryString["id"].ToString());

            }
        }

        protected void btnOIG_Click(object sender, EventArgs e)
        {
            SearchExclusions(ddlParties.Text.ToUpper().Replace("(INACTIVE) ",""),"OIG");
            DataSet dsExclusions = (DataSet)Session["dsExclusions"];
            int oigCount = dsExclusions == null || dsExclusions.Tables.Count == 0 ? 0 : dsExclusions.Tables[0].Rows.Count;
            string isAdded = hdnOIGCheck.Get("check").ToString() == "true" ? "1" : "0";
            DataSet ds = DataAccess.GetDataSet("usp_logOIG", new string[] { "@contractId", "@searchDate", "@searchTerm", "@searchResults", "@isAdded", "@addedBy" }, new string[] { Request.QueryString["id"].ToString(), DateTime.Now.ToString(), ddlParties.Text.ToUpper(), oigCount.ToString(), isAdded, Session["UserID"].ToString() });
            gvOIG.DataBind();
            if(oigCount == 0)
            {
                gvAnnualReview.Columns["approve"].Visible = true;
                btnOIG.Visible = false;
                for(int i = 0; i < gvAnnualReview.VisibleRowCount; i ++)
                {
                    string[] values = gvAnnualReview.GetRowValues(i, new string[] { "reviewer", "isApproved" }) as string[];
                    if (values != null && values[1] != null && Convert.ToBoolean(Session["isAdmin"]) == false)
                    {
                        gvAnnualReview.Columns["approve"].Visible = false;
                    }
                }
                //gvAnnualReview.DataBind();
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            using (SqlConnection myConnection = new SqlConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["cmsConn"].ConnectionString))
            {
                using (SqlCommand myCommand = new SqlCommand("usp_addCancelContract", myConnection))
                {
                    myCommand.CommandType = CommandType.StoredProcedure;
                    myCommand.CommandTimeout = 0;
                    myCommand.Parameters.AddWithValue("contractId", Request.QueryString["id"].ToString());
                    myCommand.Parameters.AddWithValue("userId", Session["UserID"].ToString());
                    SqlParameter parm = new SqlParameter();
                    parm.ParameterName = "newContractId";
                    parm.DbType = DbType.Int32;
                    parm.Direction = ParameterDirection.Output;
                    myCommand.Parameters.Add(parm);
                    //DataSet ds = new DataSet();
                    try
                    {
                        myConnection.Open();
                        myCommand.ExecuteNonQuery();
                        if (myCommand.Parameters["newContractId"] != null &&myCommand.Parameters["newContractId"].Value != null)
                        {
                            DataSet ds = DataAccess.GetDataSet("usp_getRoutingAlert", new string[] { "@contractId" }, new string[] { myCommand.Parameters["newContractId"].Value.ToString() });
                            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                            {
                                string body = "A Contract has been assigned to you for review.";
                                body += "<br/><br/>You can view the current status here: <a href='" + Request.Url.AbsoluteUri + "'>" + Request.Url.AbsoluteUri + "</a>";
                                Message.SendMail(ds.Tables[0].Rows[0]["userEmail"].ToString(), ds.Tables[0].Rows[0]["userName"].ToString(), "Contract for " + ddlParties.Text + " Action", body, null, "", ds.Tables[0].Rows[0]["userRole"].ToString());
                            }
                            Response.Redirect("contractDetails.aspx?id=" + myCommand.Parameters["newContractId"].Value.ToString() + "&tab=Upload");
                        }
                        //using (SqlDataAdapter mAdapter = new SqlDataAdapter(myCommand))
                        //{
                        //    mAdapter.Fill(ds);
                        //}
                    }
                    catch (Exception ex)
                    {
                        Message.Alert(ex.Message);
                        Message.LogError("contractDetails", "btnCancel_Click", Session["UserName"].ToString(), ex.Message + " for contract " + Request.QueryString["id"].ToString());
                    }
                }
            }
            // Create cancel contract
            // Redirect to new ContractID tab=Upload
        }

        protected void dsAnnualReview_Updating(object sender, SqlDataSourceCommandEventArgs e)
        {
            string x = GetQuery(e.Command.CommandText, e.Command.Parameters);
        }

        protected void dsContractRoute_Deleting(object sender, SqlDataSourceCommandEventArgs e)
        {
            int row = 0;
            if (e.Command.Parameters["@row_num"].Value != null)
            {
                row = Convert.ToInt32(e.Command.Parameters["@row_num"].Value);
                row -= 1;
            }
            if (row != 0)
            {
                if (!e.Command.Parameters.Contains("@statusDate"))
                {
                    var param = e.Command.CreateParameter();
                    param.ParameterName = "@statusDate";
                    param.Value = gvRouting.GetRowValues(row, "statusDate");
                    e.Command.Parameters.Add(param);
                }
                if (!e.Command.Parameters.Contains("@routingSequence"))
                {
                    var param = e.Command.CreateParameter();
                    param.ParameterName = "@routingSequence";
                    param.Value = gvRouting.GetRowValues(row, "routingSequence");
                    e.Command.Parameters.Add(param);
                }
                if (e.Command.Parameters.Contains("@row_num"))
                {
                    e.Command.Parameters.Remove(e.Command.Parameters["@row_num"]);
                }
            }
        }

        protected void gvRouting_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
        {
            ((ASPxGridView)sender).JSProperties["cpIsDeleted"] = e.Keys[0];
        }

        //protected void btnURL_Click(object sender, EventArgs e)
        //{
        //    ASPxButton btn = (ASPxButton)sender;

        //}

        //protected void gvDocuments_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        //{
        //    try
        //    {
        //        DocuSign docsign = new DocuSign();
        //        docsign.ContractID = Request.QueryString["id"];
        //        List<DocusignDocument> Documents = docsign.Documents;
        //        int docCount = 0;
        //        string msg = "";
        //        ASPxGridView gv = (ASPxGridView)sender;
        //        for (int i = 0; i < gv.VisibleRowCount; i++)
        //        {
        //            bool isToSend = false;
        //            try
        //            {
        //                isToSend = gv.Selection.IsRowSelected(i);
        //            }
        //            catch { }
        //            if (isToSend)
        //            {
        //                docCount++;
        //                DocusignDocument thisDocument = new DocusignDocument();
        //                thisDocument.RecipientEmail = "rigymmom@gmail.com";
        //                thisDocument.RecipientUserName = "TEST RECIPIENT";
        //                thisDocument.CEOEmail = "rigymmom@gmail.com";
        //                thisDocument.CEOName = "TEST CEO";
        //                string url = gv.GetRowValues(i, new string[] { "url" }).ToString();
        //                if (Server.MachineName == "CBIT-JODIE-01" || Request.Url.Host.Contains("azure"))
        //                {
        //                    url = "P:" + url.Replace("Documents/", "").Replace(" / ", "\\");
        //                }
        //                else
        //                {
        //                    url = url.Replace("http://localhost//Documents", "P://").Replace("//", "/").Replace("/", "\\");
        //                }
        //                //string dir = "P:\\Contract Database\\ContractDatabase\\" + Request.QueryString["id"].ToString(); //Server.MapPath("/Documents/"
        //                //string dir = "P:\\Contract Database\\ContractDatabase\\" + Request.QueryString["id"].ToString() + "\\Test2.pdf"; //Server.MapPath("/Documents/"
        //                string dir = url;
        //                thisDocument.DocumentName = dir;
        //                try
        //                {
        //                    using (var stream = File.Open(dir, FileMode.Open))
        //                    {
        //                        if (stream.Length > 0)
        //                        {
        //                            using (var fc = new MemoryStream())
        //                            {
        //                                stream.Seek(0, SeekOrigin.Begin);
        //                                stream.CopyTo(fc);
        //                                thisDocument.DocumentStream = fc;
        //                            }
        //                        }
        //                    }
        //                }
        //                catch (Exception ex)
        //                {
        //                    msg += ex.Message;
        //                    Server.ClearError();
        //                }
        //                docsign.Documents.Add(thisDocument);
        //            }
        //        }
        //        if (msg != "")
        //        {
        //            Message.Alert(msg);
        //        }
        //        else
        //        {
        //            if (docCount > 0)
        //            {
        //                docsign.EnvelopeSubject = "Contract Signature Required";
        //                msg = "Email sent to CEO for Signature";
        //                string status = docsign.CreateAndSend();
        //                if (status == "0")
        //                {
        //                    Message.Alert("Error sending email");
        //                }
        //                else
        //                {
        //                    Message.Alert(msg);
        //                }
        //            }
        //            else
        //            {
        //                Message.Alert("No documents have been selected.");
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message.Alert(ex.Message);
        //    }

        //}
    }

    public static class UploadControlHelper
    {
        const int DisposeTimeout = 5;
        const string TempDirectory = "~/UploadControl/Temp/";
        static readonly object storageListLocker = new object();
        static HttpContext Context { get { return HttpContext.Current; } }
        static string RootDirectory { get { return Context.Request.MapPath(TempDirectory); } }
        static IList<UploadedFilesStorage> uploadedFilesStorageList;
        static IList<UploadedFilesStorage> UploadedFilesStorageList
        {
            get
            {
                return uploadedFilesStorageList;
            }
        }
        static UploadControlHelper()
        {
            uploadedFilesStorageList = new List<UploadedFilesStorage>();
        }
        static string CreateTempDirectoryCore()
        {
            string uploadDirectory = Path.Combine(RootDirectory, Path.GetRandomFileName());
            Directory.CreateDirectory(uploadDirectory);

            return uploadDirectory;
        }

        public static UploadedFilesStorage GetUploadedFilesStorageByKey(string key)
        {
            lock (storageListLocker)
            {
                return GetUploadedFilesStorageByKeyUnsafe(key);
            }
        }

        static UploadedFilesStorage GetUploadedFilesStorageByKeyUnsafe(string key)
        {
            UploadedFilesStorage storage = UploadedFilesStorageList.Where(i => i.Key == key).SingleOrDefault();
            if (storage != null)
                storage.LastUsageTime = DateTime.Now;
            return storage;
        }

        public static string GenerateUploadedFilesStorageKey()
        {
            return Guid.NewGuid().ToString("N");
        }

        public static void AddUploadedFilesStorage(string key)
        {
            lock (storageListLocker)
            {
                UploadedFilesStorage storage = new UploadedFilesStorage
                {
                    Key = key,
                    Path = CreateTempDirectoryCore(),
                    LastUsageTime = DateTime.Now,
                    Files = new List<UploadedFileInfo>()
                };
                UploadedFilesStorageList.Add(storage);
            }
        }

        public static void RemoveUploadedFilesStorage(string key)
        {
            lock (storageListLocker)
            {
                UploadedFilesStorage storage = GetUploadedFilesStorageByKeyUnsafe(key);
                if (storage != null)
                {
                    Directory.Delete(storage.Path, true);
                    UploadedFilesStorageList.Remove(storage);
                }
            }
        }

        public static void RemoveOldStorages()
        {
            if (!Directory.Exists(RootDirectory))
                Directory.CreateDirectory(RootDirectory);
            lock (storageListLocker)
            {
                string[] existingDirectories = Directory.GetDirectories(RootDirectory);
                foreach (string directoryPath in existingDirectories)
                {
                    UploadedFilesStorage storage = UploadedFilesStorageList.Where(i => i.Path == directoryPath).SingleOrDefault();
                    if (storage == null || (DateTime.Now - storage.LastUsageTime).TotalMinutes > DisposeTimeout)
                    {
                        Directory.Delete(directoryPath, true);
                        if (storage != null)
                            UploadedFilesStorageList.Remove(storage);
                    }
                }
            }
        }

        public static UploadedFileInfo AddUploadedFileInfo(string key, string originalFileName)
        {
            UploadedFilesStorage currentStorage = GetUploadedFilesStorageByKey(key);
            UploadedFileInfo fileInfo = new UploadedFileInfo
            {
                FilePath = Path.Combine(currentStorage.Path, Path.GetRandomFileName()),
                OriginalFileName = originalFileName,
                UniqueFileName = GetUniqueFileName(currentStorage, originalFileName)
            };
            currentStorage.Files.Add(fileInfo);
            return fileInfo;
        }

        public static UploadedFileInfo GetDemoFileInfo(string key, string fileName)
        {
            UploadedFilesStorage currentStorage = GetUploadedFilesStorageByKey(key);
            return currentStorage.Files.Where(i => i.UniqueFileName == fileName).SingleOrDefault();
        }

        public static string GetUniqueFileName(UploadedFilesStorage currentStorage, string fileName)
        {
            string baseName = Path.GetFileNameWithoutExtension(fileName);
            string ext = Path.GetExtension(fileName);
            int index = 1;
            while (currentStorage.Files.Any(i => i.UniqueFileName == fileName))
                fileName = string.Format("{0} ({1}){2}", baseName, index++, ext);

            return fileName;
        }

    }
}