﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using DevExpress.Web;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.IO;

namespace Magnolia.app
{

    public partial class newPartyRequest : System.Web.UI.Page
    {
        protected void Page_PreLoad(object sender, EventArgs e)
        {
            UploadControlHelper.RemoveOldStorages();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ((Master)Page.Master).SetHeader = "Request New Party/Vendor";
            ((Master)Page.Master).SetHelpURL = "newPartyRequest.aspx";
            if (!IsPostBack)
            {
                SubmissionID = UploadControlHelper.GenerateUploadedFilesStorageKey();
                UploadControlHelper.AddUploadedFilesStorage(SubmissionID);
                Session["newPartyId"] = "";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                dsPartyRequest.Insert();
                string buttonText = "";
                if (hdnButton.Count > 0)
                {
                    buttonText = hdnButton.Get("button").ToString();
                }
                if (buttonText == "Send")
                {
                    SendRequest();
                }
                else
                {
                    formParty.FindItemOrGroupByName("PartyRequest").ClientVisible = false;
                    formParty.FindItemOrGroupByName("Upload").ClientVisible = true;
                }
                //DataSet ds = DataAccess.GetDataSet("usp_getChiefEmail", new string[] { "contractId" }, new string[] { hdnContractId.Get("contractId").ToString() });
                //if (ddlPaid.Value.ToString() == "1")
                //{
                //    body = "A request to add a new paid Party/Vendor has been submitted.";
                //    body += "<br/>Party/Vendor Name: " + txtBusinessName.Text;
                //    body += "<br/>Contact Name: " + txtContactName.Text;
                //    body += "<br/>Address: " + txtAddress.Text;
                //    body += "<br/>City: " + txtCity.Text;
                //    body += "<br/>State: " + txtState.Text;
                //    body += "<br/>Zip Code: " + txtZip.Text;
                //    body += "<br/>Phone Number: " + txtPhone.Text;
                //    body += "<br/>Email Address: " + txtEmail.Text;
                //    body += "<br/>Requestor: " + Session["UserName"].ToString();
                //    body += "<br/>Requestor Email: <a href='mailto:" + Session["email"].ToString() + "'>" + Session["email"].ToString() + "</a>";
                //    //Message.SendMail("purchasinggroup@mrhc.org", "Purchasing Group", "CMS - New Paid Party/Vendor Request", body);
                //}
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                {
                    Message.Alert("Error sending request: " + ex.Message);
                }
                else
                {
                    Message.LogError("newPartyRequest", "btnSave_Click", Session["UserName"].ToString(), ex.Message);
                }
            }
        }

        public void SendRequest()
        {
                string url = Request.Url.AbsoluteUri.Replace("newPartyRequest", "adminPartyRequests");
                string body = "A Request to add a new Party/Vendor has been Submitted by " + Session["UserName"] + " that requires your approval.";
                body += "<br/><br/>Please review the Party/Vendor reuqest here: <a href='" + url + "'>" + url + "</a>";
                Message.SendMail("CMSAdministrators@mrhc.org", "CMS Admin", "Pending New Party Request for " + txtBusinessName.Text.ToString() + " Action", body);
                Message.AlertAndRedirect("Request has been sent.", "contracts.aspx");
        }

        protected string SubmissionID
        {
            get
            {
                return HiddenField.Get("SubmissionID").ToString();
            }
            set
            {
                HiddenField.Set("SubmissionID", value);
            }
        }

        UploadedFilesStorage UploadedFilesStorage
        {
            get { return UploadControlHelper.GetUploadedFilesStorageByKey(SubmissionID); }
        }

        protected void ProcessSubmit(List<UploadedFileInfo> fileInfos)
        {
            foreach (UploadedFileInfo fileInfo in fileInfos)
            {
                // process uploaded files here
                string subFolder = Session["newPartyid"].ToString();
                string folder = "PARTY\\PENDING";
                string dir = System.Configuration.ConfigurationManager.AppSettings["documentPath"] + folder + "\\" + subFolder;
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
                byte[] fileContent = File.ReadAllBytes(fileInfo.FilePath);
                string fileName = fileInfo.OriginalFileName;
                using (var stream = File.Open(fileInfo.FilePath, FileMode.Open))
                {
                    if (stream.Length > 0)
                    {
                        bool fileExists = File.Exists(dir + "/" + fileName);
                        string contractIdDupe = "";
                        DataSet dsDupe = DataAccess.GetDataSet2("select c.contractId from documents d join contracts c on d.contractId = c.folder and c.contractTypeId = d.contractTypeId and d.filename = @filename", new string[] { "@filename" }, new string[] { fileName });
                        if (dsDupe != null && dsDupe.Tables.Count > 0 && dsDupe.Tables[0].Rows.Count > 0)
                        {
                            fileExists = true;
                            contractIdDupe = dsDupe.Tables[0].Rows[0]["contractId"].ToString();
                        }
                        if (fileExists == false || (fileExists == true && Convert.ToBoolean(Session["isAdmin"]) == true && cbReplaceFile.Checked))
                        {
                            if (fileExists)
                            {
                                File.Delete(dir + "/" + fileName);
                            }
                            using (var fc = File.Create(dir + "\\" + fileName))
                            {
                                stream.Seek(0, SeekOrigin.Begin);
                                stream.CopyTo(fc);
                            }
                            DataSet ds = DataAccess.GetDataSet("usp_addPendingDocument", new string[] { "@fileName", "@path", "@uploadId", "@uploadDocumentTypeId", "@requestId" },
                                new string[] { fileName, dir, Session["UserID"].ToString(), ddlUploadDocumentType.Value.ToString(), subFolder });
                            SendRequest();
                        }
                        else
                        {
                            UploadedFilesTokenBox.IsValid = false;
                            UploadedFilesTokenBox.ErrorText = "File already exists.";
                            if (contractIdDupe == "")
                            {
                                Message.Alert("This file already exists in the database. If this is the correct document, please rename the file and try again.");
                            }
                            else
                            {
                                Message.Alert("This file name already exists in the database for contract " + contractIdDupe + ". If this is the correct document, please rename the file and try again.");
                            }
                        }
                    }
                }
            }
        }

        protected void DocumentsUploadControl_FileUploadComplete(object sender, DevExpress.Web.FileUploadCompleteEventArgs e)
        {
            bool isSubmissionExpired = false;
            if (UploadedFilesStorage == null)
            {
                isSubmissionExpired = true;
                UploadControlHelper.AddUploadedFilesStorage(SubmissionID);
            }
            UploadedFileInfo tempFileInfo = UploadControlHelper.AddUploadedFileInfo(SubmissionID, e.UploadedFile.FileName);
            e.UploadedFile.SaveAs(tempFileInfo.FilePath);
            if (e.IsValid)
                e.CallbackData = tempFileInfo.UniqueFileName + "|" + isSubmissionExpired;
        }

        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            ValidateInputData();
            List<UploadedFileInfo> resultFileInfos = new List<UploadedFileInfo>();
            bool allFilesExist = true;
            if (UploadedFilesStorage == null)
                UploadedFilesTokenBox.Tokens = new TokenCollection();
            foreach (string fileName in UploadedFilesTokenBox.Tokens)
            {
                UploadedFileInfo demoFileInfo = UploadControlHelper.GetDemoFileInfo(SubmissionID, fileName);
                FileInfo fileInfo = new FileInfo(demoFileInfo.FilePath);
                if (fileInfo.Exists)
                {
                    resultFileInfos.Add(demoFileInfo);
                }
                else
                    allFilesExist = false;
            }

            if (allFilesExist && resultFileInfos.Count > 0)
            {
                ProcessSubmit(resultFileInfos);
                UploadControlHelper.RemoveUploadedFilesStorage(SubmissionID);
                ASPxEdit.ClearEditorsInContainer(FormLayout, true);
            }
            else
            {
                UploadedFilesTokenBox.ErrorText = "Submit failed because files have been removed from the server due to the 5 minute timeout.";
                UploadedFilesTokenBox.IsValid = false;
            }
        }

        void ValidateInputData()
        {
            bool isInvalid = ddlUploadDocumentType.Value == null || UploadedFilesTokenBox.Tokens.Count == 0;
            if (isInvalid)
                throw new Exception("Document Type and File Name are required.");
        }

        protected void dsPartyRequest_Inserted(object sender, SqlDataSourceStatusEventArgs e)
        {
            Session["newPartyId"] = e.Command.Parameters["@requestId"].Value.ToString();
        }
    }
}