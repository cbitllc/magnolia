﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;

namespace Magnolia.app
{
    public partial class contractQuestionnaire : System.Web.UI.Page
    {
        public DataSet dsExclusions;

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (ddlRouting.Value != null)
            //{
            //    //SqlDataSource1.SelectParameters["routingId"].DefaultValue = ddlRouting.Value.ToString();
            //    //Diagram.DataBind();
            //    popRouteLookup.ContentUrl = "diagramViewer.aspx?id=" + ddlRouting.Value.ToString();
            //}
            if (!IsPostBack)
            {
                ((Master)Page.Master).SetHeader = "New Request";
                ((Master)Page.Master).SetHelpURL = "contractQuestionnaire.aspx";
                formContract.FindItemOrGroupByName("ContractTerms").ClientVisible = false;
                formContract.FindItemOrGroupByName("Details").ClientVisible = false;
                formContract.FindItemOrGroupByName("Regulations").ClientVisible = false;
                formContract.FindItemOrGroupByName("Buttons").ClientVisible = false;
                deStartDate.Date = DateTime.Now;
                deEndDate.Date = DateTime.Now;
                Session["partyId"] = "0";
                if (hdnOIGCheck.Count > 0 && hdnOIGCheck.Contains("check"))
                {
                    hdnOIGCheck.Remove("check");
                }
                hdnOIGCheck.Add("check", "false");
                if (hdnOIGCount.Count > 0 && hdnOIGCount.Contains("count"))
                {
                    hdnOIGCount.Remove("count");
                }
                if(hdnContractId.Count> 0 && hdnContractId.Contains("contractId"))
                {
                    hdnContractId.Remove("contractId");
                }
                if (hdnDepartment.Count > 0 && hdnDepartment.Contains("department"))
                {
                    hdnDepartment.Remove("department");
                }
                if (hdnNewPartyName.Count > 0 && hdnNewPartyName.Contains("businessName"))
                {
                    hdnNewPartyName.Remove("businessName");
                }
                hdnOIGCount.Add("count", 0);
                hdnContractId.Add("contractId", 0);
                hdnDepartment.Add("department", "-1");
                hdnNewPartyName.Add("businessName", "");
                Session["businessName"] = "";
                ddlActions.DataBind();
            }
            if (Session["dsExclusions"] != null)
            {
                gvExclusions.DataSource = (DataSet)Session["dsExclusions"];
                gvExclusions.DataBind();
            }
            if (Session["businessName"] != null && Session["businessName"].ToString() != "")
            {
                txtPartyBusinessName.Text = Session["businessName"].ToString();
                formContract.FindItemOrGroupByName("Party").Visible = true;
            }
            if (ddlActions.Items.Count > 0)
            {
                DevExpress.Web.ListEditItem obj = ddlActions.Items.FindByText("CANCEL/INACTIVATE");
                if (obj == null)
                {
                    obj = ddlActions.Items.FindByValue(5);
                }
                if (obj != null)
                {
                    ddlActions.Items.RemoveAt(obj.Index);
                }
            }
        }

        protected void btnSaveParty_Click(object sender, EventArgs e)
        {
            try
            {
                dsParty.Insert();
                ddlParties.DataBind();
                formContract.FindItemOrGroupByName("Party").Visible = true;
                popParty.ShowOnPageLoad = false;
                lblPartyValidation.Visible = false;
                lblPartyValidation.Text = "";
                ddlDepartments.SelectedIndex = Convert.ToInt16(hdnDepartment.Get("department"));
                if (hdnSender.Get("sender").ToString().Contains("ddlParties"))
                {
                    ddlParties.SelectedIndex = ddlParties.Items.FindByText(txtPartyBusinessName.Text.ToUpper()).Index;
                    formContract.FindItemByFieldName("contractTypeId").ClientVisible = true;
                }
            }
            catch (Exception ex)
            {
                lblPartyValidation.Visible = true;
                if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                {
                    popParty.ShowOnPageLoad = true;
                    lblPartyValidation.Text="Business Name already exists.";
                }
                else
                {
                    lblPartyValidation.Text = ex.Message;
                    Message.LogError("contractQuestionnaire", "btnSaveParty_Click", Session["UserName"].ToString(), ex.Message);
                }
            }
        }

        protected void cbContract_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            if (e.Parameter == "Save")
            {
            }
            else
            {
                dsContract.SelectParameters["contractId"].DefaultValue = e.Parameter;
                formContract.DataBind();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                formContract.FindItemOrGroupByName("ContractTerms").Visible = true;
                formContract.FindItemOrGroupByName("Details").Visible = true;
                formContract.FindItemOrGroupByName("Regulations").Visible = true;
                formContract.FindItemOrGroupByName("Buttons").Visible = true;
                if (deEndDate.Date > deStartDate.Date.AddYears(5))
                {
                    Message.Alert("The Contract length cannot be greater than five years. The dates you have entered are greater than five years.");
                }
                else
                {
                    if (ddlAutoRenewal.Value.ToString() == "1" && ((spinDaysNoBreach.Value == null && spinDaysBreach.Value == null) || (spinDaysBreach.Number == 0 && spinDaysNoBreach.Number == 0)))
                    {
                        Message.Alert("If Contract is Autorenewal then either Days Breach or Days No Breach must be entered.");
                    }
                    else
                    {
                        if (memoDetails.Text.Length > 2000)
                        {
                            Message.Alert("Details cannot be longer than 2000 Characters. You have entered " + memoDetails.Text.Length);
                        }
                        else
                        {
                            if (Request.QueryString["id"] != null && Request.QueryString["id"] != "0")
                            {
                                dsContract.Update();
                            }
                            else
                            {
                                dsContract.Insert();
                            }
                            if (Convert.ToInt64(spinEstimatedTotalCost.Value) >= 50000 || Convert.ToInt64(spinAnnualCost.Value) >= 50000)
                            {
                                DataSet dsCost = DataAccess.GetDataSet("usp_logHighCostTracking", new string[] { "@contractId", "@userId", "@previousTotalCost", "@newTotalCost", "@previousAnnualCost", "@newAnnualCost" },
                                    new string[] { hdnContractId.Get("contractId").ToString(), Session["UserID"].ToString(), "0", spinEstimatedTotalCost.Value.ToString(), "0", spinAnnualCost.Value.ToString() });
                                string body = "A Contract has been added with an annual or total cost >= $50,000 by " + Session["UserName"] + ".";
                                body += "<br/><br/>You can view the current here: <a href='" + Request.Url.AbsoluteUri.Replace("Questionnaire", "Details") + "?id=" + hdnContractId.Get("contractId").ToString() + "'>" + Request.Url.AbsoluteUri.Replace("Questionnaire", "Details") + "?id=" + hdnContractId.Get("contractId").ToString() + "</a>";
                                Message.SendMail("", "", "Contract for " + ddlParties.Text + " has a High Annual Cost", body, null, "", "Contract Managers");
                            }
                            string isAdded = hdnOIGCheck.Get("check").ToString() == "true" ? "1" : "0";
                            DataSet ds = DataAccess.GetDataSet("usp_logOIG", new string[] { "@contractId", "@searchDate", "@searchTerm", "@searchResults", "@isAdded", "@addedBy" }, new string[] { hdnContractId.Get("contractId").ToString(), DateTime.Now.ToString(), Session["businessName"].ToString().ToUpper(), hdnOIGCount.Get("count").ToString(), isAdded, Session["UserID"].ToString() });
                            ClientScript.RegisterClientScriptBlock(Page.GetType(), "saved", "<script>alert('Contract has been saved.');window.location='contractDetails.aspx?id=" + hdnContractId.Get("contractId").ToString() + "&tab=Upload';</script>");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Message.Alert(ex.Message);
                Message.LogError("contractQuestionnaire", "btnSave_Click", Session["UserName"].ToString(), ex.Message);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            dsContract.Delete();
        }

        protected void dsContract_Updating(object sender, SqlDataSourceCommandEventArgs e)
        {
            lblQuery.Text = GetQuery(e.Command.CommandText, e.Command.Parameters);
        }

        protected void dsContract_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            lblQuery.Text = GetQuery(e.Command.CommandText, e.Command.Parameters);
        }

        protected void dsContract_Inserting(object sender, SqlDataSourceCommandEventArgs e)
        {
            lblQuery.Text = GetQuery(e.Command.CommandText,e.Command.Parameters);
        }

        private string GetQuery(string CommandQuery,System.Data.Common.DbParameterCollection parmCollection)
        {
            string query = CommandQuery;
            string parms = "";
            foreach (System.Data.SqlClient.SqlParameter parm in parmCollection)
            {
                parms += parm.ParameterName + " = ";
                if (parm.Value == null)
                {
                    parms += "null,";

                }
                else
                {
                    if (parm.DbType == System.Data.DbType.String || parm.DbType == System.Data.DbType.DateTime)
                    {
                        parms += "'" + parm.Value.ToString() + "',";
                    }
                    else
                    {
                        parms += parm.Value.ToString() + ",";
                    }
                }
            }
            query = query + " " + parms.Substring(0, parms.Length - 1);
            return query;
        }

        protected void dsContract_Inserted(object sender, SqlDataSourceStatusEventArgs e)
        {
            if (hdnContractId.Contains("contractId"))
            {
                hdnContractId.Remove("contractId");
            }
            if (e.Command.Parameters["@contractId"].Value != null)
            {
                hdnContractId.Add("contractId", e.Command.Parameters["@contractId"].Value.ToString());
                bool isValid = false;
                if (e.Command.Parameters["@isValid"].Value != null && e.Command.Parameters["@isValid"].Value != System.DBNull.Value)
                {
                    isValid = Convert.ToBoolean(e.Command.Parameters["@isValid"].Value);
                }
                //if (Session["UserID"].ToString() != "97")
                //{
                    //if (isValid == false && hdnContractId.Get("contractId").ToString() != "")
                    //{
                    //    DataSet ds = DataAccess.GetDataSet("usp_getChiefEmail", new string[] { "contractId" }, new string[] { hdnContractId.Get("contractId").ToString() });
                    //    string url = Request.Url.AbsoluteUri.Replace("Questionnaire", "Details") + "?id=" + hdnContractId.Get("contractId").ToString();
                    //    string body = "A Contract has been Submitted by " + Session["UserName"] + " that requires your approval before it can be processed.";
                    //    body += "<br/><br/>Please review the contract here: <a href='" + url + "'>" + url + "</a>";
                    //    Message.SendMail(ds.Tables[0].Rows[0]["emailAddress"].ToString(), ds.Tables[0].Rows[0]["fullName"].ToString(), "Pending Contract for " + ddlParties.Text + " Action", body);
                    //}
                    //else
                    //{
                        System.Data.DataSet ds = DataAccess.GetDataSet("usp_getRoutingAlert", new string[] { "@contractId" }, new string[] { hdnContractId.Get("contractId").ToString() });
                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            string body = "A Contract has been assigned to you for review.";
                            body += "<br/><br/>You can view the current status here: <a href='" + Request.Url.AbsoluteUri.Replace("Questionnaire", "Details") + "?id=" + hdnContractId.Get("contractId").ToString() + "'>" + Request.Url.AbsoluteUri.Replace("Questionnaire", "Details") + "?id=" + hdnContractId.Get("contractId").ToString() + "</a>";
                            Message.SendMail(ds.Tables[0].Rows[0]["userEmail"].ToString(), ds.Tables[0].Rows[0]["userName"].ToString(), "Contract for " + ddlParties.Text + " Action", body, null, "", ds.Tables[0].Rows[0]["userRole"].ToString());
                        } 
                    //}
                //
                if (e.Exception != null)
                {
                    sqlErrorMessage.Text = e.Exception.Message;
                    sqlErrorMessage.ClientVisible = true;
                    lblQuery.ClientVisible = true;
                    e.ExceptionHandled = true;
                    ClientScript.RegisterClientScriptBlock(Page.GetType(), "error", "<script>alert('Error Saving Contract.');</script>");
                }
            }
            else
            {
                Message.Alert("Error adding contract.");
            }
        }

        protected void dsContract_Selected(object sender, SqlDataSourceStatusEventArgs e)
        {
            if (e.Exception != null)
            {
                sqlErrorMessage.Text = e.Exception.Message;
                sqlErrorMessage.ClientVisible = true;
                lblQuery.ClientVisible = true;
                e.ExceptionHandled = true;
                ClientScript.RegisterClientScriptBlock(Page.GetType(), "error", "<script>alert('Error getting Contract.');window.location='contracts.aspx';</script>");
            }
        }

        protected void dsContract_Updated(object sender, SqlDataSourceStatusEventArgs e)
        {
            if (e.Exception != null)
            {
                sqlErrorMessage.Text = e.Exception.Message;
                sqlErrorMessage.ClientVisible = true;
                lblQuery.ClientVisible = true;
                e.ExceptionHandled = true;
                ClientScript.RegisterClientScriptBlock(Page.GetType(), "error", "<script>alert('Error Saving Contract.');window.location='contracts.aspx';</script>");
            }
        }

        protected void ddlParties_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if(!ddlParties.Text.ToUpper().Contains("ADD NEW"))
                {
                    ExclusionCheck(ddlParties.Text, false);
                    formContract.FindItemOrGroupByName("Party").Visible = true;
                    ddlContractTypes.ClientVisible = true;
                    txtBusinessName.Text = ddlParties.Text;
                }
                if (Session["partyId"].ToString() != "0")
                {
                    formContract.FindItemOrGroupByName("ContractTerms").ClientVisible = true;
                    formContract.FindItemOrGroupByName("Details").ClientVisible = true;
                    formContract.FindItemOrGroupByName("Regulations").ClientVisible = true;
                    formContract.FindItemOrGroupByName("Buttons").ClientVisible = true;
                }
                Session["partyId"] = ddlParties.Value.ToString();
            }
            catch (Exception ex)
            {
                Message.LogError("contractQuestionnaire", "ddlParties_SelectedIndexChanged", Session["UserName"].ToString(), ex.Message);
                throw (ex);
            }
        }

        protected void btnSearchExclusions_Click(object sender, EventArgs e)
        {
            try
            {
                ExclusionCheck(txtBusinessName.Text, true);
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                {
                    throw new Exception("Business Name already exists.");
                }
                else
                {
                    Message.LogError("contractQuestionnaire", "btnSearchExclusions_Click", Session["UserName"].ToString(), ex.Message);
                    throw (ex);
                }
            }
        }

        public void ExclusionCheck(string businessName, bool isNew)
        {
            try
            {
                dsExclusions = null;
                Session["dsExclusions"] = null;
                //string exactSearch = @"<exactName>" + txtBusinessName.Text.ToUpper() + "</exactName>";
                //string nameSearch = @"<name>" + txtBusinessName.Text.ToUpper() + "</name>";
                //string partialName = @"<partialName>" + businessName.Replace("&","") + "</partialName>";
                //string middleName = @"<middle>" + businessName.Replace("&", "") + "</middle>";
                //ParseSoap(exactSearch);
                //ParseSoap(nameSearch);
                //ParseSoap(partialName);
                //ParseSoap(middleName);
                DataSet dsTableExclusions = DataAccess.GetDataSet("usp_getExclusions", new string[] { "@businessName", "@isFuzzy" }, new string[] {businessName.Replace("(INACTIVE) ",""), cbFuzzy.Checked.ToString() });
                if (dsTableExclusions != null && dsTableExclusions.Tables.Count > 0 && dsTableExclusions.Tables[0].Rows.Count > 0)
                {
                    if (dsExclusions == null || dsExclusions.Tables.Count == 0 || dsExclusions.Tables[0].Rows.Count == 0)
                    {
                        dsExclusions = new DataSet();
                        DataTable dt = dsTableExclusions.Tables[0].Clone();
                        dsExclusions.Tables.Add(dt);
                    }
                    //else
                    //{
                    dsExclusions.Tables[0].Merge(dsTableExclusions.Tables[0], true, MissingSchemaAction.Ignore);
                    //}
                }
                if (hdnOIGCheck.Count > 0 && hdnOIGCheck.Contains("check"))
                {
                    hdnOIGCheck.Remove("check");
                }
                hdnOIGCheck.Add("check", "true");
                if (hdnOIGCount.Count > 0 && hdnOIGCount.Contains("count"))
                {
                    hdnOIGCount.Remove("count");
                }
                hdnOIGCount.Add("count", dsTableExclusions == null || dsTableExclusions.Tables.Count == 0 ? 0 : dsTableExclusions.Tables[0].Rows.Count);
                rbGSA.SelectedIndex = 0;
                if (dsTableExclusions != null && dsTableExclusions.Tables.Count > 0 && dsTableExclusions.Tables[0].Rows.Count > 0)
                {
                    DataTable dt = dsTableExclusions.Tables[0].DefaultView.ToTable(true, "Classification", "EntityName", "FirstName", "MiddleInitial", "LastName", "Address", "City", "State", "ZipCode", "ExclusionType", "ActiveDate");
                    dsTableExclusions.Tables.RemoveAt(0);
                    dsTableExclusions.Tables.Add(dt);
                    gvExclusions.DataSource = dsTableExclusions;
                    gvExclusions.DataBind();
                    Session["dsExclusions"] = dsTableExclusions;
                    divExclusionResults.Visible = true;
                    popExclusions.ShowOnPageLoad = true;
                }
                else
                {
                    if (isNew == true)
                    {
                        popParty.ShowOnPageLoad = true;
                        hdnNewPartyName.Set("businessName", businessName);
                        txtPartyBusinessName.Value = businessName;
                        txtPartyBusinessName.Text = businessName;
                        txtPartyContactName.Focus();
                        ddlDepartments.SelectedIndex = Convert.ToInt16(hdnDepartment.Get("department"));
                    }
                    popExclusions.ShowOnPageLoad = false;
                    //ddlParties.SelectedIndex = Convert.ToInt16(hdnDepartment.Get("department"));
                    formContract.FindItemOrGroupByName("Party").Visible = true;
                }
                Session["businessName"] = businessName;
                DataSet ds = DataAccess.GetDataSet("usp_logOIG", new string[] { "@contractId", "@searchDate", "@searchTerm", "@searchResults", "@isAdded", "@addedBy" }, new string[] { "0", DateTime.Now.ToString(), businessName, hdnOIGCount.Get("count").ToString(), "0", Session["UserID"].ToString() });
            }
            catch (Exception ex)
            {
                Message.LogError("contractQuestionnaire", "ExclusionCheck", Session["UserID"].ToString(), ex.Message);
                if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                {
                    throw new Exception("Business Name already exists.");
                }
                else
                {
                    throw;
                }
            }
        }

        public string GetSearchResults(string xml)
        {
            string result = "";
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                   | SecurityProtocolType.Tls11
                   | SecurityProtocolType.Tls12
                   | SecurityProtocolType.Ssl3;
                HttpWebRequest ws = (HttpWebRequest)WebRequest.Create("https://gw.sam.gov/SAMWS/1.0/ExclusionSearch");
                ws.ContentType = "text/xml;charset=\"utf-8\"";
                ws.Accept = "text/xml";
                ws.Method = "POST";
                
                XmlDocument soapEnvelopeXml = new XmlDocument();
                //xml = @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:sam=""http://www.sam.gov""> <soapenv:Header/> <soapenv:Body> <sam:doSearch> <exclusionSearchCriteria> <exactName>Bad Medicine Group, Inc.</exactName> </exclusionSearchCriteria> </sam:doSearch> </soapenv:Body> </soapenv:Envelope>";
                soapEnvelopeXml.LoadXml(xml);
                try
                {
                    using (Stream stream = ws.GetRequestStream())
                    {
                        try
                        {
                            soapEnvelopeXml.Save(stream);
                        }
                        catch (Exception ex)
                        {
                            Message.LogError("contractQuestionnaire", "GetSearchResults-Request Stream", Session["UserName"].ToString(), ex.Message);
                            throw new Exception("Error in Request Stream. " + ex.Message);
                        } 
                    }
                    try
                    {
                        using (WebResponse response = ws.GetResponse())
                        {
                            try
                            {
                                using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                                {
                                    result = rd.ReadToEnd();
                                }
                            }
                            catch (Exception ex)
                            {
                                Message.LogError("contractQuestionnaire", "GetSearchResults-Response Stream", Session["UserName"].ToString(), ex.Message);
                                throw new Exception("Error in Response Stream. " + ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Message.LogError("contractQuestionnaire", "GetSearchResults-Response", Session["UserName"].ToString(), ex.Message);
                        throw new Exception("Error Getting Response. " + ex.Message);
                    } 
                }
                catch (Exception ex)
                {
                    string searchString = ddlParties.Text.ToUpper();
                    Message.LogError("contractQuestionnaire", "GetSearchResults", Session["UserID"].ToString(), "Cannot connect to SAM Exclusion service. " + ex.Message);
                    Message.SendMail("CMSAdministrators@mrhc.org", "IT Alerts", "Contract Management System - SAM Exclusion Connection Error", "There was an error when connecting to the SAM Exclusion Web Service for vendor " + searchString + " at https://gw.sam.gov/SAMWS/1.0/ExclusionSearch. <br/><br/>" + xml);
                    Message.Alert(ex.Message);
                    Server.ClearError();
                }
            }
            catch (Exception ex)
            {
                Message.LogError("contractQuestionnaire", "GetSearchResults", Session["UserID"].ToString(), ex.Message);
                Message.Alert(ex.Message);
                Server.ClearError();
            }
            return result;
        }

        public int ParseSoap(string searchType)
        {
            try
            {
                string xml = @"<soapenv:Envelope xmlns:soapenv = ""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:sam=""http://www.sam.gov"">";
                xml += @"<soapenv:Header></soapenv:Header><soapenv:Body><sam:doSearch><exclusionSearchCriteria>";
                xml += @"%SearchTerms%";
                xml += @"</exclusionSearchCriteria></sam:doSearch></soapenv:Body></soapenv:Envelope>";
                string respString = GetSearchResults(xml.Replace("%SearchTerms%", searchType));
                if (respString.Length > 0)
                {
                    XDocument xDoc = XDocument.Parse(respString);
                    IEnumerable<XElement> responses = xDoc.Elements().Elements().Elements().Elements();
                    IEnumerable<XElement> elements = from c in xDoc.Descendants("excludedEntity") select c;
                    IEnumerable<XElement> successful = from c in xDoc.Descendants("successful") select c;
                    IEnumerable<XElement> count = from c in xDoc.Descendants("count") select c;
                    IEnumerable<XElement> exclusionType = from c in xDoc.Descendants("exclusionType") select c;
                    if (elements.Count() > 0)
                    {
                        AddExclusionRows(elements);
                    }
                    if (successful.FirstOrDefault().Value == "true" && Convert.ToInt16(count.FirstOrDefault().Value) > 0)
                    {
                        if (exclusionType.FirstOrDefault().Value != "")
                        {
                            return 1;
                        }
                    }
                }
                else
                {
                    Message.Alert("No results were returned from the SAM Exclusion WebService.");
                }
            }
            catch (Exception ex)
            {
                Message.LogError("contractQuestionnaire", "ParseSoap", Session["UserID"].ToString(), ex.Message);
                Message.Alert(ex.Message);
            }
            return 0;
        }

        public void AddExclusionRows(IEnumerable<XElement> elements)
        {
            try
            {
                if (dsExclusions == null)
                {
                    dsExclusions = new DataSet();
                    DataTable dt = new DataTable();
                    dt.Columns.Add(new DataColumn() { ColumnName = "Classification", DataType = typeof(String) });
                    dt.Columns.Add(new DataColumn() { ColumnName = "EntityName", DataType = typeof(String) });
                    dt.Columns.Add(new DataColumn() { ColumnName = "FirstName", DataType = typeof(String) });
                    dt.Columns.Add(new DataColumn() { ColumnName = "MiddleInitial", DataType = typeof(String) });
                    dt.Columns.Add(new DataColumn() { ColumnName = "LastName", DataType = typeof(String) });
                    dt.Columns.Add(new DataColumn() { ColumnName = "Address", DataType = typeof(String) });
                    dt.Columns.Add(new DataColumn() { ColumnName = "City", DataType = typeof(String) });
                    dt.Columns.Add(new DataColumn() { ColumnName = "State", DataType = typeof(String) });
                    dt.Columns.Add(new DataColumn() { ColumnName = "ZipCode", DataType = typeof(String) });
                    dt.Columns.Add(new DataColumn() { ColumnName = "ExclusionType", DataType = typeof(String) });
                    dt.Columns.Add(new DataColumn() { ColumnName = "ActiveDate", DataType = typeof(String) });
                    //dt.Columns.Add(new DataColumn() { ColumnName = "TerminationDate", DataType = typeof(String) });
                    dsExclusions.Tables.Add(dt);
                }
                foreach (XElement element in elements)
                {
                    DataRow dr = dsExclusions.Tables[0].NewRow();
                    XElement classification = (from c in element.Descendants("classification") select c).FirstOrDefault();
                    dr["Classification"] = classification == null ? "" : classification.Value.ToString();
                    XElement name = (from c in element.Descendants("name") select c).FirstOrDefault();
                    dr["EntityName"] = name == null ? "" : name.Value.ToString();
                    XElement first = (from c in element.Descendants("first") select c).FirstOrDefault();
                    dr["FirstName"] = first == null ? "" : first.Value.ToString();
                    XElement middle = (from c in element.Descendants("middle") select c).FirstOrDefault();
                    dr["MiddleInitial"] = middle == null ? "" : middle.Value.ToString();
                    XElement last = (from c in element.Descendants("last") select c).FirstOrDefault();
                    dr["LastName"] = last == null ? "" : last.Value.ToString();
                    XElement street = (from c in element.Descendants("street1") select c).FirstOrDefault();
                    dr["Address"] = street == null ? "" : street.Value.ToString();
                    XElement city = (from c in element.Descendants("city") select c).FirstOrDefault();
                    dr["City"] = city == null ? "" : city.Value.ToString();
                    XElement state = (from c in element.Descendants("state") select c).FirstOrDefault();
                    dr["State"] = state == null ? "" : state.Value.ToString();
                    XElement zip = (from c in element.Descendants("zip") select c).FirstOrDefault();
                    dr["ZipCode"] = zip == null ? "" : zip.Value.ToString();
                    XElement exclusionType = (from c in element.Descendants("exclusionType") select c).FirstOrDefault();
                    dr["ExclusionType"] = exclusionType == null ? "" : exclusionType.Value.ToString();
                    XElement activeDate = (from c in element.Descendants("activeDate") select c).FirstOrDefault();
                    dr["ActiveDate"] = activeDate == null || activeDate.Value.ToString() == "" ? "" : Convert.ToDateTime(activeDate.Value).ToString("MM/dd/yyyy");
                    dsExclusions.Tables[0].Rows.Add(dr);
                }
            }
            catch (Exception ex)
            {
                Message.LogError("contractQuestionnaire", "AddExclusionRows", Session["UserID"].ToString(), ex.Message);
                Message.Alert(ex.Message);
            }
        }

        protected void btnExclusionSave_Click(object sender, EventArgs e)
        {
            if (ddlParties.Text.ToUpper().Contains("ADD NEW PARTY"))
            {
                popParty.ShowOnPageLoad = true;
            }
            txtPartyBusinessName.Text = txtBusinessName.Text;
            hdnNewPartyName.Set("businessName", txtBusinessName.Text);
            formContract.FindItemOrGroupByName("Party").Visible = true;
            txtPartyContactName.Focus();
            popExclusions.ShowOnPageLoad = false;
            ddlDepartments.SelectedIndex = Convert.ToInt16(hdnDepartment.Get("department"));
            Session["businessName"] = txtPartyBusinessName.Text;
        }

        protected void popParty_Load(object sender, EventArgs e)
        {
            txtPartyBusinessName.Text = Session["businessName"].ToString();
        }

        protected void ddlRouting_SelectedIndexChanged(object sender, EventArgs e)
        {
            //SqlDataSource1.SelectParameters["routingId"].DefaultValue = ddlRouting.Value.ToString();
            //Diagram.DataBind();
            //popRouteLookup.ContentUrl = "diagramViewer.aspx?id=" + ddlRouting.Value.ToString();
        }

        private void Page_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError();
            Message.Alert(exc.Message);

            // Handle specific exception.
            if (exc is System.Web.HttpUnhandledException)
            {
                Message.LogError("contractQuestionnaire", "Page_Error", Session["UserID"].ToString(), exc.Message);
                Message.Alert("An error occurred on this page. " + exc.Message);
            }
            // Clear the error from the server.
            Server.ClearError();
        }

        //protected void ddlRoutesPopup_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    DevExpress.Web.ASPxComboBox ddl = (DevExpress.Web.ASPxComboBox)sender;
        //    if (ddl.Value != null)
        //    {
        //        SqlDataSource1.SelectParameters["routingId"].DefaultValue = ddl.Value.ToString();
        //        Diagram.DataBind();
        //        dsRoute.SelectParameters["routingId"].DefaultValue = ddl.Value.ToString();
        //        gvRoutes.DataBind();
        //        if (gvRoutes.VisibleRowCount > 0)
        //        {
        //            try
        //            {
        //                string description = gvRoutes.GetRowValues(0, new string[] { "description" }).ToString();
        //                txtRouteDescription.Text = description;
        //            }
        //            catch (Exception ex)
        //            {
        //                Message.Alert("Error getting routing description: " + ex.Message);
        //            }
        //        }
        //    }
        //}
    }
}