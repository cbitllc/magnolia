﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;

namespace Magnolia.app
{
    public partial class adminSignatureTracking : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ((Master)Page.Master).SetHeader = "Admin Signature Tracking";
            ((Master)Page.Master).SetHelpURL = "admin.aspx";
            if (Session["isAdmin"] == null || Session["isAdmin"] == System.DBNull.Value || Convert.ToBoolean(Session["isAdmin"]) == false)
            {
                Response.Redirect("default.aspx");
            }
        }

        protected void btnResend_Click(object sender, EventArgs e)
        {
            ASPxButton btn = (ASPxButton)sender;
            GridViewDataItemTemplateContainer container = (GridViewDataItemTemplateContainer)btn.NamingContainer;
            int rowId = container.VisibleIndex;
            string msg = "";
            string envelopeId = gvTracking.GetRowValues(rowId, "envelopeId").ToString();
            DataSet ds = DataAccess.GetDataSet("usp_getDocusignTrackingByEnvelope", new string[] { "@envelopeId" }, new string[] { envelopeId });
            if (ds != null && ds.Tables.Count >0 && ds.Tables[0].Rows.Count>0)
            {
                DocuSign docsign = new DocuSign();
                docsign.RecipientEmail = ds.Tables[0].Rows[0]["recipientEmail"].ToString();
                docsign.RecipientUserName = ds.Tables[0].Rows[0]["fullName"].ToString();
                docsign.ContractID = ds.Tables[0].Rows[0]["contractId"].ToString();
                docsign.CEOEmail = Session["ceoEmail"].ToString(); // ds.Tables[0].Rows[0]["recipientEmail"].ToString();
                docsign.CEOName = "CEO"; //ds.Tables[0].Rows[0]["fullName"].ToString();
                // string dir = ds.Tables[0].Rows[0]["fileName"].ToString()
                //string contractType = ds.Tables[0].Rows[0]["contractType"].ToString();
                //string folder = "";
                //switch (contractType)
                //{
                //    case "EDUCATION": folder = "EmployeeEducationAssistanceDB";
                //        break;
                //    case "LEASE/RENT": folder = "LeaseDatabase";
                //        break;
                //    case "PROVIDER": folder = "PhysicianContractDatabase";
                //        break;
                //    default: folder = "ContractDatabase";
                //        break;
                //}
                //string dir = ds.Tables[0].Rows[0]["fileName"].ToString();// ConfigurationManager.AppSettings["documentPath"] + folder + "P:\\Contract Database\\ContractDatabase\\1\\CM_01-3M.2018.pdf"; //Server.MapPath("/Documents/"
                docsign.DocumentName = ds.Tables[0].Rows[0]["fileName"].ToString();
                using (var stream = System.IO.File.Open(docsign.DocumentName, System.IO.FileMode.Open))
                {
                    if (stream.Length > 0)
                    {
                        using (var fc = new System.IO.MemoryStream())
                        {
                            stream.Seek(0, System.IO.SeekOrigin.Begin);
                            stream.CopyTo(fc);
                            docsign.DocumentStream = fc;
                        }
                    }
                }
                docsign.EnvelopeSubject = "Contract Signature Required";
                msg = "Email sent to CEO for Signature";
                string status = docsign.CreateAndSend();
                if (status == "0")
                {
                    Message.Alert("Error sending email");
                }
                else
                {
                    Message.Alert(msg);
                }
            }
            else
            {
                Message.Alert("Envelope not found.");
            }
            //string dir = "P:\\Contract Database\\ContractDatabase\\" + Request.QueryString["id"].ToString(); //Server.MapPath("/Documents/"
        }

        protected void btnRetrieve_Click(object sender, EventArgs e)
        {
            ASPxButton btn = (ASPxButton)sender;
            string envelopeId = btn.CommandArgument;
            if (envelopeId != "")
            {
                DocuSign doc = new DocuSign();
                if (doc.CheckStatusByEnvelopeId(envelopeId)){
                    Message.Alert("Document imported from Docusign");
                    gvTracking.DataBind();
                }
                else
                {
                    Message.Alert("Document has not yet been completed.");
                }
            }
        }

        protected void btnResend_Init(object sender, EventArgs e)
        {

        }

        protected void btnRetrieve_Init(object sender, EventArgs e)
        {
            ASPxButton btn = (ASPxButton)sender;
            GridViewDataItemTemplateContainer container = (GridViewDataItemTemplateContainer)btn.NamingContainer;
            int rowId = container.VisibleIndex;
            bool isSigned = false;
            try
            {
                isSigned = Convert.ToBoolean(gvTracking.GetRowValues(rowId, "isEmailSigned"));
            }
            finally
            {
                btn.Visible = isSigned;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            dsDocusignConfig.Update();
        }
    }
}