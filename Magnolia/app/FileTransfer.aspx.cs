﻿using System;
using System.Web.UI;
using System.IO;
using System.Data;

namespace Magnolia.app
{
    public partial class FileTransfer : System.Web.UI.Page
    {
        const string contractPath = @"P:\Contract Database\ContractDatabase\";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ((Master)Page.Master).SetHeader = "Existing Contract Processing";
                txtPath.Text = contractPath;
            }
        }

        protected void btnTransfer_Click(object sender, EventArgs e)
        {
            try
            {
                string directory = contractPath;
                if (txtPath.Text != "")
                {
                    directory = txtPath.Text;
                }
                string[] folders = Directory.GetDirectories(directory);
                foreach (string folder in folders)
                {
                    string[] fileNames = Directory.GetFiles(folder);
                    string[] folderParts = folder.Split('\\');
                    string folderPart = folderParts[folderParts.Length - 1];
                    string path = contractPath + folderPart + @"\";
                    foreach (string fileName in fileNames)
                    {
                        string dir = folder; //+ folderPart; //Server.MapPath("/Documents/" 
                        //if (Directory.Exists(dir))
                        //{
                        //    Directory.CreateDirectory(dir);
                        //}
                        //string allDir = Server.MapPath("/Documents/AllFiles/");
                        //if (!Directory.Exists(allDir))
                        //{
                        //    Directory.CreateDirectory(allDir);
                        //}
                        string[] fileParts = fileName.Split('\\');
                        string filePart = fileParts[fileParts.Length - 1];
                        string newPath = folder + @"\" + filePart;
                        //if (!File.Exists(newPath))
                        //{
                        //    File.Copy(path + filePart, newPath);
                        //}
                        //if (!File.Exists(allDir + filePart))
                        //{
                        //    File.Copy(path + filePart, allDir + filePart);
                        //}
                        DataSet ds = DataAccess.GetDataSet("usp_addDocument",
                            new string[] { "@fileName", "@path", "@description", "@contractId", "@uploadId" },
                            new string[] { filePart, folder, "INITIAL TRANSFER", folderPart, "22" });
                    }
                }
                Message.Alert("Process has completed.");
            }
            catch (Exception ex)
            {
                Message.Alert(ex.Message);
            }
        }
    }
}