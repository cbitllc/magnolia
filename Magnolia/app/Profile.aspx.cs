﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Magnolia.app
{
    public partial class Profile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Master.SetHeader = "My Profile";
            ((Master)Page.Master).SetHelpURL = "Profile.aspx";
        }

        protected void btnSaveUser_Click(object sender, EventArgs e)
        {
            dsUser.Update();
            Message.Alert("Changes to your profile have been saved.");
        }

        protected void dsUser_Updating(object sender, SqlDataSourceCommandEventArgs e)
        {
            if (Convert.ToDateTime(e.Command.Parameters["@bypassStart"].Value) < Convert.ToDateTime("1/1/1900"))
            {
                e.Command.Parameters["@bypassStart"].Value = null;
            }
            if (Convert.ToDateTime(e.Command.Parameters["@bypassEnd"].Value) < Convert.ToDateTime("1/1/1900"))
            {
                e.Command.Parameters["@bypassEnd"].Value = null;
            }
        }
    }
}