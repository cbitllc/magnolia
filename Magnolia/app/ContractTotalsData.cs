﻿namespace Magnolia.app
{
    public class ContractTotalsData
    {
        public string completionPercent { get; set; }
        public string completionCount { get; set; }
        public string incompletionCount { get; set; }
        public string totalInspectionCount { get; set; }
        public string completeCurrentUser { get; set; }
        public string incompleteCurrentUser { get; set; }
        public string totalCurrentUser { get; set; }
        public string percentCurrentUser { get; set; }
    }
}