﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OCR.aspx.cs" Inherits="Magnolia.app.OCR" MasterPageFile="~/app/Master.master" %>
<%@ Register Assembly="DevExpress.Web.v20.2, Version=20.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <dx:ASPxLabel ID="lblContractId" runat="server" Text="Enter Contract Number: "></dx:ASPxLabel>
    <dx:ASPxTextBox ID="txtContractId" runat="server"></dx:ASPxTextBox>
    <script type="text/javascript">
        var uploadInProgress = false,
            submitInitiated = false,
            uploadErrorOccurred = false;
            uploadedFiles = [];
        function onFileUploadComplete(s, e) {
            var callbackData = e.callbackData.split("|"),
                uploadedFileName = callbackData[0],
                isSubmissionExpired = callbackData[1] === "True";
            uploadedFiles.push(uploadedFileName);
            if(e.errorText.length > 0 || !e.isValid)
                uploadErrorOccurred = true;
            if(isSubmissionExpired && UploadedFilesTokenBox.GetText().length > 0) {
                var removedAfterTimeoutFiles = UploadedFilesTokenBox.GetTokenCollection().join("\n");
                alert("The following files have been removed from the server due to the defined 5 minute timeout: \n\n" + removedAfterTimeoutFiles);
                UploadedFilesTokenBox.ClearTokenCollection();
            }
        }
        function onFileUploadStart(s, e) {
            uploadInProgress = true;
            uploadErrorOccurred = false;
            UploadedFilesTokenBox.SetIsValid(true);
        }
        function onFilesUploadComplete(s, e) {
            uploadInProgress = false;
            for(var i = 0; i < uploadedFiles.length; i++)
                UploadedFilesTokenBox.AddToken(uploadedFiles[i]);
            updateTokenBoxVisibility();
            uploadedFiles = [];
            if(submitInitiated) {
                SubmitButton.SetEnabled(true);
                SubmitButton.DoClick();
            }
        }
        function onSubmitButtonInit(s, e) {
            s.SetEnabled(true);
        }
        function onSubmitButtonClick(s, e) {
            ASPxClientEdit.ValidateGroup();
            if(!formIsValid())
                e.processOnServer = false;
            else if(uploadInProgress) {
                s.SetEnabled(false);
                submitInitiated = true;
                e.processOnServer = false;
            }
        }
        function onTokenBoxValidation(s, e) {
            var isValid = DocumentsUploadControl.GetText().length > 0 || UploadedFilesTokenBox.GetText().length > 0;
            e.isValid = isValid;
            if(!isValid) {
                e.errorText = "No files have been uploaded. Upload at least one file.";
            }
        }
        function onTokenBoxValueChanged(s, e) {
            updateTokenBoxVisibility();
        }
        function updateTokenBoxVisibility() {
            var isTokenBoxVisible = UploadedFilesTokenBox.GetTokenCollection().length > 0;
            UploadedFilesTokenBox.SetVisible(isTokenBoxVisible);
        }
        function formIsValid() {
            return !ValidationSummary.IsVisible() && DescriptionTextBox.GetIsValid() && UploadedFilesTokenBox.GetIsValid() && !uploadErrorOccurred;
        }
        </script>
    <dx:ASPxHiddenField runat="server" ID="HiddenField" ClientInstanceName="HiddenField" />
    <dx:ASPxFormLayout ID="FormLayout" runat="server" Width="100%" ColCount="1" UseDefaultPaddings="false">
        <Items>
            <dx:LayoutGroup ShowCaption="False" GroupBoxDecoration="None" Width="50%" UseDefaultPaddings="false" ColumnCount="1">
                <Items>
                    <dx:LayoutItem Caption="Description" CaptionStyle-Font-Bold="true" CaptionSettings-Location="Top" ColumnSpan="1">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer>
                                <dx:ASPxTextBox runat="server" ID="DescriptionTextBox" ClientInstanceName="DescriptionTextBox" NullText="Enter Description..."
                                    Width="200px" EncodeHtml="true">
                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="Text" ErrorTextPosition="Bottom"><%-- ValidationGroup="DescriptionValidation"--%>
                                        <RequiredField IsRequired="True" ErrorText="Description is required" />
                                    </ValidationSettings>
                                </dx:ASPxTextBox>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutGroup Caption="Documents" ColumnCount="1">
                        <Items>
                            <dx:LayoutItem ShowCaption="False" ColumnSpan="1">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                        <div id="dropZone">
                                            <dx:ASPxUploadControl runat="server" ID="DocumentsUploadControl" ClientInstanceName="DocumentsUploadControl" Width="100%"
                                                AutoStartUpload="true" ShowProgressPanel="True" ShowTextBox="false" BrowseButton-Text="Add documents" FileUploadMode="OnPageLoad"
                                                OnFileUploadComplete="DocumentsUploadControl_FileUploadComplete">
                                                <AdvancedModeSettings EnableMultiSelect="true" EnableDragAndDrop="true" ExternalDropZoneID="dropZone" />
                                                <ValidationSettings MaxFileSize="4194304"></ValidationSettings>
                                                <ClientSideEvents
                                                    FileUploadComplete="onFileUploadComplete"
                                                    FilesUploadComplete="onFilesUploadComplete"
                                                    FilesUploadStart="onFileUploadStart" />
                                            </dx:ASPxUploadControl>
                                            <br />
                                            <dx:ASPxTokenBox runat="server" Width="100%" ID="UploadedFilesTokenBox" ClientInstanceName="UploadedFilesTokenBox"
                                                NullText="Select the documents to submit" AllowCustomTokens="false" ClientVisible="false">
                                                <ClientSideEvents Init="updateTokenBoxVisibility" ValueChanged="onTokenBoxValueChanged" Validation="onTokenBoxValidation" />
                                                <ValidationSettings EnableCustomValidation="true" />
                                            </dx:ASPxTokenBox>
                                            <br />
                                            <p class="Note">
                                                <dx:ASPxLabel ID="MaxFileSizeLabel" runat="server" Text="Maximum file size: 4 MB." Font-Size="8pt" />
                                            </p>
                                            <dx:ASPxValidationSummary runat="server" ID="ValidationSummary" ClientInstanceName="ValidationSummary" RenderMode="Table" Width="250px" ShowErrorAsLink="false" />
                                        </div>
                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                        </Items>
                    </dx:LayoutGroup>
                    <dx:LayoutItem ShowCaption="False" HorizontalAlign="Right" ColumnSpan="1">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer>
                                <dx:ASPxButton runat="server" ID="SubmitButton" ClientInstanceName="SubmitButton" Text="Submit" AutoPostBack="True"
                                    OnClick="SubmitButton_Click"  ClientEnabled="false"><%-- ValidateInvisibleEditors="true"--%>
                                    <ClientSideEvents Init="onSubmitButtonInit" Click="onSubmitButtonClick" />
                                </dx:ASPxButton>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:EmptyLayoutItem Height="5" />
                </Items>
        </dx:LayoutGroup>
            <dx:LayoutGroup GroupBoxDecoration="None" ShowCaption="False" Name="ResultGroup" Visible="false" Width="50%" UseDefaultPaddings="false">
                <Items>
                    <dx:LayoutItem ShowCaption="False">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer>
                                <dx:ASPxRoundPanel ID="RoundPanel" runat="server" HeaderText="Uploaded files" Width="100%">
                                    <PanelCollection>
                                        <dx:PanelContent>
                                            <b>Description:</b>
                                            <dx:ASPxLabel runat="server" ID="DescriptionLabel" />
                                            <br />
                                            <br />
                                            <dx:ASPxListBox ID="SubmittedFilesListBox" runat="server" Width="100%" Height="150px">
                                                <ItemStyle CssClass="ResultFileName" />
                                                <Columns>
                                                    <dx:ListBoxColumn FieldName="OriginalFileName" />
                                                    <dx:ListBoxColumn FieldName="FileSize" Width="15%"/>
                                                </Columns>
                                            </dx:ASPxListBox>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                </dx:ASPxRoundPanel>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                </Items>
            </dx:LayoutGroup>
        </Items>
        <SettingsAdaptivity AdaptivityMode="SingleColumnWindowLimit" SwitchToSingleColumnAtWindowInnerWidth="600" />
    </dx:ASPxFormLayout>     
    <asp:SqlDataSource ID="dsDocuments" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>"
        SelectCommand="select d.fileName,d.path,d.documentId,d.description,d.path + d.fileName url,dt.documentType,dt.documentTypeId,
        case when dt.documentTypeId = 1 then '/images/Word.png' when dt.documentTypeId = 2 then '/images/Excel.png'
        when dt.documentTypeId = 3 then '/images/PDF.png' else '/images/Other.png' end as ImageUrl 
        from documents d join documentTypes dt on d.documentTypeId = dt.documentTypeId where contractId=@contractId"
        InsertCommand="usp_addDocument" InsertCommandType="StoredProcedure"
        DeleteCommand="delete from documents where documentId=@documentId" OnDeleting="dsDocuments_Deleting">
        <SelectParameters>
            <asp:ControlParameter ControlID="txtContractId" PropertyName="Text" Name="contractId" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="fileName" />
            <asp:Parameter Name="path" />
            <asp:Parameter Name="description" />
            <asp:ControlParameter ControlID="txtContractId" PropertyName="Text" Name="contractId" />
            <asp:SessionParameter Name="uploadId" SessionField="UserID" />
        </InsertParameters>
        <DeleteParameters>
            <asp:Parameter Name="documentId" />
        </DeleteParameters>
    </asp:SqlDataSource>
</asp:Content>