﻿using System;
using System.Web.UI;

namespace Magnolia.app
{
    public partial class adminOIGLog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ((Master)Page.Master).SetHeader = "Admin OIG Log";
            ((Master)Page.Master).SetHelpURL = "admin.aspx";
            if (Session["isAdmin"] == null || Session["isAdmin"] == System.DBNull.Value || Convert.ToBoolean(Session["isAdmin"]) == false)
            {
                Response.Redirect("default.aspx");
            }
            System.Data.DataSet ds = DataAccess.GetDataSet("usp_getSAMApiKey", null, null);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                txtAPI.Text = ds.Tables[0].Rows[0]["apiKey"].ToString();
            }
        }

        protected void gvOIG_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.Caption == "Party")
            {
                string company = gvOIG.GetRowValues(e.VisibleIndex, new string[] { "businessName" }).ToString();
                if (company == "Admin" || e.CellValue.ToString() == "0")
                {
                    e.Cell.Controls.Clear();
                    DevExpress.Web.ASPxLabel lbl = new DevExpress.Web.ASPxLabel();
                    lbl.Text = "Admin";
                    e.Cell.Controls.Add(lbl);
                }
            }
        }

        private void Page_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError();
            Message.Alert(exc.Message);

            // Handle specific exception.
            if (exc is System.Web.HttpUnhandledException)
            {
                Message.Alert("An error occurred on this page. Please verify your information to resolve the issue.");
            }
            // Clear the error from the server.
            Server.ClearError();
        }

        protected void btnAPI_Click(object sender, EventArgs e)
        {
            //Update API
            System.Data.DataSet ds = DataAccess.GetDataSet2("update SAMApi set apiKey=@apiKey", new string[] { "@apiKey" },new string[] { txtAPI.Text});
            Message.Alert("API Key has been updated.");
        }

        protected void cbAPI_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            try {
                cbAPI.JSProperties["cppdfxx"] = "";
                System.Data.DataSet ds = DataAccess.GetDataSet2("update SAMApi set apiKey=@apiKey", new string[] { "@apiKey" }, new string[] { e.Parameter.ToString() });
                cbAPI.JSProperties["cppdfxx"] = "API Key has been updated.";
                //Message.Alert("API Key has been updated.");
                }
            catch (Exception ex)
            {
                cbAPI.JSProperties["cppdfxx"] = ex.Message;
            }

        }
    }
}