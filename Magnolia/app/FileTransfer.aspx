﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FileTransfer.aspx.cs" MasterPageFile="~/app/Master.master" Inherits="Magnolia.app.FileTransfer" %>
<%@ MasterType VirtualPath="~/app/Master.master" %>
<%@ Register Assembly="DevExpress.Web.v20.2, Version=20.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div style="display:flex; flex-direction:row;width:100%;">
        <dx:ASPxLabel ID="lblTranser" runat="server" Text="Enter the folder path:" Font-Size="Medium"></dx:ASPxLabel>
        &nbsp;&nbsp;&nbsp;
        <dx:ASPxTextBox ID="txtPath" runat="server" Width="300"></dx:ASPxTextBox>
        <dx:ASPxButton ID="btnTransfer" runat="server" Text="Begin" OnClick="btnTransfer_Click" Font-Size="Medium"></dx:ASPxButton>
    </div>
</asp:Content>