﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="adminRoutingManagement.aspx.cs" Inherits="Magnolia.app.adminRoutingManagement" MasterPageFile="~/app/Master.master" %>
<%@ Register Assembly="DevExpress.Web.v20.2, Version=20.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <script type="text/javascript">
        var isResetRequired = false;
        function onChanged(s, e) {
            isResetRequired = true;
            gvConfig.GetEditor("configurationId").PerformCallback(s.GetValue());
        }
        function onEndCallback(s, e) {
            if (isResetRequired) {
                isResetRequired = false;
                s.SetSelectedIndex(0);
            }
        }
    </script>
    <dx:ASPxGridView ID="gvConfig" ClientInstanceName="gvConfig" runat="server" DataSourceID="dsConfig" KeyFieldName="configurationId;configurationType" 
        OnCellEditorInitialize="gvConfig_CellEditorInitialize" OnStartRowEditing="gvConfig_StartRowEditing" OnRowInserted="gvConfig_RowInserted" OnRowUpdated="gvConfig_RowUpdated"><%-- OnHtmlEditFormCreated="gvConfig_HtmlEditFormCreated"--%>
        <Columns>
            <dx:GridViewCommandColumn ShowEditButton="true" CellStyle-HorizontalAlign="Left" Width="20%">
                    <HeaderTemplate>
                        <a href="javascript:gvConfig.AddNewRow()" style="text-decoration:underline;">New</a>
                    </HeaderTemplate>
                </dx:GridViewCommandColumn>
            <dx:GridViewDataComboBoxColumn FieldName="configurationType" Caption="Type" CellStyle-Wrap="False">
                <PropertiesComboBox>
                    <Items>
                        <dx:ListEditItem Text="ACTION" Value="ACTION" />
                        <dx:ListEditItem Text="CONTRACT TYPE" Value="CONTRACT TYPE" />
                    </Items>
                    <ClientSideEvents SelectedIndexChanged="onChanged" />
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn FieldName="configurationId" Caption="Name" CellStyle-Wrap="False">
                <PropertiesComboBox DataSourceID="dsConfig" TextField="configurationName" ValueField="configurationId">
                    <ClientSideEvents EndCallback="onEndCallback" />
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn FieldName="routingId" Caption="Route" CellStyle-Wrap="False">
                <PropertiesComboBox DataSourceID="dsRoutes" TextField="routingName" ValueField="routingId"></PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataColumn FieldName="configurationDate" Caption="Date Configured" EditFormSettings-Visible="False" CellStyle-Wrap="False"></dx:GridViewDataColumn>
            <dx:GridViewDataColumn FieldName="fullName" Caption="Configured By" EditFormSettings-Visible="False" CellStyle-Wrap="False"></dx:GridViewDataColumn>
            <dx:GridViewCommandColumn ShowDeleteButton="true" Caption=" "></dx:GridViewCommandColumn>
            <dx:GridViewDataColumn FieldName="configurationId" Visible="false"></dx:GridViewDataColumn>
            <dx:GridViewDataColumn FieldName="routingId" Visible="false"></dx:GridViewDataColumn>
            <dx:GridViewDataColumn FieldName="userId" Visible="false"></dx:GridViewDataColumn>
        </Columns>
        <SettingsDataSecurity AllowEdit="true" AllowDelete="true" AllowInsert="true" />
        <SettingsEditing Mode="PopupEditForm"></SettingsEditing>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="dsConfig" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
        SelectCommand="usp_getRoutingConfiguration" SelectCommandType="StoredProcedure"
        UpdateCommand="usp_updateRoutingConfiguration" UpdateCommandType="StoredProcedure"
        InsertCommand="usp_addRoutingConfiguration" InsertCommandType="StoredProcedure"
        DeleteCommand="usp_deleteRoutingConfiguration" DeleteCommandType="StoredProcedure">
        <UpdateParameters>
            <asp:Parameter Name="configurationId" />
            <asp:Parameter Name="configurationType" />
            <asp:Parameter Name="routingId" />
            <asp:SessionParameter Name="userId" SessionField="UserID" />
        </UpdateParameters>
        <InsertParameters>
            <asp:Parameter Name="configurationId" />
            <asp:Parameter Name="configurationType" />
            <asp:Parameter Name="routingId" />
            <asp:SessionParameter Name="userId" SessionField="UserID" />
        </InsertParameters>
        <DeleteParameters>
            <asp:Parameter Name="configurationId" />
            <asp:Parameter Name="configurationType" />
        </DeleteParameters>
    </asp:SqlDataSource> 
    <asp:SqlDataSource ID="dsConfigNames" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
        SelectCommand="usp_getRoutingConfigurationNames" SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="dsRoutes" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
        SelectCommand="usp_getRoutes" SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>
</asp:Content>