﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="adminParties.aspx.cs" Inherits="Magnolia.app.adminParties" MasterPageFile="~/app/Master.master" %>
<%@ Register Assembly="DevExpress.Web.v20.2, Version=20.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <style>
        input {text-transform:uppercase;}
        textarea {text-transform:uppercase;}
        span {text-transform:uppercase;}
        .spanTitle{text-transform:capitalize;}
        .gridHeader {  
            background-color: white;  
            color:black;
        }  
        .gridHeader a {  
            color: black;  
        }  
    </style>
    <script type="text/javascript">
		function OnEndCallback(s, e) {
			if (s.cpIsUpdating == true && s.cpHasExclusions) {
				s.cpIsUpdating = false;
                cbParties.PerformCallback();
                popExclusions.ShowAtPos(100, 100);
            }
            if (s.cpMessage) { //check  
                    alert(s.cpMessage); //alert  
                    delete s.cpMessage; //delete  
                } 
		}

		function OnUpdateClick(s, e) {
			gvParties.PerformCallback("Validate");
        }
        function OnGetRowValues(value) {
            // Right code
            //alert(value);
            hdnPartyId.Set('partyId', value);
            // This code will cause an error
            // alert(value[0]);
        }
    </script>                        
    <div style="overflow:auto">
        <dx:ASPxCallbackPanel ID="cbParties" ClientInstanceName="cbParties" runat="server" Width="100%" OnCallback="cbParties_Callback">
            <PanelCollection>
                <dx:PanelContent>
                    <script type="text/javascript">
                        function AdjustSize(s, e) {
                            gvParties.SetHeight(window.innerHeight * .75);
                            gvParties.SetWidth(window.innerWidth * .90)
                        }
                        function OnLinkClicked(s, e) {
                            alert(s.GetRowKey());
                            hdnPartyId.Set('partyId', s.GetRowKey());
                            alert('partyId=' + hdnPartyId.Get('partyId'));
                            popAddDocument.Show();
                        }
                    </script>
                    <dx:ASPxGridView ID="gvParties" ClientInstanceName="gvParties" runat="server" DataSourceId="dsParty" KeyFieldName="partyId" EditFormLayoutProperties-EncodeHtml="false" SettingsBehavior-EncodeErrorHtml="false"  
                        OnCustomErrorText="gvParties_CustomErrorText" OnRowInserting="gvParties_RowInserting" OnRowDeleted="gvParties_RowDeleted" OnHtmlDataCellPrepared="gvParties_HtmlDataCellPrepared"><%-- OnRowValidating="gvParties_RowValidating" OnCustomCallback="gvParties_CustomCallback" OnRowInserting="gvParties_RowInserting" ClientSideEvents-BeginCallback="function(s,e){cbParties.PerformCallback();}"--%>
                        <ClientSideEvents EndCallback="OnEndCallback" Init="AdjustSize" />
                        <Columns>
                            <dx:GridViewCommandColumn ShowEditButton="true" CellStyle-HorizontalAlign="Left" HeaderStyle-CssClass="gridHeader" Width="50">
                                <HeaderTemplate>
                                    <%--<a href="javascript:popExclusions.Show();txtBusinessName.Focus();" style="text-decoration:underline;">New</a>--%>
                                    <a href="javascript:gvParties_CustomErrorText.AddNewRow()" style="text-decoration:underline;">New</a>
                                </HeaderTemplate>
                            </dx:GridViewCommandColumn>
                            <dx:GridViewDataColumn FieldName="partyId" Visible="false"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="businessName" Caption="Business Name" Name="businessName" HeaderStyle-CssClass="gridHeader" SettingsHeaderFilter-Mode="CheckedList"><%-- EditFormSettings-Visible="False" ReadOnly="true"--%>
                                <EditItemTemplate>
                                    <dx:ASPxTextBox ID="txtNewBusinessName" ClientInstanceName="txtNewBusinessName" runat="server" Text='<%# Bind("businessName") %>'></dx:ASPxTextBox>
                                </EditItemTemplate>
                            </dx:GridViewDataColumn>
                            <%--<dx:GridViewDataComboBoxColumn FieldName="VendorId" Caption="Meditech<br/>Vendor" HeaderStyle-CssClass="gridHeader">
                                <SettingsHeaderFilter Mode="CheckedList" ></SettingsHeaderFilter>
                                <PropertiesComboBox DataSourceID="dsMeditechVendor" TextField="VendorNameCombined" ValueField="VendorId" AllowNull="true" NullDisplayText=" " NullText=" "></PropertiesComboBox>
                            </dx:GridViewDataComboBoxColumn>--%>
<%--                            <dx:GridViewDataComboBoxColumn FieldName="departmentId" Caption="Department" HeaderStyle-CssClass="gridHeader" SettingsHeaderFilter-Mode="CheckedList">
                                <PropertiesComboBox ValueField="departmentId" TextField="department" DataSourceID="dsDepartments"  AllowNull="true" NullDisplayText=" " NullText=" "></PropertiesComboBox>
                            </dx:GridViewDataComboBoxColumn>--%>
                            <dx:GridViewDataColumn Caption="Existing Contracts" Visible="false"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="contactName" Caption="Contact Name" HeaderStyle-CssClass="gridHeader" SettingsHeaderFilter-Mode="CheckedList">
                                <EditItemTemplate>
                                    <dx:ASPxTextBox ID="txtNewContactName" runat="server" Text='<%# Bind("contactName") %>'></dx:ASPxTextBox>
                                </EditItemTemplate>
                            </dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="address" Caption="Address" HeaderStyle-CssClass="gridHeader" SettingsHeaderFilter-Mode="CheckedList"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="city" Caption="City" HeaderStyle-CssClass="gridHeader" SettingsHeaderFilter-Mode="CheckedList"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="state" Caption="State" HeaderStyle-CssClass="gridHeader" SettingsHeaderFilter-Mode="CheckedList"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="zip" Caption="Zip<br/>Code" HeaderStyle-CssClass="gridHeader" SettingsHeaderFilter-Mode="CheckedList" HeaderStyle-HorizontalAlign="Center"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="phone" Caption="Phone<br/>Number" HeaderStyle-CssClass="gridHeader" SettingsHeaderFilter-Mode="CheckedList" HeaderStyle-HorizontalAlign="Center"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="email" Caption="Email<br/>Address" HeaderStyle-CssClass="gridHeader" SettingsHeaderFilter-Mode="CheckedList" HeaderStyle-HorizontalAlign="Center"></dx:GridViewDataColumn>
                            <dx:GridViewDataComboBoxColumn FieldName="partyTypeId" PropertiesComboBox-DataSourceID="dsPartyTypes" Caption="Party Types" HeaderStyle-CssClass="gridHeader" SettingsHeaderFilter-Mode="CheckedList">
                                <PropertiesComboBox TextField="partyType" ValueField="partyTypeId"></PropertiesComboBox>
                            </dx:GridViewDataComboBoxColumn>
                            <dx:GridViewDataDateColumn FieldName="approvalDate" Caption="Approval<br/>Date" CellStyle-HorizontalAlign="Center" Settings-FilterMode="DisplayText" HeaderStyle-CssClass="gridHeader" HeaderStyle-HorizontalAlign="Center" PropertiesDateEdit-DisplayFormatString="MM/dd/yyyy"></dx:GridViewDataDateColumn>
                            <dx:GridViewDataColumn Name="Documents" Caption="Documents" AllowTextTruncationInAdaptiveMode="false" HeaderStyle-CssClass="gridHeader" ShowInCustomizationForm="false"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Upload<br/>Document" HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center" ShowInCustomizationForm="false">
                                <DataItemTemplate>
                                    <dx:ASPxHyperLink ID="hyperLink" runat="server" OnInit="hyperLink_Init" Cursor="pointer" Text="Upload"></dx:ASPxHyperLink>
                                </DataItemTemplate>
                            </dx:GridViewDataColumn>
                            <dx:GridViewDataCheckColumn FieldName="inactive" Caption="Inactive?">
                                <PropertiesCheckEdit>
                                    <ClientSideEvents CheckedChanged="function (s,e) {if (s.GetChecked() == true) { txtNewBusinessName.SetText('(INACTIVE) ' + txtNewBusinessName.GetText()); } else { txtNewBusinessName.SetText(txtNewBusinessName.GetText().replace('(INACTIVE) ','')); } }" />
                                </PropertiesCheckEdit>
                            </dx:GridViewDataCheckColumn>
                            <dx:GridViewCommandColumn ShowDeleteButton="true" CellStyle-HorizontalAlign="Left" Caption=" " HeaderStyle-CssClass="gridHeader"></dx:GridViewCommandColumn>
                        </Columns>
                        <Settings ShowHeaderFilterButton="true" />
                        <SettingsBehavior ConfirmDelete="true" />
                        <SettingsDataSecurity AllowEdit="true" AllowDelete="true" AllowInsert="true"/> <%----%>
                        <%--<SettingsEditing Mode="Inline" UseFormLayout="true"></SettingsEditing>--%>
                        <%--<EditFormLayoutProperties SettingsItems-HorizontalAlign="Right"></EditFormLayoutProperties>--%>
                        <SettingsPager Mode="ShowPager" PageSize="20">
                            <PageSizeItemSettings Position="Right" Visible="false"></PageSizeItemSettings>
                        </SettingsPager>
                        <SettingsExport EnableClientSideExportAPI="true" ExcelExportMode="WYSIWYG" />
                        <SettingsPopup>
                            <EditForm>
                                <SettingsAdaptivity Mode="Always" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" MinWidth="600" />
                            </EditForm>
                        </SettingsPopup>
                        <SettingsEditing Mode="PopupEditForm" UseFormLayout="true"></SettingsEditing>
                        <SettingsText ConfirmDelete="Are you sure you want to delete this Party/Vendor?" />
                        <Settings VerticalScrollBarMode="Auto" />
                        <SettingsResizing ColumnResizeMode="Control" />
                        <SettingsSearchPanel Visible="true" ShowApplyButton="true" ShowClearButton="true" AllowTextInputTimer="false"/>
                        <SettingsCommandButton RenderMode="Link" EditButton-RenderMode="Link" DeleteButton-RenderMode="Link">
                            <SearchPanelApplyButton RenderMode="Button" Styles-Style-Font-Size="Medium"></SearchPanelApplyButton>
                            <SearchPanelClearButton RenderMode="Button" Styles-Style-Font-Size="Medium"></SearchPanelClearButton>
                        </SettingsCommandButton>
                    </dx:ASPxGridView><%--OnInserting="dsParty_Inserting"--%>
                    <%--<asp:SqlDataSource ID="dsMeditechVendor" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                        SelectCommand="select distinct VendorId, VendorId + ' - ' + VendorName VendorNameCombined, VendorName from importvendor order by vendorname"></asp:SqlDataSource>--%>
                    <asp:SqlDataSource ID="dsParty" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                        DeleteCommand="usp_deleteParty" DeleteCommandType="StoredProcedure" OnDeleted="dsParty_Deleted"
                        InsertCommand="usp_addParty" InsertCommandType="StoredProcedure" OnInserting="dsParty_Inserting"
                        UpdateCommand="usp_updateParty" UpdateCommandType="StoredProcedure"
                        SelectCommand="usp_getParties" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:Parameter Name="includeInactive" DefaultValue="1" />
                        </SelectParameters>
                        <InsertParameters>
                            <asp:Parameter Name="businessName" />
                            <asp:Parameter Name="contactName" />
                            <asp:Parameter Name="address" />
                            <asp:Parameter Name="city" />
                            <asp:Parameter Name="state" />
                            <asp:Parameter Name="zip" />
                            <asp:Parameter Name="phone" />
                            <asp:Parameter Name="email" />
                            <asp:Parameter Name="partyTypeId" />
                            <asp:Parameter Name="approvalDate" />
                            <asp:Parameter Name="inactive" />
                            <asp:Parameter Name="vendorId" DefaultValue="" />
                            <asp:Parameter Name="departmentId" />
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="businessName" />
                            <asp:Parameter Name="contactName" />
                            <asp:Parameter Name="address" />
                            <asp:Parameter Name="city" />
                            <asp:Parameter Name="state" />
                            <asp:Parameter Name="zip" />
                            <asp:Parameter Name="phone" />
                            <asp:Parameter Name="email" />
                            <asp:Parameter Name="partyTypeId" />
                            <asp:Parameter Name="approvalDate" />
                            <asp:Parameter Name="partyId" />
                            <asp:Parameter Name="inactive" />
                            <asp:Parameter Name="vendorId" DefaultValue="" />
                            <asp:Parameter Name="departmentId" />
                        </UpdateParameters>
                        <DeleteParameters>
                            <asp:Parameter Name="partyId" />
                            <asp:Parameter Name="count" Direction="Output" DbType="Int16" Size="4" />
                        </DeleteParameters>
                    </asp:SqlDataSource>
<%--                    <asp:SqlDataSource ID="dsDepartments" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                        SelectCommand="usp_getDepartments" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:Parameter Name="includeInactive" DefaultValue="1" />
                        </SelectParameters>
                    </asp:SqlDataSource>--%>
                    <asp:SqlDataSource ID="dsPartyTypes" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
                        SelectCommand="usp_getPartyTypes" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                    <dx:ASPxPopupControl ID="popExclusions" ClientInstanceName="popExclusions" runat="server" Modal="true" HeaderText="" Left="75" Top="100" ShowCloseButton="true" AllowDragging="true" AllowResize="true">
                        <SettingsAdaptivity Mode="Always" VerticalAlign="WindowBottom" HorizontalAlign="WindowCenter" MinHeight="80%" MinWidth="90%" />
                        <ContentCollection>
                            <dx:PopupControlContentControl>
                                <div style="display:flex; flex-direction:row;">
                                    <dx:ASPxLabel id="lblBusinessName" runat="server" Text="Business Name:"></dx:ASPxLabel>
                                    &nbsp;&nbsp;&nbsp;
                                    <dx:ASPxTextBox ID="txtBusinessName" ClientInstanceName="txtBusinessName" runat="server" ValidationSettings-RequiredField-IsRequired="true" Width="200" ValidationSettings-ValidationGroup="ValidateExclusion"></dx:ASPxTextBox>
                                    &nbsp;&nbsp;&nbsp;
                                    <dx:ASPxCheckBox ID="cbFuzzy" runat="server" Checked="false" Text="Include Fuzzy Search Results?"></dx:ASPxCheckBox>
                                    &nbsp;&nbsp;&nbsp;
                                    <dx:ASPxButton ID="btnSearchExclusions" runat="server" Text="Search Exclusions" OnClick="btnSearchExclusions_Click"></dx:ASPxButton>
                                    <br /><br />
                                </div>
                                <div id="divExclusionResults" runat="server" visible="false" style="width:100%;">
                                    <dx:ASPxLabel ID="lblExclusions" ClientInstanceName="lblExclusions" runat="server" Text="The party/vendor entered matches one or more on the exclusions list.<br/>If you are sure your party/vendor is not the same as one listed below, click Add Party below." Font-Bold="true" ForeColor="Red" Font-Size="Small" EncodeHtml="false"></dx:ASPxLabel>
                                    <br /><br />
                                    <dx:ASPxGridView ID="gvExclusions" runat="server" ClientInstanceName="gvExclusions" Width="100%">
                                        <Columns>
                                            <dx:GridViewDataColumn FieldName="Classification" Caption="Classification"></dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn FieldName="EntityName" Caption="Entity Name"></dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn FieldName="FirstName" Caption="First Name"></dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn FieldName="MiddleInitial" Caption="Middle Initial"></dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn FieldName="LastName" Caption="Last Name"></dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn FieldName="Address" Caption="Address"></dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn FieldName="City" Caption="City"></dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn FieldName="State" Caption="State"></dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn FieldName="ZipCode" Caption="Zip Code"></dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn FieldName="ExclusionType" Caption="Exclusion Type"></dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn FieldName="ActiveDate" Caption="Active Date"></dx:GridViewDataColumn>
                                        </Columns>
                                        <Settings ShowHeaderFilterButton="true"  />
                                        <SettingsPager Mode="ShowPager" PageSize="10"></SettingsPager>
                                    </dx:ASPxGridView>
                                    <br /><br />
                                    <div style="width:100%;text-align:left;">
                                        <dx:ASPxButton ID="btnExclusionCancel" runat="server" Text="Cancel">
                                            <ClientSideEvents Click="function(s,e){popExclusions.Hide();}" />
                                        </dx:ASPxButton>
                                        &nbsp;&nbsp;&nbsp;
                                        <dx:ASPxButton ID="btnExclusionSave" runat="server" Text="Add Party" AutoPostBack="true" OnClick="btnExclusionSave_Click"></dx:ASPxButton>
                                    </div>
                                    <br /><br />
                                    <div style="width:100%;text-align:left;">
                                        <dx:ASPxLabel ID="lblExclusionNote" ClientInstanceName="lblExclusionNote" runat="server" Font-Bold="true" ForeColor="Red" Text="*Only Add Party if you are sure your party is not the same as one listed above."></dx:ASPxLabel>
                                    </div>
                                </div>
                            </dx:PopupControlContentControl>
                        </ContentCollection>
                    </dx:ASPxPopupControl>
                    <dx:ASPxHiddenField ID="hdnOIGCheck" runat="server"></dx:ASPxHiddenField>
                    <dx:ASPxHiddenField ID="hdnPartyId" ClientInstanceName="hdnPartyId" runat="server"></dx:ASPxHiddenField>
                    <asp:SqlDataSource ID="dsDocuments" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>"
                        SelectCommand="usp_getDocuments" SelectCommandType="StoredProcedure"
                        InsertCommand="usp_addDocument" InsertCommandType="StoredProcedure"
                        UpdateCommand="usp_updateDocument" UpdateCommandType="StoredProcedure" OnUpdating="dsDocuments_Updating"
                        DeleteCommand="usp_deleteDocument" DeleteCommandType="StoredProcedure" OnDeleting="dsDocuments_Deleting">
                        <SelectParameters>
                            <asp:QueryStringParameter Name="contractId" QueryStringField="id" />
                        </SelectParameters>
                        <InsertParameters>
                            <asp:Parameter Name="fileName" />
                            <asp:Parameter Name="path" />
                            <asp:Parameter Name="uploadDocumentTypeId" />
                            <asp:Parameter Name="folder" />
                            <asp:QueryStringParameter Name="contractId" QueryStringField="id" />
                            <asp:SessionParameter Name="uploadId" SessionField="UserID" />
                            <asp:Parameter Name="partyId" />
                            <asp:Parameter Name="contractTypeId" />
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="uploadDocumentTypeId" />
                            <asp:SessionParameter Name="uploadId" SessionField="UserID" />
                            <asp:Parameter Name="documentId" />
                            <asp:Parameter Name="contractTypeId" />
                            <asp:Parameter Name="partyId" />
                            <asp:Parameter Name="contractId" />
                            <asp:Parameter Name="folder" Direction="Output" DbType="String" Size="20" />
                            <asp:Parameter Name="prevFolder" Direction="Output" DbType="String" Size="20" />
                        </UpdateParameters>
                        <DeleteParameters>
                            <asp:Parameter Name="documentId" />
                        </DeleteParameters>
                    </asp:SqlDataSource>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxCallbackPanel>
    </div>
    <dx:ASPxPopupControl ID="popAddDocument" ClientInstanceName="popAddDocument" runat="server" Modal="true" HeaderText="" Left="75" Top="100" ShowCloseButton="true" AllowDragging="true" AllowResize="true">
        <SettingsAdaptivity Mode="Always" VerticalAlign="WindowCenter" HorizontalAlign="WindowCenter" MinHeight="30%" MinWidth="40%" />
        <ContentCollection>
            <dx:PopupControlContentControl>
                <script type="text/javascript">
                    var uploadInProgress = false,
                        submitInitiated = false,
                        uploadErrorOccurred = false;
                    uploadedFiles = [];
                    function onFileUploadComplete(s, e) {
                        var callbackData = e.callbackData.split("|"),
                            uploadedFileName = callbackData[0],
                            isSubmissionExpired = callbackData[1] === "True";
                        uploadedFiles.push(uploadedFileName);
                        if (e.errorText.length > 0 || !e.isValid) {
                            uploadErrorOccurred = true;
                            UploadedFilesTokenBox.isValid = false;
                            alert("Error uploading file(s)");
                        }
                        if (isSubmissionExpired && UploadedFilesTokenBox.GetText().length > 0) {
                            var removedAfterTimeoutFiles = UploadedFilesTokenBox.GetTokenCollection().join("\n");
                            alert("The following files have been removed from the server due to the defined 5 minute timeout: \n\n" + removedAfterTimeoutFiles);
                            UploadedFilesTokenBox.ClearTokenCollection();
                        }
                    }
                    function onFileUploadStart(s, e) {
                        uploadInProgress = true;
                        uploadErrorOccurred = false;
                        UploadedFilesTokenBox.SetIsValid(true);
                    }
                    function onFilesUploadComplete(s, e) {
                        uploadInProgress = false;
                        for (var i = 0; i < uploadedFiles.length; i++)
                            UploadedFilesTokenBox.AddToken(uploadedFiles[i]);
                        updateTokenBoxVisibility();
                        uploadedFiles = [];
                        if (submitInitiated) {
                            SubmitButton.SetEnabled(true);
                            SubmitButton.DoClick();
                        }
                    }
                    function onSubmitButtonInit(s, e) {
                        s.SetEnabled(true);
                    }
                    function onSubmitButtonClick(s, e) {
                        ASPxClientEdit.ValidateGroup();
                        if (!formIsValid()) {
                            e.processOnServer = false;
                        }
                        else if (uploadInProgress) {
                            s.SetEnabled(false);
                            submitInitiated = true;
                            e.processOnServer = false;
                        }
                    }
                    function onTokenBoxValidation(s, e) {
                        var isValid = DocumentsUploadControl.GetText().length > 0 || UploadedFilesTokenBox.GetText().length > 0;
                        e.isValid = isValid;
                        if (!isValid) {
                            e.errorText = "No files have been uploaded. Upload at least one file.";
                        }
                    }
                    function onTokenBoxValueChanged(s, e) {
                        updateTokenBoxVisibility();
                    }
                    function updateTokenBoxVisibility() {
                        var isTokenBoxVisible = UploadedFilesTokenBox.GetTokenCollection().length > 0;
                        UploadedFilesTokenBox.SetVisible(isTokenBoxVisible);
                    }
                    function formIsValid() {
                        return !ValidationSummary.IsVisible() && ddlUploadDocumentType.GetIsValid() && UploadedFilesTokenBox.GetIsValid() && !uploadErrorOccurred;
                    }
                </script>
                <dx:ASPxHiddenField runat="server" ID="HiddenField" ClientInstanceName="HiddenField" />
                <dx:ASPxFormLayout ID="FormLayout" runat="server" Width="100%" ColCount="1" UseDefaultPaddings="false">
                    <Items>
                        <dx:LayoutGroup ShowCaption="False" GroupBoxDecoration="None" Width="100%" UseDefaultPaddings="false" ColumnCount="1">
                            <Items>
                                <dx:LayoutItem Caption="Document Type" CaptionStyle-Font-Bold="true" CaptionSettings-Location="Top" ColumnSpan="1">
                                    <LayoutItemNestedControlCollection>
                                        <dx:LayoutItemNestedControlContainer>
                                            <dx:ASPxComboBox ID="ddlUploadDocumentType" ClientInstanceName="ddlUploadDocumentType" runat="server" DataSourceID="dsUploadDocumentTypes" TextField="uploadDocumentType" ValueField="uploadDocumentTypeId">
                                                <ValidationSettings  Display="Dynamic" ErrorDisplayMode="Text" ErrorTextPosition="Bottom" ValidationGroup="DocumentTypeValidation">
                                                    <RequiredField IsRequired="True" ErrorText="Document Type is required" />
                                                </ValidationSettings>
                                            </dx:ASPxComboBox>
                                            <asp:SqlDataSource ID="dsUploadDocumentTypes" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>"
                                                SelectCommand="usp_getUploadDocumentTypes" SelectCommandType="StoredProcedure" OnSelecting="dsUploadDocumentTypes_Selecting">
                                                <SelectParameters>
                                                    <asp:Parameter Name="folder" DefaultValue="PARTY" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                        </dx:LayoutItemNestedControlContainer>
                                    </LayoutItemNestedControlCollection>
                                </dx:LayoutItem>
                                <dx:LayoutItem Caption="Replace Existing File" CaptionStyle-Font-Bold="true" CaptionSettings-Location="Left" ColumnSpan="1" Name="ReplaceFile" ClientVisible="false">
                                    <LayoutItemNestedControlCollection>
                                        <dx:LayoutItemNestedControlContainer>
                                            <dx:ASPxCheckBox ID="cbReplaceFile" runat="server" Checked="false"></dx:ASPxCheckBox>
                                        </dx:LayoutItemNestedControlContainer>
                                    </LayoutItemNestedControlCollection>
                                </dx:LayoutItem>
                                <dx:LayoutGroup Caption="Documents" ColumnCount="1">
                                    <Items>
                                        <dx:LayoutItem ShowCaption="False" ColumnSpan="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <div id="dropZone">
                                                        <dx:ASPxUploadControl runat="server" ID="DocumentsUploadControl" ClientInstanceName="DocumentsUploadControl" Width="100%"
                                                            AutoStartUpload="true" ShowProgressPanel="True" ShowTextBox="false" BrowseButton-Text="Add documents" FileUploadMode="OnPageLoad"
                                                            OnFileUploadComplete="DocumentsUploadControl_FileUploadComplete">
                                                            <AdvancedModeSettings EnableMultiSelect="true" EnableDragAndDrop="true" ExternalDropZoneID="dropZone" />
                                                            <ValidationSettings MaxFileSize="4194304" ShowErrors="true" MaxFileSizeErrorText="File size is too big" GeneralErrorText="Error uploading file(s)"></ValidationSettings>
                                                            <ClientSideEvents
                                                                FileUploadComplete="onFileUploadComplete"
                                                                FilesUploadComplete="onFilesUploadComplete"
                                                                FilesUploadStart="onFileUploadStart" />
                                                        </dx:ASPxUploadControl>
                                                        <br />
                                                        <dx:ASPxTokenBox runat="server" Width="100%" ID="UploadedFilesTokenBox" ClientInstanceName="UploadedFilesTokenBox"
                                                            NullText="Select the documents to submit" AllowCustomTokens="false" ClientVisible="false">
                                                            <ClientSideEvents Init="updateTokenBoxVisibility" ValueChanged="onTokenBoxValueChanged" Validation="onTokenBoxValidation" />
                                                            <ValidationSettings EnableCustomValidation="true"/><%-- ValidationGroup="UploadValidation" --%>
                                                        </dx:ASPxTokenBox>
                                                        <br />
                                                        <p class="Note">
                                                            <dx:ASPxLabel ID="MaxFileSizeLabel" runat="server" Text="Maximum file size: 4 MB." Font-Size="8pt" />
                                                        </p>
                                                        <dx:ASPxValidationSummary runat="server" ID="ValidationSummary" ClientInstanceName="ValidationSummary" RenderMode="BulletedList" Width="250px" ShowErrorAsLink="false" ForeColor="Red" Font-Bold="true" /><%-- ValidationGroup="UploadValidation"--%>
                                                    </div>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                    </Items>
                                </dx:LayoutGroup>
                                <dx:LayoutItem ShowCaption="False" HorizontalAlign="Right" ColumnSpan="1">
                                    <LayoutItemNestedControlCollection>
                                        <dx:LayoutItemNestedControlContainer>
                                            <dx:ASPxButton runat="server" ID="SubmitButton" ClientInstanceName="SubmitButton" Text="Submit" AutoPostBack="True"
                                                OnClick="SubmitButton_Click"  ClientEnabled="false" ValidateInvisibleEditors="true">
                                                <ClientSideEvents Init="onSubmitButtonInit" Click="onSubmitButtonClick" />
                                            </dx:ASPxButton>
                                        </dx:LayoutItemNestedControlContainer>
                                    </LayoutItemNestedControlCollection>
                                </dx:LayoutItem>
                                <dx:EmptyLayoutItem Height="5" />
                            </Items>
                        </dx:LayoutGroup>
                        <dx:LayoutGroup GroupBoxDecoration="None" ShowCaption="False" Name="ResultGroup" Visible="false" Width="50%" UseDefaultPaddings="false">
                            <Items>
                                <dx:LayoutItem ShowCaption="False">
                                    <LayoutItemNestedControlCollection>
                                        <dx:LayoutItemNestedControlContainer>
                                            <dx:ASPxRoundPanel ID="RoundPanel" runat="server" HeaderText="Uploaded files" Width="100%">
                                                <PanelCollection>
                                                    <dx:PanelContent>
                                                        <b>Description:</b>
                                                        <dx:ASPxLabel runat="server" ID="DescriptionLabel" />
                                                        <br />
                                                        <br />
                                                        <dx:ASPxListBox ID="SubmittedFilesListBox" runat="server" Width="100%" Height="150px">
                                                            <ItemStyle CssClass="ResultFileName" />
                                                            <Columns>
                                                                <dx:ListBoxColumn FieldName="OriginalFileName" />
                                                                <dx:ListBoxColumn FieldName="FileSize" Width="15%"/>
                                                            </Columns>
                                                        </dx:ASPxListBox>
                                                    </dx:PanelContent>
                                                </PanelCollection>
                                            </dx:ASPxRoundPanel>
                                        </dx:LayoutItemNestedControlContainer>
                                    </LayoutItemNestedControlCollection>
                                </dx:LayoutItem>
                            </Items>
                        </dx:LayoutGroup>
                    </Items>
                    <SettingsAdaptivity AdaptivityMode="SingleColumnWindowLimit" SwitchToSingleColumnAtWindowInnerWidth="1000" />
                </dx:ASPxFormLayout>                        
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</asp:Content>