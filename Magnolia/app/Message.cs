﻿using System;
using System.Web;
using System.Net.Mail;
using System.Data;

namespace Magnolia.app
{
    public class Message
    {
        public static void Alert(string msg)
        { 
            System.Web.UI.Page Page = (System.Web.UI.Page)(HttpContext.Current.Handler);
            if (Page != null)
            {
                msg = msg.Replace("'", @"\'");
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "msg_box" + DateTime.Now.Millisecond, "alert('" + msg + "');", true);
            }
        }

        public static void AlertAndRedirect(string msg, string location)
        {
            System.Web.UI.Page Page = (System.Web.UI.Page)(HttpContext.Current.Handler);
            if (Page != null)
            {
                msg = msg.Replace("'", @"\'");
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "msg_box" + DateTime.Now.Millisecond, "alert('" + msg + "');window.location='" + location + "';", true);
            }
        }

        public static void SendMail(string to_email, string to_name, string subject, string htmlmessage, System.IO.MemoryStream Mem2 = null, string fileName = "", string roleName = "", string ccEmail = "", string ccName = "")
        {
            MailMessage mail = new MailMessage();
            if (System.Configuration.ConfigurationManager.AppSettings["system"] == "DEV")
            {
                mail.To.Clear();
                mail.To.Add("CMSAdministrators@mrhc.org");
            }
            else
            {
                if (to_email != "")
                {
                    MailAddress to = new MailAddress(to_email, to_name);
                    mail.To.Add(to);
                }
                if (ccEmail != "")
                {
                    MailAddress cc = new MailAddress(ccEmail, ccName);
                    mail.To.Add(cc);
                }
            }
            mail.From = new MailAddress("contracts@mrhc.org");
            if (htmlmessage.Contains("fully executed") == true && System.Configuration.ConfigurationManager.AppSettings["system"] != "DEV")
            {
                mail.CC.Add(new MailAddress("CMSAdministrators@mrhc.org"));
            }
            if (roleName != "" && roleName == "Contract Managers" && System.Configuration.ConfigurationManager.AppSettings["system"] != "DEV")
            {
                DataSet dtMgr = DataAccess.GetDataSet("usp_getContractManagers", null, null);
                if (dtMgr != null && dtMgr.Tables.Count > 0 && dtMgr.Tables[0].Rows.Count > 0)
                {
                    mail.To.Clear();
                    foreach (DataRow row in dtMgr.Tables[0].Rows)
                    {
                        if (row["emailAddress"] != null && row["emailAddress"].ToString().ToLower() != "tmoore@mhrc.org")
                        {
                            mail.To.Add(new MailAddress(row["emailAddress"].ToString(), row["fullName"].ToString()));
                        }
                    }
                }
            }
            //if (htmlmessage.Contains("A Contract has been assigned to you for review.") && to_email == "ldavid@mhrc.org")
            //{
            //    mail.CC.Add(new MailAddress("tmoore@mhrc.org", "Tracy Moore"));
            //}
            if (System.Configuration.ConfigurationManager.AppSettings["system"] == "DEV")
            {
                subject = "TEST SITE - " + subject;
            }
            mail.Subject = subject;
            mail.Body = htmlmessage;
            mail.IsBodyHtml = true;
            if (Mem2 != null)
            {
                Mem2.Seek(0, System.IO.SeekOrigin.Begin);
                Attachment Att = new Attachment(Mem2, fileName + ".pdf", "application/pdf");
                mail.Attachments.Add(Att);
            }
            SmtpClient smtp = new SmtpClient();
            smtp.Port = 25;
            smtp.Host = "relay.mrhc.org";
            smtp.EnableSsl = true;
            HttpServerUtility Server = HttpContext.Current.Server;
            if (System.Configuration.ConfigurationManager.AppSettings["system"] == "LOCALHOST")
            {
                smtp.PickupDirectoryLocation = @"c:\emails\";
                smtp.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                smtp.EnableSsl = false;
            }
            //System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls
            try
            {
                if (System.Configuration.ConfigurationManager.AppSettings["sendMail"].ToString() == "TRUE")
                {
                    DataSet ds = DataAccess.GetDataSet("usp_logEmail", new string[] { "@emailTo", "@subject", "@body" }, new string[] { mail.To.ToString(), mail.Subject, mail.Body });
                    smtp.Send(mail);
                }
            }
            catch (Exception ex)
            {
                LogError("Message", "SendMail", HttpContext.Current.Session["userName"].ToString(), ex.Message);
                Server.ClearError();
            }
        }

        public static void LogError(string fileName, string functionName, string userName, string errorMessage)
        {
            if (errorMessage != "Thread was being aborted.")
            {
                if (fileName.Length > 500) { fileName = fileName.Substring(0, 499); }
                if (functionName.Length > 500) { functionName = functionName.Substring(0, 499); }
                if (userName.Length > 500) { userName = userName.Substring(0, 499); }
                if (errorMessage.Length > 4000) { errorMessage = errorMessage.Substring(0, 3999); }
                try
                {
                    DataSet ds = DataAccess.GetDataSet("usp_addError", new string[] { "@fileName", "@functionName", "@userName", "@errorMessage" },
                        new string[] { fileName, functionName, userName, errorMessage });
                }
                catch (Exception ex)
                {
                    DataSet ds = DataAccess.GetDataSet("usp_addError", new string[] { "@fileName", "@functionName", "@userName", "@errorMessage" },
                        new string[] { "Message.cs", "LogError", userName, ex.Message });
                }
                finally
                {
                    HttpContext.Current.Server.ClearError();
                }
            }
        }

    }
}