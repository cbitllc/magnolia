﻿using System;
using System.Linq;
using System.Data.SqlClient;
using System.Web.UI;
using System.Data;

namespace Magnolia.app
{
    public partial class adminRouting : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ((Master)Page.Master).SetHeader = "Admin Routes";
            ((Master)Page.Master).SetHelpURL = "admin.aspx";
            if (!Page.IsPostBack)
            {
                cbRoutes.JSProperties["cpAlertMessage"] = "";
            }
        }

        private void Page_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError();
            Message.Alert(exc.Message);

            // Handle specific exception.
            if (exc is System.Web.HttpUnhandledException)
            {
                Message.Alert("An error occurred on this page. Please verify your information to resolve the issue.");
            }
            // Clear the error from the server.
            Server.ClearError();
        }

        protected void gvRoutes_Init(object sender, EventArgs e)
        {
            DataSet ds = null;
            using (SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cmsConn"].ToString()))
            {
                string query = "usp_getMaxRoleCount";
                using (SqlCommand myCommand = new SqlCommand(query, myConnection))
                {
                    myCommand.CommandType = CommandType.StoredProcedure;
                    myCommand.CommandTimeout = 0;
                    using (SqlDataAdapter mAdapter = new SqlDataAdapter(myCommand))
                    {
                        myConnection.Open();
                        ds = new DataSet();
                        mAdapter.Fill(ds);
                    }
                }
                myConnection.Close();
            }
            int max = 0;
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                max = Convert.ToInt16(ds.Tables[0].Rows[0]["roleCount"]);
            }
            DevExpress.Web.ASPxGridView grid = sender as DevExpress.Web.ASPxGridView;
            (grid.Columns["Sequence"] as DevExpress.Web.GridViewDataSpinEditColumn).PropertiesSpinEdit.MaxValue = max;
        }

        protected void ddlRoutes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlRoutes.Value != null)
            {
                SqlDataSource1.SelectParameters["routingId"].DefaultValue = ddlRoutes.Value.ToString();
                Diagram.DataBind();
                dsRoute.SelectParameters["routingId"].DefaultValue = ddlRoutes.Value.ToString();
                dsRoute.InsertParameters["routingId"].DefaultValue = ddlRoutes.Value.ToString();
                gvRoutes.DataBind();
                if (gvRoutes.VisibleRowCount > 0)
                {
                    try
                    {
                        string description = gvRoutes.GetRowValues(0, new string[] { "description" }).ToString();
                        txtRouteDescription.Text = description;
                    }
                    catch (Exception ex)
                    {
                        Message.Alert("Error getting routing description: " + ex.Message);
                    }
                }
            }
        }

        protected void SqlDataSource1_Deleting(object sender, System.Web.UI.WebControls.SqlDataSourceCommandEventArgs e)
        {
            string x = GetQuery(e.Command.CommandText, e.Command.Parameters);
        }

        protected void btnCloneRoute_Click(object sender, EventArgs e)
        {
            int routingId = Convert.ToInt32(ddlCloneRoutes.Value);
            SqlParameter parm = null;
            using (SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cmsConn"].ToString()))
            {
                string query = "usp_addRoute";
                using (SqlCommand myCommand = new SqlCommand(query, myConnection))
                {
                    myCommand.Parameters.Add(new SqlParameter("routingId", routingId));
                    myCommand.Parameters.Add(new SqlParameter("routingName", txtName.Text.ToUpper()));
                    parm = new SqlParameter();
                    parm.ParameterName = "newRoutingId";
                    parm.DbType = DbType.Int32;
                    parm.Direction = ParameterDirection.Output;
                    myCommand.Parameters.Add(parm);
                    myCommand.CommandType = CommandType.StoredProcedure;
                    myCommand.CommandTimeout = 0;
                    myConnection.Open();
                    myCommand.ExecuteNonQuery();
                }
                myConnection.Close();
            }
            dsRoute.SelectParameters["routingId"].DefaultValue = parm.Value.ToString();
            gvRoutes.DataBind();
            ddlRoutes.DataBind();
            DevExpress.Web.ListEditItem obj = ddlRoutes.Items.FindByValue(parm.Value.ToString());
            if (obj != null)
            {
                ddlRoutes.SelectedIndex = obj.Index;
            }
            popNewRoute.ShowOnPageLoad = false;
            Diagram.DataBind();
        }

        //protected void dsRoute_Deleting(object sender, System.Web.UI.WebControls.SqlDataSourceCommandEventArgs e)
        //{
        //    Diagram.DataBind();
        //}

        protected void dsRoute_Deleted(object sender, System.Web.UI.WebControls.SqlDataSourceStatusEventArgs e)
        {
            if (e.Exception != null)
            {
                string sqlErrorMessage = e.Exception.Message;
                Message.Alert("Error deleting sequence: " + e.Exception.Message);
            }
            //else
            //{
            //    Diagram.DataBind();
            //}
        }

        protected void dsRoute_Updating(object sender, System.Web.UI.WebControls.SqlDataSourceCommandEventArgs e)
        {
        }

        protected void dsRoute_Updated(object sender, System.Web.UI.WebControls.SqlDataSourceStatusEventArgs e)
        {
        }

        protected void gvRoutes_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            try
            {
                dsRoute.UpdateParameters["rid"].DefaultValue = e.NewValues["rid"].ToString();
                dsRoute.UpdateParameters["parentId"].DefaultValue = e.NewValues["parentId"].ToString();
                dsRoute.UpdateParameters["roleId"].DefaultValue = e.NewValues["roleId"].ToString();
                dsRoute.Update();
                e.Cancel = true;
                gvRoutes.CancelEdit();
            }
            catch (Exception ex)
            {
                Message.Alert(ex.Message);
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            gvRoutes.DataBind();
            Diagram.DataBind();
        }

        protected void btnDeleteRoute_Click(object sender, EventArgs e)
        {
            try
            {
                dsRoutes.Delete();
                ddlRoutes.SelectedIndex = -1;
                ddlRoutes.DataBind();
                gvRoutes.DataBind();
                dsRoute.SelectParameters["routingId"].DefaultValue = "0";
                Diagram.DataBind();
            }
            catch (Exception ex)
            {
                Message.Alert(ex.Message);
            }
        }

        protected void btnSaveDescription_Click(object sender, EventArgs e)
        {
            try
            {
                dsRoutes.Update();
                Message.Alert("Route description has been updated.");
            }
            catch (Exception ex)
            {
                Message.Alert(ex.Message);
            }
        }

        protected void dsRoute_Inserting(object sender, System.Web.UI.WebControls.SqlDataSourceCommandEventArgs e)
        {
            e.Command.Parameters["@routingId"].Value = gvRoutes.GetRowValues(0, new string[] { "routingId" });
            string x = GetQuery(e.Command.CommandText, e.Command.Parameters);
        }

        private string GetQuery(string CommandQuery, System.Data.Common.DbParameterCollection parmCollection)
        {
            string query = CommandQuery;
            string parms = "";
            foreach (System.Data.SqlClient.SqlParameter parm in parmCollection)
            {
                parms += parm.ParameterName + " = ";
                if (parm.Value == null)
                {
                    parms += "null,";

                }
                else
                {
                    if (parm.DbType == System.Data.DbType.String || parm.DbType == System.Data.DbType.DateTime)
                    {
                        parms += "'" + parm.Value.ToString() + "',";
                    }
                    else
                    {
                        parms += parm.Value.ToString() + ",";
                    }
                }
            }
            query = query + " " + parms.Substring(0, parms.Length - 1);
            return query;
        }

        protected void cbRoutes_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            //gvRoutes.DataBind();
            Diagram.DataBind();
        }

        protected void gvRoutes_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
        {
            //Message.Alert("Please click the refresh button to see your reflected changes");
            //cbRoutes.JSProperties["cpAlertMessage"] = "Please click the refresh button to see your reflected changes";
        }

        protected void dsRoutes_Deleting(object sender, System.Web.UI.WebControls.SqlDataSourceCommandEventArgs e)
        {
            string query = e.Command.CommandText;
            string x = e.Command.Parameters[0].Value.ToString();
        }
    }
}