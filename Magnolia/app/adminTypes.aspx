﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="adminTypes.aspx.cs" Inherits="Magnolia.app.adminTypes" MasterPageFile="~/app/Master.master" %>
<%@ Register Assembly="DevExpress.Web.v20.2, Version=20.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <style>
        input {text-transform:uppercase;}
        textarea {text-transform:uppercase;}
        span {text-transform:uppercase;}
        .spanTitle{text-transform:capitalize;}
        .gridHeader {  
            background-color: white;  
            color:black;
        }  
        .gridHeader a {  
            color: black;  
        }  
    </style>
    <div style="width:25%; top:170px;">
        <dx:ASPxLabel ID="lblTypes" runat="server" Text="Select Type:" Font-Bold="true"></dx:ASPxLabel>
        &nbsp;&nbsp;&nbsp;
        <dx:ASPxComboBox ID="ddlTypes" ClientInstanceName="ddlTypes" runat="server" OnSelectedIndexChanged="ddlTypes_SelectedIndexChanged" AutoPostBack="true">
            <Items>
                <dx:ListEditItem Text="Actions" Value="actions" />
                <dx:ListEditItem Text="Contract Types" Value="contract" />
                <dx:ListEditItem Text="Departments" Value="department" />
                <dx:ListEditItem Text="Document Types" Value="document" />
                <dx:ListEditItem Text="EOC" Value="EOC" />
                <dx:ListEditItem Text="Party Types" Value="party" />
                <dx:ListEditItem Text="Payment Term" Value="payment" />
                <dx:ListEditItem Text="Purchase Groups" Value="purchaseGroups" />
                <dx:ListEditItem Text="Status Types" Value="status" />
            </Items>
        </dx:ASPxComboBox>
    </div>
    <div style="width:60%; top:170px; height:100%; left:40%; position:fixed;">
        <dx:ASPxGridView ID="gvActions" ClientInstanceName="gvActions" runat="server" DataSourceID="dsActions"
            KeyFieldName="actionId" Width="50%" Visible="false" Styles-Header-Font-Bold="true" OnRowDeleted="gvActions_RowDeleted">
            <ClientSideEvents EndCallback="function(s, e) {  
                if (s.cpMessage) { //check  
                    alert(s.cpMessage); //alert  
                    delete s.cpMessage; //delete  
                }  
            }" />
            <Columns>
                <dx:GridViewCommandColumn ShowEditButton="true" CellStyle-HorizontalAlign="Left" Width="20%">
                    <HeaderTemplate>
                        <a href="javascript:gvActions.AddNewRow()" style="text-decoration:underline;">New</a>
                    </HeaderTemplate>
                </dx:GridViewCommandColumn>
                <dx:GridViewDataColumn FieldName="action" Caption="Action"></dx:GridViewDataColumn>
                <dx:GridViewDataCheckColumn FieldName="inactive" Caption="Inactive?"></dx:GridViewDataCheckColumn>
                <dx:GridViewDataColumn FieldName="actionId" Visible="false"></dx:GridViewDataColumn>
                <dx:GridViewCommandColumn ShowDeleteButton="true" ButtonRenderMode="Link" Caption=" "></dx:GridViewCommandColumn>
            </Columns>
            <SettingsBehavior ConfirmDelete="true" />
            <SettingsText ConfirmDelete="Are you sure you want to delete this action?" />
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="dsActions" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
            SelectCommand="usp_getActions" SelectCommandType="StoredProcedure"
            InsertCommand="usp_addAction" InsertCommandType="StoredProcedure"
            UpdateCommand="usp_updateAction" UpdateCommandType="StoredProcedure"
            DeleteCommand="usp_deleteAction" DeleteCommandType="StoredProcedure" OnDeleting="dsActions_Deleting" OnDeleted="dsActions_Deleted">
            <InsertParameters>
                <asp:Parameter Name="action" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="actionId" />
                <asp:Parameter Name="action" />
                <asp:Parameter Name="inactive" />
            </UpdateParameters>
            <SelectParameters>
                <asp:Parameter Name="inactive" DefaultValue="1" />
            </SelectParameters>
            <DeleteParameters>
                <asp:Parameter Name="actionId" />
                <asp:Parameter Name="count" Direction="Output" DbType="Int16" Size="4" />
            </DeleteParameters>
        </asp:SqlDataSource>
        <dx:ASPxGridView ID="gvContractTypes" ClientInstanceName="gvContractTypes" runat="server" DataSourceID="dsContractTypes" 
            KeyFieldName="contractTypeId" Width="50%" Visible="false" Styles-Header-Font-Bold="true" OnRowDeleted="gvContractTypes_RowDeleted" OnRowUpdating="gvContractTypes_RowUpdating">
            <ClientSideEvents EndCallback="function(s, e) {  
                if (s.cpMessage) { //check  
                    alert(s.cpMessage); //alert  
                    delete s.cpMessage; //delete  
                }  
            }" />
            <Columns>
                <dx:GridViewCommandColumn ShowEditButton="true" ShowNewButton="true" CellStyle-HorizontalAlign="Left" Width="20%">
                    <HeaderTemplate>
                        <a href="javascript:gvContractTypes.AddNewRow()" style="text-decoration:underline;">New</a>
                    </HeaderTemplate>
                </dx:GridViewCommandColumn>
                <dx:GridViewDataColumn FieldName="contractType" Caption="Contract Type"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="contractTypeId" Visible="false"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="folder" Caption="Folder"></dx:GridViewDataColumn>
                <dx:GridViewDataCheckColumn FieldName="inactive" Caption="Inactive?"></dx:GridViewDataCheckColumn>
                <dx:GridViewCommandColumn ShowDeleteButton="true" CellStyle-HorizontalAlign="Left" Caption=" " Width="20%"></dx:GridViewCommandColumn>
            </Columns>
            <SettingsBehavior ConfirmDelete="true" />
            <SettingsDataSecurity AllowDelete="true" AllowEdit="true" AllowInsert="true" />
            <SettingsEditing Mode="Inline" UseFormLayout="true"></SettingsEditing>
            <SettingsPager Mode="ShowAllRecords"></SettingsPager>
            <SettingsText ConfirmDelete="Are you sure you want to delete this contract type?" />
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="dsContractTypes" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
            SelectCommand="usp_getContractTypes" SelectCommandType="StoredProcedure"
            InsertCommand="usp_addContractType" InsertCommandType="StoredProcedure" OnInserted="dsContractTypes_Inserted"
            UpdateCommand="usp_updateContractType" UpdateCommandType="StoredProcedure"
            DeleteCommand="usp_deleteContractType" DeleteCommandType="StoredProcedure" OnDeleted="dsContractTypes_Deleted">
            <SelectParameters>
                <asp:Parameter Name="includeInactive" DefaultValue="1" />
            </SelectParameters>
            <InsertParameters>
                <asp:Parameter Name="contractType" />
                <asp:Parameter Name="inactive" />
                <asp:Parameter Name="folder" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="contractType" />
                <asp:Parameter Name="contractTypeId" />
                <asp:Parameter Name="inactive" />
                <asp:Parameter Name="folder" />
            </UpdateParameters>
            <DeleteParameters>
                <asp:Parameter Name="contractTypeId" />
                <asp:Parameter Name="count" Direction="Output" DbType="Int16" Size="4" />
            </DeleteParameters>
        </asp:SqlDataSource>
        <dx:ASPxGridView ID="gvDepartments" ClientInstanceName="gvDepartments" runat="server" DataSourceID="dsDepartments" KeyFieldName="departmentId" Width="50%" Visible="false" Styles-Header-Font-Bold="true" OnRowDeleted="gvDepartments_RowDeleted">
            <ClientSideEvents EndCallback="function(s, e) {  
                if (s.cpMessage) { //check  
                    alert(s.cpMessage); //alert  
                    delete s.cpMessage; //delete  
                }  
            }" />
            <Columns>
                <dx:GridViewCommandColumn ShowEditButton="true" CellStyle-HorizontalAlign="Left" Width="20%"></dx:GridViewCommandColumn>
                <dx:GridViewDataColumn FieldName="department" Caption="Department" Width="100%" EditFormSettings-Visible="False" ReadOnly="true" ShowInCustomizationForm="false"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="exec1" Caption="Executive 1"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="exec2" Caption="Executive 2"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="deptldr1" Caption="Dept Leader 1"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="deptldr2" Caption="Dept Leader 2"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="prefix" Caption="Prefix" visible="false"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="nextPrefixNumber" Caption="Next Prefix Number" Visible="false"></dx:GridViewDataColumn>
                <dx:GridViewDataCheckColumn FieldName="inactive" Caption="Inactive?"></dx:GridViewDataCheckColumn>
                <dx:GridViewDataColumn FieldName="departmentId" Visible="false"></dx:GridViewDataColumn>
            </Columns>
            <SettingsBehavior ConfirmDelete="true" />
            <SettingsDataSecurity AllowEdit="true" /> 
            <EditFormLayoutProperties SettingsItems-HorizontalAlign="Right"></EditFormLayoutProperties>
            <SettingsPager Mode="ShowPager" PageSize="15"></SettingsPager>
            <SettingsText ConfirmDelete="Are you sure you want to delete this department?" />
            <Settings ShowHeaderFilterButton="true" />
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="dsDepartments" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
            DeleteCommand="usp_deleteDepartment" DeleteCommandType="StoredProcedure" OnDeleted="dsDepartments_Deleted"
            InsertCommand="usp_addDepartment" InsertCommandType="StoredProcedure" 
            SelectCommand="usp_getDepartments" SelectCommandType="StoredProcedure"
            UpdateCommand="usp_updateDepartment" UpdateCommandType="StoredProcedure">
            <SelectParameters>
                <asp:Parameter Name="includeInactive" DefaultValue="1" />
            </SelectParameters>
            <InsertParameters>
                <asp:Parameter Name="department" />
                <asp:Parameter Name="inactive" />
                <%--<asp:Parameter Name="prefix" />
                <asp:Parameter Name="nextPrefixNumber" />--%>
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="department" />
                <asp:Parameter Name="departmentId" />
                <%--<asp:Parameter Name="prefix" />
                <asp:Parameter Name="nextPrefixNumber" />--%>
                <asp:Parameter Name="inactive" />
            </UpdateParameters>
            <DeleteParameters>
                <asp:Parameter Name="departmentId" />
                <asp:Parameter Name="count" Direction="Output" DbType="Int16" Size="4" />
            </DeleteParameters>
        </asp:SqlDataSource>
        <dx:ASPxGridView ID="gvUploadDocumentTypes" ClientInstanceName="gvUploadDocumentTypes" runat="server" DataSourceID="dsUploadDocType" KeyFieldName="uploadDocumentTypeId" Visible="false" Width="50%" Styles-Header-Font-Bold="true" OnRowDeleted="gvUploadDocumentTypes_RowDeleted">
            <ClientSideEvents EndCallback="function(s, e) {  
                if (s.cpMessage) { //check  
                    alert(s.cpMessage); //alert  
                    delete s.cpMessage; //delete  
                }  
            }" />
            <Columns>
                <dx:GridViewCommandColumn ShowEditButton="true" CellStyle-HorizontalAlign="Left" Width="20%">
                    <HeaderTemplate>
                        <a href="javascript:gvUploadDocumentTypes.AddNewRow()" style="text-decoration:underline;">New</a>
                    </HeaderTemplate>
                </dx:GridViewCommandColumn>
                <dx:GridViewDataColumn FieldName="uploadDocumentType" Caption="Document Type" Width="100%"></dx:GridViewDataColumn>
                <dx:GridViewDataComboBoxColumn FieldName="folder" Caption="Folder">
                    <PropertiesComboBox>
                        <Items>
                            <dx:ListEditItem Text="CONTRACT" Value="CONTRACT" />
                            <dx:ListEditItem Text="PARTY" Value="PARTY" />
                        </Items>
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
                <dx:GridViewDataCheckColumn FieldName="inactive" Caption="Inactive?"></dx:GridViewDataCheckColumn>
                <dx:GridViewCommandColumn ShowDeleteButton="true" CellStyle-HorizontalAlign="Left" Caption=" " Width="20%"></dx:GridViewCommandColumn>
                <dx:GridViewDataColumn FieldName="uploadDocumentTypeId" Visible="false"></dx:GridViewDataColumn>
            </Columns>
            <SettingsBehavior ConfirmDelete="true" />
            <SettingsDataSecurity AllowDelete="true" AllowEdit="true" AllowInsert="true" />
            <SettingsEditing Mode="Inline" UseFormLayout="true"></SettingsEditing>
            <SettingsPager Mode="ShowAllRecords"></SettingsPager>
            <SettingsText ConfirmDelete="Are you sure you want to delete this document type?" />
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="dsUploadDocType" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
            DeleteCommand="usp_deleteUploadDocumentType" DeleteCommandType="StoredProcedure" OnDeleted="dsUploadDocType_Deleted"
            InsertCommand="usp_addUploadDocumentType" InsertCommandType="StoredProcedure" 
            SelectCommand="usp_getUploadDocumentTypes" SelectCommandType="StoredProcedure"
            UpdateCommand="usp_updateUploadDocumentType" UpdateCommandType="StoredProcedure">
            <SelectParameters>
                <asp:Parameter Name="includeInactive" DefaultValue="1" />
            </SelectParameters>
            <InsertParameters>
                <asp:Parameter Name="uploadDocumentType" />
                <asp:Parameter Name="folder" />
                <asp:Parameter Name="inactive" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="uploadDocumentType" />
                <asp:Parameter Name="uploadDocumentTypeId" />
                <asp:Parameter Name="folder" />
                <asp:Parameter Name="inactive" />
            </UpdateParameters>
            <DeleteParameters>
                <asp:Parameter Name="uploadDocumentTypeId" />
                <asp:Parameter Name="count" Direction="Output" DbType="Int16" Size="4" />
            </DeleteParameters>
        </asp:SqlDataSource>
        <dx:ASPxGridView ID="gvEOC" ClientInstanceName="gvEOC" runat="server" DataSourceID="dsEOC" KeyFieldName="eocId" Width="50%" Visible="false" Styles-Header-Font-Bold="true" OnRowDeleted="gvEOC_RowDeleted">
            <ClientSideEvents EndCallback="function(s, e) {  
                if (s.cpMessage) { //check  
                    alert(s.cpMessage); //alert  
                    delete s.cpMessage; //delete  
                }  
            }" />
            <Columns>
                <dx:GridViewCommandColumn ShowEditButton="true" CellStyle-HorizontalAlign="Left" Width="20%">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataColumn FieldName="eoc" Caption="EOC" ReadOnly="true" ShowInCustomizationForm="false" EditFormSettings-Visible="False"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="shortDescription" Caption="EOC Description" ReadOnly="true" ShowInCustomizationForm="false" EditFormSettings-Visible="False"></dx:GridViewDataColumn>
                <dx:GridViewDataCheckColumn FieldName="inactive" Caption="Inactive?"></dx:GridViewDataCheckColumn>
                <dx:GridViewDataColumn FieldName="eocId" Visible="false"></dx:GridViewDataColumn>
            </Columns>
            <SettingsBehavior ConfirmDelete="true" />
            <SettingsDataSecurity AllowEdit="true" /> 
            <EditFormLayoutProperties SettingsItems-HorizontalAlign="Right"></EditFormLayoutProperties>
            <SettingsPager Mode="ShowPager" PageSize="15"></SettingsPager>
            <SettingsText ConfirmDelete="Are you sure you want to delete this EOC?" />
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="dsEOC" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
            DeleteCommand="usp_deleteEoc" DeleteCommandType="StoredProcedure" OnDeleted="dsEOC_Deleted"
            InsertCommand="usp_addEoc" InsertCommandType="StoredProcedure" 
            SelectCommand="usp_getEocs" SelectCommandType="StoredProcedure"
            UpdateCommand="usp_updateEoc" UpdateCommandType="StoredProcedure">
            <SelectParameters>
                <asp:Parameter Name="includeInactive" DefaultValue="1" />
            </SelectParameters>
            <InsertParameters>
                <asp:Parameter Name="eoc" />
                <asp:Parameter Name="eocDescpription" />
                <asp:Parameter Name="inactive" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="eoc" />
                <asp:Parameter Name="eocId" />
                <asp:Parameter Name="eocDescription" />
                <asp:Parameter Name="inactive" />
            </UpdateParameters>
            <DeleteParameters>
                <asp:Parameter Name="eoc" />
                <asp:Parameter Name="count" Direction="Output" DbType="Int16" Size="4" />
            </DeleteParameters>
        </asp:SqlDataSource>
        <dx:ASPxGridView ID="gvPartyTypes" ClientInstanceName="gvPartyTypes" runat="server" DataSourceID="dsPartyTypes" KeyFieldName="partyTypeId" Width="50%" Visible="false" Styles-Header-Font-Bold="true" OnRowDeleted="gvPartyTypes_RowDeleted">
            <ClientSideEvents EndCallback="function(s, e) {  
                if (s.cpMessage) { //check  
                    alert(s.cpMessage); //alert  
                    delete s.cpMessage; //delete  
                }  
            }" />
            <Columns>
                <dx:GridViewCommandColumn ShowEditButton="true" CellStyle-HorizontalAlign="Left" Width="20%">
                    <HeaderTemplate>
                        <a href="javascript:gvPartyTypes.AddNewRow()" style="text-decoration:underline;">New</a>
                    </HeaderTemplate>
                </dx:GridViewCommandColumn>
                <dx:GridViewDataColumn FieldName="partyType" Caption="Party Type" Width="100%"></dx:GridViewDataColumn>
                <dx:GridViewDataCheckColumn FieldName="inactive" Caption="Inactive?"></dx:GridViewDataCheckColumn>
                <dx:GridViewCommandColumn ShowDeleteButton="true" CellStyle-HorizontalAlign="Left" Caption=" " Width="20%"></dx:GridViewCommandColumn>
                <dx:GridViewDataColumn FieldName="partyTypeId" Visible="false"></dx:GridViewDataColumn>
            </Columns>
            <SettingsBehavior ConfirmDelete="true" />
            <SettingsDataSecurity AllowDelete="true" AllowEdit="true" AllowInsert="true" />
            <SettingsEditing Mode="Inline" UseFormLayout="true"></SettingsEditing>
            <SettingsPager Mode="ShowAllRecords"></SettingsPager>
            <SettingsText ConfirmDelete="Are you sure you want to delete this party type?" />
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="dsPartyTypes" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
            DeleteCommand="usp_deletePartyType" DeleteCommandType="StoredProcedure" OnDeleted="dsPartyTypes_Deleted" 
            InsertCommand="usp_addPartyType" InsertCommandType="StoredProcedure" 
            SelectCommand="usp_getPartyTypes" SelectCommandType="StoredProcedure"
            UpdateCommand="usp_updatePartyType" UpdateCommandType="StoredProcedure">
            <InsertParameters>
                <asp:Parameter Name="partyType" />
                <asp:Parameter Name="inactive" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="partyType" />
                <asp:Parameter Name="partyTypeId" />
                <asp:Parameter Name="inactive" />
            </UpdateParameters>
            <DeleteParameters>
                <asp:Parameter Name="partyTypeId" />
                <asp:Parameter Name="count" Direction="Output" DbType="Int16" Size="4" />
            </DeleteParameters>
        </asp:SqlDataSource>
        <dx:ASPxGridView ID="gvPaymentType" ClientInstanceName="gvPaymentType" runat="server" DataSourceID="dsPaymentType"
            KeyFieldName="paymentTypeId" Width="50%" Visible="false" Styles-Header-Font-Bold="true" OnRowDeleted="gvPaymentType_RowDeleted">
            <ClientSideEvents EndCallback="function(s, e) {  
                if (s.cpMessage) { //check  
                    alert(s.cpMessage); //alert  
                    delete s.cpMessage; //delete  
                }  
            }" />
            <Columns>
                <dx:GridViewCommandColumn ShowEditButton="true" CellStyle-HorizontalAlign="Left" Width="20%">
                    <HeaderTemplate>
                        <a href="javascript:gvPaymentType.AddNewRow()" style="text-decoration:underline;">New</a>
                    </HeaderTemplate>
                </dx:GridViewCommandColumn>
                <dx:GridViewDataColumn FieldName="paymentType" Caption="Payment Type"></dx:GridViewDataColumn>
                <dx:GridViewDataCheckColumn FieldName="inactive" Caption="Inactive?"></dx:GridViewDataCheckColumn>
                <dx:GridViewDataColumn FieldName="paymentTypeId" Visible="false"></dx:GridViewDataColumn>
                <dx:GridViewCommandColumn ShowDeleteButton="true" ButtonRenderMode="Link" Caption=" "></dx:GridViewCommandColumn>
            </Columns>
            <SettingsBehavior ConfirmDelete="true" />
            <SettingsText ConfirmDelete="Are you sure you want to delete this payment type?" />
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="dsPaymentType" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
            SelectCommand="usp_getPaymentTypes" SelectCommandType="StoredProcedure"
            InsertCommand="usp_addPaymentType" InsertCommandType="StoredProcedure"
            UpdateCommand="usp_updatePaymentType" UpdateCommandType="StoredProcedure"
            DeleteCommand="usp_deletePaymentType" DeleteCommandType="StoredProcedure" OnDeleted="dsPaymentType_Deleted">
            <InsertParameters>
                <asp:Parameter Name="paymentType" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="paymentTypeId" />
                <asp:Parameter Name="paymentType" />
                <asp:Parameter Name="inactive" />
            </UpdateParameters>
            <SelectParameters>
                <asp:Parameter Name="inactive" DefaultValue="1" />
            </SelectParameters>
            <DeleteParameters>
                <asp:Parameter Name="paymentTypeId" />
                <asp:Parameter Name="count" Direction="Output" DbType="Int16" Size="4" />
            </DeleteParameters>
        </asp:SqlDataSource>
        <dx:ASPxGridView ID="gvPurchaseGroups" ClientInstanceName="gvPurchaseGroups" runat="server" DataSourceID="dsPurchaseGroups" KeyFieldName="purchaseGroupId" Width="50%" Visible="false" Styles-Header-Font-Bold="true" OnRowDeleted="gvPurchaseGroups_RowDeleted">
            <ClientSideEvents EndCallback="function(s, e) {  
                if (s.cpMessage) { //check  
                    alert(s.cpMessage); //alert  
                    delete s.cpMessage; //delete  
                }  
            }" />
            <Columns>
                <dx:GridViewCommandColumn ShowEditButton="true" CellStyle-HorizontalAlign="Left" Width="20%">
                    <HeaderTemplate>
                        <a href="javascript:gvPurchaseGroups.AddNewRow()" style="text-decoration:underline;">New</a>
                    </HeaderTemplate>
                </dx:GridViewCommandColumn>
                <dx:GridViewDataColumn FieldName="groupName" Caption="Purchase Group" Width="100%"></dx:GridViewDataColumn>
                <dx:GridViewDataCheckColumn FieldName="inactive" Caption="Inactive?"></dx:GridViewDataCheckColumn>
                <dx:GridViewCommandColumn ShowDeleteButton="true" CellStyle-HorizontalAlign="Left" Caption=" " Width="20%"></dx:GridViewCommandColumn>
                <dx:GridViewDataColumn FieldName="purchaseGroupId" Visible="false"></dx:GridViewDataColumn>
            </Columns>
            <SettingsBehavior ConfirmDelete="true" />
            <SettingsDataSecurity AllowDelete="true" AllowEdit="true" AllowInsert="true" />
            <SettingsEditing Mode="Inline" UseFormLayout="true"></SettingsEditing>
            <SettingsPager Mode="ShowAllRecords"></SettingsPager>
            <SettingsText ConfirmDelete="Are you sure you want to delete this purchase group?" />
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="dsPurchaseGroups" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
            DeleteCommand="usp_deletePurchaseGroup" DeleteCommandType="StoredProcedure" OnDeleted="dsPurchaseGroups_Deleted"
            InsertCommand="usp_addPurchaseGroup" InsertCommandType="StoredProcedure" 
            SelectCommand="usp_getPurchasingGroups" SelectCommandType="StoredProcedure"
            UpdateCommand="usp_updatePurchasingGroup" UpdateCommandType="StoredProcedure">
            <InsertParameters>
                <asp:Parameter Name="groupName" />
                <asp:Parameter Name="inactive" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="groupName" />
                <asp:Parameter Name="purchaseGroupId" />
                <asp:Parameter Name="inactive" />
            </UpdateParameters>
            <DeleteParameters>
                <asp:Parameter Name="purchaseGroupId" />
                <asp:Parameter Name="count" Direction="Output" DbType="Int16" Size="4" />
            </DeleteParameters>
        </asp:SqlDataSource>
        <dx:ASPxGridView ID="gvStageTypes" ClientInstanceName="gvStageTypes" runat="server" DataSourceID="dsStageTypes" KeyFieldName="stageTypeId" Width="50%" Visible="false" Styles-Header-Font-Bold="true">
            <Columns>
                <dx:GridViewCommandColumn ShowEditButton="true" CellStyle-HorizontalAlign="Left" Width="20%">
                    <HeaderTemplate>
                        <a href="javascript:gvStageTypes.AddNewRow()" style="text-decoration:underline;">New</a>
                    </HeaderTemplate>
                </dx:GridViewCommandColumn>
                <dx:GridViewDataColumn FieldName="stageType" Caption="Stage Type" Width="100%"></dx:GridViewDataColumn>
                <dx:GridViewCommandColumn ShowDeleteButton="true" CellStyle-HorizontalAlign="Left" Caption=" " Width="20%"></dx:GridViewCommandColumn>
                <dx:GridViewDataColumn FieldName="stageTypeId" Visible="false"></dx:GridViewDataColumn>
            </Columns>
            <SettingsBehavior ConfirmDelete="true" />
            <SettingsDataSecurity AllowDelete="true" AllowEdit="true" AllowInsert="true" />
            <SettingsEditing Mode="Inline" UseFormLayout="true"></SettingsEditing>
            <SettingsPager Mode="ShowAllRecords"></SettingsPager>
            <SettingsText ConfirmDelete="Are you sure you want to delete this stage type?" />
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="dsStageTypes" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
            DeleteCommand="usp_deleteStageType" DeleteCommandType="StoredProcedure" 
            InsertCommand="usp_addStageType" InsertCommandType="StoredProcedure" 
            SelectCommand="usp_getStageTypes" SelectCommandType="StoredProcedure"
            UpdateCommand="usp_updateStageType" UpdateCommandType="StoredProcedure">
            <InsertParameters>
                <asp:Parameter Name="stageType" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="stageType" />
                <asp:Parameter Name="stageTypeId" />
            </UpdateParameters>
            <DeleteParameters>
                <asp:Parameter Name="stageTypeId" />
            </DeleteParameters>
        </asp:SqlDataSource>
        <dx:ASPxGridView ID="gvStatusTypes" ClientInstanceName="gvStatusTypes" runat="server" DataSourceID="dsStatusTypes" KeyFieldName="statusTypeId" Width="50%" Visible="false" Styles-Header-Font-Bold="true" OnRowDeleted="gvStatusTypes_RowDeleted">
            <ClientSideEvents EndCallback="function(s, e) {  
                if (s.cpMessage) { //check  
                    alert(s.cpMessage); //alert  
                    delete s.cpMessage; //delete  
                }  
            }" />
            <Columns>
                <dx:GridViewCommandColumn ShowEditButton="true" CellStyle-HorizontalAlign="Left" Width="20%">
                    <HeaderTemplate>
                        <a href="javascript:gvStatusTypes.AddNewRow()" style="text-decoration:underline;">New</a>
                    </HeaderTemplate>
                </dx:GridViewCommandColumn>
                <dx:GridViewDataColumn FieldName="statusType" Caption="Status Type" Width="100%"></dx:GridViewDataColumn>
                <dx:GridViewDataCheckColumn FieldName="inactive" Caption="Inactive?"></dx:GridViewDataCheckColumn>
                <dx:GridViewCommandColumn ShowDeleteButton="true" CellStyle-HorizontalAlign="Left" Caption=" " Width="20%"></dx:GridViewCommandColumn>
            </Columns>
            <SettingsBehavior ConfirmDelete="true" />
            <SettingsDataSecurity AllowDelete="true" AllowEdit="true" AllowInsert="true" />
            <SettingsEditing Mode="Inline" UseFormLayout="true"></SettingsEditing>
            <SettingsPager Mode="ShowAllRecords"></SettingsPager>
            <SettingsText ConfirmDelete="Are you sure you want to delete this status type?" />
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="dsStatusTypes" runat="server" ConnectionString="<%$ ConnectionStrings:cmsConn %>" 
            DeleteCommand="usp_deleteStatusType" DeleteCommandType="StoredProcedure" OnDeleted="dsStatusTypes_Deleted"
            InsertCommand="usp_addStatusType" InsertCommandType="StoredProcedure" 
            SelectCommand="usp_getStatusTypes" SelectCommandType="StoredProcedure"
            UpdateCommand="usp_updateStatusType" UpdateCommandType="StoredProcedure">
            <InsertParameters>
                <asp:Parameter Name="statusType" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="statusType" />
                <asp:Parameter Name="statusTypeId" />
                <asp:Parameter Name="inactive" />
            </UpdateParameters>
            <SelectParameters>
                <asp:Parameter Name="inactive" DefaultValue="1" />
            </SelectParameters>
            <DeleteParameters>
                <asp:Parameter Name="statusTypeId" />
                <asp:Parameter Name="count" Direction="Output" DbType="Int16" Size="4" />
            </DeleteParameters>
        </asp:SqlDataSource>
    </div>
</asp:Content>