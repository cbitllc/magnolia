﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Magnolia.app
{
    public partial class adminAlerts : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ((Master)Page.Master).SetHeader = "Admin Alerts";
            ((Master)Page.Master).SetHelpURL = "admin.aspx";
            if (Session["isAdmin"] == null || Session["isAdmin"] == System.DBNull.Value || Convert.ToBoolean(Session["isAdmin"]) == false)
            {
                Response.Redirect("default.aspx");
            }
        }
    }
}