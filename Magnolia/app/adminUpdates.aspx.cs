﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Magnolia.app
{
    public partial class adminUpdates : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ((Master)Page.Master).SetHeader = "Admin Updates";
            ((Master)Page.Master).SetHelpURL = "admin.aspx";
        }

        protected void btnRestoreSAM_Click(object sender, EventArgs e)
        {
            string message = restoreTable("SAM","usp_restoreSAM");
            if (message.All(char.IsNumber))
            {
                Message.Alert("SAM Exclusion data (" + message + "rows ) restored from backup");
            }
            else
            {
                Message.Alert("Restore process error: " + message);
            }
        }

        protected void btnRestoreEOC_Click(object sender, EventArgs e)
        {
            string message = restoreTable("EOC", "usp_restoreEOC");
            if (message.All(char.IsNumber))
            {
                Message.Alert("EOC data (" + message + "rows ) restored from backup");
            }
            else
            {
                Message.Alert("Restore process error: " + message);
            }
        }

        protected void btnRestoreDept_Click(object sender, EventArgs e)
        {
            string message = restoreTable("Depet", "usp_restoreDepartment");
            if (message.All(char.IsNumber))
            {
                Message.Alert("Department data (" + message + " rows) restored from backup");
            }
            else
            {
                Message.Alert("Restore process error: " + message);
            }
        }

        protected void btnRestoreVendor_Click(object sender, EventArgs e)
        {
            string message = restoreTable("Vendor", "usp_restoreVendor");
            if (message.All(char.IsNumber))
            {
                Message.Alert("Vendor data (" + message + " rows) restored from backup");
            }
            else
            {
                Message.Alert("Restore process error: " + message);
            }
        }

        private string restoreTable(string tableName, string queryName)
        {
            System.Data.DataSet ds = DataAccess.GetDataSet(queryName, new string[] { }, new string[] { });
            string returnMessage = "";
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0]["message"] != DBNull.Value)
            {
                returnMessage = ds.Tables[0].Rows[0]["message"].ToString();
            }
            return returnMessage;
        }
    }
}