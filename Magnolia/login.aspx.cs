﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Management.Automation;
using System.ServiceModel.Security;
using System.Web.Security;
using DevExpress.XtraDiagram.Native;
using Magnolia.app;

namespace Magnolia
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                FormsAuthentication.SignOut();
        }

        protected void btnlogin_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = null;
            string query = "usp_getUserLDAPLogin";
            bool isAdmin = false;
            string ldap = "";
            string fullName = "";
            AppUser user = null;
            ErrorText.InnerText = "";
            string ip = "JODIE";
            if (System.Configuration.ConfigurationManager.AppSettings["system"] != "LOCALHOST")
            {
                ip = Request.UserHostAddress;
            }
            if (System.Configuration.ConfigurationManager.AppSettings["system"] == "LOCALHOST" || Request.Url.Host.Contains("azure"))
            {
                query = "usp_getUserLogin"; 
                user = new AppUser();
                if(txtuser.Value == "pals1")
                {
                    user.Name = "pals1";
                    user.DisplayName = "Shannon Palmer";
                    user.EmailAddress = "";
                    user.SamAccountName = "pals1";
                    user.GroupMembership.Add("CN=G.CMS.A_Employment,OU=SECURITY_GROUPS,DC=mrhc,DC=org");
                    user.GroupMembership.Add("CN=G.CMS.A_Education,OU=SECURITY_GROUPS,DC=mrhc,DC=org");
                    query = "usp_getUserLDAPLogin";
                }
                if (txtuser.Value == "barj4")
                {
                    user.Name = "barj4";
                    user.DisplayName = " Jodie Baril";
                    user.EmailAddress = "jodie@cbit-llc.com";
                    user.SamAccountName = "barj4";
                    user.GroupMembership.Add("CN=G.CMS.Administrators,OU=SECURITY_GROUPS,DC=mrhc,DC=org");
                    user.GroupMembership.Add("CN=G.FS.Contract_Database_Modify,OU=SECURITY_GROUPS,DC=mrhc,DC=org,");
                    query = "usp_getUserLDAPLogin";
                }
                if (txtuser.Value == "test2")
                {
                    user.Name = "test_user";
                    user.DisplayName = " TEST USER";
                    user.EmailAddress = "test@test.com";
                    user.SamAccountName = "test2";
                    user.GroupMembership.Add("CN=G.CMS.MC,OU=SECURITY_GROUPS,DC=mrhc,DC=org");
                    //user.GroupMembership.Add("CN=G.FS.Contract_Database_Modify,OU=SECURITY_GROUPS,DC=mrhc,DC=org,");
                    query = "usp_getUserLDAPLogin";
                }
                if (txtuser.Value == "svccms")
                {
                    user.Name = "svccms";
                    user.DisplayName = "Contract Management Service";
                    user.EmailAddress = "";
                    user.SamAccountName = "svccms";
                    user.GroupMembership.Add("CN=G.CMSA.EDUCATION,OU=SECURITY_GROUPS,DC=mrhc,DC=org");
                    user.GroupMembership.Add("CN=G.CMSA.LEASE,OU=SECURITY_GROUPS,DC=mrhc,DC=org");
                    user.GroupMembership.Add("CN=G.FS.Contract_Database_Modify,OU=SECURITY_GROUPS,DC=mrhc,DC=org,");
                    query = "usp_getUserLDAPLogin";
                }
                if (txtuser.Value == "davbr")
                {
                    user.Name = "davbr";
                    user.DisplayName = "Brian Davis";
                    user.EmailAddress = "bdavis@mrhc.org";
                    user.SamAccountName = "davbr";
                    user.GroupMembership.Add("CN=G.CMS.Administrators,OU=SECURITY_GROUPS,DC=mrhc,DC=org");
                    user.GroupMembership.Add("CN=G.CMS.EXDirector,OU=SECURITY_GROUPS,DC=mrhc,DC=org");
                    user.GroupMembership.Add("CN=G.CMS.IT,OU=SECURITY_GROUPS,DC=mrhc,DC=org");
                    user.CanSeeEmployeeContracts = true;
                    user.CanSeeEducationContracts = true;
                    user.CanSeeLeaseContracts = true;
                    query = "usp_getUserLDAPLogin";
                }
                if (txtuser.Value == "ldavid")
                {
                    user.Name = "ldavid";
                    user.DisplayName = "Lisa Davidson";
                    user.EmailAddress = "ldavid@mrhc.org";
                    user.SamAccountName = "davli";
                    user.GroupMembership.Add("CN=G.CMS.Administrators,OU=SECURITY_GROUPS,DC=mrhc,DC=org");
                    user.GroupMembership.Add("CN=G.CMS.Manager,OU=SECURITY_GROUPS,DC=mrhc,DC=org");
                    user.CanSeeEmployeeContracts = true;
                    user.CanSeeEducationContracts = true;
                    user.CanSeeLeaseContracts = true;
                    query = "usp_getUserLDAPLogin";
                }
                if (txtuser.Value == "tmoore")
                {
                    user.Name = "tmoore";
                    user.DisplayName = "Traci Moore";
                    user.EmailAddress = "tmoored@mrhc.org";
                    user.SamAccountName = "tmoore";
                    user.GroupMembership.Add("CN=G.CMS.Administrators,OU=SECURITY_GROUPS,DC=mrhc,DC=org");
                    user.GroupMembership.Add("CN=G.CMS.Manager,OU=SECURITY_GROUPS,DC=mrhc,DC=org");
                    user.CanSeeEmployeeContracts = true;
                    user.CanSeeEducationContracts = true;
                    user.CanSeeLeaseContracts = true;
                    query = "usp_getUserLDAPLogin";
                }
                if (txtuser.Value == "rbullard")
                {
                    user.Name = "rbullard";
                    user.DisplayName = "Renee Bullard";
                    user.EmailAddress = "rbullard@mrhc.org";
                    user.SamAccountName = "rbullard";
                    user.GroupMembership.Add("CN=G.CMS.A_Administrativ,OU=SECURITY_GROUPS,DC=mrhc,DC=orge");
                    user.GroupMembership.Add("CN=G.CMS.A_Lease,OU=SECURITY_GROUPS,DC=mrhc,DC=org");
                    user.GroupMembership.Add("CN=G.CMS.A_Employment,OU=SECURITY_GROUPS,DC=mrhc,DC=org");
                    user.GroupMembership.Add("CN=G.CMS.A_Education,OU=SECURITY_GROUPS,DC=mrhc,DC=org");
                    user.GroupMembership.Add("CN=G.CMS.RM,OU=SECURITY_GROUPS,DC=mrhc,DC=org");
                    user.GroupMembership.Add("CN=G.CMS.CO,OU=SECURITY_GROUPS,DC=mrhc,DC=org");
                    user.CanSeeAdministrativeContracts = true;
                    user.CanSeeEducationContracts = true;
                    user.CanSeeEmployeeContracts = true;
                    user.CanSeeePhoContracts = true;
                    user.CanSeeLeaseContracts = true;
                    query = "usp_getUserLDAPLogin";
                }
                if (txtuser.Value == "Mat Mgmt Dir" || txtuser.Value == "mmd")
                {
                    user.Name = "mmd";
                    user.DisplayName = "Mat Mgmt Dir";
                    user.GroupMembership.Add("CN=G.CMS.MM,OU=SECURITY_GROUPS,DC=mrhc,DC=org");
                    query = "usp_getUserLDAPLogin";
                }
                if (txtuser.Value == "Clinical Eng" || txtuser.Value == "clin_eng")
                {
                    user.Name = "clin_eng";
                    user.DisplayName = "Clinical Eng";
                    user.GroupMembership.Add("CN=G.CMS.CE,OU=SECURITY_GROUPS,DC=mrhc,DC=org");
                    query = "usp_getUserLDAPLogin";
                }
                foreach (string member in user.GroupMembership)
                {
                    user.ldap += member + ", ";
                    if (member.Contains("G.CMS.") == true)
                    {
                        string group = member.Replace("CN=G.CMS.", "G.CMS.");
                        int end = group.IndexOf(",") ;
                        if (user.ldapGroup != "")
                        {
                            user.ldapGroup += ",";
                        }
                        user.ldapGroup += group.Substring(0,end);
                    }
                    int startIndex = member.IndexOf("OU=") + 3;
                    string partial = member.Substring(startIndex);
                    int midIndex = partial.IndexOf(",");
                    string fullDeptName = partial.Substring(0, midIndex);
                    user.department = fullDeptName.Replace("_GROUPS", "");
                    if (member.ToUpper().Contains("G.CMS.A_EMPLOYMENT") == true)
                    {
                        user.CanSeeEmployeeContracts = true;
                    }
                    if (member.ToUpper().Contains("G.CMS.A_EDUCATION") == true)
                    {
                        user.CanSeeEducationContracts = true;
                    }
                    if (member.ToUpper().Contains("G.CMS.A_LEASE") == true)
                    {
                        user.CanSeeLeaseContracts = true;
                    }
                    if (member.ToUpper().Contains("G.CMS.MC") == true || member.ToUpper().Contains("G.CMS.RC") == true || member.ToUpper().Contains("G.CMS.PP"))
                    {
                        user.CanSeeePhoContracts = true;
                    }
                    //@userName = 'barj4', @fullName = ' Jodie Baril', @emailAddress = 'jodie@cbit-llc.com', @department = 'SECURITY_GROUPS',@ldapGroup = 'G.CMS.Administrators'
                    //@userName = 'svccms', @fullName = 'Contract Management Service', @emailAddress = '', @department = 'SECURITY_GROUPS',@ldapGroup = ''
                }
                if (user.ldapGroup == "")
                {
                    user.ldapGroup = "G.CMS.Requestor";
                }
                if (user.ldapGroup.Contains("Administrator"))
                {
                    isAdmin = true;
                }
                ldap = user.ldap;
                //string path = Server.MapPath("/Logs/ldapLog.txt");
                //if (System.IO.File.Exists(path))
                //{
                //    using (System.IO.StreamWriter sw = System.IO.File.AppendText(path))
                //    {
                //        sw.WriteLine(user.Name + ": " + user.ldapGroup);
                //    }
                //}
            }
            else
            {
                try
                {
                    //Message.LogError("login", "LDAP Auth", txtuser.Value, DateTime.Now.ToString());
                    //DataSet dsLdap = DataAccess.GetDataSet("usp_addError", new string[] { "@fileName", "@functionName", "@userName", "@errorMessage" },
                    //new string[] { "login", "LDAP Auth", txtuser.Value, DateTime.Now.ToString() });
                    string auth = LDAP.AuthenticateUser("LDAP://mrhc.org", txtuser.Value.ToString(), txtpassword.Value.ToString());
                    if (auth == "true")
                    {
                    }
                    else
                    {
                        ErrorText.InnerText = auth;
                        btnlogin.Disabled = false;
                    }
                    //dsLdap = DataAccess.GetDataSet("usp_addError", new string[] { "@fileName", "@functionName", "@userName", "@errorMessage" },
                    //new string[] { "login", "GetUser", txtuser.Value, DateTime.Now.ToString() });
                    //Message.LogError("login", "GetUser", txtuser.Value, DateTime.Now.ToString());
                    user = LDAP.GetUser(txtuser.Value.ToString());
                    if (user == null || ErrorText.InnerText != "")
                    {
                        //Session["user"] = "";
                        //Session.Abandon();
                        //FormsAuthentication.SignOut();
                        ErrorText.InnerText += " Unable to locate user in the MRHC Network. ";
                        string message = "";
                        if (user == null)
                        {
                            message = "Failed LDAP Authentication.";
                        }
                        else
                        {
                            message = user.Error;
                        }
                        if (ErrorText.InnerText != "")
                        {
                            message += ErrorText.InnerText;
                        }
                        DataSet dsLog = Magnolia.app.DataAccess.GetDataSet("usp_logLogin", new string[] { "@username", "@ip", "@ldap" }, new string[] { txtuser.Value.ToString(), ip, message });
                    }
                    else
                    {
                        if (user.Error != "" || ErrorText.InnerText != "")
                        {
                            ldap += user.Error;
                            //Session["user"] = "";
                            //Session.Abandon();
                            //FormsAuthentication.SignOut();
                            ErrorText.InnerText = "Error retrieving user. " + user.Error;
                            btnlogin.Disabled = false;
                        }
                        else
                        {
                            //ldap = "@userName='" + txtuser.Value.ToString() + "',";
                            try
                            {
                                string[] names = user.Name.Split(',');
                                for (int i = names.Length - 1; i >= 0; i--)
                                {
                                    fullName += names[i];
                                    if (i > 0)
                                    {
                                        fullName += " ";
                                    }
                                }
                                    ldap += " @fullName='" + fullName.Trim() + "',"; ;
                                    user.DisplayName = fullName;
                            }
                            catch (Exception ex)
                            {
                                Message.Alert(ex.Message);
                            }
                            try
                            {
                                ldap += " @emailAddress='" + user.EmailAddress + "',"; ;
                            }
                            catch (Exception ex)
                            {
                                Message.Alert(ex.Message);
                            }
                            try
                            {
                                ldap += " @department='" + user.department + "',";
                                if (user.ldapGroup.Contains("Administrator"))
                                {
                                    isAdmin = true;
                                }
                                //foreach (string memberOf in user.GroupMembership)
                                //{
                                //    if (memberOf.Contains("G.CMS.") == true)
                                //    {
                                //        string group = memberOf.Replace("CN=", "");
                                //        group = group.Substring(0, group.IndexOf(","));
                                //        ldapGroup = group;
                                //        if (ldapGroup.Contains("Administrators"))
                                //        {
                                //            isAdmin = true;
                                //        }
                                //        break;
                                //    }
                                //}
                                ldap += "@ldapGroup='" + user.ldapGroup + "'"; ;
                            }
                            catch (Exception ex)
                            {
                                Message.Alert(ex.Message);
                                Message.LogError("login.aspx", "btnlogin_ServerClick-Read LDAP Groups", txtuser.Value, ex.Message);
                            }
                            ldap = user.ldap;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //Session["user"] = "";
                    //Session.Abandon();
                    //FormsAuthentication.SignOut();
                    ErrorText.InnerText = ex.Message;
                    btnlogin.Disabled = false;
                    Message.LogError("login.aspx", "btnlogin_ServerClick-LDAP", txtuser.Value, ex.Message);
                }
            }
            if (ErrorText.InnerText == "")
            {
                try
                {
                    DataSet dsLog = DataAccess.GetDataSet("usp_logLogin", new string[] { "@username", "@ip", "@ldap" }, new string[] { txtuser.Value.ToString(), ip, user.ldapGroup });
                }
                catch(Exception ex)
                {
                    Message.LogError("login.aspx", "btnlogin_ServerClick-Log login", txtuser.Value, ex.Message);
                }
                //if (user.department == "")
                //{
                //    Session["user"] = "";
                //    Session.Abandon();
                //    FormsAuthentication.SignOut();
                //    ErrorText.InnerText = "User/Department is not authorized for CMS access.";
                //}
                //else
                //{
                try
                {
                    if (query == "usp_getUserLDAPLogin")
                    {
                        ds = DataAccess.GetDataSet(query, new string[] { "@username", "@fullName", "@emailAddress", "@phoneNumber", "@isAdmin", "@department", "@ldapGroup",
                        "@isEducationVisible", "@isEmploymentVisible", "@isLeaseVIsible", "@isAdministrativeVisible", "@isPhoVisible" },
                            new string[] { txtuser.Value.ToString().ToUpper(), user.DisplayName.Trim(), user.EmailAddress, user.PhoneNumber, isAdmin == true ? "1" : "0",
                        user.department, user.ldapGroup, user.CanSeeEducationContracts == true ? "1": "0", user.CanSeeEmployeeContracts == true ? "1" : "0", 
                                user.CanSeeLeaseContracts == true ? "1" : "0", user.CanSeeAdministrativeContracts == true ? "1":"0", user.CanSeeePhoContracts == true ? "1":"0" });
                    }
                    else
                    {
                        ds = DataAccess.GetDataSet(query, new string[] { "@username", "@password" }, new string[] { txtuser.Value.ToString(), txtpassword.Value });
                    }
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                    Message.Alert(ex.Message);
                    Message.LogError("login.aspx", "btnlogin_ServerClick-Login", txtuser.Value, ex.Message);
                }
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    try
                    {
                        if (ds.Tables[0].Rows[0]["fullName"] == System.DBNull.Value)
                        {
                            Session["UserName"] = txtuser.Value;
                        }
                        else
                        {
                            Session["UserName"] = ds.Tables[0].Rows[0]["fullName"].ToString();
                        }
                        if (ds.Tables[0].Rows[0]["emailAddress"] == System.DBNull.Value)
                        {
                            Session["email"] = "";
                        }
                        else
                        {
                            Session["email"] = ds.Tables[0].Rows[0]["emailAddress"].ToString();
                        }
                        if (System.Configuration.ConfigurationManager.AppSettings["system"] == "LOCALHOST" || Request.Url.Host.Contains("azure"))
                        {
                            Session["ceoEmail"] = "jodie@cbit-llc.com";
                        }
                        else
                        {
                            Session["ceoEmail"] = "ldavidson@mrhc.org"; //ds.Tables[0].Rows[0]["ceoEmail"].ToString();
                        }
                        if (ds.Tables[0].Rows[0]["roleName"] == System.DBNull.Value)
                        {
                            Session["role"] = "Contract Requestor";
                        }
                        else
                        {
                            Session["role"] = ds.Tables[0].Rows[0]["roleName"].ToString();
                        }
                        if (ds.Tables[0].Rows[0]["isAdmin"] != System.DBNull.Value)
                        {
                            Session["isAdmin"] = Convert.ToBoolean(ds.Tables[0].Rows[0]["isAdmin"]);
                        }
                        else
                        {
                            Session["isAdmin"] = false;
                        }
                        Session["UserID"] = ds.Tables[0].Rows[0]["userId"];
                        if (!string.IsNullOrEmpty(Request.QueryString["ReturnUrl"]))
                        {
                            Response.Redirect(Request.QueryString["ReturnUrl"], true);
                        }
                        else
                        {
                            if (System.Configuration.ConfigurationManager.AppSettings["system"] == "LOCALHOST")
                            {
                                Response.Redirect("app/contracts.aspx");
                            }
                            else
                            {
                                FormsAuthentication.RedirectFromLoginPage(txtuser.Value.ToString().ToUpper(), false);
                            }
                        }
                    }
                    catch(Exception ex)
                    {
                        ErrorText.InnerText = "Error creating user from database.";
                        btnlogin.Disabled = false;
                        Message.LogError("login.aspx", "btnlogin_ServerClick-Set Session Variables", txtuser.Value, ex.Message);
                    }
                }
                else
                {
                    ErrorText.InnerText = "Unable to authenticate user in the CMS database.";
                    btnlogin.Disabled = false;
                    Message.LogError("login.aspx", "btnlogin_ServerClick-Login Results", txtuser.Value, "User not found");
                }
                //}
            }
        }
    }
}