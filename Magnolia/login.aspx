﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="Magnolia.login" %>
<%@ Register Assembly="DevExpress.Web.v20.2, Version=20.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<!DOCTYPE html>
<html class="wide wow-animation scrollTo smoothscroll" lang="en">
<head>
    <!-- Site Title-->
    <title>Login</title>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Montserrat:400,700%7CLato:300,300italic,400,700,900%7CYesteryear">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <!-- Page-->
    <div class="page text-center">
        <!-- Page Content-->
        <main class="page-content bg-shark-radio">
            <div class="one-page">
                <div class="one-page-header">
                    <!--Navbar Brand-->
                </div>
                <!-- Login-->
                <section>
                    <div class="shell">
                        <div class="range">
                            <div class="section-110 section-cover range range-xs-center range-xs-middle">
                                <div class="cell-xs-8 cell-sm-6 cell-md-4">
                                    <div class="panel section-34 section-sm-41 inset-left-20 inset-right-20 inset-sm-left-20 inset-sm-right-20 inset-lg-left-30 inset-lg-right-30 bg-white shadow-drop-md">
                                        <!-- Icon Box Type 4-->
                                        <div class="rd-navbar-brand">
                                            <div class="offset-top-24 text-darker big text-bold">MRHC Contract Management System</div>
                                            <%-- <a href="default.aspx">
                                                <img style='margin-top: -5px; margin-left: -15px;' width='300' height='126' src='images/childs/cbit.png' alt='' /></a>--%>
                                        </div>
                                        <div>
                                            <div class="offset-top-24 text-darker big text-bold">Login to your account</div>
                                            <p class="text-extra-small text-dark offset-top-4">Enter your credentials below</p>
                                        </div>
                                        <!-- RD Mailform-->
                                        <form class="text-left offset-top-30" data-form-output="form-output-global" data-form-type="contact" runat="server" id="form1">
                                            <div class="form-group">
                                                <div class="input-group input-group-sm">
                                                    <span class="input-group-addon input-group-addon-inverse"><span class="input-group-icon mdi mdi-account-outline"></span></span>
                                                    <input class="form-control" id="txtuser" placeholder="Your Login" type="text" name="txtlogin" data-constraints="@Required" runat="server">
                                                </div>
                                            </div>
                                            <div class="form-group offset-top-20">
                                                <div class="input-group input-group-sm">
                                                    <span class="input-group-addon input-group-addon-inverse"><span class="input-group-icon mdi mdi-lock-open-outline"></span></span>
                                                    <input class="form-control" id="txtpassword" placeholder="Your Password" type="password" name="txtpassword" data-constraints="@Required" runat="server">
                                                </div>
                                            </div>
                                            <label ID="ErrorText" style="color:red" runat="server"></label>
                                            <button class="btn btn-sm btn-icon btn-block btn-primary offset-top-20" runat="server" id="btnlogin" onclick="this.disabled=true;ErrorText.innerHTML='';" onserverclick="btnlogin_ServerClick">Sign In <%--<span class="icon mdi mdi-arrow-right-bold-circle-outline"></span>--%></button>
                                            <dx:ASPxPopupControl ID="popPrivacy" runat="server" ClientInstanceName="popPrivacy" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" HeaderText="MRHC Contract Management System<br/>Privacy Policy" EncodeHtml="false" HeaderStyle-HorizontalAlign="Center" ShowCloseButton="true" Modal="true" Width="450">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl Width="100%">
                                                        <dx:ASPxRoundPanel ID="pnlPrivacy" runat="server" ShowHeader="false" Font-Size="Large" HorizontalAlign="Justify">
                                                            <PanelCollection>
                                                                <dx:PanelContent>
                                                                    This is a secured, private computer system owned by Magnolia Regional Health Center (MRHC). 
                                                                    All information contained on this system is deemed to be PRIVATE, PROPRIETARY, CONFIDENTIAL, and the property of MRHC, its affiliates, divisions, or subsidiaries. 
                                                                    Unauthorized access or use is strictly prohibited. Any use of MRHC resources must be in compliance with MRHC policies. 
                                                                    Any unauthorized access to or use of MRHC Resources may be punishable in a court of law and include termination of employment or contract with MRHC. 
                                                                    <br /><br />
                                                                    <div style="width:100%;text-align:center;">
                                                                        <dx:ASPxButton ID="btnClose" runat="server" Text="Close" Theme="Metropolis" EnableTheming="true" Font-Size="Medium">
                                                                            <ClientSideEvents Click="function (s,e) {popPrivacy.Hide();}" />
                                                                        </dx:ASPxButton>
                                                                    </div>
                                                                </dx:PanelContent>
                                                            </PanelCollection>
                                                        </dx:ASPxRoundPanel>
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                            </dx:ASPxPopupControl>
                                        </form>
                                        <!--
                      <div class="offset-top-30 text-sm-left text-dark text-extra-small"><a class="text-picton-blue" href="#">Forgot your password?</a>
                        <div class="offset-top-0">Haven’t an account? <a class="text-picton-blue" href="register.html">Sign up here</a>.</div>
                      </div>
-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div class="one-page-footer">
                    <p class="small" style="color: rgba(255,255,255, 0.3);">&copy; <span class="copyright-year"></span>. <a href="javascript:popPrivacy.Show();">Privacy Policy</a></p>
                </div>
            </div>
        </main>
    </div>
    <!-- Global RD Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- PhotoSwipe Gallery-->
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="pswp__bg"></div>
        <div class="pswp__scroll-wrap">
            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>
            <div class="pswp__ui pswp__ui--hidden">
                <div class="pswp__top-bar">
                    <div class="pswp__counter"></div>
                    <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                    <button class="pswp__button pswp__button--share" title="Share"></button>
                    <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                    <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                    <div class="pswp__preloader">
                        <div class="pswp__preloader__icn">
                            <div class="pswp__preloader__cut">
                                <div class="pswp__preloader__donut"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                    <div class="pswp__share-tooltip"></div>
                </div>
                <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
                <div class="pswp__caption">
                    <div class="pswp__caption__center"></div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>