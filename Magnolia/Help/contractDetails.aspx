﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="contractDetails.aspx.cs" Inherits="Magnolia.Help.contractDetails" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Contract Details Help</title>
</head>
<body>
    <form id="form1" runat="server">
        <h4>Contract Details</h4>
        <div style="font-size:12pt">
            <span id="spanContractDetails" runat="server">Contracts Details displays all the detailed information entered when creating the contract. If you were the
            one who entered the contract originally, you will be allowed to edit the contract, provided that it hasn't yet
            been approved by the next routing level.</span>
            <asp:BulletedList ID="listFunction" runat="server">
                <asp:ListItem Text="Documents - provides access to a list of all documents uploaded for the contract. Clicking a document will download that file to your local machine."></asp:ListItem>
                <asp:ListItem Text="Upload Document - To upload a document for the contract, enter a description for the document and click Add documents. Browse to the desired file and click OK. Click the Submit button. When the file upload is complete, the Document list will display the uploaded file."></asp:ListItem> 
                <asp:ListItem Text="Bypass Tracking - displays all details for any step in the process where a routing step has been bypassed. This can only can be initiated by an Admin user. Additionally, steps are automatically bypassed by the system if a user has established bypass start and stop dates in their profiles (e.g. vacation time)."></asp:ListItem>
                <asp:ListItem Text="Return Tracking - displays all details for any step in the process where a contract has been returned to a previous user. Contracts can only be returned by an Admin user and a reason for returning must be entered. The return reasons will be displayed here."></asp:ListItem>
                <asp:ListItem Text="OIG Tracking - All contract Parties must be checked to ensure the Party is not listed on the OIG Exclusion list. OIG Tracking displays all details for when that check was performed."></asp:ListItem>
                <asp:ListItem Text="Additional Data - Any additional information that needs to be tracked along with the contract should be entered here. Clicking New will allow you to enter additional information. The Type should describe the kind of data you are entering. The Value should give the details for the type. All users can enter additional data."></asp:ListItem>
                <asp:ListItem Text="Routing - will show you who and when has approved/ disapproved the contract as well as the current and future levels in the routing process. If a step in the process has been bypassed, the row will have a gray background."></asp:ListItem>
            </asp:BulletedList>
            <h4>Email Notifications</h4>
            When a contract is assigned to you for review or approval, you will receive an email notification with a link to the contract. A daily email reminder will also be sent for any outstanding contracts.
            <br /><br />
            <a href="AdminManual.pdf" target="_blank" id="hlManual" runat="server">View/Download Manual</a>
        </div>
    </form>
</body>
</html>
