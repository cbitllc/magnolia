﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="contracts.aspx.cs" Inherits="Magnolia.Help.contracts" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>All Contracts Help</title>
</head>
<body>
    <form id="form1" runat="server">
        <h4>All Contracts</h4>
        <div style="font-size:12pt">
            The Contracts page lists all contracts available to you within the Contract Management System. 
            This page provides limited details pertaining to each contract, including: Contract Type, Department, Party, 
            Start &amp; End Date, Requestor, Status, Assigned to Person &amp; Role, Document. 
            <asp:BulletedList ID="listFunction" runat="server">
                <asp:ListItem Text="Clicking New will bring you to the New Request Entry page."></asp:ListItem>
                <asp:ListItem Text="Clicking on a Party Name will bring you to the Contract Details for the selected contract."></asp:ListItem> 
                <asp:ListItem Text="Clicking a document link will automatically download that file to your local machine."></asp:ListItem>
            </asp:BulletedList>
            <% if (Convert.ToBoolean(Session["isAdmin"].ToString()) == true) { %>
                <br />Contracts awaiting your approval are displayed with a light green background.
            <%} %>
            <br />Contracts you have submitted are displayed with a light yellow background.
            <br />Contracts that have been denied are displayed with a light red background.
            <h4>Sorting and Filtering</h4>
            Clicking on a column name will sort the contract list by the selected column. Clicking the same name a
            second time will reverse the sort results. 
            <br /><br /><span id="spanMyContracts" runat="server">Clicking My Contracts at the top right will filter the results to display on contracts submitted by you.</span>
            <br /><br />Entering text in the search bar above the contract list will filter the list to show only those contracts
            matching the entered text (all fields displayed will apply the filter). Multiple words can be entered in the
            search bar to fine-tune your search.
            <br /><br />Contracts can be also be filtered by individual columns by clicking the funnel next to the column
            name. A list of all values in that column will appear which allows you to select one or more values to filter by. 
            <br /><br />This box also allows you to enter text to filter the list of items displayed. This is particularly useful when
            there are a large number of items available in the list.
            <h4>Email Notifications</h4>
            When a contract is assigned to you for review or approval, you will receive an email notification with a link to the contract. A daily email reminder will also be sent for any outstanding contracts.
            <br /><br />
            <a href="AdminManual.pdf" target="_blank" id="hlManual" runat="server">View/Download Manual</a>
        </div>
    </form>
</body>
</html>
