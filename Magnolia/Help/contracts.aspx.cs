﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Magnolia.Help
{
    public partial class contracts : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToBoolean(Session["isAdmin"].ToString()) == true)
            {
                listFunction.Items.Add(new ListItem() { Text = "Clicking on a user name will bring you to the Admin Edit User page for the selected user." });
                hlManual.HRef = "AdminManual.pdf";
                spanMyContracts.InnerText= "Clicking My Contracts at the top right will filter the results to display only contracts currently assigned to you or submitted by you.";
            }
            else
            {
                hlManual.HRef = "Manual.pdf"; ;
            }
        }
    }
}