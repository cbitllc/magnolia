﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="newPartyRequest.aspx.cs" Inherits="Magnolia.Help.newPartyRequest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <h4>New Party Request</h4>
        <divv style="font-size:12pt">
            New parties can only be added by an administrator. 
            To request a new party be added to the system, complete the new party form and click the Send Request button. 
            Contract Management Administrators are then automatically notified that a new Party Request has been submitted. 
            Once they review the information, you will be notified by an administrator whether or not the new Party has been added 
            or whether you should select an alternate Party.
        </divv>
        <br /><br />
        <a href="AdminManual.pdf" target="_blank" id="hlManual" runat="server">View/Download Manual</a>
    </form>
</body>
</html>
