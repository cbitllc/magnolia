﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="contractQuestionnaire.aspx.cs" Inherits="Magnolia.Help.contractQuestionnaire" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <h4>New Request</h4>
        <div style="font-size:12pt">
            To start with a new request, first select a <b>Department</b>.
            <br /><br />
            Next select an <b>Existing Party</b> from the list, or select
            <b>"Add New Party"</b> to add a new party. When you add a
            new party, you must check it against the OIG
            database. A pop-up will be displayed to check if the
            name is listed as restricted in the OIG database.
            <br /><br />
            Enter the <b>New Party Name</b> in the Busines Name box.
            Select "Include Fuzzy Search Results" if you are unsure of
            how to spell the name. Note that selecting this option
            may include a large number of results. Click <b>Search
            Exlusions</b> to complete the name search.
            <br /><br />
            The results of the <b>OIG search</b> will be dispalyed. You must determine
            if the party you are trying to add matches any of the results. 
            Results can be sorted or filtered by any of the displayed fields to
            help you make your determination. If you are sure the party you are
            entering does not match any of the search results, Click Add Party
            to continue.
            <br /><br />
            <b>Party Names</b> must be unique. If you try to add a name that
            already exists in the list, you will receive an error message. You
            will need to either modify the name to make it unique or
            Cancel and select the correct name in the party list.
            <br /><br />
            Next select the <b>Contract Type</b>. If the contract type
            you wish to select is not available in the list, an
            Admin will need to be notified to add the new Type.
            <br /><br />
            Select the <b>Contract Action</b>.
            <br /><br />
            Select the <b>Routing Name</b>. If you need a custom routing
            created, you will need to contact an Admin.
            <br /><br />
            After selecting the Routing Name, you need to select if this
            is part of an <b>HPG Group</b>. If you select no, you will need to
            select the Purchase Group.
            <br /><br />
            Select the <b>Purchase Group</b> or "Other Group" and select the appropriate Vendor.
            <br /><br />
            Once you have selected HPG or Purchase Group, begin entering values for the contract term.
            <br /><br />
            Select the contract <b>Start and End Dates</b>. The Contract
            length will be calculated automatically and be
            displayed in Years. Contracts can only be for a single
            year. If your start and end dates are greater than one
            year, you will see the following message:
            <br /><br />
            <i>"The Contract length cannot be greater than one year. The dates you have entered are greater than one year."</i>
            <br /><br />
            Select an <b>Auto Renewal</b> option. If you select Yes, the
            contract will be saved as pending and will not be
            processed until a Chief Officer signs off. You will see
            the following message:
            <br /><br />
            <i>"This selection will require Chief Officer initials before it can be submitted for signatures."</i>
            <br /><br />
            Select an <b>Outclause</b> option. If you select no, the contract will be saved
            as pending and will not be processed until a Chief Officer signs off.
            You will see the following message:
            <br /><br />
            <i>"This selection will require Chief Officer initials before it can be submitted for signatures."</i>
            <br /><br />
            If you select yes, you must enter the number of days before <b>Notification</b> for
            both with a breach and without a breach. If the notification days with a
            breach is greater than the notification days without a breach, you will see
            the following message:
            <br /><br />
            <i>"Notification Days Breach is more than Notification Days No Breach. Please verify your entries."</i>
            <br /><br />
            Enter the <b>Payment Term</b> and the <b>General Ledger</b>
            information. If you select Other for Payment Term, you
            need to enter the term in the Other Term box.
            <br /><br />
            After you enter the General Ledger, complete the <b>Regulations</b> portion by answering the Yes/No questions.
            <br /><br />
            If you try to <b>Save</b> the Contract before answering all of
            the regulation questions, you will be shown which
            questions you have left blank. You must answer all the
            questions to Save the contract. You have the option of
            saving the Contract after each section has been
            completed, beginning with the Contract Terms section.
            <h4>Email Notifications</h4>
            When a contract is assigned to you for review or approval, you will receive an email notification with a link to the contract. A daily email reminder will also be sent for any outstanding contracts.
        </div>
    </form>
</body>
</html>
