﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Magnolia.Help
{
    public partial class contractDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToBoolean(Session["isAdmin"].ToString()) == true)
            {
                hlManual.HRef = "AdminManual.pdf";
                spanContractDetails.InnerText += @"<br/>Click Delete to delete the contract, Return to send it back to a previous routing step, Reject to reject the contract, Bypass to skips a step, or Save to save any changes. If the contract is currently assigned to you, the Bypass button will say ""Approve"" instead of ""Bypass"".Click Approve to approve the contract and send it on to the next person in the routing process.";
            }
            else
            {
                hlManual.HRef = "Manual.pdf"; 
            }
        }
    }
}