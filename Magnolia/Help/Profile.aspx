﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="Magnolia.Help.Profile" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Edit Profile Help</title>
</head>
<body>
    <form id="form1" runat="server">
        <h4>Edit Profile</h4>
        <div style="font-size:12pt">
            Edit Profile allows each user to maintain their own personal data. Users can edit their Phone Number,
            Email Address and Bypass Dates. Bypass dates should be entered each time you will be out of the office for
            extended periods of time (e.g. vacation) in order to keep the Contract workflow in motion while you are absent.
        </div>
            <br /><br />
            <a href="AdminManual.pdf" target="_blank" id="hlManual" runat="server">View/Download Manual</a>
    </form>
</body>
</html>
