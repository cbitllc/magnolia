﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Magnolia.Help
{
    public partial class newPartyRequest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToBoolean(Session["isAdmin"].ToString()) == true)
            {
                hlManual.HRef = "AdminManual.pdf";
            }
            else
            {
                hlManual.HRef = "Manual.pdf"; ;
            }
        }
    }
}