﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="admin.aspx.cs" Inherits="Magnolia.Help.admin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <h4 id="header" runat="server" name="Top">Admin</h4>
        <ul>
            <li><a href="#divrouting">Routing</a></li>
            <li><a href="#divparties">Parties</a></li>
            <li><a href="#divOIGLog">OIG Log</a></li>
            <li><a href="#divdocTypes">Document Types</a></li>
            <li><a href="#divusers">Users</a></li>
        </ul>        
        <div style="font-size:12pt" id="divrouting" runat="server" name="divrouting">
            <h4>Routing&nbsp&nbsp&nbsp<a href="#Top">Top</a></h4>
            Admin Routing displays the steps involved in the selected routing process. The grid displays all the user roles that need to approve a contract and in which order. The graph on the right is a visual representation of the route. Routing steps can be edited from within the text grid or within the visual graph. The role is the name of the user role that needs to do the approval and the parent role needs to be assigned to the step that comes just before the current role. This establishes the correct routing order. 
            <asp:BulletedList ID="listRouting" runat="server">
                <asp:ListItem Text="Clicking edit within the grid allows you to modify the selected routing step."></asp:ListItem> 
                <asp:ListItem Text="Clicking delete will delete the selected routing step."></asp:ListItem>
                <asp:ListItem Text="Clicking New in the top left corner will allow you to insert a routing step."></asp:ListItem>
            </asp:BulletedList>
            <br />After changes have been made, clicking Refresh to the right of the list of routing names will refresh the graph on the right. Similarly, the graph on the right can be modified by right-clicking on a routing step and then clicking Refresh to update both the graph on the right and the grid on the left.
            <h4>Edit Route</h4>
            When you click on the edit button of a routing step, you will be able to change both the role and the parent role for the selected step. The Role should be set to the desired user role for the selected step. The Parent Role should be set to the user role that comes directly before the selected step in order to establish the routing sequence.
            <h4>Add New Route</h4>
            Clicking on New Route next to the list of Route Names allows you to create a new Route. You must give the new route a distinct name. You can clone an existing Route by selecting an existing Route name from the list or you can select "Add All Steps" from the Route name list to add all steps to your new route. Once created, you can modify the Route to fit your existing needs.
        </div>
        <div style="font-size:12pt" id="divparties" runat="server" name="divparties">
            <h4>Parties&nbsp&nbsp&nbsp<a href="#Top">Top</a></h4>
            The Parties/Vendor tab displays all parties that have been added to the system. Parties can be sorted by clicking on the column name and filtered by clicking on the funnel next to the column name. 
            <asp:BulletedList ID="listParties" runat="server">
                <asp:ListItem Text="Click on New in the top left corner to add a new party."></asp:ListItem>
                <asp:ListItem Text="Click on Edit to modify the party details. Click on Delete to delete a party."></asp:ListItem> 
            </asp:BulletedList>
            When a new party is added, the party will be verified against the OIG database.
        </div>
        <div style="font-size:12pt" id="divOIGLog" runat="server" name="divOIGLog">
            <h4>OIG Log&nbsp&nbsp&nbsp<a href="#Top">Top</a></h4>
            The OIG Log displays all parties that have been checked against the OIG database. The log displays the party name, the date the OIG database was searched, the text that was used in the search, how many records were possible matches, whether or not the party was added and who performed the search.
        </div>
        <div style="font-size:12pt" id="divdocTypes" runat="server" name="divdocTypes">
            <h4>Document Types&nbsp&nbsp&nbsp<a href="#Top">Top</a></h4>
            Admin Types allows you to modify the values for the different types associated with a contract. Selecting a type from the list will display an edit form for each type.
            <asp:BulletedList ID="listDocTypes" runat="server">
                <asp:ListItem Text="Contract Types will allow you to modify the different types of contracts that will be used in the list box when adding or modifying contracts."></asp:ListItem>
                <asp:ListItem Text="Selecting Departments allows you to add, edit and delete the Departments that will be available in the Department list when adding or modifying contracts."></asp:ListItem>
                <asp:ListItem Text="Selecting Document Types allows you to add, edit and delete the Document Types that will be available when uploading documents. If a document type is not listed, it will default to OTHER. "></asp:ListItem>
                <asp:ListItem Text="Selecting Party Types allows you to add, edit and delete the Party Types that will be available in the Party Type list when adding or modifying contracts."></asp:ListItem>
                <asp:ListItem Text="Selecting Status Types allows you to add, edit and delete the available Statuses for a contract."></asp:ListItem>
            </asp:BulletedList>
        </div>
        <div style="font-size:12pt" id="divusers" runat="server" name="divusers">
            <h4>Users&nbsp&nbsp&nbsp<a href="#Top">Top</a></h4>
            Selecting a user allows you to add, edit and delete the Users allowed to use the Contract Management System. The User Name must match the user's Windows login. The email address will be used for all email alerts. The role will define what functions the user will be able to perform.
            <br /><br />
            The User Roles allows you to add, edit and delete the roles users are assigned to. These roles are also used in the Routing Process.
        </div>
        <h4>Email Notifications</h4>
        When a contract is assigned to you for review or approval, you will receive an email notification with a link to the contract. A daily email reminder will also be sent for any outstanding contracts.
        <br /><br />
        <a href="AdminManual.pdf" target="_blank" id="hlManual" runat="server">View/Download Manual</a>
    </form>
</body>
</html>
